!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module Topologie
!===============================================================================
!                      Description de la topologie du réseau
!                     Données géométriques de base des noeuds
!
! numérotation des STOP : 19nn
!
! types dérivés : voir le module Adis_Types dans Adis-TS_Parametres.f90
!
!
! routines
!        - calculerRang : calcul du rang "hydraulique" de chaque bief
!        - creerNoeuds : création de la liste des noeuds à partir de la
!                        liste des biefs
!        - initTopoGeometrie : création d'une topologie et de la géométrie
!                              à partir d'un fichier de topologie
!        - init_from_file_CAS : initialisation de Topologie/nodes(*)
!        - init_nodes_to_zero : initialise Topologie/nodes(*) à zéro
!        - lireReseau : lecture du fichier NET et initialisation du
!                       tableau biefs
!        - lire_Sediment : lecture des classes de sédiments dans le fichier SED
!
! fonctions
!  NB : les routines préfixées par ptr utilisent des pointeurs
!        - ptr_surface : renvoie la surface d'un noeud pour une cote donnée
!        - surface     : renvoie la surface d'un noeud pour une cote donnée
!        - ptr_volume  : renvoie le volume d'un noeud pour une cote donnée
!        - volume     : renvoie le volume d'un noeud pour une cote donnée
!
!===============================================================================
use iso_fortran_env, only: output_unit, error_unit
use Parametres, only: long, lnode
use Adis_Types, only: dataset, sediment, profil, bief, noeud, rezo
use Utilitaires, only: next_string
use Geometrie_Section, only: newBief, addBief, removeProfil
implicit none

character (len=:),allocatable :: topo_basedir


contains

subroutine initTopoGeometrie(netFile, modele, les_Sediments)
!==============================================================================
!     Création d'une topologie et de la géométrie à partir
!                d'un fichier de topologie
!
!==============================================================================
   !prototype
   character(len=*), intent(in) :: netfile
   type (dataset), intent(inout) :: modele
   type (sediment), intent(in), optional :: les_Sediments(:)

   !variables locales
   type (profil), allocatable :: jeu_profils(:), jeu_profils_temp(:)
   type(bief),allocatable :: biefs(:)
   integer :: ib, debut, fin, ibb, iam, iav, ip1, im1, i, is, ntmp
   real(kind=long), allocatable :: x(:)
   real(kind=long), parameter :: dx00 = 2.001_long
   logical :: err_track
   character(len=7) :: wtmp
   integer :: nomax, ibmax, ismax

   write(output_unit,*) 'Entrée dans initTopoGeometrie()'
   ! calculs

   write(output_unit,*) 'Fichier de définition du réseau : ',netfile
   !call lireReseau(netfile, modele%biefs, modele%net%ibmax)
   call lireReseau(netfile, biefs, modele%net%ibmax)
   call move_alloc(from=biefs,to=modele%biefs)
   write(output_unit,*) '==> Lecture du réseau : OK'

   ! ici on connaît maintenant le nombre de biefs, on peut faire les allocations qui en dépendent
   ! allocation du tableau modele%nodes : on ne prend pas la peine de prendre le nombre de nœuds exact
   ibmax = modele%net%ibmax
   allocate(modele%nodes(ibmax+1))


   call creerNoeuds(modele%biefs, modele%net%ibmax, modele%nodes, modele%net%nomax)
   nomax = modele%net%nomax
   write(output_unit,*) '==> Création des nœuds : OK'

   ! allocation des tableaux de modele%net
   allocate(modele%net%lbz(nomax,2),modele%net%lbamv(nomax))
   allocate(modele%net%iub(ibmax),modele%net%ibu(ibmax))

   call calculerRang(modele%biefs, modele%nodes, modele%net)
   write(output_unit,*) '==> Reconstruction de la topologie du réseau : OK'

   write(output_unit,'(a)') ' ==> Liste des biefs et Rangs de calcul'
   modele%net%ismax = 0  !initialisation de la variable car elle sert à compter les profils
   do ib = 1, modele%net%ibmax
      write(output_unit,*) ''
      write(output_unit,'(a,i0,2a)') ' ==> Création du bief n°',ib,' : ',modele%biefs(ib)%name
      if (present(les_Sediments)) then
         call newBief(modele%biefs(ib)%fichier_geo,jeu_profils,les_Sediments)
      else
         call newBief(modele%biefs(ib)%fichier_geo,jeu_profils)
      endif
      ntmp = size(jeu_profils)+size(modele%sections)
      allocate(jeu_profils_temp(ntmp))
      if (allocated(modele%sections)) jeu_profils_temp(1:size(modele%sections)) = modele%sections
      call addBief(jeu_profils,debut,fin, jeu_profils_temp, modele%net%ismax)
      call move_alloc(from=jeu_profils_temp,to=modele%sections)
      modele%biefs(ib)%is1 = debut  ;  modele%biefs(ib)%is2 = fin
      write(output_unit,'(a,i3,7a,i4,a,i4,(a,i3),a,2f10.2)') &
              ' Bief n°',ib,' : ',modele%biefs(ib)%name,' ; ',modele%biefs(ib)%amont,' ; ', &
              modele%biefs(ib)%aval,' ; ',debut, ' - ',fin, ' ; Rang = ', modele%net%ibu(ib),&
              ' ; ',modele%sections(debut)%pk,modele%sections(fin)%pk
      do ibb = 1, size(jeu_profils)
         call removeProfil (jeu_profils(ibb))
      enddo
   enddo
   if (allocated(jeu_profils)) deallocate(jeu_profils)
   if (allocated(jeu_profils_temp)) deallocate (jeu_profils_temp)

   ! ici on connaît maintenant le nombre total de profils, on peut faire les allocations qui en dépendent
   ismax = modele%net%ismax
   allocate(modele%im1(ismax),modele%ip1(ismax))
   allocate(modele%dxm(ismax),modele%dxp(ismax),modele%c_diff(ismax))

   !!tableau des points de calcul précédent et suivant pour chaque point de calcul
   !!vérification que chaque bief a au moins 5 points de calcul (nécessaire pour la diffusion)
   allocate(x(modele%net%ismax))
   x(1:modele%net%ismax) = modele%sections(1:modele%net%ismax)%pk
   err_track = .false.
   do ib = 1, modele%net%ibmax
      iam = modele%biefs(ib)%is1 ; iav = modele%biefs(ib)%is2
      if (iav-iam < 4) then
         write(output_unit,'(a,i3,a)') ' >>>> Erreur : le bief ',ib,' doit avoir au moins 5 points de calcul'
         err_track = .true.
      endif
      ip1 = iam+1 ; ; do while (ip1<iav) ; if (abs(x(iam)-x(ip1)) > dx00) exit ; ip1=ip1+1 ; enddo
      modele%ip1(iam) = ip1  ;  modele%im1(iam) = -1
      modele%dxm(iam) = -1._long  ;  modele%dxp(iam) = abs(x(iam)-x(ip1))
      do i = iam+1, iav-1
         !sections avant et après en ignorant les dx nuls
         ip1=i+1 ; do while (ip1<iav) ; if (abs(x(i)-x(ip1)) > dx00) exit ; ip1=ip1+1 ; enddo
         im1=i-1 ; do while (im1>iam) ; if (abs(x(i)-x(im1)) > dx00) exit ; im1=im1-1 ; enddo
         modele%im1(i) = im1
         modele%ip1(i) = ip1
         modele%dxm(i) = abs(x(i)-x(im1))
         modele%dxp(i) = abs(x(i)-x(ip1))
      enddo
      im1=iav-1 ; do while (im1>iam) ; if (abs(x(iav)-x(im1)) > dx00) exit ; im1=im1-1 ; enddo
      modele%im1(iav) = im1  ;  modele%ip1(iav) = -1
      modele%dxm(iav) = abs(x(iav)-x(im1))  ;  modele%dxp(iav) = -1._long
   enddo
   deallocate(x)
   if (err_track) stop 1901

end subroutine initTopoGeometrie


subroutine lireReseau(fichier,biefs,ibmax)
!==============================================================================
!         lecture du fichier NET et initialisation du tableau biefs(*)
!
! format NET
!     - * en colonne 1 : commentaire
!     - une ligne par bief, séparateur espace, avec :
!        - nom du bief
!        - nom du noeud amont
!        - nom du noeud aval
!        - nom du fichier de géométrie (format ST2) associé
!     - exemple :
!                <-------------------------
!                *exemple de fichier NET
!                bief1 AAA BBB saar.st
!                *
! entrée : fichier au format NET
! sortie : le tableau biefs(:) indexé par l'ordre de lecture
!          le nombre de biefs
!
!==============================================================================
   ! prototype
   character(len=*), intent(in) :: fichier  ! fichier NET
   type(bief),allocatable, intent(out) :: biefs(:)
   integer, intent(out) :: ibmax
   ! variables locales
   integer :: luu, ierr = 0, next = 0, ib = 0, nligne = 0, nxt0
   integer :: err_track
   character(len=80) :: ligne
   logical :: bExist
   ! calculs
   inquire(file=fichier,exist=bExist)
   if (.not.bExist) then
      write(error_unit,*) '>>>> Erreur : le fichier ',trim(fichier),' n''existe pas'
      stop 1902
   endif
   open(newunit=luu,file=fichier,form='formatted',status='old',iostat=ierr)
   if (ierr /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier de topologie ', trim(fichier),' impossible'
      stop 1903
   endif
   ! comptage des lignes pour estimer le nombre de biefs
   ib = 0 ; nligne = 0
   do while (ierr == 0)
      nligne = nligne+1
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
         write(error_unit,*) '>>>> Erreur 1 de lecture de ',fichier,' a la ligne ',nligne
         stop 1904
      elseif (ierr < 0) then
         exit
      else
         if (ligne(1:1) == '*' .or. trim(ligne)=='') cycle
         if (trim(ligne) == '') cycle
         ib = ib+1
      endif
   enddo
   ibmax = ib
   allocate(biefs(ibmax))

   rewind(luu)
   ib = 0 ; nligne = 0 ; ierr = 0
   do while (ierr == 0)
      nligne = nligne+1
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
         write(error_unit,*) '>>>> Erreur 1 de lecture de ',fichier,' a la ligne ',nligne
         stop 1905
      elseif (ierr < 0) then
         exit
      else
         if (ligne(1:1) == '*' .or. trim(ligne)=='') cycle
         if (trim(ligne) == '') cycle
         ib = ib+1 ; next = 1 ; nxt0 = next ; err_track = 0
         biefs(ib)%name = next_string(ligne,'',next) ; nxt0 = next
         biefs(ib)%amont = next_string(ligne,'',next)
         if (next-nxt0+1 > lnode) err_track = err_track+1 ; nxt0 = next
         biefs(ib)%aval = next_string(ligne,'',next)
         if (next-nxt0+1 > lnode) err_track = err_track+1 ; nxt0 = next
         biefs(ib)%fichier_geo = topo_basedir//next_string(ligne,'',next)
         if (err_track > 0) then
            write(output_unit,'(a,i3,a)') ' >>>> Erreur dans fichier NET à la ligne ',nligne,' :'
            write(output_unit,'(2a)') ' >>>> ',trim(ligne)
            write(output_unit,'(a,i3,a,i2,a)') ' >>>> il y a ',err_track,' noeud(s) dont le nom a plus de ',lnode,' caractères'
            write(output_unit,'(a)') ' >>>> Vérifier les lignes suivantes du fichier'
            stop 1906
         endif
      endif
   enddo
   ibmax = ib
   close (luu)
end subroutine lireReseau


subroutine creerNoeuds(biefs, ibmax, nodes, nomax)
!==============================================================================
!       création de la liste des noeuds à partir de la liste des biefs
!       affectation des numéros de noeud dans biefs() en fonction des
!       noms de noeud
!
! entrée : un jeu de biefs
!          le nombre de biefs
! sortie : le jeu de noeuds correspondant
!          les biefs pour faire le lien biefs <-> noeuds
!          le nombre de noeuds
!
!==============================================================================
   ! -- prototype --
   type(bief), intent(inout) :: biefs(*)
   type(noeud), intent(out) :: nodes(*)
   integer, intent(out) :: nomax
   integer, intent(in) :: ibmax
   ! -- variables locales --
   integer :: ib, nn, nbm, nbv

   ! calculs
   ! 1er bief qui existe toujours
   nodes(1)%name = biefs(1)%amont  ;  biefs(1)%nam = 1
   nodes(2)%name = biefs(1)%aval  ;  biefs(1)%nav = 2
   nomax = 2
   ! biefs suivants : création des noeuds etaffectation des
   !                  numéros de noeuds
   do ib = 2, ibmax
      nbm = 0  ;  nbv = 0
      do nn = 1, nomax
         if (trim(biefs(ib)%amont) == trim(nodes(nn)%name))  nbm = nn
         if (trim(biefs(ib)%aval)  == trim(nodes(nn)%name))  nbv = nn
      enddo
      if (nbm == 0) then
         nomax = nomax+1
         nodes(nomax)%name = biefs(ib)%amont
         biefs(ib)%nam = nomax
      else
         biefs(ib)%nam = nbm
      endif
      if (nbv == 0) then
         nomax = nomax+1
         nodes(nomax)%name = biefs(ib)%aval
         biefs(ib)%nav = nomax
      else
         biefs(ib)%nav = nbv
      endif
   enddo
   !identification des noeuds amont et aval, ceux qui portent les CL
   do nn = 1, nomax
      nodes(nn)%cl = 0  !initialisation
      nbm = 0  ;  nbv = 0
      do ib = 1, ibmax
         if (biefs(ib)%nam == nn) nbm = nbm+1
         if (biefs(ib)%nav == nn) nbv = nbv+1
      enddo
      if (nbm == 0) nodes(nn)%cl = -1
      if (nbv == 0) nodes(nn)%cl = +1
   enddo
end subroutine creerNoeuds


subroutine calculerRang (biefs, nodes, net)
!==============================================================================
!            calcul du rang "hydraulique" de chaque bief
!
! entrée : le tableau des biefs
! sortie : le réseau avec les tableaux iub(*) et ibu(*), lbamv(*), lbz(*), nzno(*)
!          iba et ibz (maille), nclv
!
!==============================================================================
   ! -- prototype --
   type(bief), intent(in) :: biefs(*)
   type(noeud), intent(in) :: nodes(*)
   type(rezo), intent(inout) :: net
   ! -- variables locales --
   integer,allocatable :: niveau(:), connexion(:,:)
   integer :: ib, kb, nbb, nivo, iubtmp, ne, jb
   logical :: permute

   ! allocations
   allocate(niveau(net%ibmax),connexion(net%ibmax,net%ibmax))
   ! calculs
   !construction de la table de connexion : liste des biefs amont d'un bief donné
   connexion = 0
   do ib = 1, net%ibmax
      do kb = 1, net%ibmax
         if (biefs(kb)%nav == biefs(ib)%nam) connexion(kb,ib) = 1
      enddo
   enddo
   !calcul des niveaux hydrauliques
   niveau = 0   !niveau hydraulique => niveau(ib) = max(niveaux biefs amont de ib) +1
   nbb = net%ibmax  ! nb de biefs sans niveau
   do ib = 1, net%ibmax
      if (maxval(connexion(:,ib)) == 0) then
         niveau(ib) = 1
         nbb = nbb-1
      endif
   enddo
   do while (nbb > 0)
      do ib = 1, net%ibmax
         if (niveau(ib) == 0) then
            nivo = 0
            do kb = 1, net%ibmax
               if (connexion(kb,ib) > 0 .and. niveau(kb) == 0) then
                  nivo = 0
                  exit     !on ne remplit pas un niveau si le niveau inférieur est vide
               else if (connexion(kb,ib) > 0 .and. niveau(kb) > 0) then
                  nivo = max (nivo, niveau(kb))
               endif
            enddo
            if (nivo > 0) then
               niveau(ib) = nivo+1
               nbb = nbb-1
            endif
         endif
      enddo
   enddo
   write(output_unit,*) '==> calcul des niveaux hydrauliques : OK'

   !calcul des rangs hydrauliques
   permute = .true.
   do ib = 1, net%ibmax
      net%iub(ib) = ib
   enddo
   do while (permute)
      permute = .false.
      do ib = 2, net%ibmax
         if (niveau(net%iub(ib)) < niveau(net%iub(ib-1)) .and. net%iub(ib) > net%iub(ib-1)) then
            permute = .true.
            iubtmp = net%iub(ib)
            net%iub(ib) = net%iub(ib-1)
            net%iub(ib-1) = iubtmp
         endif
      enddo
   enddo
   do ib = 1, net%ibmax
      do kb = 1, net%ibmax
         if (net%iub(kb) == ib) net%ibu(ib) = kb
      enddo
   enddo
   write(output_unit,*) '==> calcul des rangs hydrauliques : OK'

   !tableau lbamv
   do ne = 1, net%nomax
      if (nodes(ne)%cl > 0) then  !recherche du bief qui part de ne
         do ib = 1, net%ibmax
            if (biefs(ib)%nam == ne) then
               net%lbamv(ne) = net%ibu(ib)
               exit
            endif
         enddo
      else if (nodes(ne)%cl < 0) then  !recherche du bief qui arrive en ne
         do ib = 1, net%ibmax
            if (biefs(ib)%nav == ne) then
               net%lbamv(ne) = net%ibu(ib)
               exit
            endif
         enddo
      else
         net%lbamv(ne) = 0
      endif
   enddo

   !tableau lbz
   do ne = 1, net%nomax
      do ib = 1, net%ibmax  !boucle sur le rang de calcul
         if (biefs(net%iub(ib))%nam == ne) then
            net%lbz(ne,1) = ib
            exit
         endif
      enddo
      do ib = net%ibmax, 1, -1  !boucle sur le rang de calcul
         if (biefs(net%iub(ib))%nam == ne) then
            net%lbz(ne,2) = ib
            exit
         endif
      enddo
   enddo

   !recherche du premier bief de maille = le premier dont le noeud amont est un défluent
   net%iba = net%ibmax+1
   do ib = 1, net%ibmax  !boucle sur le rang de calcul
      kb = net%iub(ib)  ;  ne = biefs(kb)%nam
      if (net%lbz(ne,1) /= net%lbz(ne,2)) then
         net%iba = ib
         exit
      endif
   enddo

   !recherche du dernier bief de maille
   !si plusieurs CL aval => net%ibz = net%ibmax
   net%nclv = 0
   do ne = 1, net%nomax
      if (nodes(ne)%cl < 0) net%nclv = net%nclv+1
   enddo
   if (net%nclv > 1) then
      net%ibz = net%ibmax
   else
      do ib = net%ibmax, 1, -1  !boucle sur le rang de calcul
         kb = net%iub(ib)  ;  ne = biefs(kb)%nav  ; nbb = 0  ! on compte les biefs qui arrivent en ne
         do jb = 1, net%ibmax
            if (biefs(jb)%nav == ne) nbb = nbb+1
         enddo
         if (nbb > 1) then
            net%ibz = ib
            exit
         endif
      enddo
   endif

   !correction de iub pour indiquer les biefs de la maille
   do ib = net%iba, net%ibz  !boucle sur le rang de calcul
      net%iub(ib) = -net%iub(ib)
   enddo
end subroutine calculerRang

#ifdef avec_casiers
subroutine init_nodes_to_zero (nodes, nomax)
!==============================================================================
!             initialisation à zéro des lois S(z) des noeuds
!
! entrée : tableau des noeuds nodes(:), nombre de noeuds nomax
! sortie : nodes(:) mise à jour
!
!==============================================================================
   ! -- prototype --
   type(noeud), intent(inout) :: nodes(*)
   integer, intent(in) :: nomax
   ! -- variables locales --
   integer :: neu

   do neu = 1, NOmax
      nodes(neu)%name = ''
      nodes(neu)%cl = 0
      nodes(neu)%np = 0
      nodes(neu)%ip = 0
      nodes(neu)%zne(1:nnsup) = 0._long
      nodes(neu)%sne(1:nnsup) = 0._long
      nodes(neu)%vne(1:nnsup) = 0._long
   enddo
end subroutine init_nodes_to_zero


subroutine init_from_file_CAS(filename, nodes, nomax)
!==============================================================================
!    initialisation des données à partir de la lecture d'un fichier CAS
!
!            met à jour nodes(*) avec les données de filename
!
! entrée : filename, nom de fichier
!          tableau des noeuds nodes(:)
!          nombre de noeuds nomax
! sortie : nodes(:) mise à jour
!==============================================================================
   ! -- prototype --
   character(len=*), intent(in) :: filename
   type(noeud), intent(inout) :: nodes(*)
   integer, intent(in) :: nomax
   ! -- variables locales --
   integer :: luu ! unité logique
   integer :: ierr, neu, nbss, npneu, next_field
   character :: a1*1, u0*3, a2*80
   real(kind=long) :: u1, u2, dz, ds

   ! calcul
   write(output_unit,*) 'Ouverture du fichier ',filename
   open(newunit=luu,file=filename,status='old',form='formatted',iostat=ierr)
   if (ierr /= 0) then
      write(error_unit,'(a)') ' Ouverture du fichier CAS impossible'
      stop 1907
   endif
   nbss=0  !'nbss'+1 est le nombre de lignes du fichier de description des casiers
   do neu = 1, nomax
      write(output_unit,*) 'Noeud numero ',neu,' : ',nodes(neu)%name
      rewind luu
      read(luu,'(a1,a3)',iostat=ierr) a1, u0
      ! recherche d'une loi S(h) pour le noeud courant
      do while (.not.(a1 == '$'.and. u0 == nodes(neu)%name) .and. ierr == 0)
         read(luu,'(a1,a3)',iostat=ierr) a1, u0
         npneu = 0  ! initialisation du compteur de niveaux
      enddo
      if (ierr == 0) then
         write(output_unit,*) 'Lecture de la loi S(h) pour le noeud ',nodes(neu)%name
         ! loi S(h) trouvée => lecture de la loi
         a2 = ' '
         do while (a2(1:1) /= '$' .and. ierr == 0)
            read(luu,'(a)', iostat=ierr) a2
            write(output_unit,*) a2
            nbss=nbss+1
            if (a2(1:1) == '$') then
               continue
            else if (a2(1:1) /= '*') then
               next_field = 1  ;  ierr = 1
               u1 = next_real(a2,' ;',next_field) ; ierr = min(ierr,next_field)
               u2 = next_real(a2,' ;',next_field) ; ierr = min(ierr,next_field)
               if (ierr <= 0) then
                  write(error_unit,'(3a,i4)') ' >>>> Erreur de lecture de ',filename,' a la ligne ',nbss
                  write(error_unit,'(a)') a2
                  write(error_unit,'(a)') ' >>>> Erreur de lecture de CAS'
                  stop 1908
               else
                  ierr = 0
                  npneu = npneu+1
                  nodes(neu)%zne(npneu) = u1              ! cote
                  nodes(neu)%sne(npneu) = u2*10000._long  ! surface horizontale en m2
                  if (npneu > 1) then  ! calcul du volume par intégration de la surface
                     dz = nodes(neu)%zne(npneu) - nodes(neu)%zne(npneu-1)
                     ds = nodes(neu)%sne(npneu) + nodes(neu)%sne(npneu-1)
                     nodes(neu)%vne(npneu) = nodes(neu)%vne(npneu-1) + 0.5_long*ds*dz
                  else
                     nodes(neu)%vne(npneu) = 0._long  ! initialisation du volume
                  endif
                  nodes(neu)%np = npneu
               endif
            endif
         enddo
      endif
   enddo
   close(luu)
end subroutine init_from_file_CAS


function surface(un_noeud,z)
!==============================================================================
!      renvoie la surface (horizontale) d'un noeud pour une cote donnée
!
!  ça fait joli mais ce n'est pas optimal (allocation et copie)
!  amélioration : conserver la courbe 2D pour les interpolations suivantes
!
! entrée :
!           - un_noeud, type noeud
!           - z, réel, la cote à laquelle on veut la surface (horizontale)
! sortie :
!           - la surface (réel) interpolée sur les données brutes du noeud
!
!==============================================================================
   ! -- prototype --
   type (noeud), intent(in) :: un_noeud
   real(kind=long), intent(in) :: z
   real(kind=long) :: surface
   ! -- variables locales --
   type(courbe2D) :: c2D

   ! calculs
   if (un_noeud%np == 0) then
      surface = 0._long
   else
      c2D = init_courbe2D(un_noeud%zne, un_noeud%sne, un_noeud%np)
      surface = interpole(c2D,z)
      call remove_courbe2D(c2D)
   endif
end function surface


function ptr_surface(un_noeud,z)
!==============================================================================
!      renvoie la surface (horizontale) d'un noeud pour une cote donnée
!
! utilise des pointeurs pour éviter l'allocation et la copie d'une courbe2D
!
! entrée :
!           - un_noeud, type noeud
!           - z, réel, la cote à laquelle on veut la surface (horizontale)
! sortie :
!           - la surface (réel) interpolée sur les données brutes du noeud
!
!==============================================================================
   ! -- prototype --
   type (noeud), intent(in) :: un_noeud
   real(kind=long), intent(in) :: z
   real(kind=long) :: ptr_surface
   ! -- variables locales --
   type(ptr_courbe2D) :: c2D

   ! calculs
   if (un_noeud%np == 0) then
      ptr_surface = 0._long
   else
      c2D = map_courbe2D(un_noeud%zne, un_noeud%sne, un_noeud%np, un_noeud%ip)
      ptr_surface = ptr_interpole(c2D,z)
   endif
end function ptr_surface


function volume(un_noeud,z)
!==============================================================================
!          renvoie le volume d'un noeud pour une cote donnée
!
!  ça fait joli mais ce n'est pas optimal (allocation et copie)
!  amélioration : conserver la courbe 2D pour les interpolations suivantes
!
! entrée :
!           - un_noeud, type noeud
!           - z, réel, la cote à laquelle on veut le volume
! sortie :
!           - le volume (réel) interpolée sur les données brutes du noeud
!
!==============================================================================
   ! -- prototype --
   type (noeud), intent(in) :: un_noeud
   real(kind=long), intent(in) :: z
   real(kind=long) :: volume
   ! -- variables locales --
   type(courbe2D) :: c2D

   ! calculs
   if (un_noeud%np == 0) then
      volume = 0._long
   else
      c2D = init_courbe2D(un_noeud%zne, un_noeud%vne, un_noeud%np)
      volume = interpole(c2D,z)
      call remove_courbe2D(c2D)
   endif
end function volume


function ptr_volume(un_noeud,z)
!==============================================================================
!      renvoie le volume d'un noeud pour une cote donnée
!
! utilise des pointeurs pour éviter l'allocation et la copie d'une courbe2D
!
! entrée :
!           - un_noeud, type noeud
!           - z, réel, la cote à laquelle on veut le volume
! sortie :
!           - le volume (réel) interpolé sur les données brutes du noeud
!
!==============================================================================
   ! -- prototype --
   type (noeud), intent(in) :: un_noeud
   real(kind=long), intent(in) :: z
   real(kind=long) :: ptr_volume
   ! -- variables locales --
   type(ptr_courbe2D) :: c2D

   ! calculs
   if (un_noeud%np == 0) then
      ptr_volume = 0._long
   else
      c2D = map_courbe2D(un_noeud%zne, un_noeud%sne, un_noeud%np, un_noeud%ip)
      ptr_volume = ptr_interpole(c2D,z)
   endif
end function ptr_volume
#endif /* avec_casiers */


function index_by_biefName(name, biefs, ibmax)
!==============================================================================
!      renvoie le numéro (index) dans le tableau biefs[] d'un bief désigné
!      par son nom
!      renvoie 0 si le nom n'existe pas.
!
! entrée :
!           - name : chaîne de caractères
!           - biefs : tableau des biefs
!           - ibmax : nombre de biefs
! sortie :
!           - index_by_biefName : entier
!==============================================================================
   ! -- prototype --
   integer :: index_by_biefName
   character(len=*), intent(in) :: name
   type(bief), intent(in) :: biefs(*)
   integer, intent(in) :: ibmax
   ! -- variables locales --
   integer :: n

   index_by_biefName = 0
   do n = 1, ibmax
      if (trim(biefs(n)%name) == trim(name)) then
         index_by_biefName = n
         exit
      endif
   enddo
end function index_by_biefName


function index_by_nodeName(name, nodes, nomax)
!==============================================================================
!      renvoie le numéro (index) dans le tableau nodes[] d'un noeud désigné
!      par son nom
!      renvoie 0 si le nom n'existe pas.
!
!
! entrée :
!           - name : chaîne de caractères
!           - nodes : tableau des noeuds
!           - nomax : nombre de noeuds
! sortie :
!           - index_by_nodeName : entier
!
!==============================================================================
   ! -- prototype --
   integer :: index_by_nodeName
   character(len=*), intent(in) :: name
   type(noeud), intent(in) :: nodes(*)
   integer, intent(in) :: nomax
   ! -- variables locales --
   integer :: n

   index_by_nodeName = 0
   do n = 1, nomax
      if (trim(nodes(n)%name) == trim(name)) then
         index_by_nodeName = n
         exit
      endif
   enddo
end function index_by_nodeName


end module Topologie
