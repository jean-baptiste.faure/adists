!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module Diffusion
!===============================================================================
!                 Opérateur de diffusion et termes sources
!
!      Schémas aux différences finies implicites pour l'étape de diffusion
!             et la prise en compte des termes sources
!
! numérotation des STOP : 13nn
!===============================================================================
use iso_fortran_env, only: output_unit
use parametres, only: long, g, fnl, zero
use Adis_Types, only: dataset, state, solution, data_hydrauliques, coupling_data, &
                      polluant, profil
use utilitaires, only: next_string, next_real, next_int, interpole
use Geometrie_Section, only: section_id_from_pk, section, perimetre, largeur
use DataNum, only: dt0, no_coupling, nb_threads

#ifdef openmp
use omp_lib
#endif /* openmp */

implicit none

logical :: with_diffusion = .true.

real(kind=long) :: dmol = 1.e-08_long  ! coefficient de diffusion moléculaire

integer, parameter :: strickler_mode = 1  !mode de calcul du strickler (en attendant mieux)
                                          !0 : strickler_global
                                          !1 : strickler selon MAGE
                                          !2 : strickler_granulométrique (à définir)


contains


subroutine coeff_Dispersion(modele,etat,cdisp,cd_q1,cd_q3,peclet,peclet_min,peclet_max)
!==============================================================================
!              Calcul du coefficient de dispersion
!
!==============================================================================
   ! prototype
   type (dataset), intent(in), target :: modele
   type (state), intent(in) :: etat
   real(kind=long), intent(inout) :: cdisp(:)  !coeff de diffusion dynamique
   real(kind=long), intent(inout) :: cd_q1(:), cd_q3(:) !variable composite effectivement utilisée
                                                        !pour coeff de diffusion dans la discrétisation
   real(kind=long), intent(out) :: peclet, peclet_min, peclet_max

   ! variables locales
   integer is, ip, im
   real(kind=long) :: tmp, Ks, Lg, H, dxm, dxp, r, z, Rh
   real(kind=long), parameter :: racg = sqrt(g) !3.1321_long
   real(kind=long), parameter :: aa = 2.5_long/3._long, bb = 2._long/3._long
   real(kind=long), parameter :: cc = 1._long/6._long
   real(kind=long), allocatable :: peclet_loc(:)
   type (profil), pointer :: prof

   ! allocations
   if (.not.allocated(peclet_loc)) allocate(peclet_loc(modele%net%ismax))
   ! calculs
   do is = 1, modele%net%ismax
      prof => modele%sections(is)
      select case (modele%c_diff(is)%mode)
      case (0)  !formule de Fisher
         H = etat%y(is)
         Ks = strickler_hydro(modele,is,H+modele%sections(is)%zf)
         Lg = etat%a(is) / H   !largeur équivalente
         tmp = modele%c_diff(is)%param * Lg * Lg * Ks / (racg*H**aa)
         cdisp(is) = dmol + abs(etat%u(is))*tmp
      case (1)  !formule de Elder
         H = etat%y(is)
         Ks = strickler_hydro(modele,is,H+modele%sections(is)%zf)
         tmp = modele%c_diff(is)%param * racg * H**aa / Ks
         cdisp(is) = dmol + abs(etat%u(is))*tmp
      case (2)  !formule de Iwasa
         H = etat%y(is)
         Ks = strickler_hydro(modele,is,H+modele%sections(is)%zf)
         Lg = etat%a(is) / H   !largeur équivalente
         tmp = modele%c_diff(is)%param * racg / (Ks * H**bb) * Lg*sqrt(Lg)
         cdisp(is) = dmol + abs(etat%u(is))*tmp
      case (3)  !formule constante
!         cdisp(is) = dmol + modele%c_diff(is)%param
         cdisp(is) = modele%c_diff(is)%param
      case (4)  !formule générique sans approximation Rh = h
         H = etat%y(is)
         Z = prof%zf + H
         Rh = section(prof,z) / perimetre(prof,z)
         Ks = strickler_hydro(modele,is,z)
         Lg = largeur(prof,z)
         tmp =  H * (Ks*Rh**cc/racg)**(modele%c_diff(is)%b-1._long) * (Lg/H)**modele%c_diff(is)%c
         cdisp(is) = dmol + modele%c_diff(is)%param * abs(etat%u(is)) * tmp
      case default
         write(output_unit,*) '>>>> Erreur dans coeff_Dispersion : type de formule inconnu'
         stop 1301
      end select

!      peclet_loc(is) = etat%u(is)*etat%a(is)/etat%y(is)/cdisp(is)
      if (abs(cdisp(is)) > 0._long) then
         peclet_loc(is) = 0.5_long*abs(etat%u(is))*(modele%dxm(is)+modele%dxp(is))/cdisp(is)
      elseif (abs(etat%u(is)) == 0._long) then
         peclet_loc(is) = 0._long
      else
         peclet_loc(is) = huge(0._long)
      endif
   enddo

   peclet_min = minval(peclet_loc(1:modele%net%ismax))
   peclet_max = maxval(peclet_loc(1:modele%net%ismax))
   if (peclet_max > 0.9_long*huge(0._long)) then
      peclet = huge(0._long)
   else
      peclet = sum(peclet_loc(1:modele%net%ismax)) / real(modele%net%ismax,kind=long)
   endif

   !En fait ce dont on a besoin ce sont les termes q1 et q3 de diff_fc
   !c'est donc ça que l'on va conserver
   do is = 1, modele%net%ismax
      ip = modele%ip1(is) ; im = modele%im1(is)
      if (ip < 0 .or. im < 0) cycle  !extrémité de bief.
      r =  2._long * (modele%dxm(is)+modele%dxp(is))
      dxm = r * modele%dxm(is) ; dxp = r * modele%dxp(is)
      cd_q1(is) = (cdisp(im)+cdisp(is)) * (etat%a(im)+etat%a(is)) / dxm
      cd_q3(is) = (cdisp(ip)+cdisp(is)) * (etat%a(ip)+etat%a(is)) / dxp
   enddo

end subroutine coeff_Dispersion



subroutine diff_fc(triD,fmb2,c1,iam,iav,dt,theta,x,a,a1,u1, &
                   qm,qm1,skcin,im1,ip1,cd_q1a,cd_q3a,cd_q1b,cd_q3b,ismax)
!==============================================================================
!     Calcul du système tridiagonal et du second membre intervenant
!     lors de résolution de l'étape de diffusion par la méthode des
!     différences finies semi-implicites de CRANK-NICOLSON.
!
!  Calcul adapté à la FORME CONSERVATIVE de l'équation de convection-diffusion
!+ prise en compte d'un terme de disparition cinétique en -K*C
!+ apports massiques latéraux
!
! entrée :
!        - iam, iav  : indices limites du tronçon
!        - x(*)      : abscisses des points de calculs, définit dx.
!        - ligne d'eau au temps t+dt
!           - u1(*)  : vitesse
!           - a1(*)  : section mouillée
!        - a(*)      : section mouillée au temps t
!        - c1(*)     : concentration après l'étape de convection
!        - qm(*)     : débit massique (polluant) au temps t
!        - qm1(*)    : débit massique (polluant) au temps t+dt
!        - cdisp(*)  : coefficient de diffusion au temps t
!        - cdisp1(*) : coefficient de diffusion au temps t+dt
!        - skcin     : coefficient de disparition cinétique
!        - dt        : pas de temps
!        - theta     : coefficient d'implicitation
!        - ismax     : taille effective de la matrice

! sortie :
!        - TriD (ISMAX * 3) : coeff de la matrice tridiagonale à inverser
!        - fmb2 (ISMAX)     : second membre du systeme
!     Le tableau des concentrations après prise en compte de la diffusion
!     vérifiera TriD x C1 = fmb2.
!
!==============================================================================
   !prototype
   real(kind=long), intent(inout) :: triD(ismax,3)
   real(kind=long), intent(inout) :: fmb2(:)
   real(kind=long), intent(in)  :: c1(ismax), x(ismax), a(ismax), a1(ismax), u1(ismax)
   real(kind=long), intent(in)  :: qm(ismax), qm1(ismax)       ! débits massiques
   real(kind=long), intent(in) :: cd_q1a(ismax), cd_q3a(ismax) !variable composite effectivement utilisée
                                                       !pour coeff de diffusion dans la discrétisation
   real(kind=long), intent(in) :: cd_q1b(ismax), cd_q3b(ismax) !variable composite effectivement utilisée

   integer, intent(in) :: iam, iav, im1(ismax), ip1(ismax), ismax
   real(kind=long), intent(in) :: dt, theta, skcin

   !variables locales
   integer :: iam1, iav1, i, ip, im
   real(kind=long) :: sk, sk1, unplus_sk, un_theta
   real(kind=long) :: r, s, dxp, dxm
   real(kind=long), parameter :: un = 1._long
   real(kind=long), parameter :: deux = 2._long, zero = 0.0_long
   !
   !calculs
   ! initialisation
   sk = skcin*dt*theta ; unplus_sk = un+sk ; sk1 = unplus_sk-skcin
   un_theta = un - theta
   ! Section amont
   if (u1(iam) < zero .and. u1(iam+1) <= zero) then         !sortie libre
      triD(iam,1) = zero ; triD(iam,2) = un ; triD(iam,3) = zero
      fmb2(iam) = c1(iam+1)
   else                                                     !concentration imposée
      triD(iam,1) = zero ; triD(iam,2) = un ; triD(iam,3) = zero
      fmb2(iam) = c1(iam)
   endif
   do iam1 = iam+1, ip1(iam)-1                              !on saute les sections singulières
      triD(iam1,1) = triD(iam,1) ; triD(iam1,2) = triD(iam,2) ; triD(iam1,3) = triD(iam,3)
      fmb2(iam1) = fmb2(iam)
   enddo

   ! Section aval
   if (u1(iav) > zero .and. u1(iav-1) >= zero) then         !sortie libre
      triD(iav,1) = zero ; triD(iav,2) = un ; triD(iav,3) = zero
      fmb2(iav) = c1(iav-1)
   else                                                     !concentration imposée
      triD(iav,1) = zero ; triD(iav,2) = un ; triD(iav,3) = zero
      fmb2(iav) = c1(iav)
   endif
   do iav1 = iav-1, im1(iav)+1, -1                          ! on saute les sections singulières
      triD(iav1,1) = triD(iav,1) ; triD(iav1,2) = triD(iav,2) ; triD(iav1,3) = triD(iav,3)
      fmb2(iav1) = fmb2(iav)
   enddo

!...........................Sections intérieures
   do i = ip1(iam), im1(iav)
      ! sections avant et après en ignorant les dx nuls
      ip = ip1(i) ; im = im1(i)
      dxp = abs(x(ip)-x(i)) ; dxm = abs(x(i)-x(im))

      r = dt/a1(i) ; s = un_theta*r ; r = theta*r
      triD(i,1) = -r * cd_q1b(i)
      triD(i,3) = -r * cd_q3b(i)

      triD(i,2) = unplus_sk - triD(i,1) - triD(i,3)
      fmb2(i) = sk1 * c1(i) * a(i)/a1(i)                                    &  !cinétique chimique + part convection
              + s * ( cd_q3a(i)*(c1(ip)-c1(i))-cd_q1a(i)*(c1(i)-c1(im)) )   &  !diffusion
              + s * qm(i) +  r * qm1(i)                                        !apports massiques
   enddo
end subroutine diff_fc



subroutine gauss4b(g,i1,i2,a,ismax,check)
!==============================================================================
!  Inversion d'un système linéaire tridiagonal par élimination de Gauss
!             AVEC vérification de la solution
!
! entrée :
!        - i1, i2 : indices limites du tronçon
!        - G(*)   : le second membre du système linéaire.
!        - A(*,1) : coeff de la diagonale inférieure
!        - A(*,2) : diagonale
!        - A(*,3) : diagonale supérieure
!        - ismax  : taille de la matrice
!        - check  : booléen, vrai si vérification

! sortie :
!        - G(*)   : la solution du système linéaire.
!
!==============================================================================
   !prototype
   integer,         intent(in)    :: i1, i2     !indices limites du tronçon
   real(kind=long), intent(in)    :: a(ismax,3) !diagonales de la matrice
   real(kind=long), intent(inout) :: g(ismax)   !second membre et solution
   integer,         intent(in)    :: ismax
   logical,         intent(in)    :: check
   !variables locales
   integer :: i
   real(kind=long) :: z(ismax), w(ismax), v(ismax)
   real(kind=long) :: delta, err, toutpetit = 1.e-03_long

   ! Initialisation
   if (.not.(a(i1,2) > toutpetit) .and. .not.(a(i1,2) < -toutpetit)) then
      write(output_unit,*) ' matrice non inversible (cas 1)'
      write(output_unit,*) ' ligne : ',i1,a(i1,2) ; stop 1302
   endif
   z(i1) = a(i1,3) / a(i1,2)
   w(i1) = g(i1) / a(i1,2)
   ! Triangularisation
   do i = i1+1, i2
      delta = a(i,2)-a(i,1)*z(i-1)
      if (.not.(delta > toutpetit) .and. .not.(delta < -toutpetit)) then
         write(output_unit,*) ' matrice non inversible (cas 2)'
         write(output_unit,*) i1,i2,' ligne : ',i,a(i,1),a(i,2),a(i,3) ; stop 1303
      endif
      z(i) = a(i,3) / delta
      w(i) = (g(i)-a(i,1)*w(i-1)) / delta
   enddo

   if (check) v(i1:i2) = g(i1:i2)  !necessaire si verification : sauvegarde du 2nd membre

   ! Résolution du système triangulaire
   g(i2) = w(i2)
   do i = i2-1, i1, -1
      g(i) = w(i)-z(i)*g(i+1)
   enddo

   if (check) then
      ! Vérification
      z(i1) = a(i1,2)*g(i1) + a(i1,3)*g(i1+1)
      do i = i1+1, i2-1
         z(i) = a(i,1)*g(i-1) + a(i,2)*g(i) + a(i,3)*g(i+1)
      enddo
      z(i2) = a(i2,1)*g(i2-1) + a(i2,2)*g(i2)

      w(i1:i2) = abs(z(i1:i2) - v(i1:i2))

      err = maxval(w(i1:i2))
      if (.not.(err < 0.000001_long)) then
         write(output_unit,*) ' Gauss4b : ', err, maxval(abs(z(i1:i2))), maxval(abs(v(i1:i2)))
         stop 1304
      endif
   endif
end subroutine gauss4b



subroutine gauss3(g,i1,i2,a,ismax)
!===============================================================================
!  Inversion d'un système linéaire tridiagonal par élimination de Gauss
!
! entrée :
!        - i1, i2 : indices limites du tronçon
!        - G(*)   : le second membre du système linéaire.
!        - A(*,1) : coeff de la diagonale inférieure
!        - A(*,2) : diagonale
!        - A(*,3) : diagonale supérieure
!        - ismax  : taille de la matrice

! sortie :
!        - G(*)   : la solution du système linéaire.
!
!===============================================================================
   !prototype
   integer,         intent(in)    :: i1, i2     !indices limites du tronçon
   real(kind=long), intent(inout) :: a(ismax,3) !diagonales de la matrice
   real(kind=long), intent(inout) :: g(ismax)   !second membre et solution
   integer,         intent(in)    :: ismax      !taille de la matrice
   !variables locales
   integer :: i
   real(kind=long) :: err

!---Vérification du second membre
   err = maxval(abs(g(i1:i2)))
   if (err > 1.e+30_long) then
      write(output_unit,*) ' Gauss3 : 2e membre géant :', err
      stop 1314
   endif

!---Décomposition de la matrice  A
   if (a(i1,2) == 0.0_long) then
      write(output_unit,*) ' matrice non inversible (cas 1)'
      write(output_unit,*) ' ligne : ',i1,a(i1,2) ; stop 1305
   endif
! Triangularisation avec diagonale unité
   a(i1,3) = a(i1,3)/a(i1,2)
   g(i1)   = g(i1)/a(i1,2)
   do i = i1+1, i2
      a(i,2) = a(i,2)-a(i-1,3)*a(i,1)
      if (a(i,2) == 0.0_long) then
         write(output_unit,*) ' matrice non inversible (cas 2)'
         write(output_unit,*) i1,i2 ; write(output_unit,*) ' ligne : ',i,a(i,2) ; stop 1306
      endif
      a(i,3) = a(i,3)/a(i,2)
      g(i)   = (g(i)-g(i-1)*a(i,1))/a(i,2)
      if (abs(g(i)) < 1.e-99_long) g(i) = 0._long  !on met à 0 les underflows
   enddo

!---Résolution du système A.F = G avec A triangulaire supérieure à diagonale unité
   do i = i2-1, i1, -1
      g(i) = g(i)-a(i,3)*g(i+1)
   enddo
end subroutine gauss3



subroutine termes_sources (npol, modele, etat2, sol1, sol2, data_hyd, global, dt_max)
!==============================================================================
!    Calcul des termes sources : apports latéraux, dépôts et érosion
!                                pour un polluant
!
!  sorties : sol2%qm() et sol2%c_eq()
!  entrées : les autres variables
!            etat2 =  état hydraulique à la fin du pas de temps
!==============================================================================
   ! prototype
   integer, intent(in) :: npol
   type (dataset), intent(inout), target :: modele
   type (state), intent(in) :: etat2
   type (solution), intent(in), target :: sol1
   type (solution), intent(inout) :: sol2
   type (data_hydrauliques), intent(in), target :: data_hyd(:)
   type (coupling_data), intent(in), target :: global
   real(kind=long), intent(out) :: dt_max
   ! variables locales
   integer :: is, ib, np
   real(kind=long) :: qm0, Ctotal, Mmin, Mrg, Mrd
   real(kind=long), parameter :: alpha = 1._long
   real(kind=long), pointer :: Lmin, Lmoy, Smin, Smoy
   real(kind=long) :: Dsm, Dsg, Dsd  ! diamètres
   real(kind=long) :: tau_min, tau_moy
   real(kind=long) :: H_min, H_moy, dp, dp_ero, dp_dep, c_eq_min_dep, c_eq_min_ero,c_eq_moy_ero, c_eq_moy_dep, alg, ald
   real(kind=long) :: dx
   real(kind=long) :: k_skin

   type (polluant), pointer :: poll
   real(kind=long), pointer :: c_total(:)
   real(kind=long), parameter :: c_bottom = 1.E-12, m_bottom = 0.001_long

   ! calculs
   if (no_coupling) then
      C_total => sol1%c(:,npol)
   else
      C_total => global%c_total_MES
   endif
   dt_max = dt0

   ! initialisation avec les apports latéraux
   sol2%qm(1:modele%net%ismax,0:3,npol) = 0.0_long
   sol2%c_eq_ero(1:modele%net%ismax,1:3,npol) = 0.0_long
   sol2%c_eq_dep(1:modele%net%ismax,1:3,npol) = 0.0_long
   if (modele%nbal(npol) > 0) then
      do np = 1, modele%nbal(npol)
         if (modele%AL_pol(np,npol)%is2 == modele%AL_pol(np,npol)%is1) then  !apport ponctuel --> pris en compte dans la phase convection
            cycle
         else
            dx = abs(modele%AL_pol(np,npol)%xmax - modele%AL_pol(np,npol)%xmin)
            qm0 = interpole(modele%AL_pol(np,npol)%qmt,etat2%time) / dx
            sol2%qm(modele%AL_pol(np,npol)%is1+1:modele%AL_pol(np,npol)%is2-1,0,npol) = qm0
         endif
      enddo
   endif

   H_min = 1.0_long ; H_moy = 1.0_long

   ! dépôts et érosions
   poll => modele%poll(npol)
   if (poll%particule_type > 0) then
      !Cas d'un sédiment fin
      do ib = 1, modele%net%ibmax
         !pas de flux de dépôt-érosion dans la 1ère et la dernière section (conditions aux limites)
         do is = modele%biefs(ib)%is1+1, modele%biefs(ib)%is2-1

            ! données de couplage
            dsm = global%dsm(is)  ;  dsg = global%dsg(is)  ;  dsd = global%dsd(is)
            Ctotal = c_total(is)
            !
            Mrg = sol1%masse(is,1,npol)  ;  Mmin = sol1%masse(is,2,npol)  ;  Mrd = sol1%masse(is,3,npol)

            !! contribution du dépôt/érosion en lit mineur
            Lmin => data_hyd(is)%lmin   ;  Smin => data_hyd(is)%smin

            k_skin = strickler_peau(dsm) !on suppose que dsm représente le d90 de la couche de sédiments du fond

            tau_min = data_hyd(is)%tau_min / (k_skin * k_skin)         !contrainte au fond effective
            tau_min = tau_min / modele%corr_tau_consolide(is,npol)     !correction pour consolidation : cette division correspond
                                                                       !à une multiplication de la contrainte critique
                                                                       !voir concentration_equilibre()

            c_eq_min_ero = concentration_equilibre_ero(tau_min, poll)  !concentration d'équilibre erosion
            c_eq_min_dep = concentration_equilibre_dep(tau_min, poll)  !concentration d'équilibre depot
            sol2%c_eq_ero(is,2,npol) = c_eq_min_ero
            sol2%c_eq_dep(is,2,npol) = c_eq_min_dep
            H_min = Smin / Lmin
            dp_ero = poll%ws * disponibility(c_eq_min_ero,Ctotal,Mmin) * abs(c_eq_min_ero-Ctotal) * Lmin/H_min
            dp_dep = poll%ws * disponibility(c_eq_min_dep,Ctotal,Mmin) * abs(c_eq_min_dep-Ctotal) * Lmin/H_min
            if (dp_dep < 0.0_long) then                   !!!dépôt
               if (sol1%c(is,npol) > c_bottom) then
                  dp = poll%apd_dep*dp_dep*sol1%c(is,npol)/Ctotal
                  dt_max = min(dt_max,-alpha*sol1%c(is,npol)*Smin/dp)
               else
                  dp = zero
               endif
            else if (dp_ero > 0.0_long) then              !!!érosion
               if (Mmin > m_bottom) then
                  dp = poll%apd_ero*dp_ero
                  dt_max = min(dt_max,alpha*Mmin/dp)
               else
                  dp = 0.0_long
               endif
            else !on force dp = 0 si dp = NaN
               dp = 0.0_long
            endif
            sol2%qm(is,2,npol) = dp

            !! contribution du dépôt/érosion en lit moyen
            if (data_hyd(is)%smoy > 1._long .and. (modele%sections(is)%l_g+modele%sections(is)%l_d) > 1._long) then  !on néglige les lits moyens tout petits
               alg = modele%sections(is)%l_g / (modele%sections(is)%l_g+modele%sections(is)%l_d) !proportion gauche
               ald = modele%sections(is)%l_d / (modele%sections(is)%l_g+modele%sections(is)%l_d) !proportion droite
               Lmoy => data_hyd(is)%lmoy  ;  Smoy => data_hyd(is)%smoy
               H_moy = Smoy / Lmoy

               !!!!majeur gauche
               k_skin = strickler_peau(dsg) !on suppose que dsg représente le d90 de la couche de sédiments du fond
               tau_moy = data_hyd(is)%tau_moy / (k_skin * k_skin)          !contrainte au fond effective
               tau_moy = tau_moy / modele%corr_tau_consolide(is,npol)      !correction pour consolidation
               c_eq_moy_ero = concentration_equilibre_ero(tau_moy, poll)           !concentration d'équilibre erosion
               c_eq_moy_dep = concentration_equilibre_dep(tau_moy, poll)           !concentration d'équilibre depot
               sol2%c_eq_ero(is,1,npol) = c_eq_moy_ero
               sol2%c_eq_dep(is,1,npol) = c_eq_moy_dep

               dp_ero = poll%ws * alg * disponibility(c_eq_moy_ero,Ctotal,Mrg) * abs(c_eq_moy_ero-Ctotal) * Lmoy/H_moy
               dp_dep = poll%ws * alg * disponibility(c_eq_moy_dep,Ctotal,Mrg) * abs(c_eq_moy_dep-Ctotal) * Lmoy/H_moy
               if (dp_dep < 0.0_long) then                   !!!dépôt
                  if (sol1%c(is,npol) > c_bottom) then
                     dp = poll%apd_dep*dp_dep*sol1%c(is,npol)/Ctotal
                     dt_max = min(dt_max,-alpha*alg*sol1%c(is,npol)*Smoy/dp)
                  else
                     dp = zero
                  endif
               else if (dp_ero > 0.0_long) then              !!!érosion
                  if (Mrg > m_bottom) then
                     dp = poll%apd_ero * dp_ero
                     dt_max = min(dt_max,alpha*Mrg/dp)
                  else
                     dp = 0.0_long
                  endif
               else !on force dp = 0 si dp = NaN
                  dp = 0.0_long
               endif
               sol2%qm(is,1,npol) = dp

               !!!!majeur droit
               k_skin = strickler_peau(dsd) !on suppose que dsd représente le d90 de la couche de sédiments du fond
               tau_moy = data_hyd(is)%tau_moy / (k_skin * k_skin)          !contrainte au fond effective
               tau_moy = tau_moy / modele%corr_tau_consolide(is,npol)      !correction pour consolidation
               c_eq_moy_ero = concentration_equilibre_ero(tau_moy,poll)            !concentration d'équilibre erosion
               c_eq_moy_dep = concentration_equilibre_dep(tau_moy,poll)            !concentration d'équilibre depot
               sol2%c_eq_ero(is,3,npol) = c_eq_moy_ero
               sol2%c_eq_dep(is,3,npol) = c_eq_moy_dep
               dp_ero = poll%ws * ald * disponibility(c_eq_moy_ero,Ctotal,Mrd) * abs(c_eq_moy_ero-Ctotal) * Lmoy/H_moy
               dp_dep = poll%ws * ald * disponibility(c_eq_moy_dep,Ctotal,Mrd) * abs(c_eq_moy_dep-Ctotal) * Lmoy/H_moy
               if (dp_dep < 0.0_long) then                   !!!dépôt
                  if (sol1%c(is,npol) > c_bottom) then
                     dp = poll%apd_dep*dp_dep*sol1%c(is,npol)/Ctotal
                     dt_max = min(dt_max,-alpha*ald*sol1%c(is,npol)*Smoy/dp)
                  else
                     dp = zero
                  endif
               else if (dp_ero > 0.0_long) then              !!!érosion
                  if (Mrd > m_bottom) then
                     dp = poll%apd_ero * dp_ero
                     dt_max = min(dt_max,alpha*Mrd/dp)
                  else
                     dp = 0.0_long
                  endif
               else !on force dp = 0 si dp = NaN
                  dp = 0.0_long
               endif
               sol2%qm(is,3,npol) = dp

            else
               sol2%qm(is,1,npol) = 0.0_long
               sol2%qm(is,3,npol) = 0.0_long
               sol2%c_eq_ero(is,1,npol) = 0.0_long
               sol2%c_eq_ero(is,3,npol) = 0.0_long
               sol2%c_eq_dep(is,1,npol) = 0.0_long
               sol2%c_eq_dep(is,3,npol) = 0.0_long
            endif
            !! total de toutes les contributions
            ! sol2%qm(i,:,:) est le flux de dépôt-érosion sur les 2 demies mailles de part et d'autre de la section i
            sol2%qm(is,0,npol) = sum(sol2%qm(is,0:3,npol))
         enddo
      enddo
   endif
end subroutine termes_sources


subroutine update_stocks_MES(modele,sol1,sol2,dt)
!==============================================================================
! mise à jour des stocks de sédiments fins dans les =/= lits
!==============================================================================
   !prototype
   type (dataset), intent(inout) :: modele
   type (solution), intent(inout) :: sol1,sol2
   real(kind=long), intent(in) :: dt
   !variables locales
   integer :: np, ib, is, ismax, ibmax, rang
   logical, save :: first_call = .true.
   real(kind=long) :: anp
   real(kind=long), save, allocatable :: al_g(:,:), al_m(:,:), al_d(:,:)

   if (modele%nbmes == 0) return

   ismax = modele%net%ismax  ;  ibmax = modele%net%ibmax
   !fichier de sortie pour le contrôle des résultats
   if (first_call) then
      allocate (al_g(modele%net%ismax,modele%nbpol),al_m(modele%net%ismax,modele%nbpol),al_d(modele%net%ismax,modele%nbpol))
      do np = 1, modele%nbpol
         anp = modele%poll(np)%rho * (1._long - modele%poll(np)%p)  !rapport masse / volume
         do ib = 1, ibmax
            do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
               if (modele%sections(is)%l_g < 0.01_long) then
                  al_g(is,np) = 0.0_long
               else
                  al_g(is,np) = 1._long / (anp * modele%sections(is)%l_g)
               endif
               if (modele%sections(is)%l_m < 0.01_long) then
                  al_m(is,np) = 0.0_long
               else
                  al_m(is,np) = 1._long / (anp * modele%sections(is)%l_m)
               endif
               if (modele%sections(is)%l_d < 0.01_long) then
                  al_d(is,np) = 0.0_long
               else
                  al_d(is,np) = 1._long / (anp * modele%sections(is)%l_d)
               endif
            enddo
         enddo
      enddo
      first_call = .false.
   endif
   sol2%C_total(1:ismax) = 0.0_long ; sol2%Hg(1:ismax) = 0.0_long ; sol2%Hm(1:ismax) = 0.0_long ; sol2%Hd(1:ismax) = 0.0_long

   ! sol2%qm(i,:,:) est le flux de dépôt-érosion sur les 2 demies mailles de part et d'autre de la section i
   ! sol2%masse(i,:,:) est la masse linéïque sur les demies mailles de part et d'autre de la section i
   !$omp parallel default(shared) private(np,rang,ib,is) &
   !$omp firstprivate(nb_threads) num_threads(nb_threads)
   do np = 1, modele%nbpol
#ifdef openmp
      rang = omp_get_thread_num()
      if (mod(np,nb_threads) /= rang) cycle
#endif /* openmp */
      if (modele%poll(np)%particule_type == 0) cycle
         do ib = 1, ibmax
            do is = modele%biefs(ib)%is1+1, modele%biefs(ib)%is2-1
               sol2%masse(is,1:3,np) = max(0.0_long,sol1%masse(is,1:3,np) - dt*sol2%qm(is,1:3,np))
            enddo
            sol2%masse(modele%biefs(ib)%is1,1:3,np) = 0.0_long
            sol2%masse(modele%biefs(ib)%is2,1:3,np) = 0.0_long
         enddo
   enddo
   !$omp end parallel

   !épaisseurs de sédiments
   do np = 1, modele%nbpol
      if (modele%poll(np)%particule_type > 0) then
         sol2%C_total(1:ismax) = sol2%C_total(1:ismax) + sol2%C(1:ismax,np)
         !conversion masse linéïque --> épaisseur
         sol2%Hg(1:ismax) = sol2%Hg(1:ismax) + sol2%masse(1:ismax,1,np)*al_g(1:ismax,np)
         sol2%Hm(1:ismax) = sol2%Hm(1:ismax) + sol2%masse(1:ismax,2,np)*al_m(1:ismax,np)
         sol2%Hd(1:ismax) = sol2%Hd(1:ismax) + sol2%masse(1:ismax,3,np)*al_d(1:ismax,np)
      endif
   enddo

   ! mis à jour du cumul des variations d'épaisseur des sédiments
   do is = 1, ismax
      !lit moyen gauche
      sol2%dhg(is) = sol1%dhg(is) + sol2%Hg(is) - sol1%Hg(is)
      !lit mineur
      sol2%dhm(is) = sol1%dhm(is) + sol2%Hm(is) - sol1%Hm(is)
      !lit moyen droit
      sol2%dhd(is) = sol1%dhd(is) + sol2%Hd(is) - sol1%Hd(is)
   enddo

end subroutine update_stocks_MES



function disponibility(Ceq, C, mass)
!==============================================================================
!  estimation de la disponibilité du sédiment
!
!  mass    : masse de sédiment disponible au fond
!==============================================================================
   ! prototype
   real(kind=long) :: disponibility
   real(kind=long), intent(in) :: Ceq, C, mass

   ! calcul
   if (Ceq < C) then
      disponibility = -1._long  !dépôt
   else if (Ceq > C .and. mass > 0.0_long) then
      disponibility = 1._long   !érosion
   else
      disponibility = 0.0_long   !rien
   endif
end function disponibility



function concentration_equilibre_dep(tau, poll)
!==============================================================================
!  estimation de la concentration d'équilibre
!
!==============================================================================
   ! prototype
   real(kind=long) :: concentration_equilibre_dep
   real(kind=long), intent(in) :: tau
   type (polluant), intent(in) :: poll

   ! calcul
   concentration_equilibre_dep = poll%ac*max(tau/poll%tcr_dep-1._long,0.0_long)**poll%bc
end function concentration_equilibre_dep



function concentration_equilibre_ero(tau, poll)
!==============================================================================
!  estimation de la concentration d'équilibre
!
!==============================================================================
   ! prototype
   real(kind=long) :: concentration_equilibre_ero
   real(kind=long), intent(in) :: tau
   type (polluant), intent(in) :: poll

   ! calcul
   concentration_equilibre_ero = poll%ac*max(tau/poll%tcr_ero-1._long,0.0_long)**poll%bc
end function concentration_equilibre_ero



subroutine lire_Dif (modele, difFile)
!==============================================================================
!       lecture du fichier de coefficients de diffusion
!
!           initialisation du tableau modele%c_diff
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + difFile = nom du fichier DIF
!==============================================================================

   !prototype
   type (dataset), intent(inout) :: modele
   character (len=fnl), intent(in) :: difFile
   ! variables locales
   character (len=80) :: ligne
   character (len=30) :: tmp, formule
   integer :: lu, ios, nligne, nf, ich, i, bief_id, is1, is2, ismax
   real(kind=long) :: valeur, pkmin, pkmax, b, c

   ismax = modele%net%ismax

   !initialisation avec la formule de Fisher standard
   modele%c_diff(1:ismax)%mode = 0
   modele%c_diff(1:ismax)%param = 0.011_long


   if (difFile == '') return
   write(output_unit,*)
   write(output_unit,'(2a)') ' Lecture du fichier des paramètres de diffusion : ',trim(difFile)
   open(newunit=lu,file=trim(difFile), status='old', form='formatted')
   nligne = 0

   do
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      nligne = nligne+1
      !conversion en majuscules
      do i = 1, 80
         ich = ichar(ligne(i:i))
         if (ich > 96 .and. ich < 123) ligne(i:i) = char(ich-32)
      enddo
      !décodage des lignes
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then  !commentaire ou ligne vide
         cycle
      else if (ligne(1:6) == 'DEFAUT' .or. ligne(1:7) == 'DEFAULT') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         formule = next_string(ligne,'',nf)
         valeur = next_real(ligne,'',nf)
         write(output_unit,'(a,f8.4,2a)') ' Coefficient de diffusion par défaut = ',valeur,' pour la formule de ',formule
         if (trim(formule) == 'FISHER') then
            modele%c_diff(1:ismax)%mode = 0
            modele%c_diff(1:ismax)%param = valeur
            modele%c_diff(1:ismax)%b = 2._long
            modele%c_diff(1:ismax)%c = 2._long
         elseif (trim(formule) == 'ELDER') then
            modele%c_diff(1:ismax)%mode = 1
            modele%c_diff(1:ismax)%param = valeur
            modele%c_diff(1:ismax)%b = 0.0_long
            modele%c_diff(1:ismax)%c = 0.0_long
         elseif (trim(formule) == 'IWASA') then
            modele%c_diff(1:ismax)%mode = 2
            modele%c_diff(1:ismax)%param = valeur
            modele%c_diff(1:ismax)%b = 0.0_long
            modele%c_diff(1:ismax)%c = 1.5_long
         elseif (trim(formule) == 'CONSTANTE') then
            modele%c_diff(1:ismax)%mode = 3
            modele%c_diff(1:ismax)%param = valeur
            modele%c_diff(1:ismax)%b = 0.0_long
            modele%c_diff(1:ismax)%c = 0.0_long
         elseif (trim(formule) == 'GENERIQUE') then
            b = next_real(ligne,'',nf)
            c = next_real(ligne,'',nf)
            modele%c_diff(1:ismax)%mode = 4
            modele%c_diff(1:ismax)%param = valeur
            modele%c_diff(1:ismax)%b = b
            modele%c_diff(1:ismax)%c = c
         else
            write(output_unit,*) '>>>> Erreur a la ligne ',nligne,' de ',trim(difFile)
            write(output_unit,*) 'Type de formule de calcul du coefficient de diffusion INCONNU : ',formule
            stop 1307
         endif
      elseif (ligne(1:6) == 'FISHER') then
         nf = 1  ;  formule = next_string(ligne,'=',nf)
         bief_id = next_int(ligne,'',nf)
         pkmin = next_real(ligne,'',nf)  ;  pkmax = next_real(ligne,'',nf)
         valeur = next_real(ligne,'',nf)
         is1 = section_id_from_pk (modele, bief_id, pkmin, 0.1_long)
         is2 = section_id_from_pk (modele, bief_id, pkmax, 0.1_long)
         if (is1 > is2) stop 1308
         modele%c_diff(is1:is2)%mode = 0
         modele%c_diff(is1:is2)%param = valeur
         modele%c_diff(is1:is2)%b = 2._long
         modele%c_diff(is1:is2)%c = 2._long
         write(output_unit,'(a,i3,a,f10.2,a,f10.2,a,f8.4,2a)') ' Coefficient de diffusion au bief ',&
                    bief_id,' de ',modele%sections(is1)%pk,' a ',modele%sections(is2)%pk,' : ',&
                    valeur,' pour la formule de type ',formule
      elseif (ligne(1:5) == 'ELDER') then
         nf = 1  ;  formule = next_string(ligne,'=',nf)
         bief_id = next_int(ligne,'',nf)
         pkmin = next_real(ligne,'',nf)  ;  pkmax = next_real(ligne,'',nf)
         valeur = next_real(ligne,'',nf)
         is1 = section_id_from_pk (modele, bief_id, pkmin, 0.1_long)
         is2 = section_id_from_pk (modele, bief_id, pkmax, 0.1_long)
         if (is1 > is2) stop 1309
         modele%c_diff(is1:is2)%mode = 1
         modele%c_diff(is1:is2)%param = valeur
         modele%c_diff(is1:is2)%b = 0.0_long
         modele%c_diff(is1:is2)%c = 0.0_long
         write(output_unit,'(a,i3,a,f10.2,a,f10.2,a,f8.4,2a)') ' Coefficient de diffusion au bief ',&
                    bief_id,' de ',modele%sections(is1)%pk,' a ',modele%sections(is2)%pk,' : ',&
                    valeur,' pour la formule de type ',formule
      elseif (ligne(1:5) == 'IWASA') then
         nf = 1  ;  formule = next_string(ligne,'=',nf)
         bief_id = next_int(ligne,'',nf)
         pkmin = next_real(ligne,'',nf)  ;  pkmax = next_real(ligne,'',nf)
         valeur = next_real(ligne,'',nf)
         is1 = section_id_from_pk (modele, bief_id, pkmin, 0.1_long)
         is2 = section_id_from_pk (modele, bief_id, pkmax, 0.1_long)
         if (is1 > is2) stop 1310
         modele%c_diff(is1:is2)%mode = 2
         modele%c_diff(is1:is2)%param = valeur
         modele%c_diff(is1:is2)%b = 0.0_long
         modele%c_diff(is1:is2)%c = 1.5_long
         write(output_unit,'(a,i3,a,f10.2,a,f10.2,a,f8.4,2a)') ' Coefficient de diffusion au bief ',&
                    bief_id,' de ',modele%sections(is1)%pk,' a ',modele%sections(is2)%pk,' : ',&
                    valeur,' pour la formule de type ',formule
      elseif (ligne(1:9) == 'CONSTANTE') then
         nf = 1  ;  formule = next_string(ligne,'=',nf)
         bief_id = next_int(ligne,'',nf)
         pkmin = next_real(ligne,'',nf)  ;  pkmax = next_real(ligne,'',nf)
         valeur = next_real(ligne,'',nf)
         is1 = section_id_from_pk (modele, bief_id, pkmin, 0.1_long)
         is2 = section_id_from_pk (modele, bief_id, pkmax, 0.1_long)
         if (is1 > is2) stop 1311
         modele%c_diff(is1:is2)%mode = 3
         modele%c_diff(is1:is2)%param = valeur
         modele%c_diff(is1:is2)%b = 0.0_long
         modele%c_diff(is1:is2)%c = 0.0_long
         write(output_unit,'(a,i3,a,f10.2,a,f10.2,a,f8.4,2a)') ' Coefficient de diffusion au bief ',&
                    bief_id,' de ',modele%sections(is1)%pk,' a ',modele%sections(is2)%pk,' : ',&
                    valeur,' pour la formule de type ',formule
      elseif (ligne(1:9) == 'GENERIQUE') then
         nf = 1  ;  formule = next_string(ligne,'=',nf)
         bief_id = next_int(ligne,'',nf)
         pkmin = next_real(ligne,'',nf)  ;  pkmax = next_real(ligne,'',nf)
         valeur = next_real(ligne,'',nf)
         b = next_real(ligne,'',nf)
         c = next_real(ligne,'',nf)
         is1 = section_id_from_pk (modele, bief_id, pkmin, 0.1_long)
         is2 = section_id_from_pk (modele, bief_id, pkmax, 0.1_long)
         if (is1 > is2) stop 1312
         modele%c_diff(is1:is2)%mode = 4
         modele%c_diff(is1:is2)%param = valeur
         modele%c_diff(is1:is2)%b = b
         modele%c_diff(is1:is2)%c = c
         write(output_unit,'(a,i3,a,f10.2,a,f10.2,3(a,f8.4),2a)') ' Coefficient de diffusion au bief ',&
                    bief_id,' de ',modele%sections(is1)%pk,' a ',modele%sections(is2)%pk,' : ',&
                    valeur,' b = ',b,' c = ',c,' pour la formule de type ',formule
      else
         write(output_unit,*) '>>>> Erreur a la ligne ',nligne,' de ',trim(difFile)
         write(output_unit,*) 'Type de formule de calcul du coefficient de diffusion INCONNU : ',trim(ligne)
         stop 1313
      endif
   enddo
   close (lu)
end subroutine lire_Dif



function strickler_peau(d90)
!==============================================================================
!      calcul du strickler à partir du d90
!
! Ce strickler représente le frottement pur, il est utilisé pour évaluer la
! contrainte au fond
!
! Pour le strickler représentant la résistance à l'écoulement, voir la
! fonction strickler_hydro()
!==============================================================================
   implicit none
   real(kind=long) :: strickler_peau
   real(kind=long), intent(in) :: d90
   real(kind=long), parameter :: un6 = 1._long/6._long

   strickler_peau = 26._long/d90**un6
end function strickler_peau



function strickler_hydro(modele,is, z) result(strickler)
!==============================================================================
!      calcul du strickler au sens de la résistance à l'écoulement donc
!           pas seulement le frottement sur le fond et les berges
!                dans un profil pour une altitude donnée
!
! Ce strickler est utilisé pour évaluer le coefficient de dispersion (diffusion)
! Pour le strickler représentant le frottement pur voir strickler_peau()
!
! FIXME: améliorer l'implémentation
! actuellement cette fonction renvoie la valeur du strickler du lit en eau
! si l'écoulement est débordant c'est le strickler du lit majeur, sinon celui du mineur
! TODO: voir si on pourrait évaluer un strickler global équivalent (Debord ?)
!
! entrée :
!           - le jeu de données
!           - indice du profil en travers concerné
!           - z, réel, la cote de la surface libre
! sortie : le strickler
!==============================================================================
   ! -- prototype --
   type (dataset), intent(in), target :: modele !le jeu de données
   integer, intent(in)                :: is  !l'indice du profil
   real (kind=long), intent(in)       :: z   !la cote
   real(kind=long)                    :: strickler !le coefficient de Strickler
   ! -- variables locales --
   integer :: li1, li2
   real (kind=long) :: zmoy
   type (profil), pointer :: prof

   !calculs
   prof => modele%sections(is)
   li1 = prof%li(1)  ;  li2 = prof%li(2)
   zmoy = min(prof%xyz(li1)%z,prof%xyz(li2)%z)
   if (z > zmoy) then
      strickler = 0.5*(prof%K(1)+prof%K(3))  !strickler du lit moyen
   else
      strickler = prof%K(2)  !strickler du lit mineur
   endif
end function strickler_hydro


end module Diffusion
