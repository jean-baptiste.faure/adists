!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module Utilitaires
!===============================================================================
!   Ce module regroupe diverses structures de données génériques
!   et des routines de manipulation de ces structures
!
! numéros des STOP : 20nn
!===============================================================================
use iso_fortran_env, only: error_unit
use parametres, only : long, fnl, slash, zero
use adis_types, only: ptr_courbe2D, courbe2D
use,intrinsic :: iso_c_binding

     type, bind(c), public :: c_tm
        integer(kind=c_int) :: tm_sec   ! Seconds after minute (0 - 59).
        integer(kind=c_int) :: tm_min   ! Minutes after hour (0 - 59).
        integer(kind=c_int) :: tm_hour  ! Hours since midnight (0 - 23).
        integer(kind=c_int) :: tm_mday  ! Day of month (1 - 31).
        integer(kind=c_int) :: tm_mon   ! Month (0 - 11).
        integer(kind=c_int) :: tm_year  ! Year (current year minus 1900).
        integer(kind=c_int) :: tm_wday  ! Day of week (0 - 6; Sunday = 0).
        integer(kind=c_int) :: tm_yday  ! Day of year (0 - 365).
        integer(kind=c_int) :: tm_isdst ! Positive if daylight saving time is in effect.
     end type c_tm

  interface
     function c_mktime(timeptr) bind(c, name='mktime')
        import :: c_long, c_tm
        implicit none
        type(c_tm),intent(in) :: timeptr
        integer(kind=c_long)  :: c_mktime
     end function c_mktime
  end interface

contains

function map_courbe2D(xx, yy, n, irg)
!===============================================================================
! défini une courbe2D sous forme de pointeurs sur des listes d'abscisses
! et d'ordonnées
! ce n'est pas un pointeur vers une courbe2D mais un type dérivé composé
! de pointeurs
!
! entrées :
!     - xx  : tableau de réels pour les abscisses
!     - yy  : tableau de réels pour les ordonnées
!     - n   : entier, le nombre de points de la courbe
!     - irg : entier, l'indice courant initial
! résultat : de type ptr_courbe2D
!
!===============================================================================
   integer, intent(in), target :: n, irg
   real(kind=long), intent(in), target :: xx(:), yy(:)
   type(ptr_courbe2D) :: map_courbe2D
   ! il faudrait sans doute vérifier que xx et yy ont le même nombre d'éléments
   map_courbe2D%np => n
   map_courbe2D%x => xx
   map_courbe2D%y => yy
   map_courbe2D%ip => irg
end function map_courbe2D


function init_courbe2D(xx,yy,n)
!===============================================================================
! construit une courbe 2D à partir d'une liste d'abscisses et d'ordonnées
!         construit une courbe 2D à partir de 2 listes d'abscisses
!                 et d'ordonnées et du nombre de valeurs
! entrées :
!     - xx : tableau de réels pour les abscisses
!     - yy : tableau de réels pour les ordonnées
!     - n  : entier, le nombre de points de la courbe
! résultat : de type courbe2D
!
!===============================================================================
   integer, intent(in) :: n
   real(kind=long) :: xx(:), yy(:)
   type(courbe2D) :: init_courbe2D
   integer :: i, err

   allocate(init_courbe2D%yx(n),stat=err)
   if (err > 0) then
      stop 2011
   else
      init_courbe2D%np = n
      init_courbe2D%ip = 1
      do i=1, n
         init_courbe2D%yx(i)%x = xx(i)
         init_courbe2D%yx(i)%y = yy(i)
      enddo
   endif
end function init_courbe2D


subroutine remove_courbe2D(c2D)
!===============================================================================
!                    désallocation d'une courbe2D
!
! entrées :
!     - c2D : type courbe2D, la courbe à désallouer
! résultat : aucun
! usage : call remove_courbe2D(ma_courbe2D)
! renvoie une erreur fatale si la désallocation échoue.
!
!===============================================================================
   type(courbe2D) :: c2D
   integer :: err
   deallocate(c2D%yx,stat=err)
   if (err > 0) stop 2015
end subroutine remove_courbe2D


function interpole(une_courbe2D,xx) result(yy)
!===============================================================================
!              interpolation linéaire sur une courbe2D
! entrées :
!     - une_courbe2D : la courbe sur laquelle on interpole
!     - xx           : la valeur pour laquelle on interpole
! résultat : réel
!
!===============================================================================
   type(courbe2D) :: une_courbe2D
   real(kind=long),intent(in) :: xx
   real(kind=long) :: yy
   integer :: ip0, i
   real(kind=long) :: dx, dy

   if (une_courbe2D%np == 1) then  !cas d'un point unique
      yy = une_courbe2D%yx(1)%y
      return
   endif
   ip0 = une_courbe2D%ip
   if (xx >= une_courbe2D%yx(ip0+1)%x) then  !recherche vers le haut
      if (xx >= une_courbe2D%yx(une_courbe2D%np)%x) then
         yy = une_courbe2D%yx(une_courbe2D%np)%y  !on prolonge par une constante
         return
      else
         do i = ip0+1, une_courbe2D%np-1
            if (xx >= une_courbe2D%yx(i)%x .AND. xx < une_courbe2D%yx(i+1)%x) then
               une_courbe2D%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   elseif (xx < une_courbe2D%yx(ip0)%x) then  !recherche vers le bas
      if (xx < une_courbe2D%yx(1)%x) then
         !prolongement par une constante ; dans une vie antérieure on avait mis un stop
         yy = une_courbe2D%yx(1)%y
         une_courbe2D%ip = 1
      else
         do i = ip0-1, 1, -1
            if (xx >= une_courbe2D%yx(i)%x .AND. xx < une_courbe2D%yx(i+1)%x) then
               une_courbe2D%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   endif
   ip0 = une_courbe2D%ip
   dy = une_courbe2D%yx(ip0+1)%y - une_courbe2D%yx(ip0)%y
   dx = une_courbe2D%yx(ip0+1)%x - une_courbe2D%yx(ip0)%x
   yy = une_courbe2D%yx(ip0)%y + dy/dx*(xx-une_courbe2D%yx(ip0)%x)
end function interpole


function ptr_interpole(une_courbe2D,xx) result(yy)
!==============================================================================
!         interpolation linéaire sur une courbe2D version pointeur.
!
! entrées :
!     - une_courbe2D : type ptr_courbe2D, la courbe sur laquelle on interpole
!     - xx           : la valeur pour laquelle on interpole
! résultat : réel
!
!==============================================================================
   type(ptr_courbe2D) :: une_courbe2D
   real(kind=long),intent(in) :: xx
   real(kind=long) :: yy
   integer :: ip0, i
   real(kind=long) :: dx, dy

   ip0 = une_courbe2D%ip
   if (xx >= une_courbe2D%x(ip0+1)) then  !recherche vers le haut
      if (xx >= une_courbe2D%x(une_courbe2D%np)) then
         yy = une_courbe2D%y(une_courbe2D%np)  !on prolonge par une constante
         return
      else
         do i = ip0+1, une_courbe2D%np-1
            if (xx >= une_courbe2D%x(i) .AND. xx < une_courbe2D%x(i+1)) then
               une_courbe2D%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   elseif (xx < une_courbe2D%x(ip0)) then  !recherche vers le bas
      if (xx < une_courbe2D%x(1)) then
         yy = une_courbe2D%y(1)
         une_courbe2D%ip = 1
      else
         do i = ip0-1, 1, -1
            if (xx >= une_courbe2D%x(i) .AND. xx < une_courbe2D%x(i+1)) then
               une_courbe2D%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   endif
   ip0 = une_courbe2D%ip
   dy = une_courbe2D%y(ip0+1) - une_courbe2D%y(ip0)
   dx = une_courbe2D%x(ip0+1) - une_courbe2D%x(ip0)
   yy = une_courbe2D%y(ip0) + dy/dx*(xx-une_courbe2D%x(ip0))
end function ptr_interpole


function next_int(str,separator,next_field)
   !==============================================================================
   !      Renvoie le prochain entier dans la chaîne STR
   !      STR est divisée en champs par des séparateurs ; en plus de la liste
   !      indiquée dans separator, on a toujours au moins l'espace comme séparateur
   !
   !  En entrée next_field est l'index de départ de la recherche
   !  En sortie next_field est l'index du prochain champ à lire
   !
   !  En cas d'erreur la fonction renvoie next_int = 0 et next_field = 0
   !  De la sorte si on appelle une fonction next_<xxx> à la suite en lui
   !  cette valeur de next_field, elle échouera à son tour.
   !
   !==============================================================================
   implicit none
   !---prototype --
   integer :: next_int
   character(*), intent(in) :: str       ! chaîne à scanner
   character(*), intent(in) :: separator ! liste des séparateurs valides en plus de l'espace
   integer, intent(inout) :: next_field  ! index du début du champ suivant
   !---variables locales
   integer :: i, j, k, start, ierr, lt
   character(len=10) :: fmt
   character(len=5) :: full_sep

   start = next_field
   ! début de la recherche avant le 1er caractère -> erreur
   if (start <= 0 .or. start > len_trim(str)) then
      next_int = 0 ; next_field = 0  ;  return !entier non trouvé
   endif
   ! vérification de la liste des séparateurs
   i = scan('0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@_%!?*|~',separator)
   if (i > 0) then
      write(error_unit,*) '>>>> ERREUR dans next_int() : liste de séparateurs invalide : '//trim(separator)
      write(error_unit,*) '>>>> Les séparateurs autorisés en plus de l''espace sont , ; :'
      call do_crash('next_int()')
   endif
   ! détection d'un champ vide : 1 séparateur non espace ou un caractère
   !                            non numérique avant caractères numérique
   i = scan(str(start:),'-+0123456789')
   j = scan(str(start:),'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@_%!?*|~')
   if (i==0 .OR. (j>0 .and. i>j)) then
      !il n'y a pas de caractère numérique avant le prochain caractère alphabétique ou spécial
      next_int = 0 ; next_field = 0 ; return !entier non trouvé
   else
      i = start-1+i   !index du 1er caractère numérique avant le prochain caractère alphabétique ou spécial
   endif

   j = scan(str(start:),trim(separator))
   if (j>0 .and. start-1+j<i) then
      ! il y a un séparateur avant le 1er caractère numérique -> champ vide == entier nul
      j = start-1+j      !index du 1er séparateur
      next_int = 0  ;  next_field = j+1
   else
      ! cas normal : pas de séparateur non espace avant le 1er caractère numérique
      !k = scan(str(i:),separator//' ')-1  !nombre de caractères précédant le 1er espace ou séparateur
      lt = len_trim(separator)
      if (lt >= 5) then
         stop '>>>> BUG dans Next_Int()'
      else
         full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:) = repeat(' ',5-lt)
         k = scan(str(i:),full_sep)-1  !nombre de caractères précédant le 1er espace ou séparateur
      endif
      if (k < 0) then
         k = len_trim(str)-i+1 !pas de séparateur -> fin de la chaîne
         j = len_trim(str)+1
      else
         !recherche de la fin du champ : on passe les espaces qui suivent l'entier à lire
         do j=i+k,len_trim(str) !on cherche le 1er caractère non espace
            if (str(j:j)/=' ') exit
         enddo
         if (scan(trim(separator),str(j:j)) >0) j=j+1 !si le 1er caractère non espace est un séparateur, le champ suivant commence après
      endif
      write(fmt,'(a,i1,a)') '(i',k,')'
      next_field = max(i+k,j)
      !lecture de l'entier dans la sous-chaîne str(i:i+k-1)
      read(str(i:i+k-1),fmt,iostat=ierr) next_int
      if (ierr /= 0) then
         write(error_unit,'(2a)') ' >>>> Erreur dans NEXT_INT() en lecture de ',str(i:i+k-1)
         stop 189
      endif
   endif
end function next_int
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function next_real(str,separator,next_field)
   !==============================================================================
   !      Renvoie le prochain réel dans la chaîne STR
   !      STR est divisée en champs par des séparateurs ; en plus de la liste
   !      indiquée dans separator, on a toujours au moins l'espace comme séparateur
   !
   !  En entrée next_field est l'index de départ de la recherche
   !  En sortie next_field est l'index du prochain champ à lire
   !
   !  En cas d'erreur la fonction renvoie next_real = 0 et next_field = 0
   !  De la sorte si on appelle une fonction next_<xxx> à la suite en lui
   !  donnant cette valeur de next_field, elle échouera à son tour.
   !==============================================================================
   implicit none
   !---prototype --
   real(kind=long) :: next_real
   character(*), intent(in) :: str       ! chaîne à scanner
   character(*), intent(in) :: separator ! liste des séparateurs valides en plus de l'espace
   integer, intent(inout) :: next_field  ! index du début du champ suivant c'est à dire après le séparateur
   !---variables locales
   integer :: i, j, k, start, ierr, lt
   character(len=10) :: fmt
   character(len=5) :: full_sep

   start = next_field
   ! début de la recherche avant le 1er caractère -> erreur
   if (start <= 0 .or. start > len_trim(str)) then
      !print*,' >>>> Next_Real() : non trouvé 1 : ',trim(str(start:)),' # ',trim(str)
      next_real = zero ; next_field = 0  ;  return !réel non trouvé
   endif
   ! vérification de la liste des séparateurs
   i = scan('0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@_%!?*|~',separator)
   if (i > 0) then
      write(error_unit,*) '>>>> ERREUR dans next_real() : liste de séparateurs invalide : '//trim(separator)
      write(error_unit,*) '>>>> Les séparateurs autorisés en plus de l''espace sont , ; :'
      call do_crash('next_real()')
   endif
   ! détection d'un champ vide : 1 séparateur non espace ou un caractère
   !                            non numérique avant caractères numérique
   i = scan(str(start:),'-+.0123456789')
   j = scan(str(start:),'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'&#$§@_%!?*|~')
   if (i==0 .OR. (j>0 .and. i>j)) then
      !il n'y a pas de caractère numérique avant le prochain caractère alphabétique ou spécial
      !print*,' >>>> Next_Real() : non trouvé 2 : ',trim(str(start:)),' # ',trim(str)
      next_real = zero ; next_field = 0  ;  return !réel non trouvé
   else
      i = start-1+i   !index du 1er caractère numérique avant le prochain caractère alphabétique ou spécial
   endif

   j = scan(str(start:),separator)
   if (j>0 .and. start-1+j<i) then
      ! il y a un séparateur avant le 1er caractère numérique -> champ vide == réel nul
      j = start-1+j      !index du 1er séparateur
      !print*,' >>>> Next_Real() : champ vide : ',trim(str(start:)),' # ',trim(str)
      next_real = zero  ;  next_field = j+1
   else
      ! cas normal : pas de séparateur non espace avant le 1er caractère numérique
      !k = scan(str(i:),separator//' ')-1  !nombre de caractères précédant le 1er espace ou séparateur
      lt = len_trim(separator)
      if (lt >= 5) then
         stop '>>>> BUG dans Next_Real()'
      else
         full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:) = repeat(' ',5-lt)
         k = scan(str(i:),full_sep)-1  !nombre de caractères précédant le 1er espace ou séparateur
         if (k == -1) k = len_trim(str(i:)) !dernier champ de la chaîne
         write(fmt,'(a,i2.2,a)') '(f',k,'.0)'
      endif
      !recherche de la fin du champ : on passe les espaces qui suivent le réel à lire
      do j=i+k,len_trim(str) !on cherche le 1er caractère non espace
         if (str(j:j) /= ' ') exit
      enddo
      if (scan(separator,str(j:j)) >0) j=j+1 !si le 1er caractère non espace est un séparateur, le champ suivant commence après
      next_field = max(i+k,j)
      !lecture du réel dans la sous-chaîne str(i:i+k-1)
      read(str(i:i+k-1),fmt,iostat=ierr) next_real
      if (ierr /= 0) then
         write(error_unit,'(2a)') ' >>>> Erreur dans NEXT_REAL() en lecture de ',str(i:i+k-1)
         stop 190
      endif
      !print*,' >>>> Next_Real() réussi : ',next_real,'#',trim(str(start:)),' # ',trim(str)
   endif
end function next_real
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function next_string(str,separator,next_field)
   !==============================================================================
   !   Renvoie la prochaine sous-chaîne dans la chaîne STR
   !
   !  STR est divisée en champs par des séparateurs ; en plus de la liste
   !  indiquée dans separator, on a toujours au moins l'espace comme séparateur
   !
   !  En entrée next_field est l'index de départ de la recherche
   !  En sortie next_field est l'index du prochain champ à lire
   !
   !  En cas d'erreur la fonction renvoie next_string = ' ' et next_field = 0
   !  De la sorte si on appelle une fonction next_<xxx> à la suite en lui
   !  cette valeur de next_field, elle échouera à son tour.
   !==============================================================================
   implicit none
   !---prototype --
   character(len=:), allocatable :: next_string
   character(len=*), intent(in) :: str       ! chaîne à scanner
   character(len=*), intent(in) :: separator ! liste des séparateurs valides
   integer, intent(inout) :: next_field  ! index du début du champ suivant c'est à dire après le séparateur
   !---variables locales
   integer :: i, j, k, start, lt
   character(len=5) :: full_sep = repeat(' ',5)

   i = len_trim(str)
   allocate(character(len=i) :: next_string)

   start = next_field
   ! cas pathologiques
   if (start > len_trim(str)) then
      next_string = ' '  ;  next_field = start  ;  return
   else if (start == len_trim(str)) then
      next_string = str(start:)  ;  next_field = start  ;  return
   else if (start == 0) then
      next_string = ' ' ;  next_field = 0  ;  return
   else
      next_string = ' '
   endif
   ! vérification de la liste des séparateurs
   i = scan('0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'&#$§@_%!?*|~',separator)
   if (i > 0) then
      write(error_unit,*) '>>>> ERREUR dans next_string() : liste de séparateurs invalide : '//trim(separator)
      write(error_unit,*) '>>>> Les séparateurs autorisés en plus de l''espace sont . , ; :'
      call do_crash('next_string()')
   endif
   ! détection d'un champ vide : 1 séparateur non espace avant caractère alphanumérique ou spécial
   i = scan(str(start:),trim(separator))  !recherche du 1er séparateur
   j = scan(str(start:),'0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'&#$§@_%!?*|~')
   if (i>0 .and. i<j) then
      !il n'y a pas de caractère alphabétique ou spécial avant le prochain séparateur
      next_string = ' ' ; next_field = i+1  ;  return ! champ vide
   endif
   lt = len_trim(separator)
   full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:lt+2) = ' '
   i = scan(str(start:),full_sep)
   if (i == 0) then  !on est à la fin de la chaîne
      ! ni séparateur ni espace -> on prend tout reste de la chaîne
      next_field = len_trim(str)
      next_string = str(start:)
      return
   endif
   ! recherche du 1er caractère admissible
   i = scan(str(start:),'0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@+-_%!?')
   if (i>0) then
      i = start-1+i   !index du 1er caractère admissible
   else
      next_string = ' ' ; next_field = start
      return ! string non trouvée
   endif
   ! recherche de la fin du champ
   j = scan(str(start:),trim(separator))
   if (j>0 .and. start-1+j<i) then
      ! il y a un séparateur avant le 1er caractère admissible -> champ vide
      j = start-1+j      !index du 1er séparateur
      next_string = ' '  ;  next_field = j+1
   else
      ! cas normal : pas de séparateur non espace avant le 1er caractère admissible
      lt = len_trim(separator)
      if (lt >= 5) then
         stop '>>>> BUG dans Next_String()'
      else
         full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:) = repeat(' ',5-lt)
         k = scan(str(i:),full_sep)-1  !nombre de caractères précédant le 1er espace ou séparateur
      endif
      ! recherche de la fin du champ : 1er caractère non espace après la fin du champ
      do j=i+k,len_trim(str) !on cherche le 1er caractère non espace
         if (str(j:j)/=' ') exit
      enddo
      if (scan(separator,str(j:j)) >0) j=j+1 !si le caractère cherché est un séparateur, le champ suivant commence après
      next_field = max(i+k,j)
      ! récupération de la sous-chaîne
      next_string = str(i:i+k-1)
   endif
end function next_string


logical function between(z, z1, z2)
!==============================================================================
!          Teste si un réel est strictement compris entre 2 autres
!
! renvoie vrai si z est compris entre z1 et z2
! Ne fait pas d'hypothèse sur l'ordre de z1 et z2
!
! entrées : z1 et z2 réels
!
!==============================================================================
   ! prototype
   real(kind=long), intent(in) :: z, z1, z2

   between = z > min(z1,z2) .and. z < max(z1,z2)
end function between


subroutine mise_en_veille()
!==============================================================================
!   Mise en attente du programme
!==============================================================================
   ! variables
   logical :: reprise
   integer :: lu

   open(newunit=lu,file='pause',status='unknown')
   close(lu,status='delete')
   do
      call sleep(3) !attente de 3 secondes
      inquire(file='go',exist=reprise)
      if (reprise) then
         open(newunit=lu,file='go',status='unknown')
         close(lu,status='delete')
         exit
      endif
   enddo
end subroutine mise_en_veille


subroutine remplacer(texte,c1,c2)
!==============================================================================
!   Remplacement d'un caractère par un autre
!==============================================================================
   ! variables
   character(len=*), intent(inout) :: texte
   character :: c1*1, c2*1
   integer :: n

   do n=1, len(texte)
      if (texte(n:n) == c1) texte(n:n) = c2
   enddo
end subroutine remplacer


subroutine capitalize(in, out)
!==============================================================================
!   Mettre une chaîne en lettres capitales
!==============================================================================
   implicit none
   ! prototype
   character(len=*), intent(in) :: in
   character(len=*), intent(out) :: out
   ! variables locales
   integer :: offset, ic, i

   out = in
   offset = ichar('A') - ichar('a')
   do i = 1, len_trim(in)
      ic = ichar(in(i:i))
      if (ic >= ichar('a') .AND. ic <= ichar('z')) then
         out(i:i) = char(ic+offset)
      endif
   enddo
end subroutine capitalize


subroutine do_crash(from)
!==============================================================================
!  provoque une division par zéro pour obtenir un traceback
!  à utiliser pour identifier l'arbre d'appel d'une procédure qui échoue.
!==============================================================================
   !prototype
   character(len=*),intent(in) :: from  !nom de la procédure appelante
   !variables
   integer :: nap=0

   nap = nap+1
   write(*,*) ' >>>> Do_Crash : appel par ',trim(from)
   write(*,*) ' Do_Crash : ',1/(nap-1)
   stop 2001
end subroutine do_crash


subroutine bubble_sort(a)
   real(kind=long), intent(in out), dimension(:) :: a
   real(kind=long) :: temp
   integer :: i, j
   logical :: swapped

   do j = size(a)-1, 1, -1
      swapped = .false.
      do i = 1, j
         if (a(i) > a(i+1)) then
            temp = a(i)
            a(i) = a(i+1)
            a(i+1) = temp
            swapped = .true.
         end if
      end do
      if (.not. swapped) exit
   end do
end subroutine bubble_sort


subroutine heure(tt,hms)
!==============================================================================
!        écriture de l'heure sous le format hh:mm:ss
!  input  : tt en secondes
!  output : hms en format +jjjjj:hh:mm:ss
!
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long),intent(in) :: tt  !secondes
   character(len=15),intent(out) :: hms  !+jjjjj:hh:mm:ss
   ! -- les variables --
   character(len=1) :: signe
   integer :: ijj,ith,itmin,itsec
   real(kind=long) :: th,t,tmin,tsec

   if (tt < 0._long) then
      signe = '-'
   else
      signe = ' '
   endif
   t = abs(tt)
   ijj = iabs(floor(t/86400._Long))
   th = t-float(ijj)*86400._Long
   ith = floor(th/3600._long)
   tmin = th-float(ith)*3600._long
   itmin = floor(tmin/60._long)
   tsec = tmin-float(itmin)*60._long
   itsec = floor(tsec)
   if (ijj<10000) then
      write(hms,'(a1,i4.2,3(a,i2.2))') signe,ijj,'j:',ith,':',itmin,':',itsec
   else
      write(hms,'(i5.2,3(a,i2.2))')          ijj,'j:',ith,':',itmin,':',itsec
   endif
end subroutine heure


function c_heure(tt)
   implicit none
   ! -- le prototype --
   real(kind=long),intent(in) :: tt  !secondes
   character(len=15) :: c_heure  !+jjjjj:hh:mm:ss
   ! -- les variables --
   character(len=15) :: hms  !+jjjjj:hh:mm:ss


   call heure(tt,hms)
   c_heure = hms
end function c_heure


! function lire_Date(date)
! !==============================================================================
! !  Lecture d'une date dans une chaîne de caractères et conversion en secondes
! !  La date peut-être donnée sous la forme 0000j:00:00:00 ou 0000:00:00:00
! !==============================================================================
!    implicit none
!    ! -- le prototype --
!    real(kind=long) :: lire_Date  !secondes
!    character(len=15),intent(in) :: date  !+jjjjj:hh:mm:ss
!    ! -- les variables --
!    integer :: nf, jjj, hh, mm, ss, k, l
!    character (len=15) :: cjjj
!
!    nf = 1
!    cjjj = next_string(date,':',nf)
!    k = len_trim(trim(cjjj))
!    if (cjjj(k:k) == 'j' .or. cjjj(k:k) == 'J') then
!       l = 1  ;  JJJ = next_int(cjjj(1:k-1),'',l)
!    else
!       l = 1  ;  JJJ = next_int(cjjj(1:k),'',l)
!    endif
!    HH = next_int(date,':',nf)
!    MM = next_int(date,':',nf)   ;  SS = next_int(date,':',nf)
!    lire_Date = real(jjj,long)*86400._long + real(hh,long)*3600._long + real(mm,long)*60 + real(ss,long)
! end function lire_Date

function lire_date(message, tinf, date_format)
   !==============================================================================
   !     lecture d'une date en JJ:HH:MM:SS sur la chaine message
   !     l'utilisation du séparateur permet de ne pas avoir de limite sur la
   !     taille des champs
   !==============================================================================
! TODO gestion des erreurs
   implicit none
   ! prototype
   integer(kind=long) :: lire_date
   character(len=*),intent(in) :: message
   ! variables locales
   integer :: next_field
   integer(kind=long) :: j, h, m, s
   integer :: k, l
   character(len=3), parameter :: separateur=':'
   type(c_tm) :: tm
   logical,optional :: date_format
   real(kind=Long),optional :: tinf
   integer :: tri1, ierr
   character (len=15) :: cjjj

   tm%tm_sec    = 0
   tm%tm_min    = 0
   tm%tm_hour   = 0
   tm%tm_mday   = 0
   tm%tm_mon    = 0
   tm%tm_year   = 0
   tm%tm_wday   = -1
   tm%tm_yday   = -1
   tm%tm_isdst  = -1

   next_field = 1
   tri1 = scan(message,'-') + 1
!   if (scan(message,'/').ne.0) then
   if (tri1 > 1 .and. scan(message(tri1:),'-').ne.0) then
      !s'il y a 2 tirets on suppose qu'on a un format de date ISO
      read(message(1:4),'(i4)',iostat=ierr) tm%tm_year
      tm%tm_year = tm%tm_year-1900
      read(message(6:7),'(i4)',iostat=ierr) tm%tm_mon
      tm%tm_mon = tm%tm_mon-1
      read(message(9:10),'(i4)',iostat=ierr) tm%tm_mday
      read(message(12:13),'(i4)',iostat=ierr) tm%tm_hour
      read(message(15:16),'(i4)',iostat=ierr) tm%tm_min
      read(message(18:19),'(i4)',iostat=ierr) tm%tm_sec
      lire_date = c_mktime(tm)
   else
      if (scan(message,':').ne.0) then
         ! format jjj:hh:mm
         cjjj = next_string(message,':',next_field)
         k = len_trim(trim(cjjj))
         if (cjjj(k:k) == 'j' .or. cjjj(k:k) == 'J') then
             l = 1  ;  j = int(abs(next_int(cjjj(1:k-1),'',l)),kind=long)
         else
             l = 1  ;  j = int(abs(next_int(cjjj(1:k),'',l)),kind=long)
         endif
!          j = int(abs(next_int(message,separateur,next_field)),kind=long)
         h = int(abs(next_int(message,separateur,next_field)),kind=long)
         m = int(abs(next_int(message,separateur,next_field)),kind=long)
         s = 0
         if (next_field .ne. 0) then
            s = int(abs(next_int(message,separateur,next_field)),kind=long)
         endif
         lire_date = ((j*24_long + h) * 60_long + m) * 60_long + s
         if (scan(message,'-') /= 0) lire_date = -lire_date
      else
         ! juste des minutes
         lire_date=int(60*abs(next_real(message,'',next_field)),kind=long)
         if (scan(message,'-') /= 0) lire_date = -lire_date
      endif
      if (present(date_format) .and. present(tinf)) then
         if (date_format) then ! tinf was  DD/MM/YY HH:MM:SS
            ! the date is relative to tinf
            lire_date = lire_date + int(tinf,kind=long)
         endif
      endif
   endif

end function lire_date


character(len=fnl) function get_filepath(filename,working_dir)
! reconstruit le chemin d'un fichier à partir de son nom complet relatif ou absolu
   implicit none
   ! -- Prototype --
   character(len=fnl), intent(in) :: filename, working_dir
   ! -- variables locales --
   integer :: k

   k = scan(filename,slash,.true.)
   if (k == 0) then !pas de séparateur de répertoire -> dans le dossier de travail
      get_filepath = trim(working_dir)
   else if (filename(1:1) == slash) then
      get_filepath = filename(1:k)
   else
      get_filepath = trim(working_dir)//filename(1:k)
   endif

end function get_filepath


character(len=fnl) function get_filename(repFile,key)
! lit dans un fichier REP le nom d'un fichier de données désigné par le mot clé associé
! exemple : repFile = etude.rep , key=BIN -> le nom du fichier BIN inscrit dans etude.rep
   implicit none
   ! -- Prototype --
   character(len=fnl), intent(in) :: repFile
   character(len=3), intent(in) :: key
   ! -- variables locales --
   integer :: lu, ios=-1
   character(len=fnl) :: ligne
   character(len=3) :: ext

   get_filename = ''
   open(newunit=lu,file=trim(repFile),form='formatted',status='old',iostat=ios)
   if (ios /= 0) then
      write(*,*) '>>>> Impossible d''ouvrir le fichier ',trim(repFile)
      stop 2002
   endif
   call capitalize(key,ext)
   do
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      if (ligne(1:3) == ext) then
         get_filename = trim(ligne(5:))
         exit
      else
         cycle
      endif
   enddo
   close(lu)
end function get_filename


function date_et_heure(tt)
!renvoie le temps t converti en jjjjj:hh:mm:ss
   implicit none
   ! -- prototype --
   real(kind=long),intent(in) :: tt
   character(len=15) :: date_et_heure
   ! -- les variables --
   character(len=1) :: signe
   character(len=15) :: hms
   integer :: ijj,ith,itmin,itsec
   real(kind=long) :: th,t,tmin,tsec

   if (tt < 0._long) then
      signe = '-'
   else
      signe = ' '
   endif
   t = abs(tt)
   ijj = iabs(floor(t/86400._Long))
   th = t-float(ijj)*86400._Long
   ith = floor(th/3600._long)
   tmin = th-float(ith)*3600._long
   itmin = floor(tmin/60._long)
   tsec = tmin-float(itmin)*60._long
   itsec = floor(tsec)
   if (ijj<10000) then
      write(hms,'(a1,i4.2,3(a,i2.2))') signe,ijj,'j:',ith,':',itmin,':',itsec
   else
      write(hms,'(i5.2,3(a,i2.2))')          ijj,'j:',ith,':',itmin,':',itsec
   endif
   date_et_heure = hms
end function date_et_heure


function sqrt2(x)
   !==============================================================================
   !   version de sqrt adoucie au voisinage de 0 par un polynome de degrés 2
   !   tangent à sqrt() en alpha et de pente finie en 0.
   !
   !==============================================================================
   implicit none
   !prototype
   real(kind=long), intent(in) :: x
   real(kind=long) :: sqrt2
   !variables locales
   real(kind=long), parameter :: alpha=0.005_long

   if (x > alpha) then
      sqrt2 = sqrt(x)
   elseif (x < zero) then
      write(error_unit,*) ' >>>> Erreur dans SQRT2() : argument négatif => ',x
      stop 197
   else
      sqrt2 = x * (3._long -x/alpha) / (2._long*sqrt(alpha))
   endif
end function sqrt2


subroutine Erreur_Lecture(fichier,ligne)
!==============================================================================
!     Erreur de lecture d'un fichier de données déclenchée
!     par next_int() ou next_real()
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: fichier, ligne

   write(error_unit,*)
   write(error_unit,*) ' >>>>'
   write(error_unit,*) ' >>>> Erreur de lecture du fichier ',trim(fichier),' à la ligne '
   write(error_unit,*) ' >>>> ',ligne
   write(error_unit,*) ' >>>>'
   write(error_unit,*)
   stop 042
end subroutine Erreur_Lecture


subroutine Err032(pk,ib)
   use parametres, only: long
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   real(kind=long)    :: pk
   character(len=30) :: titre_erreur = ' >>>> ERREUR 032 <<<<'

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(error_unit,'(1x,a,g0,a,i3)') 'Il n''y a pas de section singulière au pk ',pk,' du bief ',ib
end subroutine Err032


end module Utilitaires
