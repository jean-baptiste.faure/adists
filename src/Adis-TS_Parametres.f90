!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
!                     Structure de données de ADIS version 3
!
!##############################################################################
!
! numérotation des STOP : 17nn
!
module Parametres
!===============================================================================
!       definition des dimensions maximales des tableaux, de constantes
!       numeriques et de divers constantes physiques et mathematiques.
!===============================================================================
   implicit none

   integer, parameter :: long = kind(1.0d0)  ! double precision pour les reels
   integer, parameter :: sp = kind(1.)       ! simple précision
!   integer, parameter :: issup = 6001        ! nb max de sections ou points de calcul
!   integer, parameter :: ibsup = 150         ! nb max de biefs ou branches
!   integer, parameter :: nosup = ibsup+1     ! nb max de noeuds = ibsup+1
   integer, parameter :: nnsup = 20          ! nombre maximum de points par noeud (loi hauteur-surface/volume)
   integer, parameter :: ncmax = 3           ! nombre maximum de couches sédimentaires
   integer, parameter :: ngmax = 3           ! nombre maximum de classes granulométriques
   integer, parameter :: nzsup = 3           ! nombre de zones de strickler par profil
   integer, parameter :: nssup = 400         ! nombre maximum d'ouvrages élémentaires et de sections singulières / ouvrages composés
   integer, parameter :: nesup = 15          ! nombre maximum d'ouvrages élémentaires par section singulière

   integer, parameter :: cache_size_default = 16 !taille par défaut en Mo du cache des lignes d'eau

   real(kind=long), parameter :: zero = 0._long
   real(kind=long), parameter :: un = 1._long
   real(kind=long), parameter :: pi = 3.14159265359_long
   real(kind=long),parameter :: deuxtiers = 2._long/3._long

   integer, parameter :: lnode = 10          !nombre de caractères autorisés pour les noms des noeuds
   integer, parameter :: fnl = 128           !nombre de caractères autorisés pour les noms de fichier

   real(kind=long), parameter :: g = 9.81_long     !gravité
   real(kind=long), parameter :: nu = 1.e-06_long  !viscosité cinématique
   real(kind=long), parameter :: rho = 1000._long  !masse volumique de l'eau
   real(kind=long), parameter :: gnu2 = g/(rho*nu*nu)

   !real(kind=long), parameter :: dt_bottom = 1.0_long

   character (len=6), parameter :: defaut = 'defaut'

   real(kind=long), parameter :: precision_alti = 0.00001_long

   character(len=1) :: slash

   include 'OS_slash.fi'

   ! FIXME: comment choisir une bonne valeur pour seuil_miseAjour_geometrie
   real(kind=long) :: seuil_miseAjour_geometrie = 1.e-04_long

   !--valeur du signal SIGTSTP (ask suspension from keyboard, ctrl+Z)
   integer, parameter :: SIGTSTP = 20
   !--valeur du signal SIGINT (ask interruption from keyboard, ctrl+C)
   integer, parameter :: SIGINT = 2

end module parametres



module DataNum
!===============================================================================
!                    Paramètres numériques divers
!
!===============================================================================
   use parametres, only : long
   implicit none

   real(kind=long) :: theta      ! coefficient d'implicitation (partie diffusion)
   real(kind=long) :: t0, tmax   ! temps de début et fin de simulation (secondes)
   real(kind=long) :: dt0        ! pas de temps maximal (fourni par l'utilisateur)
   real(kind=long) :: dtcsv      ! pas de temps de stockage en fichier csv (texte)
   real(kind=long) :: dtbin      ! pas de temps de stockage en fichier binaire
   real(kind=long) :: dtscr      ! pas de temps d'écriture à l'écran
   real(kind=long) :: dtMage     ! pas de temps maximal entre 2 simulations hydrauliques
   real(kind=long) :: dt_bottom  ! pas de temps minimal admissible
   real(kind=long) :: c_initiale ! concentration initiale uniforme
   integer :: nb_threads
   logical :: no_coupling
   real(kind=long) :: current_time
   logical :: alarm_ON
   real(kind=long) :: too_big
   real(kind=long) :: t_screen

end module DataNum



module adis_types
!===============================================================================
!        Module contenant la définition de tous les types dérivés
!                     utilisés dans Adis-TS
!
!===============================================================================
use parametres, only: long, lnode, fnl, ncmax, nzsup, zero, slash

implicit none

! types utilitaires
type point_net
   integer          :: ib  !identifiant bief
   real(kind=long)  :: pk  !pk du point
end type point_net



type point2D
   real(kind=long) :: x
   real(kind=long) :: y
end type point2D



type courbe2D
   integer :: np ! nombre de points de la courbe
   type(point2D),allocatable :: yx(:)
   integer :: ip ! indice courant
end type courbe2D



type ptr_courbe2D
   integer, pointer :: np
   real(kind=long) , pointer :: x(:)
   real(kind=long) , pointer :: y(:)
   integer, pointer ::ip
end type ptr_courbe2D



type bief
! Bief  : les biefs du réseau ; un bief relie un noeud amont
!         (1er noeud) à un noeud aval (2e noeud)
!         contient les noms des fichiers de données topo
!         et sédimentaires associées et des index sur la liste
!         des profils en travers qui en constituent la géométrie
   character(len=15) :: name         !nom du bief
   character(len=lnode)  :: amont    !nom du noeud amont
   character(len=lnode)  :: aval     !nom du noeud aval
   character(len=fnl) :: fichier_geo  !fichier de géométrie au format ST2
   integer           :: nam, nav     !numéro des noeuds amont et aval (la cohérence
                                     !avec les noms des noeuds amont et aval est
                                     !automatique lors de la création des noeuds)
   integer           :: is1, is2     !index de début et fin dans la collection
                                     !des profils jeuSection (module Geometrie_Section)
end type bief



type noeud
! Noeud : les noeuds du réseau ; contient la discrétisation altitude - surface
   character(len=lnode) :: name  ! nom du noeud
   integer :: cl !position dans le réseau :
                 ! +1 : noeud et CL amont : le noeud est noeud aval d'aucun bief
                 ! -1 : noeud et CL aval : le noeud est noeud amont d'aucun bief
                 !  0 : noeud intérieur (apport ponctuel) : ni noeud amont ni noeud aval
#ifdef avec_casiers
   integer :: np    ! nombre de niveaux du profil = taille utile des tableaux
                    ! si np = 0 : pas de surface
   real(kind=long) :: zne(nnsup) ! tabulation des cotes pour decrire la géométrie du noeud
   real(kind=Long) :: sne(nnsup) ! tabulation des surfaces pour decrire la géométrie du noeud
                                 ! sne(n) est la surface à la cote zne(n)
   real(kind=Long) :: vne(nnsup) ! tabulation des volumes pour decrire la géométrie du noeud
                                 ! vne(n) est le volume sous la cote zne(n)
   integer :: ip ! niveau d'interpolation courant (dernier utilisé)
                 ! la dernière cote utilisée est entre zne(ip) et zne(ip+1)
#endif /* avec_casiers */
end type noeud



type rezo
! Réseau : contient les dimensions effectives et variables globales du réseau
!          les tables d'index permettant de parcourir les biefs et les noeuds
   ! dimensions effectives
   integer :: IBMax    ! nb de biefs ou branches du réseau
   integer :: NOMax    ! nb de noeuds
   integer :: NCLM     ! nb de C.L. amont
   integer :: NCLV     ! nb de C.L. aval
   integer :: NSMax    ! nb de sections singulieres
   integer :: ISmax    ! nb de sections ou points de calcul

   ! topologie
   integer :: iba          ! rang du dernier bief amont de la maille
                           ! iba = -1 ==> aucun bief à l'amont de la maille ==> ne sait pas faire
                           ! iba > 0 ==> iba = nombre de biefs amonts de la maille
                           !            iba=ibmax s'il n'y a pas de maille
                           ! iba = 0 ==> impossible
   integer :: ibz          ! rang du premier bief aval de la maille
                           ! = ibmax+1-nombre de biefs aval de la maille
                           ! = ibmax+1 si la maille reste ouverte ou si le réseau est ramifié
   integer, allocatable :: lbz(:,:) ! lbz(n,1) = rang du premier bief qui part du nœud n
                                    ! lbz(n,2) = rang du dernier bief qui part du nœud n
   integer, allocatable :: iub(:)   ! abs(iub(ib)) = numéro dans l'ordre des données du bief de rang ib
                           ! dans l'ordre de calcul de la ligne d'eau.
                           ! En particulier :
                           ! iub(1) = numéro du premier bief amont du modèle
                           ! iub(ibmax) = numéro du dernier bief aval du modèle
                           ! iub(ib) est négatif si le bief de rang ib est dans la maille
   integer, allocatable :: ibu(:)   ! ibu(kb) = rang de calcul du bief de numéro kb dans l'ordre
                           !           des données

   integer, allocatable :: lbamv(:) ! liste des biefs amont et aval (repérés par leur rang)
                           ! ces biefs portent les c.l. :
                           ! Si le nœud n est nœud amont du réseau,
                           !               lbamv(n) = rang du bief qui part de n
                           ! Si le nœud n est nœud aval du réseau,
                           !               lbamv(n) = rang du bief qui arrive en n
                           ! Si le noeud n est un noeud intérieur,
                           !               lbamv(n) = 0
!   integer, allocatable :: iss(:)   !--->ISS(IS) = indicateur de singularité de la section IS
!                           ! on appelle section singulière la section aval d'un couple singulier
!                           ! c'est à dire de 2 sections à la même abscisse entre lesquelles on peut
!                           ! placer une loi d'ouvrage (cf. MAGE)
!                           ! ISS(IS) = 0 ==> IS est section regulière
!                           ! ISS(IS) = NS >0 ==> IS est section singulière et NS est le numéro de
!                           !                     l'ouvrage composé correspondant à IS par NSS(1,...)
end type rezo



type pointTS
!type pour les données géométriques brutes
   character(len=3) :: tag = '' !étiquette du point ; permet de définir globalement les lits (mêmes tags)
   real(kind=long) :: x, y      !coordonnées géographiques du point
   real(kind=long) :: z         !cote du fond => z-zc(1) = épaisseur de la couche supérieure
   integer         :: nc        !nombre de couches de sédiments (locales)
   real(kind=long) :: zc(ncmax) !cote du plancher de chaque couche en partant du haut
                                !dim utile = nc
                                !zc(nc) = -9999.
                                !NB : il faut prévoir un mécanisme pour assurer que les zc sont décroissants.
   real(kind=long) :: d50(ncmax)   !valeur locale du d50  ; dim utile = nc
   real(kind=long) :: sigma(ncmax) !valeur locale de l'étendue granulométrique ; dim utile = nc
   real(kind=long) :: tau(ncmax)   !valeur locale de la contrainte de mise en mouvement ; dim utile = nc
end type pointTS



type pointLC
!type pour la tabulation largeur-cote
   real(kind=long) :: h  !tirant d'eau (profondeur)
   real(kind=long) :: l  !largeur au miroir ; tient compte de la présence d'iles : l(n) <= abs(yd(n)-yg(n))
   real(kind=long) :: s  !section mouillée ; tient compte de la présence d'iles
   real(kind=long) :: p  !périmètre mouillé ; tient compte de la présence d'iles
end type pointLC



type pointAC
!type de point pour la projection abscisses-cotes d'un profil XYZ
   real(kind=long) :: y  !abscisse en travers
   real(kind=long) :: z  !altitude
end type pointAC



type profilAC
!type pour la projection abscisses-cotes d'un profil XYZ
   real(kind=long) :: pk  !pk du profil (abscisse en long)
   integer :: np          !nb de points réel du profil en travers
                          !nb+2 : taille effective du tableau yz
   type (pointAC), allocatable :: yz(:)  !à dimensionner de 0 à np+1 pour permettre
                                         !d'ajouter un point à gauche et à droite décalés de 100 m en altitude
end type profilAC



type profil
!type profil TS complet
   character(len=20) :: name = ''
   real(kind=long) :: pk             !abscisse en long (position)
   real(kind=long) :: zf             !cote du fond
   !real(kind=long) :: K = 25._long   !coefficient de Strickler global
   real(kind=long) :: d90            !diamètre caractéristique des sédiments du fond stable

   !données brutes
   integer :: np        !nombre de points du profil=taille effective du tableau xyz
   type(pointTS), allocatable :: xyz(:) !liste ordonnée des points du profil

   !données tabulées largeurs-cotes pour la couche supérieure
   logical :: wh_aJour  !faux si xyz a été modifié sans refaire la tabulation wh
                        !NB : à gérer par le programmeur
   integer :: nh        !nombre de niveaux du profil
                        !taille effective du tableau wh
   integer :: ip        !niveau d'interpolation courant (dernier utilisé)
                        !la dernière hauteur d'eau utilisée est entre wh(ip)%h et wh(ip+1)%h
   type(pointLC), allocatable :: wh(:)  ! ordonnés par ordre wh(n)%h croissant

   integer :: nzone !nombre de zones de frottement dans le profil, a priori 3 zones
   integer :: li(nzsup-1)  !limites (indices) de zones dans un profil XYZ (li(nz) < li(nz+1), nz ≤ nzone-1
   real(kind=long) :: K(nzsup)  !strickler : si li < li(1) alors K = K(1) ;
                                !            si li(nz-1) < li ≤ li(nz) alors K = K(nz) pour nz = 2 à nzone-1
                                !            si li ≥ li(nzone-1) alors K = K(nzone)

   ! zone de dépôt-érosion dans chacun des 3 lits
   integer :: ibg, ibd  !index des points des berges gauche et droite
   integer :: irg, ird  !index des points des rives gauche et droite (limites du lit mineur)
   integer :: ifg, ifd  !index des points pied de berge du lit mineurs
   real (kind=long) :: l_g, l_m, l_d  ! largeurs utiles des lits moyens gauche et droits et mineur
                                      ! pour le stockage des sédiments calculées à partir des index précédents
   ! NOTE: tant que les modifications de la géométrie sont uniquement verticales, il n'est pas nécessaire de mettre ces largeurs à jour

   integer :: iss ! indicateur de singularité de la section
                  ! on appelle section singulière la section aval d'un couple singulier
                  ! c'est à dire de 2 sections entre lesquelles on a placé une loi d'ouvrage
                  ! ISS = 0      ==> la section est régulière (pas d'ouvrage)
                  ! ISS = NS > 0 ==> la section est singulière et NS est le numéro de l'ouvrage
                  !                  composé tel que NSS(1,NS) = numéro de la section


end type profil



! Pour les types ConditionLimite et Prise il est inutile d'y insérer le
! noeud qui porte la donnée car on y accède plutôt depuis les noeuds.
! C'est donc aux noeuds de connaître les CL et Prises qu'ils portent.
! En pratique, les CL sont dans un tableau indexé par le n° du noeud qui les porte et par le polluant (voir type dataset)

type ConditionLimite  ! Pour toutes les conditions aux limites (eau, polluant et solide)
                      ! amont, aval et apports intermédiaires
   character(len=3) :: type_cl  ! soit 'CDT' pour les CL amont, soit 'MDT' pour les apports ponctuels intermédiaires
   type (courbe2D)  :: fdt      ! données discrètes de la CL
end type ConditionLimite


type Apport_Lateral  ! pour les apports répartis (latéraux) en volume ou en masse
   character(len=3) :: type_al   ! soit "QDT soit "MDT"
   integer          :: ib        ! numéro du bief qui reçoit l'apport
   integer          :: is1       ! section amont du secteur qui reçoit l'apport
   integer          :: is2       ! section aval du secteur qui reçoit l'apport
   real(kind=long)  :: xmin      ! pm amont du secteur qui reçoit l'apport
   real(kind=long)  :: xmax      ! pm aval du secteur qui reçoit l'apport
   type(courbe2D)   :: qmt       ! apport en (m3/s ; kg/s)
end type Apport_Lateral


type state  ! état hydraulique
   real(kind=long) :: time
   !NB : l'indice correspond à celui de dataset%sections
   real(kind=long), allocatable :: x(:)  !abscisse, pk
   real(kind=long), allocatable :: y(:)  !tirant d'eau
   real(kind=long), allocatable :: a(:)  !surface mouillée
   real(kind=long), allocatable :: q(:)  !débit
   real(kind=long), allocatable :: u(:)  !vitesse moyenne
end type state


type coeff_diff
   integer :: mode !indicateur de la formule de calcul à utiliser ; Fisher = 0, Elder = 1, Iwasa = 2
   real(kind=long) :: param !paramètre de la formule de calcul
   !On ne peut pas stocker ici la valeur du coefficient de diffusion calculé par la formule indiquée
   !car celle-ci varie avec le temps et
   real(kind=long) :: b, c !exposants de la formule générique
   ! Elder  => b=c=0
   ! Fisher => b=c=2
   ! Iwasa  => b=0, c=1.5
end type coeff_diff


type polluant
   character (len=30) :: name
   integer :: particule_type  !0 si c'est un soluté
                              !>0 si c'est un sédiment fin ; voir settling_velocity()
   real (kind=long) :: d      !diamètre de la classe granulométrique si MES, -1 sinon
   real (kind=long) :: rho    !masse volumique du sédiment fin : défaut = 2650 g/L
   real (kind=long) :: p      !porosité
   real (kind=long) :: theta_ero  !theta critique erosion (calculé ou donné par l'utilisateur)
   real (kind=long) :: theta_dep  !theta critique depot (calculé ou donné par l'utilisateur)
   real (kind=long) :: tcr_dep !tau critique depot (calculé)
   real (kind=long) :: tcr_ero !tau critique erosion (calculé)
   real (kind=long) :: ws     !vitesse de sédimentation (calculée)
   real (kind=long) :: cdc_riv  !coefficient de disparition cinétique en rivière. 0 si conservatif ou MES
   real (kind=long) :: cdc_cas  !coefficient de disparition cinétique en casier. 0 si conservatif ou MES
! TODO: ajouter ici les paramètres caractéristiques nécessaires pour décrire le comportement des polluants
   real (kind=long) :: apd_dep  !coefficient Apd de la loi de dépôt-érosion
   real (kind=long) :: apd_ero  !coefficient Apd de la loi de dépôt-érosion
   real (kind=long) :: ac, bc   !coefficients Ac et Bc pour le calcul de concentration d'équilibre
   character(len=fnl) :: file_CL  !nom du fichier d'apports ponctuels pour ce polluant
   character(len=fnl) :: file_ALD !nom du fichier d'apports latéraux distribués pour ce polluant
   character(len=fnl) :: file_INI !nom du fichier d'état initial pour ce polluant

end type polluant


! dataset : un jeu de données complet (à compléter)
type dataset
   type (rezo) :: net           !le reseau : ensemble des données topologiques à l'exception
                                !des biefs et des nœuds eux-mêmes
   type (bief), allocatable :: biefs(:)  !les biefs du réseau indexés dans l'ordre de
                                !lecture dans le fichier NET
   type (noeud), allocatable :: nodes(:) !les noeuds du réseau
   type (profil), allocatable :: sections(:) !collection des profils

   integer, allocatable :: im1(:) !im1(i) est l'indice de la dernière section précédant la section i d'abscisse différente : i-1 si i-1 n'est pas singulière
   integer, allocatable :: ip1(:) !ip1(i) est l'indice de la première section suivant la section i d'abscisse différente : i+1 si i n'est pas singulière
   real(kind=long), allocatable :: dxm(:) , dxp(:)  !pas d'espace amont (dxm) et aval (dxp)

   type (coeff_diff), allocatable :: c_diff(:)
!===> Polluant / sédiments / Conditions aux limites / Apports
   integer :: nbpol  !nombre de polluants & classes granulométriques différents suivis en parallèle
                     !correspond au nombre de fichiers de données d'apports en concentration aux noeuds amont
   integer :: nbmes  !nombre de polluant de type MES

   type(polluant), allocatable :: poll(:) !Liste des polluants simulés
   integer, allocatable :: MES_corr(:,:)  !repérage des MES identiques

   type(ConditionLimite), allocatable :: CL_pol(:,:) !CL polluants, MES, etc. : apports/prélèvement ponctuels aux noeuds
                                                     !pour les noeuds amont c'est une concentration
                                                     !pour les noeuds intérieurs c'est un débit massique d'apport
                                                     !dim1 = index du noeud qui porte la CL
                                                     !dim2 = n° polluant & classe granulométrique (< nbpol+1)
                                                     !       idem index dans poll()
   integer,allocatable :: nbal(:) !nombre de secteurs recevant des apports latéraux = nb de blocs de données dans fichier des A.L.
                                    !une valeur par polluant
   type(Apport_Lateral), allocatable :: AL_pol(:,:) !Apports latéraux polluants et MES (débits massiques)
                                                     !dim1 = index de l'apport = numéro d'ordre dans le fichier de données
                                                     !dim2 = n° polluant & classe granulométrique (< nbpol+1)

   real(kind=long), allocatable :: corr_tau_consolide(:,:)  !dim1 = issup
end type dataset

type cache
   integer :: size  !taille du cache en octets
   integer :: nble  !nombre d'état hydrauliques stockés
   character (len=fnl) :: file  !nom du fichier BIN
   logical :: is_long !faux si BIN ne contient que des enregistrements Q et Z
   integer, dimension(:), allocatable :: irg  !irg(is) = rang de calcul de la section is selon MAGE
   type (state), dimension(:), allocatable :: LE
end type cache


type solution
   real(kind=long) :: time                     !date en secondes
                                               !dim1 = n° de point de calcul
   real(kind=long),allocatable :: c(:,:)       !concentrations ; dim2 = index polluant
!   real(kind=long),allocatable :: c_total_MES(:)   !somme des concentrations en MES
   real(kind=long),allocatable :: qm(:,:,:)    !débit massique latéral total ; dim2 = index lit
                                               !dim2 commence à 0 : total des contributions
                                               !                               dim3 = index polluant
   real(kind=long),allocatable :: masse(:,:,:) !masse déposée au fond par lit ;
                                               ! ATTENTION : il s'agit d'une densité linéïque
                                               ! dim1 = index tronçon, la masse est comptée entre is et is+1
                                               ! dim2 = index lit
                                               ! dim3 = index polluant
   real(kind=long), allocatable :: V_in(:), V_out(:)          !masses de polluant entrante et sortante
                                                              !(y compris par érosion et dépôt au fond quand il s'agit d'un sédiment fin)
   real(kind=long), allocatable :: stock_h2o(:), stock_fnd(:) !stocks de polluant dans l'eau et au fond
                                                              !(quand il s'agit d'un sédiment fin)
   real(kind=long), allocatable :: st0_h2o(:), st0_fnd(:)     !stocks de polluant à l'instant initial dans l'eau et au fond
                                                              !(quand il s'agit d'un sédiment fin)
!   real(kind=long), allocatable :: dsg(:), dsm(:), dsd(:)   ! diamètre du sédiment le plus grossier présent au fond
   real(kind=long),allocatable :: c_eq_ero(:,:,:) !concentration d'équilibre erosion dans chaque lit
                                                  ! dim1 = index section
                                                  ! dim2 = index lit
                                                  ! dim3 = index polluant
   real(kind=long),allocatable :: c_eq_dep(:,:,:) !concentration d'équilibre depot dans chaque lit
                                                  ! dim1 = index section
                                                  ! dim2 = index lit
                                                  ! dim3 = index polluant

   real(kind=long), allocatable :: Hg(:), Hm(:), Hd(:) !épaisseur moyenne de sédiments fins
   real(kind=long), allocatable :: C_total(:)          !concentration totale = somme des concentrations de tous les sédiments

   real(kind=long), allocatable :: dhg(:), dhm(:), dhd(:)  !variations de la cote du fond dans chaque lit depuis
                                                           !la dernière simulation hydraulique
end type solution


type coupling_data
! regroupe les données qui servent à coupler les équations
! ce sont des données qui requièrent les informations de tous les polluants pour être calculées
! elles sont initialisées par la routine couplage()
   real(kind=long) :: time                         !date en secondes à laquelle les données sont évaluées

   real(kind=long),allocatable :: c_total_MES(:)   !somme des concentrations en MES

   real(kind=long), allocatable :: dsg(:), dsm(:), dsd(:)   ! diamètre du sédiment le plus grossier présent au fond

end type coupling_data


type sediment
! données brutes du fichier sediment auquel chaque point des fichiers ST fait référence
   character(len=10) :: tag
! NOTE: pas sûr qu'on doive utiliser le d50 plutôt que le d90 ; le d50 est utilisé par PamHyr-RubarBE
   real(kind=long) :: d50
   real(kind=long) :: sigma
   real(kind=long) :: tau
end type sediment


type data_hydrauliques
! données hydrauliques en un point de calcul déterminées à partir de la ligne d'eau
! évalué par debord_all()
   real (kind=long) :: Qmin, Qmoy, Vmin, Vmoy, Deb, Beta
   real (kind=long) :: lmin, pmin, smin, rmin
   real (kind=long) :: lmoy, pmoy, smoy, rmoy
   real (kind=long) :: tau_min, tau_moy
end type data_hydrauliques

contains
subroutine state_constructor(a_state,nsize)
   ! -- prototype --
   type (state), intent(inout) :: a_state
   integer, intent(in) :: nsize

   ! -- variables locales --
   integer :: err1, err2, err3, err4, err5

   allocate(a_state%x(nsize),stat=err1) ; if (err1 /= 0) write(*,*) 'Allocation impossible pour a_state%x'
   allocate(a_state%y(nsize),stat=err2) ; if (err2 /= 0) write(*,*) 'Allocation impossible pour a_state%y'
   allocate(a_state%a(nsize),stat=err3) ; if (err3 /= 0) write(*,*) 'Allocation impossible pour a_state%a'
   allocate(a_state%q(nsize),stat=err4) ; if (err4 /= 0) write(*,*) 'Allocation impossible pour a_state%q'
   allocate(a_state%u(nsize),stat=err5) ; if (err5 /= 0) write(*,*) 'Allocation impossible pour a_state%u'
   if (err1*err2*err3*err4*err5 /= 0) stop 1701
end subroutine state_constructor

subroutine init_datahyd(dh)
! initialisation à zéro des attributs de dh
   ! -- prototype --
   type(data_hydrauliques) :: dh

   dh%Qmin=zero ;  dh%Qmoy=zero ;  dh%Vmin=zero ;  dh%Vmoy=zero ;  dh%Deb=zero ;  dh%Beta=zero
   dh%lmin=zero ;  dh%pmin=zero ;  dh%smin=zero ;  dh%rmin=zero
   dh%lmoy=zero ;  dh%pmoy=zero ;  dh%smoy=zero ;  dh%rmoy=zero
   dh%tau_min=zero ;  dh%tau_moy=zero

end subroutine init_datahyd

end module adis_types


module fatal_errors
! centralisation des arrêts
! il s'agit de remplacer les instructions STOP afin de pouvoir faire
! des actions avant l'arrêt du programme
! NOTE: la variable OS est définie à la compilation par le makefile dans le fichier include OS_slash.fi
use parametres, only: OS
use adis_types, only: dataset

logical :: non_fixed_geometry = .false.

contains
subroutine fatal_error(modele,code,message)
! remplacement de STOP 'message'
   ! -- prototype --
   type(dataset), intent(in) :: modele
   ! message = message d'erreur optionnel, sera préfixé par ' >>>> Erreur : '
   character(len=*), intent(in), optional :: message
   integer, intent(in) :: code
   ! -- variables locales --
   integer :: ib
   character(len=120) :: command

   if (non_fixed_geometry) then
      !restauration des fichiers de géométrie initiaux
      if (OS(1:5) == 'Linux') then
         do ib = 1, modele%net%ibmax
            ! NOTE: a priori mv -f est plus rapide que cp puisqu'on veut remplacer le fichier cible
            write(command,'(a)') 'mv -f '//trim(modele%biefs(ib)%fichier_geo)//'_BAK'//' '//trim(modele%biefs(ib)%fichier_geo)
            call execute_command_line(command,wait=.true.)
         enddo
      else if (OS(1:3) == 'Win') then
         do ib = 1, modele%net%ibmax
            write(command,'(a)') 'del '//trim(modele%biefs(ib)%fichier_geo)
            call execute_command_line(command,wait=.true.)
            write(command,'(a)') 'ren '//trim(modele%biefs(ib)%fichier_geo)//'_BAK'//' '//trim(modele%biefs(ib)%fichier_geo)
            call execute_command_line(command,wait=.true.)
         enddo
      else
         write(*,*) '>>>> Erreur : OS non supporté'
         stop 1702
      endif
   endif
   if (present(message)) write(*,*) '>>>> Erreur : '//trim(message)
   stop code
end subroutine fatal_error

end module fatal_errors
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
