!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module ConditionsLimites
!===============================================================================
!             Données des conditions aux limites
!
! numéros des STOP : 11nn
!===============================================================================
use iso_fortran_env, only: output_unit, error_unit
use parametres, only : long
use Adis_Types, only: dataset, courbe2D
use utilitaires, only: lire_Date, next_string, next_real, next_int, interpole, do_crash
use topologie, only: index_by_biefName, index_by_nodeName
use Geometrie_Section, only: section_id_from_pk
implicit none

contains

subroutine lireCL (filename, modele, npol)
!==============================================================================
!                      lecture des Conditions aux Limites
!
!entrée : filename : nom du fichier de CL. Ce fichier contient
!                    + des C(t) aux nœuds amont
!                    + des M(t) aux nœuds intérieurs (débits massiques)
!         modele      : jeu de données complet ; type dataset
!         npol     : numéro du polluant (identifiant)
!sortie : modele%cl_pol ; type ConditionLimite
!
!==============================================================================
   ! prototype
   character(len=*), intent(in) :: filename  !nom du fichier de données
   integer, intent(in) :: npol               !numéro du polluant (identifiant)
   type (dataset), intent(inout) :: modele      !jeu de données complet (dataset)

   ! variables locales
   integer :: luu ! unité logique
   integer :: ierr, numero_noeud, next_field, nbss, jj, hh, mm, ss
   character :: ligne*80, noeud_courant*10, date*20
   character :: ext*3
   real(kind=long) :: u2, decalage
   type (courbe2D), target :: c2D
   integer, pointer :: np
   logical :: premiere_CL
   integer :: CL_maxsize, nligne

   ! calculs
   open(newunit=luu, file=filename, status='old', form='formatted',iostat=ierr)
   if (ierr > 0) then
      write(output_unit,'(3a)') ' Ouverture de ', filename,' impossible'
      stop 1101
   else
      write(output_unit,'(2a)') ' Lecture du fichier de CL : ',trim(filename)
   endif
   nbss=0  !'nbss'+1 est le nombre de lignes du fichier de description des CL
   ierr = 0
   premiere_CL = .true.
   CL_maxsize = 0 ; nligne = 0
   decalage = 0._long !initialisation inutile pour faire plaisir au compilateur
   do  !première lecture du fichier pour compter le nombre de lignes pour la CL la plus longue
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr < 0) then !fin de fichier
         rewind(luu)
         CL_maxsize = max(CL_maxsize,nligne)
         exit
      elseif (ligne(1:1) == '*') then
         !commentaire -> on ignore cette ligne
         cycle
      elseif (ligne(1:1) == '$') then
         !nouvelle CL -> on met à jour CL_maxsize et on réinitialise le compteur de lignes
         CL_maxsize = max(CL_maxsize,nligne)
         nligne = 0
      else
         !une valeur de plus pour la CL en cours
         nligne = nligne + 1
      endif
   enddo
   !ici la taille de la CL la plus longue est connue et égale à CL_maxsize
   do
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr < 0) then !fin de fichier
         ext = 'MDT'  ;  if (modele%nodes(numero_noeud)%cl > 0) ext='CDT'
         call new_CL(numero_noeud,c2D,ext,modele,npol)
         write(output_unit,*) '  Apport au noeud ',trim(modele%nodes(numero_noeud)%name),' de type ',ext
         exit
      endif
      nbss = nbss+1
      if (ligne(1:1) == '*' .or. trim(ligne) == '') then
         cycle
      else if (ligne(1:1) == '$') then
         !lecture d'une nouvelle CL (un autre noeud)
         !1 sauver les données du noeud précédent
         !2 vérifier l'existence du noeud et que c'est bien un noeud amont
         !3 initialiser la nouvelle CL (allocation)
         !distinguer le cas de la 1ère CL (rien avant)
         decalage  = 0._long  !réinitialisation
         if (premiere_CL) then
            premiere_CL = .false.
            np => c2D%np  !raccourci pour le nombre de points de la courbe
            c2D%np = 0
            allocate (c2D%yx(CL_maxsize))
         else  !on range la CL précédente
            ext = 'MDT'  ;  if (modele%nodes(numero_noeud)%cl > 0) ext='CDT'
            call new_CL(numero_noeud,c2D,ext,modele,npol)
            c2D%np = 0
            write(output_unit,*) '  Apport au noeud ',trim(modele%nodes(numero_noeud)%name),' de type ',ext
         endif
         next_field = 2 ; noeud_courant = next_string(ligne,' ',next_field)
         ! vérification de l'existence du noeud
         numero_noeud = index_by_nodeName(trim(noeud_courant), modele%nodes, modele%net%nomax)
         if (numero_noeud == 0) then
            write(error_unit,'(5a)') ' >>>> Erreur dans le fichier ',trim(filename),' : le noeud ',trim(noeud_courant),&
                            ' n''existe pas'
            stop 1102
         endif
         if (modele%net%lbz(numero_noeud,2) /= modele%net%lbz(numero_noeud,1)) then
            write(error_unit,'(5a)') ' >>>> Erreur dans le fichier ',trim(filename),' : le noeud ',trim(noeud_courant),&
                            ' est noeud défluent, pas d''apport possible'
            stop 1103
         endif
         ! décalage temporel de la C.L. (en secondes)
         decalage = next_real(ligne,' ',next_field)
      else
         if (premiere_CL) then
            write(error_unit,'(3a)') ' >>>> Erreur dans le fichier ',trim(filename),' : il ne commence pas par une ligne $'
            stop 1104
         endif
         !lecture des données pour la CL courante
         !décoder la ligne jj:hh:mm valeur
         !stocker dans une courbe2D intermédiaire
         next_field = 1  ;  ierr = 1
!          jj = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
!          hh = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
!          mm = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
!          if (ligne(next_field-1:next_field-1) == ':') then
!             ss = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
!          else
!             ss = 0
!          endif
         date = next_string(ligne,' ',next_field) ; ierr = min(ierr,next_field)

         u2 = next_real(ligne,'',next_field) ; ierr = min(ierr,next_field)
         if (ierr <= 0) then
            write(error_unit,'(3a,i4)') ' >>>> Erreur de lecture de ',filename,' a la ligne ',nbss
            write(error_unit,'(a)') ligne
            stop 1105
         else
            ierr = 0
            c2D%np = c2D%np + 1
            c2D%yx(np)%x = real(lire_Date(date),kind=long) + decalage
            c2D%yx(np)%y = u2
         endif
      endif
   enddo
   nullify(np)
   deallocate (c2D%yx)
   close(luu)
end subroutine lireCL



subroutine new_CL(nneu, c2D, typeCL, modele, npol)
!==============================================================================
! création d'une nouvelle CL : alimente cl_pol selon typeCL
!
!entrée : nneu, entier   : numéro du noeud qui porte la CL
!         c2D, courbe2D  : données de la CL (discrètes)
!         typeCL, chaîne : type de CL, CDT ou MDT
!         modele, dataset : jeu de données complet
!         npol, entier : numéro du polluant
!sortie : modele%cl_pol dans modele
!
!==============================================================================
   !prototype
   integer, intent(in) :: nneu              !numéro du noeud
   integer, intent(in) :: npol              !numéro du polluant
   character (len=3), intent(in) :: typeCL  !type de CL, CDT ou MDT
   type (courbe2D), intent(in) :: c2D       !données de la CL
   type (dataset), intent(inout) :: modele     !jeu données complet
   !variables locales
   integer :: i, nbm
   character (len=3) :: type_cl

   !calculs
   !conversion de type_cl en majuscules
   type_cl = typeCL
   do i = 1, 3
      if (ichar(type_cl(i:i)) > 96  .and. ichar(type_cl(i:i)) < 123) &
         type_cl(i:i) = char(ichar(type_cl(i:i))-32)
   enddo

   !vérification de la compatibilité du noeud avec le type de cl
   if (type_cl == 'CDT' .and. modele%nodes(nneu)%cl <= 0) then
      write(error_unit,*) 'Le noeud ',modele%nodes(nneu)%name,&
                 ' devrait être noeud amont pour porter une CL de type ',type_cl
      stop 1106
   else if (type_cl == 'MDT' .and. modele%nodes(nneu)%cl /= 0) then
      write(error_unit,*) 'Le noeud ',modele%nodes(nneu)%name,&
                 ' devrait être un noeud intérieur pour porter une CL de type ',type_cl
      stop 1107
   else if (modele%nodes(nneu)%cl < 0) then
      write(error_unit,*) 'Le noeud ',modele%nodes(nneu)%name,&
                 ' est un noeud aval et ne peut pas porter de CL de type ',type_cl
      stop 1108
   endif

   nbm = nneu
   modele%cl_pol(nbm,npol)%type_cl = type_cl
   deallocate (modele%cl_pol(nbm,npol)%fdt%yx)
   allocate (modele%cl_pol(nbm,npol)%fdt%yx(c2D%np))
   do i = 1, c2D%np
      modele%cl_pol(nbm,npol)%fdt%yx(i) = c2D%yx(i)
   enddo
   modele%cl_pol(nbm,npol)%fdt%np = c2D%np
   modele%cl_pol(nbm,npol)%fdt%ip = 1
end subroutine new_CL



subroutine new_AL(ib, xam, xav, c2D, modele, npol)
!==============================================================================
! création d'un nouvel Apport_Lateral : alimente AL_pol
!
!entrée : ib, xam, xav : bief et limites du tronçon
!         c2D, courbe2D  : données de la CL (discrètes)
!         modele, dataset : jeu de données complet
!         npol, entier : numéro du polluant (sans objet si CL hydro)
!sortie : modele%AL_pol dans modele
!
!==============================================================================
   !prototype
   integer, intent(in) :: ib                !numéro du bief
   real (kind=long), intent(in) :: xam, xav !pk amont et aval du tronçon
   integer, intent(in) :: npol              !numéro du polluant
   type (courbe2D), intent(in) :: c2D       !données de la CL
   type (dataset), intent(inout) :: modele     !jeu données complet
   !variables locales
   integer :: i, nb

   !compteur du nombre de tronçons
   modele%nbal(npol) = modele%nbal(npol) + 1
   nb = modele%nbal(npol)
   !calculs

   modele%AL_pol(nb,npol)%type_AL = 'MDT'
   modele%AL_pol(nb,npol)%ib = ib
   modele%AL_pol(nb,npol)%is1 = section_id_from_pk (modele, ib, xam, 0.001_long)
   modele%AL_pol(nb,npol)%is2 = section_id_from_pk (modele, ib, xav, 0.001_long)
   modele%AL_pol(nb,npol)%xmin = xam
   modele%AL_pol(nb,npol)%xmax = xav
   !vérification de l'ordre des sections limite du tronçon
   if ((modele%biefs(ib)%is2 - modele%biefs(ib)%is1) * (modele%AL_pol(nb,npol)%is2 - modele%AL_pol(nb,npol)%is1) < 0) then
      write(output_unit,'(2a,2f10.3,a)') ' >>>> Erreur : le tronçon d''apport latéral ', trim(modele%biefs(ib)%name), xam, xav, &
                 ' est donné dans l''ordre aval -> amont'
      stop 1109
   endif
   deallocate (modele%AL_pol(nb,npol)%qmt%yx)
   allocate (modele%AL_pol(nb,npol)%qmt%yx(c2D%np))
   do i = 1, c2D%np
      modele%AL_pol(nb,npol)%qmt%yx(i) = c2D%yx(i)
   enddo
   modele%AL_pol(nb,npol)%qmt%np = c2D%np
   modele%AL_pol(nb,npol)%qmt%ip = 1
end subroutine new_AL



subroutine initAllCLzero (modele)
!==============================================================================
!            initialisation de toutes les Conditions aux Limites et de tous
!            les Apports Latéraux à zéro
!
!entrée : le jeu de données (dataset) modele
!         utilise nbpol
!
!sortie : modele%cl_pol et modele%al_pol (implicitement)
!
!==============================================================================
   ! prototype
   type (dataset), intent(inout) :: modele
   !variables locales
   integer :: n, k

   !calculs
   if (modele%nbpol < 1) then
      write(output_unit,*) '>>>> Erreur dans initAllCLzero() : nombre de polluants nul!'
      stop 1110
   endif

   allocate (modele%CL_pol(modele%net%nomax,modele%nbpol)) !C.L.
   allocate (modele%AL_pol(modele%net%ibmax,modele%nbpol)) !A.L.

   do k = 1, modele%nbpol
      ! boucle sur les nœuds pour les C.L.
      do n = 1, modele%net%nomax
         allocate (modele%cl_pol(n,k)%fdt%yx(1))
         if (modele%nodes(n)%cl /= 0) then
            modele%cl_pol(n,k)%type_cl = 'CDT'  !concentration imposée
         else
            modele%cl_pol(n,k)%type_cl = 'MDT'  !débit massique ajouté
         endif
         modele%cl_pol(n,k)%fdt%np = 1
         modele%cl_pol(n,k)%fdt%ip = 1
         modele%cl_pol(n,k)%fdt%yx(1)%x = 0._long
         modele%cl_pol(n,k)%fdt%yx(1)%y = 0._long
      enddo
      ! boucle sur les biefs pour les A.L.
      do n = 1, modele%net%ibmax
         allocate (modele%al_pol(n,k)%qmt%yx(1))
         modele%al_pol(n,k)%type_al = 'MDT'  !débit massique ajouté
         modele%al_pol(n,k)%ib = n
         modele%al_pol(n,k)%is1 = modele%biefs(n)%is1
         modele%al_pol(n,k)%is2 = modele%biefs(n)%is2
         modele%al_pol(n,k)%xmin = modele%sections(modele%biefs(n)%is1)%pk
         modele%al_pol(n,k)%xmax = modele%sections(modele%biefs(n)%is2)%pk
         modele%al_pol(n,k)%qmt%np = 1
         modele%al_pol(n,k)%qmt%ip = 1
         modele%al_pol(n,k)%qmt%yx(1)%x = 0._long
         modele%al_pol(n,k)%qmt%yx(1)%y = 0._long
      enddo
      modele%nbal = 0 !on a tout initialisé à 0 => aucun vrai secteur d'apport.
   enddo
end subroutine initAllCLzero



subroutine remove_CL (modele)
!==============================================================================
!            Désallocation de toutes les Conditions aux Limites
!
!entrée : modele, dataset : jeu de données complet
!         utilise nbpol
!sortie : aucune
!
!==============================================================================
   ! prototype
   type (dataset), intent(inout) :: modele  !jeu de données complet
   ! variables locales
   integer :: ne, kb

   do kb = 1, modele%nbpol
      do ne = 1, modele%net%nomax
         deallocate (modele%cl_pol(ne,kb)%fdt%yx)
      enddo
   enddo
   deallocate (modele%cl_pol)
end subroutine remove_CL



function CL_noeud(modele, neu, npol, q, c, t)
!==============================================================================
!      Calcul de la concentration au noeud imposée par les apports
!      des biefs qui coulent vers ce noeud et l'apport ponctuel
!      d'un affluent non modélisé
!
!entrée :
!        modele, dataset = jeu de données complet
!        neu, entier = numéro du noeud considéré
!        npol, entier = numéro du polluant considéré
!        q, tableau de réels = débits (l'état hydraulique)
!        c, tableau de réels = concentrations actuelles (la solution)
!        t, réel = date
!sortie : valeur de la concentration, réel
!
!==============================================================================
   !prototype
   type (dataset), intent(in) :: modele
   real(kind=long) :: CL_noeud
   integer, intent(in) :: neu, npol
   real(kind=long),intent(in) :: q(*), c(*), t
   !variables
   real(kind=long) :: cam, somq, q1, q2
   integer :: kb, is

   cam = 0._long
   if (modele%nodes(neu)%cl == 0) then  !noeud intérieur : on calculera plus tard
!       ! initialisation de cam avec le débit massique de l'affluent
!       if (modele%CL_pol(neu,npol)%type_cl == 'CDT') then
!          !débit massique = concentration * débit d'eau
!          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu)%name,&
!                     ' : sur un nœud interne il faut un débit massique'
!          stop 1111
!       else if (modele%CL_pol(neu,npol)%type_cl == 'MDT') then
!          !débit massique : donnée
!          !Pas d'apport possible si noeud défluent (vérifié à la lecture du fichier de CL)
!          cam = interpole(modele%CL_pol(neu,npol)%fdt,t)
!       else
!          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu)%name
!          stop 1112
!       endif
!       !contribution des biefs amont converties en débits massiques
!       somq = 0._long
!       do kb = 1, modele%net%ibmax
!          if (modele%biefs(kb)%nav == neu) then
!             is = modele%biefs(kb)%is2
!             if (q(is) > 0._long) then !on ne garde que les débits vers le noeud
!                !débit massique = concentration * débit d'eau
!                cam = cam + q(is)*c(is)
!                somq = somq + q(is)
!             endif
!          else if (modele%biefs(kb)%nam == neu) then
!             is = modele%biefs(kb)%is1
!             if (q(is) < 0._long) then !on ne garde que les débits vers le noeud
!                !débit massique = concentration * débit d'eau
!                cam = cam - q(is)*c(is)
!                somq = somq - q(is)
!             endif
!          endif
!       enddo
!       !calcul de la somme des débits sortant du nœud pour tenir compte du laminage par un casier
! !       somq = 0._long
! !       do kb = 1, modele%net%ibmax
! !          if (modele%biefs(kb)%nam == neu) then
! !             is = modele%biefs(kb)%is1
! !             if (q(is) > 0._long) then !on ne garde que les débits provenant du nœud
! !                somq = somq + q(is)
! !             endif
! !          endif
! !       enddo
!       !concentration résultante = débit massique / débit d'eau
!       if (cam > 0._long .and. somq > 0._long) then
!          cam = cam /somq
!       else
!          cam = 0._long
!       endif
!       !
!       ! TODO trouver une meilleure méthode
!       ! TODO trouver une meilleure méthode
!       !
   else if (modele%nodes(neu)%cl > 0) then
      !C.L. amont : C imposé uniquement si débit entrant, donc > 0
      cam = interpole(modele%CL_pol(neu,npol)%fdt,t)
      is = -1
      do kb = 1, modele%net%ibmax
         !recherche du bief qui part de ce nœud (il y en a un seul puisque c'est une C.L. amont)
         if (modele%biefs(kb)%nam == neu) then
            is = modele%biefs(kb)%is1
            exit
         endif
      enddo
      if (is < 0) then
         write(error_unit,*) '>>>> ERREUR dans CL_noeud() pour le nœud ',modele%nodes(neu)%name
         call do_crash('CL_noeud')
      elseif (.not.q(is) > 0._long) then
         cam = 0._long
      endif
   else if (modele%nodes(neu)%cl < 0) then
      !C.L. aval : C imposé uniquement si débit entrant, donc < 0
      cam = interpole(modele%CL_pol(neu,npol)%fdt,t)
      is = -1
      do kb = 1, modele%net%ibmax
         !recherche du bief qui part de ce nœud (il y en a un seul puisque c'est une C.L. amont)
         if (modele%biefs(kb)%nav == neu) then
            is = modele%biefs(kb)%is2
            exit
         endif
      enddo
      if (is < 0) then
         write(error_unit,*) '>>>> ERREUR dans CL_noeud() pour le nœud ',modele%nodes(neu)%name
         call do_crash('CL_noeud')
      elseif (.not.q(is) < 0._long) then
         cam = 0._long
      endif
   else
      stop '>>>> Erreur dans CL_noeud() : appel pour une C.L. de type inconnu'
   endif
   CL_noeud = cam
end function CL_noeud



subroutine lireAL (filename, modele, npol)
!==============================================================================
!                      lecture des Apports Latéraux
!
!entrée : filename : nom du fichier des A.L. Ce fichier contient
!                    des M(t) sur des tronçons de bief (débits massiques)
!         modele      : jeu de données complet ; type dataset
!         npol     : numéro du polluant (identifiant)
!sortie : modele%AL_pol ; type Apport_Lateral
!
!==============================================================================
   ! prototype
   character(len=*), intent(in) :: filename  !nom du fichier de données
   integer, intent(in) :: npol               !numéro du polluant (identifiant)
   type (dataset), intent(inout) :: modele      !jeu de données complet (dataset)

   ! variables locales
   integer :: luu  ! unité logique
   integer :: ib               !numéro du bief contenant le tronçon
   real(kind=long) :: xam, xav !pk amont et aval du tronçon
   integer :: ierr, next_field, nbss, jj, hh, mm, ss
   character :: ligne*80, bief_courant*10
   real(kind=long) :: u2
   type (courbe2D), target :: c2D
   integer, pointer :: np
   logical :: premier_AL

   ! calculs
   open(newunit=luu, file=trim(filename), status='old', form='formatted',iostat=ierr)
   if (ierr > 0) then
      write(output_unit,'(3a)') ' Ouverture de ', trim(filename),' impossible'
      stop 1113
   else
      write(output_unit,'(2a)') ' Lecture du fichier des A.L. : ',trim(filename)
   endif
   nbss=0  !'nbss'+1 est le nombre de lignes du fichier de description des A.L.
   ierr = 0
   premier_AL = .true.
   modele%nbal(npol) = 0
   do
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr < 0) then !fin de fichier
         call new_AL(ib,xam,xav,c2D,modele,npol)
         exit
      endif
      nbss = nbss+1
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then
         cycle
      else if (ligne(1:1) == '$') then
         !lecture du nouveau AL (un autre tronçon)
         !1 sauver les données du tronçon précédent
         !2 vérifier l'existence du bief
         !3 initialiser le nouveau AL (allocation)
         !distinguer le cas du 1er AL (rien avant)
         if (premier_AL) then
            premier_AL = .false.
            np => c2D%np  !raccourci pour le nombre de points de la courbe
            c2D%np = 0
            allocate (c2D%yx(40000))
         else  !on range le AL précédent
            call new_AL(ib,xam,xav,c2D,modele,npol)
            c2D%np = 0
         endif
         next_field = 2 ; bief_courant = next_string(ligne,' ',next_field)
         ! vérification de l'existence du bief
         ib = index_by_biefName(trim(bief_courant), modele%biefs, modele%net%ibmax)
         if (ib == 0) then
            write(error_unit,'(3a)') ' >>>> Erreur dans le fichier ',trim(filename),' : le bief ',bief_courant,' n''existe pas'
            stop 1114
         else
            xam = next_real(ligne,' ',next_field)
            xav = next_real(ligne,' ',next_field)
         endif
      else
         if (premier_AL) then
            write(error_unit,'(3a)') ' >>>> Erreur dans le fichier ',trim(filename),' : il ne commence pas par une ligne $'
            stop 1115
         endif
         !lecture des données pour le AL courant
         !décoder la ligne jj:hh:mm valeur
         !stocker dans une courbe2D intermédiaire
         next_field = 1  ;  ierr = 1
         jj = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
         hh = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
         mm = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
         if (ligne(next_field-1:next_field-1) == ':') then
            ss = next_int(ligne,':',next_field) ; ierr = min(ierr,next_field)
         else
            ss = 0
         endif
         u2 = next_real(ligne,'',next_field) ; ierr = min(ierr,next_field)
         if (ierr <= 0) then
            write(error_unit,'(3a,i4)') ' >>>> Erreur de lecture de ',filename,' a la ligne ',nbss
            write(error_unit,'(a)') ligne
            stop 1116
         else
            ierr = 0
            c2D%np = c2D%np + 1
            c2D%yx(np)%x = (((jj*24._long+hh)*60._long)+mm)*60._long+ss
            c2D%yx(np)%y = u2
         endif
      endif
   enddo
   nullify(np)
   deallocate (c2D%yx)
   close(luu)
end subroutine lireAL

subroutine CL_bief_lw(c2, c1, modele, ibief, npol, q, a, a1, qm, t, dt)
!==============================================================================
!      Calcul de la concentration au noeud imposée par les apports
!      des biefs qui coulent vers ce noeud et l'apport ponctuel
!      d'un affluent non modélisé
!
!entrée :
!        modele, dataset = jeu de données complet
!        ibief, entier = indice du bief considéré
!        npol, entier = numéro du polluant considéré
!        q, tableau de réels = débits (l'état hydraulique)
!        c1, tableau de réels = concentrations actuelles (la solution)
!        t, réel = date
!sortie : valeurs de la concentration c2_am, c2_av, réel
!
!==============================================================================
   !prototype
   type (dataset), intent(in) :: modele
   real(kind=long), intent(in) :: c1(:), qm(:)
   real(kind=long), intent(inout) :: c2(:)
   integer, intent(in) :: ibief, npol
   real(kind=long),intent(in) :: a(:), a1(:), q(:), t, dt
   !variables
   real(kind=long) :: cam, somq, somq2, mam
   integer :: kb, is, neu_am, neu_av, im1, ip1, n, is1, is2
   real(kind=long) :: c2_am, c2_av
   real(kind=long) :: flux1, flux2, dtdx, dxp, dxm

!    c2_am = c2(modele%biefs(ibief)%is1)
!    c2_av = c2(modele%biefs(ibief)%is2)
   c2_am = 0._long
   c2_av = 0._long
   mam = 0._long
   neu_am = modele%biefs(ibief)%nam
   neu_av = modele%biefs(ibief)%nav
   is1 = modele%biefs(ibief)%is1
   is2 = modele%biefs(ibief)%is2
   if (modele%nodes(neu_am)%cl == 0) then  !noeud intérieur : tous calculs en débit massique
      if (q(is1) > 0._long) then
      else ! if (q(is1) <= 0._long) then
        somq = 0._long
        somq2 = 0._long
        n = 0
        ip1=modele%ip1(is1)
        dxm = 0.0
        dxp = modele%dxp(is1)
        Flux1 = 0.5_long * (q(ip1)+q(is1))*c1(ip1)
        do kb = 1, modele%net%ibmax
          if (kb == ibief) cycle
          if (modele%biefs(kb)%nav == neu_am) then
              is = modele%biefs(kb)%is2
              ip1=modele%im1(is)
              if (q(ip1) < 0._long) then !on ne garde que les débits partant du noeud
                dxm = dxm + modele%dxm(is)
                n = n+1
                somq = somq + q(ip1)
              else
                somq2 = somq2 - q(ip1)
              endif
          else if (modele%biefs(kb)%nam == neu_am) then
              is = modele%biefs(kb)%is1
              ip1=modele%ip1(is)
              if (q(ip1) > 0._long) then !on ne garde que les débits partant du noeud
                dxm = dxm + modele%dxp(is)
                n = n+1
                somq = somq - q(ip1)
              else
                somq2 = somq2 + q(ip1)
              endif
          endif
        enddo
!         ratio = (q(is1)/(somq2 + q(is1)))
        Flux2 = 0.5_long * (q(is1)+somq*(q(is1)/(somq2 + q(is1)))) * c1(is1)
        dxm = dxm / n
        dtdx = 2._long * dt / (dxp+dxm)
        c2_am = ( a(is1)*c1(is1) + dtdx*(Flux2-Flux1) + dt*(qm(is1)) ) / a1(is1)
        ! initialisation de cam avec le débit massique de l'affluent
        if (modele%CL_pol(neu_am,npol)%type_cl == 'CDT') then
          !débit massique = concentration * débit d'eau
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_am)%name,&
                      ' : sur un nœud interne il faut un débit massique'
          stop 1111
        else if (modele%CL_pol(neu_am,npol)%type_cl == 'MDT') then
          !débit massique : donnée
          !Pas d'apport possible si noeud défluent (vérifié à la lecture du fichier de CL)
          c2_am = c2_am + interpole(modele%CL_pol(neu_am,npol)%fdt,t)
        else
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_am)%name
          stop 1112
        endif
        c2(is1) = c2_am
      endif
   endif

   mam = 0._long
   if (modele%nodes(neu_av)%cl == 0) then  !noeud intérieur : tous calculs en débit massique
      if (q(is2) < 0._long) then
      else ! if (q(is2) >= 0._long) then
        somq = 0._long
        somq2 = 0._long
        n = 0
        im1=modele%im1(is2)
        dxm = modele%dxm(is2)
        dxp = 0.0
        Flux2 = 0.5_long * (q(im1)+q(is2))*c1(im1)
        do kb = 1, modele%net%ibmax
          if (kb == ibief) cycle
          if (modele%biefs(kb)%nav == neu_av) then
              is = modele%biefs(kb)%is2
              ip1=modele%im1(is)
              if (q(is) < 0._long) then !on ne garde que les débits partant du noeud
                dxp = dxp + modele%dxm(is)
                n = n+1
                somq = somq - q(ip1)
              else
                somq2 = somq2 + q(ip1)
              endif
          else if (modele%biefs(kb)%nam == neu_av) then
              is = modele%biefs(kb)%is1
              ip1=modele%ip1(is)
              if (q(is) > 0._long) then !on ne garde que les débits partant du noeud
                dxp = dxp + modele%dxp(is)
                n = n+1
                somq = somq + q(ip1)
              else
                somq2 = somq2 - q(ip1)
              endif
          endif
        enddo
        Flux1 = 0.5_long * (q(is2) + somq*(q(is2)/(somq2 + q(is2)))) * c1(is2)
        dxp = dxp / n
        dtdx = 2._long * dt / (dxp+dxm)
        c2_av = ( a(is2)*c1(is2) + dtdx*(Flux2-Flux1) + dt*(qm(is2)) ) / a1(is2)
        ! on rajoute à cam le débit massique de l'affluent
        if (modele%CL_pol(neu_av,npol)%type_cl == 'CDT') then
          !débit massique = concentration * débit d'eau
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_av)%name,&
                      ' : sur un nœud interne il faut un débit massique'
          stop 1111
        else if (modele%CL_pol(neu_av,npol)%type_cl == 'MDT') then
          !débit massique : donnée
          !Pas d'apport possible si noeud défluent (vérifié à la lecture du fichier de CL)
          c2_av = c2_av + interpole(modele%CL_pol(neu_av,npol)%fdt,t)
        else
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_av)%name
          stop 1112
        endif
        c2(is2) = c2_av
      endif
   else if (modele%nodes(neu_av)%cl < 0) then
      if (q(is2) > 0._long) then
         im1=modele%im1(is2)
         dtdx = dt / modele%dxm(is2)
         Flux1 = q(is2)*c1(is2)
         Flux2 = 0.5_long * (q(im1)+q(is2))*c1(im1)
         c2(is2) = (a(is2)*c1(is2) + dtdx*(Flux2-Flux1) + dt*qm(is2)) / a1(is2)
      endif
   endif

end subroutine CL_bief_lw

subroutine CL_bief(c2, modele, ibief, npol, q, t)
!==============================================================================
!      Calcul de la concentration au noeud imposée par les apports
!      des biefs qui coulent vers ce noeud et l'apport ponctuel
!      d'un affluent non modélisé
!
!entrée :
!        modele, dataset = jeu de données complet
!        ibief, entier = indice du bief considéré
!        npol, entier = numéro du polluant considéré
!        q, tableau de réels = débits (l'état hydraulique)
!        c2, tableau de réels = concentrations actuelles (la solution)
!        t, réel = date
!sortie : valeurs de la concentration c2_am, c2_av, réel
!
!==============================================================================
   !prototype
   type (dataset), intent(in) :: modele
   real(kind=long), intent(inout) :: c2(:)
   integer, intent(in) :: ibief, npol
   real(kind=long),intent(in) ::  q(:), t
   !variables
   real(kind=long) :: cam, somq, q1, q2, mam
   integer :: kb, is, neu_am, neu_av, im1, ip1, n, is1, is2
   real(kind=long) :: c2_am, c2_av
   real(kind=long) :: flux1, flux2, dtdx, dxp, dxm

!    c2_am = c2(modele%biefs(ibief)%is1)
!    c2_av = c2(modele%biefs(ibief)%is2)
   c2_am = 0._long
   c2_av = 0._long
   mam = 0._long
   neu_am = modele%biefs(ibief)%nam
   neu_av = modele%biefs(ibief)%nav
   is1 = modele%biefs(ibief)%is1
   is2 = modele%biefs(ibief)%is2
   if (modele%nodes(neu_am)%cl == 0) then  !noeud intérieur : tous calculs en débit massique
      if (q(is1) > 0._long) then
        !contribution des biefs amont converties en débits massiques
        somq = 0._long
        do kb = 1, modele%net%ibmax
          if (kb == ibief) cycle
          if (modele%biefs(kb)%nav == neu_am) then
              is = modele%biefs(kb)%is2
              if (q(is) > 0._long) then !on ne garde que les débits vers le noeud
                !débit massique = concentration * débit d'eau
                mam = mam + q(is)*c2(is)
                somq = somq + q(is)
              endif
          else if (modele%biefs(kb)%nam == neu_am) then
              is = modele%biefs(kb)%is1
              if (q(is) < 0._long) then !on ne garde que les débits vers le noeud
                !débit massique = concentration * débit d'eau
                mam = mam - q(is)*c2(is)
                somq = somq - q(is)
              endif
          endif
        enddo
        !concentration résultante = débit massique / débit d'eau
        if (mam > 0._long .and. somq > 0._long) then
          c2_am = c2_am + mam /somq
        endif
        ! initialisation de cam avec le débit massique de l'affluent
        if (modele%CL_pol(neu_am,npol)%type_cl == 'CDT') then
          !débit massique = concentration * débit d'eau
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_am)%name,&
                      ' : sur un nœud interne il faut un débit massique'
          stop 1111
        else if (modele%CL_pol(neu_am,npol)%type_cl == 'MDT') then
          !débit massique : donnée
          !Pas d'apport possible si noeud défluent (vérifié à la lecture du fichier de CL)
          c2_am = c2_am + interpole(modele%CL_pol(neu_am,npol)%fdt,t)
        else
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_am)%name
          stop 1112
        endif
        c2(is1) = c2_am
      endif
   endif

   mam = 0._long
   if (modele%nodes(neu_av)%cl == 0) then  !noeud intérieur : tous calculs en débit massique
      !contribution des biefs amont converties en débits massiques
      if (q(is2) < 0._long) then
        somq = 0._long
        do kb = 1, modele%net%ibmax
          if (kb == ibief) cycle
          if (modele%biefs(kb)%nav == neu_av) then
              is = modele%biefs(kb)%is2
              if (q(is) > 0._long) then !on ne garde que les débits vers le noeud
                !débit massique = concentration * débit d'eau
                mam = mam + q(is)*c2(is)
                somq = somq + q(is)
              endif
          else if (modele%biefs(kb)%nam == neu_av) then
              is = modele%biefs(kb)%is1
              if (q(is) < 0._long) then !on ne garde que les débits vers le noeud
                !débit massique = concentration * débit d'eau
                mam = mam - q(is)*c2(is)
                somq = somq - q(is)
              else
              endif
          endif
        enddo
        !concentration résultante = débit massique / débit d'eau
        if (mam > 0._long .and. somq > 0._long) then
          c2_av = c2_av + mam /somq
        endif
        ! on rajoute à cam le débit massique de l'affluent
        if (modele%CL_pol(neu_av,npol)%type_cl == 'CDT') then
          !débit massique = concentration * débit d'eau
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_av)%name,&
                      ' : sur un nœud interne il faut un débit massique'
          stop 1111
        else if (modele%CL_pol(neu_av,npol)%type_cl == 'MDT') then
          !débit massique : donnée
          !Pas d'apport possible si noeud défluent (vérifié à la lecture du fichier de CL)
          c2_av = c2_av + interpole(modele%CL_pol(neu_av,npol)%fdt,t)
        else
          write(output_unit,*) '>>>> Erreur de type de CL au noeud ',modele%nodes(neu_av)%name
          stop 1112
        endif
        c2(is2) = c2_av
      endif
   endif

end subroutine CL_bief


end module ConditionsLimites
