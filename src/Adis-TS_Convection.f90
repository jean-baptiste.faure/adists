!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module Convection
!===============================================================================
!                      Opérateur de transport
!      Schémas aux différences finies explicites pour l'étape de convection
!
!--> Contenu
! types dérivés
!   aucun
! données
!   aucune
! routines
!  - dt_convection : estimation du meilleur pas de temps pour l'étape de
!                    convection
!  - limiteurs de flux :
!        - koren
!        - limiteur_pl
!        - limiteur_r
!        - limiteur_roe
!        - limiteur_sympl
!        - minmod
!        - muscl
!        - ospre
!        - superbee
!        - vanalbada
!        - vanleer
!  - schema_TVD    : advection par le schema explicite Lax-Wendroff
!                    avec limiteurs de flux
!  - schema2_TVD   : variante de schema_TVD
!  - schema_Upwind : resolution advection par le schema explicite
!                    decentre amont
!
!===============================================================================
!
!
use parametres, only: long
use Adis_Types, only: dataset
implicit none

procedure(superbee), pointer :: limiteur => null()

contains


subroutine dt_convection (dt,x,u,iam,iav,icr)
!==============================================================================
!      estimation du meilleur pas de temps pour l'étape de convection
!                  sur la base du nombre de Courant
!
! entrées :
!     - X, tableau de réels, abscisses des points de calcul
!     - U, tableau de réels, vitesse moyenne en chaque point de calcul
!     - IAM et IAM, entiers, indices de début et fin des tableaux X et U
! sortie : dt, réel, pas de temps
!
! usage : call dt_convection(dt,x,u,iam,iav)
!         la routine renvoie le minimum de la valeur calculée et de la
!         valeur de dt fournie en entrée
!         Si la valeur de dt fournie en entrée est nulle ou négative, elle
!         est ignorée
!
!==============================================================================
   ! prototype
   real(kind=long), intent(out) :: dt
   real(kind=long), intent(in) :: x(:), u(:)
   integer, intent(in) :: iam, iav
   integer, intent(out) :: iCr

   ! variables locales
   real(kind=long), parameter :: dx00 = 1.1_long
   integer :: i, ip1, im1
   real(kind=long) :: dxm, dxp, cr, cr0

   Cr = 0._long  ;  iCr = 0
   !  boucle sur les sections de calcul pour le calcul de Gamma avec le limiteur de pente
   do i = iam+1, iav-1
      ! sections avant et après en ignorant les dx nuls
      ip1=i+1 ; do while (ip1<iav) ; if (abs(x(i)-x(ip1)) > dx00) exit ; ip1=ip1+1 ; enddo
      im1=i-1 ; do while (im1>iam) ; if (abs(x(i)-x(im1)) > dx00) exit ; im1=im1-1 ; enddo
      ! il faut tenir compte de l'orientation des biefs pour calculer les
      ! dérivées en espace : dx doit être > 0.
      dxm = abs(x(i)-x(im1)) ; dxp = abs(x(ip1)-x(i))
      ! estimation du nombre de Courant
      if (u(i) > 0._long) then
         Cr0 = u(i)/dxp
         if (Cr0 > Cr) then
            Cr =  Cr0  ;  iCr = i
         endif
      else
         Cr0 = -u(i)/dxm
         if (Cr0 > Cr) then
            Cr = Cr0  ;  iCr = i
         endif
      endif
   enddo
   ! contrôle du pas de temps
   if (Cr > 0._long) then
      dt = 0.95_long / Cr
   else
      dt = 1.e+99_long
   endif

end subroutine dt_convection



subroutine schema_TVD(c1,c,x,u,a,a1,iam,iav,cam,cav,dt,limiteur,modele)
!==============================================================================
!       Résolution de l'étape d'advection par le schema explicite
!          Lax-Wendroff avec limiteurs de flux - schemas TVD
! Algorithme adapté à la forme conservative de l'équation d'advection-diffusion
!
! entrées :
!     - DT, réel, pas de temps
!     - limiteur, procedure, nom de la routine limiteur de flux à utiliser
!     - X, tableau de réels, abscisses des points de calcul
!     - IAM et IAM, entiers, indices de début et fin des tableaux X et données
!     - CAM, réel, concentration imposée au point d'entrée amont (Si U(IAM) > 0)
!     - CAV, réel, concentration imposée au point d'entrée aval (si U(IAV) < 0)
!     - C, tableau de réels, concentration en chaque point de calcul au temps T
!                            début du pas de temps.
!     - ligne d'eau (hydraulique)
!        - U, tableau de réels, vitesse moyenne en chaque point de calcul
!        - A, tableau de réels, surface mouillée en chaque point de calcul
!                               au temps T, début du pas de temps
!        - A1, tableau de réels, surface mouillée en chaque point de calcul
!                                au temps T+DT, fin du pas de temps

! sortie : C1, tableau de réels, concentration en chaque point de calcul
!                                au temps T+DT, fin du pas de temps.
!
! usage : call schema_TVD(c1,c,x,u,a,a1,iam,iav,cam,cav,dt,'superbee')
!
!==============================================================================
   ! prototype
   type (dataset), intent(in) :: modele
   real(kind=long), intent(out) :: c1(*)
   real(kind=long), intent(in) :: dt
   real(kind=long), intent(in) :: x(*), a(*), a1(*), u(*), c(*), cam, cav
   integer :: iam, iav
   interface
      function limiteur(theta)
         use parametres, only: long
         real(kind=long), intent(in) :: theta
         real(kind=long) :: limiteur
      end function limiteur
   end interface

   ! variables locales
   integer :: i, ip1, im1
   real(kind=long), parameter :: dx00 = 0.001_long, quasinul = 1.e-12_long
   real(kind=long), parameter :: demi = 0.5_long, un = 1._long
   real(kind=long), allocatable, save :: Gamma(:)
   real(kind=long) :: dxm,dxp,theta,cr,flux1,flux2,dtdx

   ! allocation
   if (.not.allocated(Gamma)) allocate(Gamma(modele%net%ismax))
   ! calculs
   !CL de type "C imposée" si flux entrant
   if(u(iam) > 0._long) c1(iam) = cam
   if(u(iav) < 0._long) c1(iav) = cav

   ! boucle sur les sections de calcul pour le calcul de Gamma avec le limiteur de pente
   do i = iam+1, iav-1
      !sections avant et après en ignorant les dx nuls
      ip1=modele%ip1(i)
      im1=modele%im1(i)
      !il faut tenir compte de l'orientation des biefs pour calculer les dérivées en espace : dx doit être > 0.
      dxm = abs(x(i)-x(im1)) ; dxp = abs(x(ip1)-x(i))

      if (abs(a(ip1)*c(ip1)-a(i)*c(i)) < quasinul) then
         if (abs(a(i)*c(i)-a(im1)*c(im1)) < quasinul) then
            theta = dxp/dxm
         else
            theta = 1.e+99_long
         endif
      else
         theta = (a(i)*c(i)-a(im1)*c(im1))/(a(ip1)*c(ip1)-a(i)*c(i))*dxp/dxm
      endif
      ! choix du limiteur
      Gamma(i) = (a(ip1)*c(ip1)-a(i)*c(i))/dxp * limiteur(theta)
   enddo
   Gamma(iam) = Gamma(iam+1)  ;  Gamma(iav) = Gamma(iav-1)

   !boucle sur les sections de calcul pour le calcul des flux
   do i = iam+1, iav-1
      !sections avant et après en ignorant les dx nuls
      ip1=modele%ip1(i)  ;  im1=modele%im1(i)
      !il faut tenir compte de l'orientation des biefs pour calculer les dérivées en espace : dx doit être > 0.
      dxm = abs(x(i)-x(im1)) ; dxp = abs(x(ip1)-x(i))

      if (u(i) < 0._long) then
         dtdx = dt/dxp  ;  Cr = u(i)*dtdx
         Flux1 = 0._Long
         Flux2 = u(i) * (a(i)*c(i) - dxm*demi*(Cr+un)*Gamma(i))
      else
         dtdx = dt/dxm  ;  Cr = u(i)*dtdx
         Flux1 = u(i) * (a(i)*c(i) - dxp*demi*(Cr-un)*Gamma(i))
         Flux2 = 0._long
      endif
      if (u(ip1) < 0._long) then
         Flux1 = Flux1 + u(ip1) * (a(ip1)*c(ip1) - dxm*demi*(Cr+un)*Gamma(ip1))
      endif
      if (u(im1) > 0._long) then
         Flux2 = Flux2 + u(im1) * (a(im1)*c(im1) - dxp*demi*(Cr-un)*Gamma(im1))
      endif

      c1(i) = max(0._long,(a(i)*c(i)-dtdx*(Flux1-Flux2)) / a1(i))
   enddo
   !CL de type Neumann si flux sortant
   if (u(iam) < 0._long) c1(iam) = c1(iam+1)
   if (u(iav) > 0._long) c1(iav) = c1(iav-1)
end subroutine schema_TVD



subroutine schema2_TVD(c1,c,q,a,a1,iam,iav,cam,cav,dt,limiteur,modele)
!==============================================================================
!       Résolution de l'étape d'advection par le schema explicite
!          Lax-Wendroff avec limiteurs de flux - schemas TVD
! Algorithme adapté à la forme conservative de l'équation d'advection-diffusion
!
!     Variante de schema_TVD qui utilise le débit à la place de la vitesse
!
! entrées :
!     - DT, réel, pas de temps
!     - limiteur, procedure, nom de la routine limiteur de flux à utiliser
!     - X, tableau de réels, abscisses des points de calcul
!     - IAM et IAM, entiers, indices de début et fin des tableaux X et données
!     - CAM, réel, concentration imposée au point d'entrée amont (Si U(IAM) > 0)
!     - CAV, réel, concentration imposée au point d'entrée aval (si U(IAV) < 0)
!     - C, tableau de réels, concentration en chaque point de calcul au temps T
!                            début du pas de temps.
!     - ligne d'eau (hydraulique)
!        - Q, tableau de réels, débit en chaque point de calcul
!        - A, tableau de réels, surface mouillée en chaque point de calcul
!                               au temps T, début du pas de temps
!        - A1, tableau de réels, surface mouillée en chaque point de calcul
!                                au temps T+DT, fin du pas de temps

! sortie : C1, tableau de réels, concentration en chaque point de calcul
!                                au temps T+DT, fin du pas de temps.
!
! usage : call schema2_TVD(c1,c,x,q,a,a1,iam,iav,cam,cav,dt,superbee)
!
!==============================================================================
   ! prototype
   type (dataset), intent(in) :: modele
   real(kind=long), intent(out) :: c1(:)
   real(kind=long), intent(in) :: a(:), a1(:), q(:), c(:)
   real(kind=long), intent(in) :: cam, cav, dt
   integer, intent(in) :: iam, iav
   interface
      function limiteur(sigma)
         use parametres, only: long
         real(kind=long), intent(in) :: sigma
         real(kind=long) :: limiteur
      end function limiteur
   end interface

   ! variables locales
   integer :: i, ip1, im1
   real(kind=long), parameter :: dx00 = 0.001_long, quasinul = 1.e-12_long
   real(kind=long), parameter :: demi = 0.5_long, un = 1._long
   real(kind=long), allocatable :: Gamma(:)
   real(kind=long) :: sigma,flux1,flux2,dtdx, cr
   real(kind=long) :: QCim, QCpi

   ! allocation
   if (.not.allocated(Gamma)) allocate(Gamma(modele%net%ismax))
   ! calculs
   !CL de type "C imposée" si flux entrant
   if(q(iam) > 0._long) c1(iam) = cam
   if(q(iav) < 0._long) c1(iav) = cav

   ! boucle sur les sections de calcul pour le calcul de Gamma avec le limiteur de pente

   do i = iam+1, iav-1
      !sections avant et après en ignorant les dx nuls
      ip1=modele%ip1(i)  ;  im1=modele%im1(i)
      !il faut tenir compte de l'orientation des biefs pour calculer les dérivées en espace : dx doit être > 0.
      QCim = q(i)*c(i)-q(im1)*c(im1)  ;  QCpi = q(ip1)*c(ip1)-q(i)*c(i)


      if (abs(QCpi) > quasinul) then
         sigma = QCim*modele%dxp(i)/(QCpi*modele%dxm(i))
      else
         if (abs(QCim) > quasinul) then
            sigma = 1.e+99_long
         else
            sigma = modele%dxp(i)/modele%dxm(i)
         endif
      endif
      Gamma(i) = QCpi * limiteur(sigma) / modele%dxp(i)
   enddo

   Gamma(iam) = Gamma(iam+1)  ;  Gamma(iav) = Gamma(iav-1)

   !boucle sur les sections de calcul pour le calcul des flux
   do i = iam+1, iav-1
      !sections avant et après en ignorant les dx nuls
      ip1=modele%ip1(i)  ;  im1=modele%im1(i)
      !il faut tenir compte de l'orientation des biefs pour calculer les dérivées en espace : dx doit être > 0.

      if (q(i) > 0._long) then
         Cr = q(i)*dt/(a(i)*modele%dxm(i))  ;  dtdx = dt/modele%dxm(i)
         Flux1 = q(i)*c(i) - modele%dxp(i)*demi*(Cr-un)*Gamma(i)
         Flux2 = 0._long
      else
         Cr = q(i)*dt/(a(i)*modele%dxp(i))  ;  dtdx = dt/modele%dxp(i)
         Flux1 = 0._Long
         Flux2 = q(i)*c(i) - modele%dxm(i)*demi*(Cr+un)*Gamma(i)
      endif
      if (q(im1) > 0._long) Flux2 = Flux2 + (q(im1)*c(im1) - modele%dxp(i)*demi*(Cr-un)*Gamma(im1))
      if (q(ip1) < 0._long) Flux1 = Flux1 + (q(ip1)*c(ip1) - modele%dxm(i)*demi*(Cr+un)*Gamma(ip1))

      c1(i) = ( a(i)*c(i)-dtdx*(Flux1-Flux2) ) / a1(i)
      if (.not.(c1(i) > 0._long)) c1(i) = 0._long
   enddo

   !CL de type Neumann si flux sortant
   if (q(iam) < 0._long .and. q(iam+1) < 0._long) then
      i = iam
      ip1=modele%ip1(i)
      dtdx = dt / modele%dxp(i)
      Flux1 = 0.5_long * (q(ip1)+q(i))*c(ip1)  ;  Flux2 = q(i)*c(i)
      c1(i) = ( a(i)*c(i) + dtdx*(Flux2-Flux1) ) / a1(i)
   else if (q(iam) <= 0._long) then
      c1(iam) = c1(iam+1)
   endif
   if (q(iav) > 0._long .and. q(iav-1) > 0._long) then
      i = iav
      im1=modele%im1(i)
      dtdx = dt / modele%dxm(i)
      Flux1 = q(i)*c(i)    ;  Flux2 = 0.5_long * (q(im1)+q(i))*c(im1)
      c1(i) = ( a(i)*c(i) + dtdx*(Flux2-Flux1) ) / a1(i)
   else if (q(iav) >= 0._long) then
      c1(iav) = c1(iav-1)
   endif
   deallocate(Gamma)
end subroutine schema2_TVD



subroutine schema_Upwind(c1,c,q,a,a1,iam,iav,cam,cav,dt,modele)
!==============================================================================
!  Résolution de l'étape d'advection par le schema explicite decentre amont
! Algorithme adapté à la forme conservative de l'équation d'advection-diffusion

! Très bonne conservation de la masse mais diffusion numérique trop grande
! À utiliser pour contrôler un schéma TVD
!
!
! entrées :
!     - DT, réel, pas de temps
!     - X, tableau de réels, abscisses des points de calcul
!     - IAM et IAM, entiers, indices de début et fin des tableaux X et données
!     - CAM, réel, concentration imposée au point d'entrée amont (Si U(IAM) > 0)
!     - CAV, réel, concentration imposée au point d'entrée aval (si U(IAV) < 0)
!     - C, tableau de réels, concentration en chaque point de calcul au temps T
!                            début du pas de temps.
!     - ligne d'eau (hydraulique)
!        - Q, tableau de réels, débit en chaque point de calcul
!        - A, tableau de réels, surface mouillée en chaque point de calcul
!                               au temps T, début du pas de temps
!        - A1, tableau de réels, surface mouillée en chaque point de calcul
!                                au temps T+DT, fin du pas de temps

! sortie : C1, tableau de réels, concentration en chaque point de calcul
!                                au temps T+DT, fin du pas de temps.
!
! usage : call schema_Upwind(c1,c,x,q,a,a1,iam,iav,cam,cav,dt)
!
!==============================================================================
   ! prototype
   type (dataset), intent(in) :: modele
   real(kind=long), intent(out) :: c1(*)
   real(kind=long), intent(in) :: dt
   real(kind=long), intent(in) :: a(*), a1(*), q(*), c(*), cam, cav
   integer :: iam, iav, i1, i2

   ! variables locales
   integer :: i, ip1, im1
   real(kind=long), parameter :: dx00 = 0.001_long
   real(kind=long) :: flux1, flux2, dtdx

   ! calculs
   !CL de type "C imposée" si flux entrant
   if(q(iav) < 0._long) then
      c1(iav) = cav
      i2 = iav-1
   else
      i2 = iav
   endif
   if(q(iam) >= 0._long) then
      c1(iam) = cam
      i1 = iam+1
   else
      i1 = iam
   endif

   !boucle sur les sections de calcul pour le calcul des flux
   do i = i1, i2

      !il faut tenir compte de l'orientation des biefs pour calculer les dérivées en espace : dx doit être > 0.
      if(q(i) < 0._long) then
         !sections avant et après en ignorant les dx nuls
         ip1=modele%ip1(i)
         if (modele%sections(i+1)%iss == 0) then
            dtdx = dt / modele%dxp(i)
            Flux1 = q(ip1)*c(ip1)  ;  Flux2 = q(i)*c(i)
            c1(i)=( a(i)*c(i) - dtdx*(Flux1-Flux2)) / a1(i)
         else
            c1(i) = c1(i+1) ! TODO a mettre a la fin: c1(i+1) pas encore calculée
         endif
      else
         !sections avant et après en ignorant les dx nuls
         im1=modele%im1(i)
         if (modele%sections(i)%iss == 0) then
            dtdx = dt / modele%dxm(i)
            Flux1 = q(i)*c(i)  ;  Flux2 = q(im1)*c(im1)
            c1(i)=( a(i)*c(i) - dtdx*(Flux1-Flux2)) / a1(i)
         else
            c1(i) = c1(i-1)
         endif
      endif

   enddo
   !CL de type Neumann si flux sortant
!    if (q(iam) < 0._long) c1(iam) = c1(iam+1)
!    if (q(iav) > 0._long) c1(iav) = c1(iav-1)
end subroutine schema_Upwind



!==============================================================================
!                          Limiteurs de flux
!            repris tels quels de la version de Sébastien Martin (2002)
!==============================================================================

!##################### limiteurs de ROE et SWEBY :  ############################
function limiteur_roe (u,r)
!..... 1 paramètre (1 <= r <= 2), avec en particulier :
!.....            - r=1. : limiteur Minmod (hyperbee)
!.....            - r=2. : limiteur Superbee
   real(kind=long), intent(in):: u, r
   real(kind=long) :: limiteur_roe

   limiteur_roe = max(0._long,min(1._long,r*u),min(r,u))
end function


!######################### limiteur SuperBee : #################################
function superbee(u)
!....limiteur de Roe avec r=2
   real(kind=long), intent(in) :: u
   real(kind=long) :: superbee
   real(kind=long), parameter :: r=2._long

   superbee = max(0._long,min(1._long,r*u),min(r,u))
end function superbee


!######################### limiteur MinMod : ###################################
function minmod(u)
!....limiteur de Roe avec r=1
   real(kind=long), intent(in):: u
   real(kind=long) :: minmod
   real(kind=long), parameter :: r=1._long

   minmod = max(0._long,min(1._long,r*u),min(r,u))
end function minmod


!####################### limiteur de VAN LEER : ################################
function vanleer(u)
!..... 0 paramètre
   real(kind=long), intent(in):: u
   real(kind=long) :: vanleer

   vanleer = (abs(u)+u)/(1._long+abs(u))
end function vanleer


!####################### limiteur de VAN ALBADA : ###############################
function vanalbada(u)
!..... 0 paramètre
   real(kind=long), intent(in):: u
   real(kind=long) :: vanalbada

   vanalbada = max(0._long , u*(u+1._long)/(u*u+1._long))
end function vanalbada


!########################### limiteur OSPRE : ###################################
function ospre(u)
!..... 0 paramètre
   real(kind=long), intent(in):: u
   real(kind=long) :: ospre

   ospre = 1.5_long * u * (u+1._long)/(u*u+u+1._long)
end function ospre


!############################## limiteur R : ####################################
function limiteur_r(u,k)
!..... 1 paramètre : -1 <= k <= 1
!..... en particulier :
!.....      - k=0.  : limiteur de Van Leer
!.....      - k=0.5 : limiteur ISNAS
   real(kind=long), intent(in):: u,k
   real(kind=long) :: limiteur_r, tmp

   if (k >= 0._long) then
      tmp = 1._long + u
      limiteur_r = (u+abs(u))*((1._long+k)*u+1._long-k)/(tmp*tmp)
   else
      if (u >= 1._long) then
         limiteur_r = ((2._long+k)*u-k)/(1._long+u);
      else
         limiteur_r = (u+abs(u))*(-u*u+(3._long+k)*u-k)/(1._long+u*u)
      endif
   endif
end function limiteur_r


function isnas(u)
!.....      limiteur R avec k = 1/2
   real(kind=long), intent(in) :: u
   real(kind=long) :: isnas
   real(kind=long), parameter :: k = 0.5_long

   isnas = limiteur_R(u,k)
end function isnas

!############################## limiteur PL : ###################################
function limiteur_pl(u,k,m)
!..... 2 paramètres : -1 <= k <= 1 et 1 <= M <= 4
!..... en particulier :
!.....      - k=0.   et M=0. : schéma Upwind (puisque le limiteur est nul partout)
!.....      - k=1./2 et M=4. : limiteur SMART (non TVD)
!.....      - k=1./3 et M=2. : limiteur de Koren (TVD)
   real(kind=long), intent(in) :: u, k, m
   real(kind=long) :: limiteur_pl
   real(kind=long), parameter :: deux=2._long, un=1._long, zero=0._long, demi=0.5_long

   limiteur_pl = max(zero, min(deux*u, un*m, demi*(u*(un+k)+un-k)))
end function limiteur_pl


function koren(u)
!.....      limiteur PL avec k = 1/3 et m = 2
   real(kind=long), intent(in) :: u
   real(kind=long) :: koren
   real(kind=long), parameter :: k = 1._long/3._long, m = 2._long
   koren = limiteur_pl(u,k,m)
end function koren


!######################## limiteur Symmetric PL : ###############################
function limiteur_sympl(u,k)
!..... 1 paramètre : -1 <= k <= 1
!..... en particulier :
!.....      - k=0.   : limiteur MUSCL
!.....      - k=1./2 : limiteur UMIST
   real(kind=long), intent(in):: u, k
   real(kind=long) :: limiteur_sympl, temp1, temp2
   real(kind=long), parameter :: deux=2._long, un=1._long, zero5=0.5_long

   temp1 = min(zero5*(un+k)*u+zero5*(un-k),deux)
   temp2 = min(deux*u,zero5*(un-k)*u+zero5*(un+k))
   limiteur_sympl = max(0._long,temp1, temp2)
end function limiteur_sympl


function muscl(u)
   real(kind=long), intent(in):: u
   real(kind=long) :: muscl, temp
   real(kind=long), parameter :: deux=2._long, un=1._long, zero5=0.5_long

   ! NOTE: muscl = limiteur_sympl(u,0._long)
   temp = min(deux,zero5*(u+un),deux*u)
   muscl = max(0._long, temp)
end function muscl


function umist(u)
   real(kind=long), intent(in):: u
   real(kind=long) :: umist
   real(kind=long), parameter :: k =  0.5_long

   umist = limiteur_sympl(u,0.5_long)
end function umist


!######################## limiteur K : ###############################
function limiteur_K(u,k)
!..... 1 paramètre : -1 <= k <= 1
!..... en particulier :
!.....      - k=1   : CDS (Central-differencing scheme), i.e. Lax-Wendroff
!.....      - k=-1  : QUICK (quadratic upwind scheme), i.e. Warming and Beam

   real(kind=long), intent(in):: u, k
   real(kind=long) :: limiteur_K

   limiteur_K = ((1._long + k)*u + 1._long - k) * 0.5_long
end function limiteur_K


function cds(u)
   real(kind=long), intent(in):: u
   real(kind=long) :: cds
   real(kind=long), parameter :: k =  1._long

   cds = limiteur_K(u,k)
end function cds


function quick(u)
   real(kind=long), intent(in):: u
   real(kind=long) :: quick
   real(kind=long), parameter :: k =  -1._long

   quick = limiteur_K(u,k)
end function quick

end module Convection
