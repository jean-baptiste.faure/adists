#ToDo liste pour AdisTS -- mise à jour le 2021/04/27

__Note :__ _ce fichier est rédigé en utilisant la syntaxe_ [_markdown_](https://fr.wikipedia.org/wiki/Markdown)

## Général

* Migrer vers la forge gitlab d'Irstea
* Régler la question de la licence


## Documentation (Adis-TS_Documentation_utilisateur.odt)

Compléter la doc, en particulier sur les points suivants :

* pour le charriage, renvoyer vers Mage-8
* compléter les explications concernant le couplage fort avec Mage-8
* certaines options CLI ne sont pas décrites dans la doc


## Développement

### Prise en compte des ouvrages
* Le modèle de Lucie Guertault pour Génissiat est implémenté, mais il ne crée pas l'effet de chasse attendu
* Test à faire : placer les ouvrages de Génissiat (en particulier vanne de fond et vanne de demi fond) à leurs places réelles
* Généraliser l'extraction des concentrations locales des ouvrages implémentée pour Génissiat (extract_Genissiat()) à tous les ouvrages.

### Couplage MES + charriage
* Il s'agit de faire tourner ensemble AdisTS avec MES et Mage-8 avec charriage.


## Validation

* Créer un jeu de cas-tests automatiques sur le modèle de Mage.
