!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
program Adis_TS
!==============================================================================
!             Adis-TS : Programme Principal
!  -> Gère les options de la ligne de commande (cf. option -h pour les détails)
!  -> Gère le nombre de processeurs à utiliser
!  -> Appelle :
!     + le sous-programme de calcul
!     + les routines d'extraction de résultats des fichiers BIN (binaires)
!
! numérotation des STOP : rien si fin normale, sinon 10nn
!==============================================================================
   use iso_fortran_env, only: output_unit, error_unit
   use parametres, only: fnl, long, cache_size_default, OS, SIGINT, slash, zero, seuil_miseAjour_geometrie, g
   use Adis_Types, only: dataset, profil, point_net, state, cache, solution, coupling_data, data_hydrauliques, sediment, &
                         pointTS, state_constructor
   use Geometrie_Section, only: section_id_from_pk, largeursCotes, perimetre, section
   use Topologie, only: topo_basedir, initTopoGeometrie
   use ConditionsLimites, only: CL_noeud, initAllCLzero, CL_bief_lw, cl_bief
   use Hydraulique, only: getLigneDeauMAGE, debord_all, initLigneDeauMAGE
   use Convection  !, only: superbee, schema2_TVD, schema_Upwind
   use Diffusion, only: strickler_peau, termes_sources, coeff_Dispersion, diff_fc, &
                        gauss3, lire_Dif, update_stocks_MES, gauss4b, with_diffusion
   use DataNum, only: alarm_ON, nb_threads, no_coupling, too_big, dtMage, t0, dt0, t_screen, c_initiale, dt_bottom, dtbin, dtcsv, &
                      dtscr, theta, tmax, current_time
   use Geochimie, only: contrainte_locale, lirePolluant, lire_sediments, lire_D90, MES_correlation, init_Solution
   use utilitaires, only: lire_Date, interpole, next_string, next_int, next_real, get_filename, heure, remplacer, mise_en_veille, &
                           c_heure, do_crash
   use couplage_MAGE, only: mage_WorkingDir, repMage, new_hydraulic_simulation, runMage, export_geometrie, export_geometry_history,&
                            restore_repMage, modify_repMage, export_Mage_NUM
   use resultats, only: liste, entete_csv, numID, outdir, nb_out, extraire, extraire_BarChart, extraire_FDT, extraire_FDX, &
                        write_screen, write_csv, write_bin
   use fatal_errors, only: non_fixed_geometry
   use ouvrages, only: all_OuvCmp, all_OuvEle, update_positions_ouvrages, debit

#ifdef openmp
   use omp_lib
#endif /* openmp */

   implicit none

   !variables pour l'identification de la version du programme
   character (len=30) :: version = 'Version 1.0.0 du 2024-05-21'
   include 'buildID.fi'
   !character (len=80) :: lastmodif = 'Correction bug initialisation'
   !character (len=80) :: lastmodif = 'Suppression de l''option PVM'
   !character (len=80) :: lastmodif = 'Amélioration des options d''extraction de résultats'
   !character (len=80) :: lastmodif = 'Ajout d''un décalage temporel des C.L. (en secondes)'
   !character (len=80) :: lastmodif = 'Facteur correctif du Tau critique pour dépôts consolidés'
   !character (len=80) :: lastmodif = 'Option -nc pour inhiber le couplage'
   !character (len=80) :: lastmodif = 'Traitement des apports lateraux ponctuels comme les apports aux noeuds'
   !character (len=80) :: lastmodif = 'Autant de points de sortie qu''on veut dans NUM (ne pas abuser !!!)'
   !character (len=80) :: lastmodif = 'Enregistreur de commandes'
   !character (len=80) :: lastmodif = 'Fonction d''extraction d''histogramme'
   !character (len=80) :: lastmodif = 'Codes de sortie mis à jour'
   !character (len=80) :: lastmodif = 'Lecture des couches sédimentaires'
   !character (len=80) :: lastmodif = 'Option pour ne faire que le calcul de la contrainte au fond'
   !character (len=80) :: lastmodif = 'Modification des options de la ligne de commande'
   !character (len=80) :: lastmodif = 'Couplage fort avec Mage-8'
   !character (len=80) :: lastmodif = 'portage sur MS-Windows'
   !character (len=80) :: lastmodif = 'allocation dynamique de mémoire complète'
   !character (len=80) :: lastmodif = 'évolution de la géométrie ; couplage fort avec MAGE'
   !character (len=80) :: lastmodif = 'possibilité de choisir le schéma numérique - ajout du schéma explicite'
   character (len=80) :: lastmodif = 'prise en compte de l''influence des ouvrages'


   !variables pour la CLI (Command Line Interface)
   character(len=64) :: buffer  ;  integer :: iarg

   !autres variables
   type(dataset)   :: modele
   character (len=fnl) :: repFile
   character(len=fnl) :: binFile, p1, p2, p3, tmp, csvFile
   real(kind=long),allocatable :: m_total_bief(:,:)
   logical :: bexist
   integer :: ib, nbpar, n, k, i, lu, l666
   character(len=15), allocatable :: pp(:)
   type(point_net), allocatable :: pts(:)
   character(len=120) :: cmdline
   character(len=13) :: fmt
   character(len=20) :: date_contraintes

   logical :: with_bed_change = .false.
   logical :: force_Mage = .false.

   character (len=:),allocatable :: adis_WorkingDir
   character(len=1) :: wine

   integer :: cache_size = cache_size_default

   real(kind=long) :: too_big_input

   character(len=10) :: limiteur_name
   logical :: use_TVD, use_Splitting, ignore_Ouvrages

   !Définition du séparateur de fichier en fonction de l'OS
   if (OS(1:5) == 'Linux') then
      slash = '/'
   elseif (OS(1:3) == 'Win') then
      slash = '\'
   else
      write(output_unit,*) 'OS inconnu' ; stop 1001
   endif
   !Si l'OS est MS-Windows et si la variable d'environnement adis_ts_wine est définie et vaut 1
   !alors on change slash pour '/' de façon à pouvoir utiliser des données Linux.
   call get_environment_variable('ADIS_TS_WINE',wine)
   if (OS(1:3) == 'Win' .and. wine == '1') slash = '/'

   outdir = '.'//slash

   !gestion des erreurs : initialisation fatal_errors
   non_fixed_geometry = .false.
   alarm_ON = .false.
   too_big = 1.e+10_long

   !ouverture du fichier log pour enregistrer les commandes
   open(newunit=l666,file='adis-ts.log',form='formatted',status='unknown',position='append')
   call get_command(cmdline)
   write(output_unit,'(2a)') 'Ligne de commande utilisée : ',trim(cmdline)


   iarg = 1 ; call get_command_argument(iarg,buffer)
   if (len_trim(buffer) == 0) then  !il n'y a pas d'arguments sur la ligne de commande
      write(output_unit,*) 'Programme Adis-TS : ', version
      write(output_unit,*) 'Dernière nouveauté : ',trim(lastmodif)
      write(output_unit,*) 'Dernier correctif : ',trim(commitID)
      write(output_unit,*)  trim(dateCommit)
      write(output_unit,*) 'Date de compilation : ',trim(buildID)
      write(output_unit,*) 'Saisir adis-ts -h pour afficher les options disponibles'
      stop 1002

   else if (buffer(1:2) == '--') then
      write(output_unit,*) '>>>> Erreur de syntaxe : le préfixe devant les options est -'
      write(output_unit,*) 'Saisir adis-ts -h pour afficher les options disponibles'
      stop 1003

   else if (buffer(1:2) == '-v' .or. buffer(1:2) == '-V') then
      write(l666,'(2a)') '* ',trim(cmdline)
      write(output_unit,*) 'Programme Adis-TS : ', version
      write(output_unit,*) 'Dernière nouveauté : ',trim(lastmodif)
      write(output_unit,*) 'Dernier correctif : ',trim(commitID)
      write(output_unit,*)  trim(dateCommit)
      write(output_unit,*) 'Date de compilation : ',trim(buildID)
      stop

   else if (buffer(1:2) == '-x' .or. buffer(1:2)=='-X') then
      write(l666,'(a)') trim(cmdline)
      write(output_unit,*) '# Programme Adis-TS : extraction de résultats'
      call get_command_argument(2,binFile) ;  call get_command_argument(3,p1)
      call get_command_argument(4,p2) ;  call get_command_argument(5,p3)
      call extraire(binFile, p1, p2, p3,'')
      stop

   else if (buffer(1:2) == '-h' .or. buffer(1:2) == '-H') then
      write(l666,'(2a)') '* ',trim(cmdline)
      write(output_unit,*) '# Programme Adis-TS : aide sur les commandes disponibles'
      write(output_unit,*) 'Programme Adis-TS : ', version
      include 'helpfile_fr.fi'
      stop

   else if (buffer(1:2) == '-p' .or. buffer(1:2)=='-P') then
      write(l666,'(a)') trim(cmdline)
      write(output_unit,*) '# Programme Adis-TS : dessin de résultats'
      call get_command_argument(2,binFile) ;  call get_command_argument(3,p1)
      call get_command_argument(4,p2) ;  call get_command_argument(5,p3)
      tmp = trim(binFile)//'_'//trim(p1)//'_'//trim(p2)//'_'//trim(p3)
      call remplacer(tmp,':','-')
      call extraire(binFile, p1, p2, p3, tmp)
      open (newunit=lu,file='cmd',form='formatted',status='unknown')
      write(lu,*) 'plot "',trim(tmp),'" with lines'
      flush(unit=lu)
      call execute_command_line('gnuplot -persist cmd')
      close (lu,status='delete')
      stop

   else if (buffer(1:2) == '-b' .or. buffer(1:2)=='-B') then
      ! TODO: vérifier que l'option -b fonctionne correctement sous MS-Windows
      write(l666,'(a)') trim(cmdline)
      write(output_unit,*) '# Programme Adis-TS : export csv de résultats extraits du fichier BIN'
      call get_command_argument(2,binFile)
      call get_command_argument(3,p1)
      if (p1(3:3) == 'x' .or. p1(3:3) == 'X') then
         call get_command_argument(4,p2)
         read(p2,'(i3)') ib
         ! p3 est une liste de temps
         call get_command_argument(5,p3)
         ! nombre d'éléments dans p3, pour allouer pp ou pts
         nbpar = 1  ;  do n = 1, len_trim(p3)
            if (p3(n:n) == '_') nbpar = nbpar+1
         enddo
         allocate(pp(nbpar))
         k = 1  ;  nbpar = 1
         do n = 1, len_trim(p3)
            if (p3(n:n) == '_') then
               pp(nbpar) = p3(k:n-1)
               k = n+1
               nbpar = nbpar+1
            endif
         enddo
         pp(nbpar) = trim(p3(k:)) !dernier élément ou le 1er s'il n'y en a qu'un
         if (command_argument_count() > 5) then
            call get_command_argument(6,p3)
            call extraire_FDX(binFile, p1(1:1), ib, pp, nbpar, trim(p3))
         else
            call extraire_FDX(binFile, p1(1:1), ib, pp, nbpar)
         endif
         stop
      else if (p1(3:3) == 't' .or. p1(3:3) == 'T') then
         call get_command_argument(4,p2)
         ! p2 est une liste de couples ib pk
         ! nombre d'éléments dans p2, pour allouer pts
         nbpar = 1  ;  do n = 1, len_trim(p2)
            if (p2(n:n) == '_') nbpar = nbpar+1
         enddo
         allocate(pts(nbpar))
         k = 1  ;  nbpar = 1
         do n = 1, len_trim(p2)
            if (p2(n:n) == '_') then
               i = scan(p2(k:n-1),'#')
               write(fmt,'(a,i0,a)') '(i',i-1,',1x,f10.0)'
               read(p2(k:n-1),fmt) pts(nbpar)%ib,pts(nbpar)%pk
               k = n+1
               nbpar = nbpar+1
            endif
         enddo
         n = len_trim(p2)
         i = scan(p2(k:n),'#')
         write(fmt,'(a,i0,a)') '(i',i-1,',1x,f10.0)'
         read(p2(k:n),fmt) pts(nbpar)%ib,pts(nbpar)%pk
         if (command_argument_count() > 4) then
            call get_command_argument(5,p3)
            call extraire_FDT(binFile,p1(1:1),pts,nbpar, trim(p3))
         else
            call extraire_FDT(binFile,p1(1:1),pts,nbpar)
         endif
         stop
      else
         write(output_unit,*)  ' >>>> Erreur : type de courbe inconnu'
         stop 1004
      endif

   else if (buffer(1:3) == '-c ' .or. buffer(1:3)=='-C ') then
      ! TODO: vérifier que l'option -c fonctionne correctement sous MS-Windows
      write(l666,'(a)') trim(cmdline)
      write(output_unit,*) '# Programme Adis-TS : extraction de lignes d''un fichier CSV'
      call get_command_argument(2,csvFile)
      call get_command_argument(3,p3)
      ! p3 est une liste de temps
      nbpar = 1  ;  do n = 1, len_trim(p3)
         if (p3(n:n) == '_') nbpar = nbpar+1
      enddo
      allocate(pp(nbpar))
      k = 1  ;  nbpar = 1
      do n = 1, len_trim(p3)
         if (p3(n:n) == '_') then
            pp(nbpar) = p3(k:n-1)
            k = n+1
            nbpar = nbpar+1
         endif
      enddo
      pp(nbpar) = trim(p3(k:)) !dernier élément ou le 1er s'il n'y en a qu'un
      if (command_argument_count() > 3) then
         call get_command_argument(4,p3)
         call extraire_BarChart(csvFile, pp, nbpar, trim(p3))
      else
         call extraire_BarChart(csvFile, pp, nbpar)
      endif
      stop

   else
      !Lancement d'une simulation
      !enregistrement de la ligne de commande dans le fichier log
      write(l666,'(2a)') '# ',trim(cmdline) !on distingue dans le fichier de log les commandes de lancement d'une simulation

      !Options par défaut
#ifdef sequentiel
      nb_threads = 1
#endif /* sequentiel */
#ifdef openmp
      !$omp parallel
      nb_threads = omp_get_num_threads() / 2
      !$omp end parallel
#endif /* openmp */
      no_coupling = .false.
      date_contraintes = ''
      outdir = '.'//slash//'resultats'//slash
      limiteur => minmod
      use_TVD = .false. !schéma Upwind par défaut
      use_Splitting = .true. !splitting convection / diffusion par défaut
      ignore_Ouvrages = .false.

      !Décodage de la ligne de commande
      do
         if (len_trim(buffer) > 0) then !il y a des options sur la ligne de commande
            if (buffer(1:13) == '-no_diffusion') then
               with_diffusion = .false.
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (buffer(1:3) == '-nc') then
               !Option pour inhiber le couplage entre les classes granulométriques pour évaluer les dépôts-érosions
               !option incompatible avec -s
               if (date_contraintes == '') then
                  no_coupling = .true.
               else
                  no_coupling = .false.
               endif
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (buffer(1:3) == '-s=') then
               !Option pour le calcul de la contrainte au fond pour une date donnée
               !option incompatible avec -nc
               no_coupling = .false.
               date_contraintes = trim(buffer(4:))
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (buffer(1:5) == '-cpu=') then
               !Option pour définir le nombre de CPU à utiliser en parallèle
               read(buffer(6:),'(i2)') nb_threads
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (buffer(1:5) == '-out=') then
               !Option pour définir le dossier des fichiers de résultats
               outdir = '.'//slash//trim(buffer(6:))//slash
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (trim(buffer) == '-with_bed_change') then
               !Option pour activer la prise en compte du charriage
               with_bed_change = .true.
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (trim(buffer) == '-b') then
               !Option pour activer la prise en compte du charriage
               with_bed_change = .true.
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (trim(buffer) == '-m') then
               !Option pour forcer la réalisation de la simulation hydraulique lors de l'initialisation
               force_Mage = .true.
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (trim(buffer) == '-i') then
               !Option pour forcer AdisTS à ignorer les ouvrages ; tous les ouvrages sont transparents
               ignore_Ouvrages = .true.
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (buffer(1:3) == '-l=') then
               !Option pour choisir le schéma TVD et son limiteur
               limiteur_name = trim(buffer(4:))
               select case (limiteur_name)
                  case ('superbee') ; limiteur => superbee  ; use_TVD = .true.
                  case ('minmod')   ; limiteur => minmod    ; use_TVD = .true.
                  case ('vanleer')  ; limiteur => vanleer   ; use_TVD = .true.
                  case ('vanalbada'); limiteur => vanalbada ; use_TVD = .true.
                  case ('ospre')    ; limiteur => ospre     ; use_TVD = .true.
                  case ('koren')    ; limiteur => koren     ; use_TVD = .true.
                  case ('isnas')    ; limiteur => isnas     ; use_TVD = .true.
                  case ('muscl')    ; limiteur => muscl     ; use_TVD = .true.
                  case ('umist')    ; limiteur => umist     ; use_TVD = .true.
                  case ('cds')      ; limiteur => cds       ; use_TVD = .true.
                  case ('quick')    ; limiteur => quick     ; use_TVD = .true.
                  case ('none')     ; limiteur => null()    ; use_TVD = .false.
                  case ('upwind')   ; limiteur => null()    ; use_TVD = .false.
                  case ('explicite'); limiteur => null()    ; use_TVD = .false.  ;  use_Splitting = .false.
                  case default      ; limiteur => superbee  ; use_TVD = .true.
               end select
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (buffer(1:4) == '-cs=') then
               !Option pour définir la taille du cache des lignes d'eau
               if (buffer(5:5) == ' ') then
                  write(output_unit,*) 'Option -cs : valeur incorrecte (espace)'
                  stop 1005
               endif
               read(buffer(5:),*) cache_size
               if (cache_size <= 0) then
                  cache_size = cache_size_default
               else
                  cache_size = min(cache_size,100)
               endif
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else if (buffer(1:2) == '-t') then
               !Option pour traquer les valeurs de concentration trop grandes
               alarm_ON = .true.
               if (buffer(1:3) == '-t=') then
                  read(buffer(4:),*) too_big_input
                  if (too_big_input > zero) too_big = too_big_input
               endif
               iarg = iarg+1  ;  call get_command_argument(iarg,buffer)
            else
               repFile = trim(buffer)
               exit
            endif
         else
            write(error_unit,*) '>>>> Erreur : il manque le nom du fichier REP'
            stop 1006
         endif
      enddo
      !on informe fatal_errors
      non_fixed_geometry = with_bed_change

      !Création du dossier outdir s'il n'existe pas
      inquire(file=trim(outdir),exist=bexist)
      if (.not.bexist) call execute_command_line('mkdir '//trim(outdir))

      write(output_unit,*)
      write(output_unit,*) '<====================================== Programme Adis-TS ======================================>'
      write(output_unit,*)
      write(output_unit,*) version
      write(output_unit,*) 'Dernière nouveauté : ',trim(lastmodif)
      write(output_unit,*) 'Dernier correctif : ',trim(commitID)
      write(output_unit,*)  trim(dateCommit)
      write(output_unit,*) 'Date de compilation : ',trim(buildID)

#ifdef sequentiel
      if (no_coupling) then
         write(output_unit,*) '==> Simulation SANS couplage en mode séquentiel'
      else
         write(output_unit,*) '==> Simulation AVEC couplage en mode séquentiel'
      endif
#endif /* sequentiel */
#ifdef openmp
      if (no_coupling) then
         write(output_unit,'(a,i0,a)') ' ==> Simulation SANS couplage en mode parallèle avec ',nb_threads,' processeurs maximum'
      else
         write(output_unit,'(a,i0,a)') ' ==> Simulation AVEC couplage en mode parallèle avec ',nb_threads,' processeurs maximum'
      endif
      if (OS(1:5) == 'Linux') then
         call execute_command_line('echo '' ==> NOTE : valeur de ulimit -s : '' $(ulimit -s)')
         write(output_unit,'(a)') '            Si Adis-TS refuse de démarrer avec une erreur de segmentation'
         write(output_unit,'(a)') '            essayer d''augmenter cette valeur, par exemple : ulimit -s 16384'
         write(output_unit,'(a)') '            puis relancer Adis-TS'
         write(output_unit,*)
      endif
#endif /* openmp */

      if (alarm_ON) write(output_unit,'(a,1P,E10.3,a)') '--> Les concentrations supérieures à ',too_big,' sont tracées'

      write(output_unit,*)
      write(output_unit,*)
      ! NOTE: extension Gfortran : interception du signal TSTP (Ctrl+Z)
      ! NOTE: extension Gfortran : interception du signal SIGINT (Ctrl+C)
      if (OS(1:5) == 'Linux') call signal(SIGINT,ask_stop)
      call adisExec (repFile, modele, date_contraintes)
      stop
   endif



contains

subroutine adisExec (repFile, modele, date_contraintes)
!==============================================================================
!      Sous-programme de calcul principal
!      -> Lecture des données et initialisation
!      -> Boucle sur le temps
!      -> Écriture et stockage des résultats
!
!  -> Entrées :
!              repFile = nom du fichier répertoire
!              modele     = jeu de données ; type dataset
!==============================================================================
   implicit none
   ! prototype
   character(len=fnl), intent(inout) :: repFile
   type (dataset), intent(inout), target :: modele
   character (len=20), intent(inout) :: date_contraintes
   ! variables locales
   character(len=fnl) :: binFile, finFile
   type(dataset), pointer :: p_all

   integer :: npol
   real(kind=long) :: t, dt, dt_mes, dtmp
   real(kind=long), allocatable, target :: cdisp1(:), cdisp2(:)
   real(kind=long), pointer :: p_c1(:,:), p_c2(:,:), p_qm1(:,:,:), p_qm2(:,:,:), &
                               p_cdisp1(:), p_cdisp2(:)
   real(kind=long), allocatable, target :: cd_q1a(:), cd_q3a(:), cd_q1b(:), cd_q3b(:)
   real(kind=long), pointer :: p_cd_q1a(:), p_cd_q3a(:), p_cd_q1b(:), p_cd_q3b(:)
   real(kind=long) :: debut,fin, duree_effective
   !===> états hydrauliques
   type (state), target  :: etat1, etat2  !2 états successifs t et t+dt
   type (state), pointer :: at1           ! at1 pointe sur l'état à t
   type (state), pointer :: at2           ! at2 pointe sur l'état à t+dt
   type (cache) :: bin

   !===> solution
   type (solution), target :: sol1, sol2
   type (solution), pointer :: p_sol1, p_sol2
   type (coupling_data) :: global

   integer :: vtime1(8), vtime2(8)
   integer :: ndt, ndt_loc, ltmp
#ifdef openmp
   integer :: rang
#endif /* openmp */
   real(kind=long) :: t_csv, dt_moyen, t_bin, dt_inf, dt_sup, t_scr
   real(kind=long) :: dt_inf_loc, dt_sup_loc, dt_moyen_loc, peclet, peclet_min, peclet_max
   type(data_hydrauliques), allocatable, save :: data_hyd(:)
   logical :: bstop, bpause, success, geometry_hasBeen_modified
   real(kind=long), allocatable :: dt_max(:)
   real(kind=long), allocatable :: c_transfert(:,:)
   integer :: nb_flag_dt
   logical :: flag_dt

   current_time = 0._long

   call date_and_time(VALUES=vtime1)  ;  call cpu_time(debut)

   !lecture des données et initialisation
   call initAll(modele, repFile, binFile, finFile)
   nb_threads = min(nb_threads,modele%nbpol)
   bin%size = cache_size*1000000
   bin%file = trim(binFile)
   allocate(bin%irg(modele%net%ismax))

   allocate(m_total_bief(modele%net%ibmax,modele%nbpol))
   allocate (global%c_total_MES(modele%net%ismax),data_hyd(modele%net%ismax))
   allocate (global%dsg(modele%net%ismax), global%dsm(modele%net%ismax), global%dsd(modele%net%ismax))
   allocate (dt_max(modele%nbpol))
   allocate (c_transfert(size(all_OuvCmp),modele%nbpol))


   !état hydraulique initial
   call state_constructor(etat1,modele%net%ismax)
   call state_constructor(etat2,modele%net%ismax)

   !si on veut seulement la contrainte locale, on repositionne t0 à le date demandée pour
   !que la recherche de la ligne d'eau fonctionne même si elle est lointaine au point qu'il
   !soit nécessaire de remplir le cache plusieurs fois avant de la trouver si on gardait t0
   !tel que défini dans NUM
   if (date_contraintes /= '') t0 = real(lire_date(date_contraintes),kind=long)
   call initLigneDeauMAGE(t0, modele, etat1, etat2, bin, finFile)
   call getLigneDeauMAGE(t0, modele, etat1, bin)
   call update_positions_ouvrages(t0)

   !solution initiale
   call init_Solution(modele,sol1,sol2)
   call update_stocks_MES(modele,sol1,sol2,dt0)
   sol2%dhg = zero ; sol2%dhm = zero ; sol2%dhd = zero  !il faut remettre les variations d'épaisseur à 0
   call calcul_Bilans(modele, etat1, etat2, sol1, sol2, t, dt0)
   do npol = 1, modele%nbpol
      sol2%st0_h2o(npol) = sol2%stock_h2o(npol)
      sol2%st0_fnd(npol) = sol2%stock_fnd(npol)
   enddo
   sol1 = sol2  !report sur sol1 des modifs de sol2 faites par calcul_Bilans()

   !initialisation des pointeurs
   allocate(cdisp1(modele%net%ismax),cdisp2(modele%net%ismax))
   allocate(cd_q1a(modele%net%ismax),cd_q3a(modele%net%ismax),cd_q1b(modele%net%ismax),cd_q3b(modele%net%ismax))
   cdisp1(1:modele%net%ismax) = zero ;  cdisp2(1:modele%net%ismax) = zero
   cd_q1a(1:modele%net%ismax) = zero ;  cd_q1b(1:modele%net%ismax) = zero
   cd_q3a(1:modele%net%ismax) = zero ;  cd_q3b(1:modele%net%ismax) = zero
   p_c1 => sol1%c                 ;  p_c2 => sol2%c
   p_cdisp1 => cdisp1             ;  p_cdisp2 => cdisp2
   p_qm1 => sol1%qm               ;  p_qm2 => sol2%qm
   at1 => etat1                   ;  at2 => etat2
   p_sol1 => sol1                 ;  p_sol2 => sol2
   p_cd_q1a => cd_q1a             ;  p_cd_q1b => cd_q1b
   p_cd_q3a => cd_q3a             ;  p_cd_q3b => cd_q3b

   p_all => modele
!-----------------Calculs------------------------------------------------------------------------------------!

   t = t0  ;  dt = dt0 ; t_csv = t0+dtcsv ; t_bin = t0+dtbin ; t_scr = t0+dtscr
   dt_inf = dt0  ;  dt_sup = zero  ;  dt_moyen = zero  ; ndt = 0
   dt_inf_loc = dt0  ;  dt_sup_loc = zero  ;  dt_moyen_loc = zero  ;  ndt_loc = 0
   dt_max = dt0

   geometry_hasBeen_modified = .false.

   write(output_unit,*)
   call couplage_granulo (global,modele,sol1,t)
   if (date_contraintes /= '') then

      write(output_unit,*) 'On fait seulement le calcul de la contrainte locale en tout point à t = ',trim(date_contraintes)
      ! NOTE: on a remplacé t0 par date_contrainte pour initialiser le cache des lignes d'eau
      call dump_contraintes_locales(modele, bin, global, date_contraintes)

   else

      if (with_bed_change) then
         write(output_unit,'(a)') ' Début des calculs AVEC évolution de la géométrie (couplage fort avec MAGE)'
      else
         write(output_unit,'(a)') ' Début des calculs SANS évolution de la géométrie (couplage faible avec MAGE)'
      endif
      call write_Screen(modele, sol2, t, dt0, dt0, dt0, peclet, peclet_min, peclet_max, p_cdisp2, tmax-t0)
      call write_csv(modele, sol2, m_total_bief, t, nb_threads)

      call genissiat_extract(modele,at2,sol2,global,t)

      call write_bin(modele, sol2, t)

      if(with_bed_change) call export_geometrie(modele,t)


      !calcul des débits massiques à travers les ouvrages
      do npol = 1, modele%nbpol
         call fonction_transfert_ouvrages(modele,at1,global,modele%poll(npol)%ws,c_transfert(:,npol))
      enddo
      !--> Initialisation des termes sources
      call debord_all(modele,at1,data_hyd,geometry_hasBeen_modified)
      !$omp parallel default(shared) private(npol,rang) firstprivate(nb_threads) num_threads(nb_threads)
      do npol = 1, modele%nbpol
#ifdef openmp
         rang = omp_get_thread_num()
         if (mod(npol,nb_threads) /= rang) cycle
#endif /* openmp */
         call termes_sources(npol, modele, at1, sol1, sol1, data_hyd, global, dt_max(npol))
      enddo
      !$omp end parallel
      call coeff_Dispersion(modele, at1, p_cdisp1, p_cd_q1b, p_cd_q3b, peclet, peclet_min, peclet_max)

      !------------------Boucle sur le temps --------------------------------------------------------------!
      write(output_unit,*) 'Début de la boucle temporelle'
      nb_flag_dt = 0
      do while (t < tmax)
         dt = min(dt0,minval(dt_max(:)))
         call pas_de_temps(dt,at1,modele,p_cdisp1,t,flag_dt)
         if (flag_dt) nb_flag_dt = nb_flag_dt + 1
         if (t+dt > min(t_csv,t_bin,t_scr)) dt = max(dt_bottom,min(t_csv,t_bin,t_scr)-t)

         t = t+dt  ;  current_time = t
         dt_moyen = dt_moyen + dt  ;  ndt = ndt+1
         dt_moyen_loc = dt_moyen_loc + dt  ;  ndt_loc = ndt_loc+1
         dt_inf = min(dt,dt_inf)          ;  dt_sup = max(dt,dt_sup)
         dt_inf_loc = min(dt,dt_inf_loc)  ;  dt_sup_loc = max(dt,dt_sup_loc)
         if (dt < dt_bottom) then
            write(output_unit,*) 'Calcul à ',t,dt,dt_mes, dtmp
            write(output_unit,*) '>>>> Erreur : pas de temps trop petit !!!'
            stop 1007
         endif

         call getLigneDeauMAGE(t, modele, at2, bin)
         !mise à jour des positions des ouvrages mobiles d'après les données enregistrées par Mage
         call update_positions_ouvrages(t)

         !-->TERMES SOURCES : apports latéraux, dépôt-érosion, couplages entre polluants
         !   formule Debord : on ne fait le calcul que s'il y a des MES et une seule fois
         call debord_all(modele,at2,data_hyd,geometry_hasBeen_modified)
         !   mise à jour de la concentration totale en sédiment et du diamètre du sédiment
         !   le plus grossier présent au fond
         ! NOTE: il faut faire la mise à jour à chaque pas de temps car les dépôts-érosion peuvent être intenses
         call couplage_granulo(global,modele,p_sol1,t-dt)

         call coeff_Dispersion(modele, at2, p_cdisp2, p_cd_q1b, p_cd_q3b, peclet, peclet_min, peclet_max)

         !calcul des débits massiques à travers les ouvrages
         do npol = 1, modele%nbpol
            call fonction_transfert_ouvrages(modele,at2,global,modele%poll(npol)%ws,c_transfert(:,npol))
         enddo

         !$omp parallel default(shared) private(npol,rang) firstprivate(nb_threads) num_threads(nb_threads)
         !--> TERMES SOURCES : APPORTS LATERAUX & DEPOTS-EROSION
         do npol = 1, modele%nbpol
#ifdef openmp
            rang = omp_get_thread_num()
            if (mod(npol,nb_threads) /= rang) cycle
#endif /* openmp */
            call termes_sources(npol, modele, at2, p_sol1, p_sol2, data_hyd, global, dt_max(npol))
         enddo
         !$omp barrier

         !-->CONVECTION ou schéma explicte
         do npol = 1, modele%nbpol
#ifdef openmp
            rang = omp_get_thread_num()
            if (mod(npol,nb_threads) /= rang) cycle
#endif /* openmp */
            call run_Convection(modele,t,dt,npol,at1,at2,p_c1(:,npol),p_c2(:,npol),p_qm1(:,0,npol),p_cdisp1,c_transfert(:,npol))
         enddo
         !$omp barrier

         !-->DIFFUSION
         if (use_Splitting .and. with_diffusion) then
            do npol = 1, modele%nbpol
#ifdef openmp
               rang = omp_get_thread_num()
               if (mod(npol,nb_threads) /= rang) cycle
#endif /* openmp */
               call run_Diffusion(p_c2(:,npol), modele, t, dt, theta, npol, at1, at2, p_qm1(:,0,npol),  &
                                  p_qm2(:,0,npol), p_cd_q1a, p_cd_q3a, p_cd_q1b, p_cd_q3b)
            enddo
         endif
         !$omp end parallel

         !mise à jour des masses stockées au fond, de la concentration totale en sédiment,
         !du diamètre du sédiment le plus grossier présent au fond
         ! TODO: spécialiser update_stocks_MES() pour un polluant ce qui permettra de le faire entrer dans la zone parallèle
         call update_stocks_MES(modele,p_sol1,p_sol2,dt)

         !-->BILANS DE MASSE
         ! TODO: spécialiser calcul_Bilans() pour un polluant ce qui permettra de le faire entrer dans la zone parallèle
         call calcul_Bilans(modele, at1, at2, p_sol1, p_sol2, t, dt)

         !-->ECRITURES ECRAN ET FICHIER
         if (t >= t_scr) then     !-->résultats
            dtmp = dt_moyen_loc/real(ndt_loc,long)
            call write_Screen(modele, p_sol2, t, dtmp, dt_inf_loc, dt_sup_loc, peclet, peclet_min, peclet_max, p_cdisp2, tmax-t0)
            if (nb_flag_dt > 0) write(output_unit,'(a,i0,a,i0,a)') '>>>> ATTENTION : ',nb_flag_dt,'/',ndt_loc, &
                                               ' alertes pour des pas de temps plus grands que le pas de temps requis'
            dt_inf_loc = dt0  ;  dt_sup_loc = 0._long  ;  dt_moyen_loc = zero  ;  ndt_loc = 0 ; nb_flag_dt = 0
            !Arrêt anticipé si on trouve un fichier stop, Pause si fichier pause
            inquire(file='stop',exist=bstop)
            if (bstop) then  !arrêt anticipé
               open(newunit=ltmp,file='stop',form='formatted',status='unknown')
               read(ltmp,*) tmax
               tmax = tmax * 86400.
               if (tmax < t) tmax = (floor(t_csv/86400._long)+1._long)*86400._long
               close(ltmp,status='delete')  !suppression du fichier 'stop'
               write(output_unit,*) ' >>> interruption par l''utilisateur !!!'
            endif
            inquire(file='pause',exist=bpause)
            if (bpause) call mise_en_veille()
            t_scr = t_scr + dtscr ; t_screen = t_scr
         endif
         if(t >= t_csv) then
            call write_csv(modele, p_sol2, m_total_bief, t, nb_threads)
            t_csv = t_csv + dtcsv

            call genissiat_extract(modele,at2,p_sol2,global,t)

         endif
         if (t >= t_bin) then     !-->résultats
            call write_bin(modele, p_sol2, t)
            t_bin = t_bin + dtbin
         endif

         !Aura-t-on besoin d'une nouvelle simulation hydraulique pour le pas de temps suivant ?
         if ( with_bed_change .and. tmax > t) then
            if (new_hydraulic_simulation_required(t,p_sol2)) then
               call update_geometry(modele,sol1,sol2,geometry_hasBeen_modified)
               call new_hydraulic_simulation(modele,bin,at2,success)
               if (success) then
                  call initLigneDeauMAGE(at2%time, modele, at1, at2, bin, finFile)
               else
                  call restore_geometry(modele)
                  write(output_unit,'(5x,a)') 'La géométrie initiale a été restaurée'
                  stop 1008
               endif
            else
               geometry_hasBeen_modified = .false.
            endif
         endif

         !permutation etat1 <-> etat2
         if (associated(p_c1,sol1%c)) then
            p_c1 => sol2%c ; p_cdisp1 => cdisp2 ; p_qm1 => sol2%qm ; at1 => etat2
            p_c2 => sol1%c ; p_cdisp2 => cdisp1 ; p_qm2 => sol1%qm ; at2 => etat1
            p_sol1 => sol2 ; p_sol2 => sol1
            p_cd_q1a => cd_q1b ;  p_cd_q3a => cd_q3b
            p_cd_q1b => cd_q1a ;  p_cd_q3b => cd_q3a
         else
            p_c1 => sol1%c ; p_cdisp1 => cdisp1 ; p_qm1 => sol1%qm ; at1 => etat1
            p_c2 => sol2%c ; p_cdisp2 => cdisp2 ; p_qm2 => sol2%qm ; at2 => etat2
            p_sol1 => sol1 ; p_sol2 => sol2
            p_cd_q1a => cd_q1a ;  p_cd_q3a => cd_q3a
            p_cd_q1b => cd_q1b ;  p_cd_q3b => cd_q3b
         endif


      enddo
      write(output_unit,*)
      write(output_unit,*) 'Pas de temps moyen sur la simulation: ', dt_moyen/real(ndt,long)
      write(output_unit,*) 'Pas de temps min et max : ', dt_inf, dt_sup
      write(output_unit,*) 'Nombre total de pas de temps : ',ndt

      write(output_unit,*)
      write(output_unit,*) 'Calcul de la contrainte locale en tout point à la date finale'
      call heure(tmax,date_contraintes)
      call dump_contraintes_locales(modele, bin, global, date_contraintes)

   endif

!------------------Fin du programme --------------------------------------------------------------!

   !restauration du fichier repMage original
   call restore_repMage
   if (with_bed_change) then
      call update_geometry(modele,sol1,sol2,geometry_hasBeen_modified)
      call export_geometrie(modele,t)
      call export_geometry_history(modele,t)
      call restore_geometry(modele)
   endif

   !calcul et affichage du temps total et CPU de la simulation
   call cpu_time(fin)
   close (unit=1)
   call date_and_time(values = vtime2)
   duree_effective = (vtime2(5)*3600.+vtime2(6)*60.+vtime2(7)+vtime2(8)*0.001) &
                    -(vtime1(5)*3600.+vtime1(6)*60.+vtime1(7)+vtime1(8)*0.001) &
                    +(vtime2(3)-vtime1(3))*86400.
   write(output_unit,*)
   if(nb_threads > 1) then
      write(output_unit,*) 'Calcul parallèle terminé !!!'
   else
      write(output_unit,*) 'Calcul séquentiel terminé !!!'
   endif
   write(output_unit,'(a,f11.3)')' ---> durée CPU totale (Adis-TS seul) = ',fin-debut
   write(output_unit,'(a,f11.3)')' ---> durée effective (Adis-TS + MAGE) = ',duree_effective
   if (ndt > 0) write(output_unit,'(a,f11.3)')' ---> durée moyenne pour 10000 pas de temps = ',duree_effective / (0.0001*ndt)
   write(output_unit,*)
   write(output_unit,*) '>>>> Fin normale de la simulation <<<<'

end subroutine adisExec



subroutine pas_de_temps(dt,etat,modele,cdisp,t,flag_dt)
!==============================================================================
!                  calcul du pas de temps global
!
!  renvoie le minimum de la valeur calculée, de la valeur de dt0 fournie
!  dans NUM (pas de temps maximal choisi par l'utilisateur) et de la valeur
!  fournie en entrée.
!  -> appelle dt_convection() pour obtenir le pas de temps avec la contrainte
!     CR < 1 pour chaque bief
!
! entrées :
!     - dt = pas de temps ; réel (nécessaire car les temps en secondes
!            peuvent être très grands)
!     - etat = état hydraulique ; type state
!     - modele  = jeu de données ; type dataset
! sortie :
!     - dt = pas de temps ; réel
!
!==============================================================================
   implicit none
   ! prototype
   real(kind=long), intent(inout) :: dt
   type (dataset), intent(in) :: modele  ! jeu de données complet
   type (state), intent(in) :: etat   ! etat hydraulique
   real(kind=long) :: t, cdisp(:)
   logical, intent(out) :: flag_dt
   ! variables locales
   integer :: ib, i
   real(kind=long) :: Cr, dt_pe, dx

   if (dt == 0._long) then
      write(output_unit,'(3a)')  '>>>> ATTENTION : à t = ',trim(adjustl(c_heure(t))),' le pas de temps requis est nul'
      write(output_unit,'(a)')   '     Inutile de continuer, la simulation va rapidement diverger avec des bilans infinis'
      stop 1
   endif

   dt_pe = huge(0._long)
   ! contrainte dûe à la convection : nombre de Courant
   Cr = -1._long
   do ib = 1, modele%net%ibmax
      do i = modele%biefs(ib)%is1+1, modele%biefs(ib)%is2-1
         ! estimation du nombre de Courant
         if (etat%u(i) > 0._long) then
            Cr = max(Cr,etat%u(i)/abs(modele%dxp(i)))
         else
            Cr = max(Cr,-etat%u(i)/abs(modele%dxm(i)))
         endif
      enddo
   enddo
   if (Cr > 0._long) then
      dt = min(dt , 0.95_long/Cr)
   endif

   if (.not.use_Splitting) then
      do ib = 1, modele%net%ibmax
         do i = modele%biefs(ib)%is1+1, modele%biefs(ib)%is2-1
            if (etat%u(i) > 0._long) then
               dx = abs(modele%dxp(i))
               dt_pe = min(dt_pe,dx*dx/(cdisp(i)+etat%u(i)*dx))
            else
               dx = abs(modele%dxm(i))
               dt_pe = min(dt_pe,dx*dx/(cdisp(i)-etat%u(i)*dx))
            endif
         enddo
      enddo
      dt = min(dt,0.95_long*dt_pe)
   endif
   if (dt < dt_bottom) then
!      if (.not.use_Splitting) then
!         write(output_unit,'(3a,f0.3,a,f0.3,2(3x,f0.3))')  &
!                        '>>>> ATTENTION : à t = ',trim(adjustl(c_heure(t))),' le pas de temps requis (', &
!                        dt,') est plus petit que le pas de temps minimal ',dt_bottom, 0.95_long*dt_pe, 0.95_long/Cr
!      else
!         write(output_unit,'(3a,f0.3,a,f0.3,3x,f0.3)')  &
!                        '>>>> ATTENTION : à t = ',trim(adjustl(c_heure(t))),' le pas de temps requis (', &
!                        dt,') est plus petit que le pas de temps minimal ',dt_bottom, 0.95_long/Cr
!      endif
      flag_dt = .true.
      dt = dt_bottom
   else
      flag_dt = .false.
   endif
end subroutine pas_de_temps



subroutine initAll(modele, repFile, binFile, finFile)
!==============================================================================
!            initialisation complète du jeu de données
!
!  -> Lecture du fichier REP
!  -> Lecture du fichier NET
!  -> Lecture des fichiers POL
!  -> Lecture du fichier NUM
!  -> Lecture du fichier DIF
!  -> Lecture du fichier D90
!  -> Lecture du fichier SED
! TODO: ajouter la lecture du fichier CSP à définir selon journal du 10/11/2016
!
! entrées :
!     - modele = jeu de données ; type dataset
!     - repFile = nom du fichier REP
! sorties :
!     - modele = jeu de données ; type dataset
!     - binFile = nom du fichier BIN (états hydrauliques)
!     - finFile = nom du fichier Mage_fin.ini (état hydraulique final)
!
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(inout) :: modele
   character(len=fnl), intent(inout) :: repFile
   character(len=128), intent(out) :: binFile, finFile
   ! variables locales
   character (len=128) :: ligne
   integer :: lu, nbl, ios, ic, im, i, ich, nligne, nf
   character (len=fnl) :: netFile='', numFile='', difFile='', d90File='', sedFile='', netFile_adis='', netFile_mage=''
   character (len=fnl) :: sinFile=''
   character (len=fnl), allocatable :: polFile(:)
   integer :: npol, ib, kb, nbele
   logical :: bExist
   type (sediment), allocatable :: les_Sediments(:)

   !lecture du fichier REP contenant la liste des fichiers de données et sortie
   ! pas d'extension REP ou rep
   ! 1er essai
   if (scan(repFile,'.') == 0) repFile = trim(repFile)//'.REP'
   inquire(file=trim(repFile),exist=bExist)
   if (.not.bExist) repFile = repFile(1:len_trim(repFile)-4)//'.rep'
   inquire(file=trim(repFile),exist=bExist)
   if (.not.bExist) then
      write(output_unit,*) '>>>> Erreur : le fichier répertoire ',repFile(1:len_trim(repFile)-4)//'[.rep|.REP] n''existe pas'
      stop 1009
   endif
   write(output_unit,*) 'Lecture du fichier ',trim(repFile)
   !lu = 1 !unité logique
   open(newunit=lu,file=trim(repFile), status='old', form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier REP ',trim(repFile),' impossible'
      stop 1010
   endif
   nbl = 0
   do  !on compte les lignes de REP pour pouvoir allouer les listes de noms de fichier
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      if (ligne(1:1) /= '*') nbl = nbl + 1
   enddo
   allocate (polFile(nbl))
   rewind (lu)

   !call getcwd(adis_WorkingDir)
   allocate(character(len=2) :: adis_WorkingDir)
   adis_WorkingDir = '.'//slash

   !lecture de REP
   ic = 0  ;  im = 0  ;  nligne = 0
   do
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      nligne = nligne+1
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then
         cycle
      else
         do  i=1, 3
            ich=ichar(ligne(i:i))
            if (ich>96 .and. ich<123) ligne(i:i)=char(ich-32)  !conversion en majuscules
         enddo
         if (ligne(1:3) == 'NET') then
            ! NOTE: le fichier NET n'est utile que si la simulation hydraulique est faite par Mage-7
            netFile_adis = trim(ligne(5:))
            write(output_unit,*) 'netFile_adis = ',trim(netFile_adis)
         else if  (ligne(1:3) == 'NUM') then
            numFile = trim(ligne(5:))
            write(output_unit,*) 'numFile = ',trim(numFile)
         else if (ligne(1:3) == 'POL') then
            ic = ic+1 ;  nf = 5
            polFile(ic) = next_string(ligne,' ',nf)
            write(output_unit,'(a,i2.2,2a)') ' Polluant numéro ',ic,' : fichier ',trim(polFile(ic))
         else if (ligne(1:3) == 'REP') then
            write(output_unit,*) 'repMage = ',trim(ligne(5:))
            i = scan(ligne,slash,back=.true.)
            if (ligne(5:5) == slash .or. ligne(6:6) == ':') then
               !chemin absolu
               allocate(character(len=i-5) :: mage_WorkingDir)
               mage_WorkingDir = ligne(5:i)
            elseif (i > 5) then
               !chemin relatif
               allocate(character(len=i-5+len_trim(adis_WorkingDir)) :: mage_WorkingDir)
               mage_WorkingDir = trim(adis_WorkingDir)//ligne(5:i)
            else
               !il n'y a pas de séparateur de fichier dans la chaîne de caractères
               allocate(character(len=len_trim(adis_WorkingDir)) :: mage_WorkingDir)
               mage_WorkingDir = trim(adis_WorkingDir)
            endif
            repMage = trim(ligne(max(5,i+1):))
            netFile_mage = get_filename(mage_WorkingDir//repMage,'NET')
            !si la simulation hydraulique a été faite avec Mage-7 alors il n'y a pas de fichier NET dans le REP de Mage
            !il faut donc tester si netFile_mage est vide ou non
            if (netFile_mage /= '') netFile_mage = mage_WorkingDir//netFile_mage
            binFile = mage_WorkingDir//get_filename(mage_WorkingDir//repMage,'BIN')
            if (get_filename(mage_WorkingDir//repMage,'SIN') /= '') then
               sinFile = mage_WorkingDir//get_filename(mage_WorkingDir//repMage,'SIN')
            endif
            finFile = mage_WorkingDir//'Mage_fin.ini'
            write(output_unit,*) 'netFile_mage = ',trim(netFile_mage)
            write(output_unit,*) 'binFile = ',trim(binFile)
            write(output_unit,*) 'finFile = ',trim(finFile)
            write(output_unit,*) 'sinFile = ',trim(sinFile)
         else if (ligne(1:3) == 'DIF') then
            difFile = trim(ligne(5:))
            write(output_unit,*) 'difFile = ',trim(difFile)
         else if (ligne(1:3) == 'D90') then
            d90File = trim(ligne(5:))
            write(output_unit,*) 'd90File = ',trim(d90File)
         else if (ligne(1:3) == 'SED') then
            sedFile = trim(ligne(5:))
            write(output_unit,*) 'sedFile = ',trim(sedFile)
         else
            write(output_unit,*) trim(repFile),' : type de fichier inconnu à la ligne ',nligne,' : ',ligne(1:3)
         endif
      endif
   enddo
   if (with_bed_change .or. force_Mage) then
      netFile = netFile_mage
      topo_basedir = mage_WorkingDir
   else if (netFile_adis /= '') then
      netFile = netFile_adis
      topo_basedir = '.'//slash
   else
      netFile = netFile_mage
      topo_basedir = mage_WorkingDir
   endif
   if (netFile == '') then
      write(output_unit,*) '>>>> Erreur : le fichier de topologie n''est pas défini'
      stop 1011
   else
      write(output_unit,*) 'netFile = ',trim(netFile)
      write(output_unit,*) 'parent du dossier de géométrie = ',topo_basedir
   endif
   close (lu)

   !initialisation
   if (repMage == '') then
      write(output_unit,*) '>>>> le fichier REP de Mage n''a pas été défini, ' // &
                           'il manque donc les données de la simulation hydraulique'
      stop 1012
   endif
   ! lecture du fichier sedFile dont on a besoin pour initialiser la géométrie (couches sédimentaires)
   ! exclusion mutuelle des fichiers d90File et sedFile
   if (d90File /= '' .and. sedFile /= '') then
      write(output_unit,*) '>>>> le fichier de sédiments est ignoré car il y a déjà un fichier D90, on travaille sans charriage'
      write(output_unit,*) '>>>> pour travailler avec charriage (développement en cours), désactivez le fichier D90'
      print*
      sedFile = ''
   else if (d90File == '' .and. sedFile /= '') then
      call lire_Sediments(sedFile,les_Sediments)
   endif

   if (sedFile == '') then
      write(output_unit,*) 'Initialisation de la topologie et de la géométrie : ' // &
                           'appel de initTopoGeometrie() SANS couches sédimentaires'
      call initTopoGeometrie(netFile, modele)
   else
      write(output_unit,*) 'Initialisation de la topologie et de la géométrie : ' // &
                            'appel de initTopoGeometrie() AVEC couches sédimentaires'
      call initTopoGeometrie(netFile, modele, les_Sediments)
      deallocate (les_Sediments)
   endif
   if (with_bed_change) call backup_geometry(modele)

   !initialisation des ouvrages
   if (sinFile /= '') then
      call lire_SIN(modele,sinFile,nbele)
      call lire_Positions_Ouvrages_Mobiles()
   endif

   !initialisation et lecture des CL
   modele%nbpol = ic  !nombre de polluants
   modele%nbmes = 0   !initialisation du nombre de MES
   write(output_unit,*)
   write(output_unit,'(3a,i2.2)') ' Nombre de polluants définis dans ',trim(repFile),' : ',ic
   allocate (modele%poll(ic))
   allocate (modele%nbal(ic))

   call initAllCLzero (modele)
   do npol = 1, modele%nbpol
      call lirePolluant(polFile(npol),npol,modele)
      write(output_unit,'(a,i2.2,3a)') ' ==> Définition polluant ',npol,' (',trim(modele%poll(npol)%name),') : OK'
   enddo
   call MES_correlation(modele)

   !liste des valeurs à sortir
   !comptage des sorties demandées dans NUM
   open(newunit=lu,file=trim(numFile), status='old', form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier NUM ',trim(numFile),' impossible'
      stop 1013
   endif
   kb = 0
   do
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      if (ligne(1:6) == 'output') kb = kb+1
   enddo
   close(lu)
   allocate (liste(2*modele%net%ibmax+kb), entete_csv(2*modele%net%ibmax+kb), numID(modele%net%ibmax))
   kb = 0
   do ib = 1, modele%net%ibmax
      write(numID(ib),'(i3.3)') ib
      kb = kb+1  ;  liste(kb) = modele%biefs(ib)%is1  ;  entete_csv(kb)='"Bief'//numID(ib)//'_Amont"'
      kb = kb+1  ;  liste(kb) = modele%biefs(ib)%is2  ;  entete_csv(kb)='"Bief'//numID(ib)//'_Aval "'
   enddo
   nb_out = kb

   !lecture du fichier de paramètres numériques numFile
   call lire_Num(numFile)

   !si binFile n'existe pas on lance Mage avec les dates de début et de fin d'Adis-TS
   mage: block
      character(len=fnl) :: numFile
      logical :: bBIN, bFin, is_noPerm = .false., success
      real(kind=long) :: t_end
      inquire(file=trim(binFile),exist=bBIN)
      inquire(file=trim(finFile),exist=bFIN)
      if (bBIN .and. bFIN .and. .not.force_Mage) then
         exit mage
      else if (.not.bBIN) then
         write(output_unit,'(a)') ' >>>> le fichier BIN de Mage n''existe pas -> il faut lancer la simulation hydraulique'
      else if (.not.bFIN) then
         write(output_unit,'(a)') ' >>>> le fichier Mage_fin.ini n''existe pas -> il faut lancer la simulation hydraulique'
      else if (force_Mage) then
         write(output_unit,'(a)') ' >>>> l''option -m a été utilisée ->  il faut lancer la simulation hydraulique'
      endif
      if (with_bed_change) then
         t_end = t0+1.1_long*dtMage !pas la peine de faire une longue simulation, on va la refaire bientôt
      else
         t_end = tmax
      endif
      numFile = get_filename(mage_WorkingDir//repMage,'NUM')
      call export_Mage_NUM(numFile,t0,t_end,is_noPerm)
      call modify_repMage('NUM')
      call runMage(t0,t_end,success)
      if (.not.success) then
         if (with_bed_change) call restore_geometry(modele)
         stop 1014
      endif
   end block mage

   !lecture du fichier de paramètres de diffusion difFile
   call lire_Dif(modele, difFile)

   !lecture du fichier de diamètres caractéristiques du fond
   if (sedFile == '') then
      call lire_D90(modele, d90File)
   else
      !initialisation silencieuse de modele%sections(:)%d90 s'il y a sedFile mais pas d90File
      !car il faut quand même faire cette initialisation
      do i = 1, modele%net%ismax
         modele%sections(i)%d90 = 0.01_long
      enddo
   endif

end subroutine initAll



subroutine lire_Num (numFile)
!==============================================================================
!         lecture du fichier de paramètres numériques numFile
!        Initialisation des paramètres numériques correspondants :
!  -> pas de temps
!  -> dates de début et de fin de simulation
!  -> valeur par défaut des concentrations
!  -> coefficient theta (du schéma implicite pour la partie diffusion)
!
!  Entrée : numFile = nom du fichier NUM
!==============================================================================

   !prototype
   character (len=fnl), intent(in) :: numFile
   ! variables locales
   character (len=80) :: ligne
   character (len=30) :: tmp
   integer :: lu, ios, nligne, nf, jjj, hh, mm, ss, ib, kb
   real(kind=long) :: pk

   write(output_unit,*)
   write(output_unit,'(2a)') ' Lecture du fichier des paramètres numériques : ',trim(numFile)
   !lu = 1
   open(newunit=lu,file=trim(numFile), status='old', form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier NUM ',trim(numFile),' impossible'
      stop 1015
   endif
   nligne = 0
   kb = nb_out
   !initialisations
   t0 = 0._long    ;  tmax = 86400._long
   dt0 = 300._long ;  theta = 0.5_long
   dtcsv = 21600._long  ;  dtbin = 10800._long  ;  dtscr = 86400._long
   dtMage = 86400._long
   c_initiale = 0._long
   dt_bottom = 1._long
   !lecture du fichier
   do
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      nligne = nligne+1
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then
         cycle
      else if (ligne(1:10) == 'start_date') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
!          JJJ = next_int(ligne,':',nf)  ;  HH = next_int(ligne,':',nf)
!          MM = next_int(ligne,':',nf)   ;  SS = next_int(ligne,':',nf)
!          t0 = real(jjj,long)*86400._long + hh*3600._long + mm*60 + ss
         tmp = trim(adjustl(ligne(nf:)))
         t0 = real(lire_date(tmp),kind=long)
         write(output_unit,'(3a,f12.1,a)') ' Start_date = ',trim(ligne(nf:)),' (',t0,' secondes)'
      else if (ligne(1:8) == 'end_date') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
!          JJJ = next_int(ligne,':',nf)  ;  HH = next_int(ligne,':',nf)
!          MM = next_int(ligne,':',nf)   ;  SS = next_int(ligne,':',nf)
!          tmax = real(jjj,long)*86400._long + hh*3600._long + mm*60 + ss
         tmp = trim(adjustl(ligne(nf:)))
         tmax = real(lire_date(tmp),kind=long)
         write(output_unit,'(3a,f12.1,a)') ' End_date   = ',trim(ligne(nf:)),' (',tmax,' secondes)'
      else if (ligne(1:3) == 'dt0') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         dt0 = next_real(ligne,'',nf)
         write(output_unit,*) 'dt0 = ',dt0!, tmp
      else if (ligne(1:5) == 'theta') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         theta = next_real(ligne,'',nf)
         write(output_unit,*) 'theta = ',theta!, tmp
      else if (ligne(1:5) == 'dtcsv') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         dtcsv = next_real(ligne,'',nf)
         write(output_unit,*) 'dtcsv = ',dtcsv!, tmp
      else if (ligne(1:5) == 'dtbin') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         dtbin = next_real(ligne,'',nf)
         write(output_unit,*) 'dtbin = ',dtbin!, tmp
      else if (ligne(1:5) == 'dtscr') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         dtscr = next_real(ligne,'',nf)
         write(output_unit,*) 'dtscr = ',dtscr!, tmp
      else if (ligne(1:6) == 'dtmage' .or. ligne(1:6) == 'dtMage') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         dtMage = next_real(ligne,'',nf)
         write(output_unit,*) 'dtMage = ',dtMage!, tmp
      else if (ligne(1:9) == 'dt_bottom') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         dt_bottom = next_real(ligne,'',nf)
         write(output_unit,*) 'dt_bottom = ',dt_bottom
      else if (ligne(1:10) == 'c_initiale') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         c_initiale = next_real(ligne,'',nf)
         write(output_unit,*) 'c_initiale = ',c_initiale!, tmp
      else if (ligne(1:6) == 'output') then
         kb = kb+1
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         ib = next_int(ligne,'',nf)
         pk = next_real(ligne,'',nf)
         tmp = next_string(ligne,'',nf)
         liste(kb) = section_id_from_pk (modele, ib, pk, 0.01_long)
         entete_csv(kb)='"'//tmp(1:13)//'"'
         write(output_unit,*) 'output = ', ib, liste(kb)-modele%biefs(ib)%is1+1, pk, tmp
      endif
   enddo
   nb_out = kb
   close (lu)

end subroutine lire_Num



subroutine run_Convection(modele, t, dt, npol, etat1, etat2, c1, c2, qm, cdisp, c_transfert)
!==============================================================================
!      Étape de convection sur un pas de temps pour un polluant donné
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + t   = temps en secondes ; réel
!     + dt  = pas de temps en secondes ; réel
!     + npol = numéro du polluant
!     + etat1 = état hydraulique au début du pas de temps (à t-dt)
!     + etat2 = état hydraulique à la fin du pas de temps (à t)
!     + c1 = concentration au début du pas de temps (à t-dt)
!  Sortie :
!     + c2 = concentration à la fin du pas de temps (à t)
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(in) :: modele
   real(kind=long), dimension (:), intent(in) :: c1, qm, cdisp, c_transfert
   real(kind=long), dimension (:), intent(out) :: c2
   real(kind=long), intent(in) :: dt, t
   integer, intent(in) :: npol
   type (state), intent(in) :: etat1           ! etat1 pointe sur l'état à t-dt
   type (state), intent(in) :: etat2           ! etat2 pointe sur l'état à t
   ! variables locales
   integer :: iam, iav, ib, kb, np
   real(kind=long) :: cam, cav, qm0


   ! CONVECTION
   do kb = 1, modele%net%ibmax  ! boucle sur les biefs
      ib = modele%net%ibu(kb)
      iam = modele%biefs(ib)%is1 ; iav = iam
      qm0 = 0._long
      do while (iav < modele%biefs(ib)%is2)  !découpage du bief selon les apports intérieurs ponctuels
         !mise à jour des apports ponctuels
         ! NOTE: il faut utiliser le qm0 du tronçon précédent avant de calculer celui du suivant
         if (etat2%q(iam) > 0._long) then   !débit entrant dans le noeud
            if (iam == modele%biefs(ib)%is1) then
               cam = CL_noeud(modele,modele%biefs(ib)%nam,npol,etat1%q,c1(:),t)  !combinaison des apports au nœud amont
            else
               cam = c2(iam) + qm0/etat2%q(iam)
            endif
         else                        !débit sortant par le noeud
            cam = 0._long                                    !concentration inconnue
         endif

         !recherche des apports latéraux sur le tronçon ]iam -> [ et pour ce polluant
         iav = modele%biefs(ib)%is2
         qm0 = 0._long
         if (modele%nbal(npol) > 0) then
            do np = 1, modele%nbal(npol)
               if (ib == modele%AL_pol(np,npol)%ib) then !cet apport latéral est dans ce bief
                  if (modele%AL_pol(np,npol)%is1 > iam .AND. modele%AL_pol(np,npol)%is1 == modele%AL_pol(np,npol)%is2) then !il est dans le tronçon et il est ponctuel
                     qm0 = interpole(modele%AL_pol(np,npol)%qmt,etat2%time)
                     if (qm0 > 1.e-20_long) then !il n'est pas nul
                        iav = modele%AL_pol(np,npol)%is1
                        exit
                     endif
                  endif
               endif
            enddo
         endif

         ! NOTE: si le flux est entrant à l'aval, il faut utiliser qm0 tout de suite (pour ce tronçon)
         if (etat2%q(iav) < 0._long) then   !débit entrant dans le noeud
            if (iav == modele%biefs(ib)%is2) then
               cav = CL_noeud(modele,modele%biefs(ib)%nav,npol,etat2%q,c2(:),t)  !combinaison des apports au nœud aval
            else
               cav = c2(iav) + qm0/etat2%q(iav)
            endif
         else                        !débit sortant par le noeud
            cav = 0._long                                    !concentration inconnue
         endif

         !convection sur le bief courant
         if (use_Splitting) then
            if (use_TVD) then
               !call schema_TVD(c2,c1,etat1%x,etat1%u,etat1%a,etat2%a,iam,iav,cam,cav,dt,superbee,modele)
               call schema2_TVD(c2,c1,etat1%q,etat1%a,etat2%a,iam,iav,cam,cav,dt,limiteur,modele)
            else
               call schema_Upwind(c2,c1,etat1%q,etat1%a,etat2%a,iam,iav,cam,cav,dt,modele)
            endif
         else
            call schema_Explicite(c2,c1,etat1%q,etat1%a,etat2%a,iam,iav,cam,cav,dt,modele,modele%poll(npol)%cdc_riv, &
                                        qm,cdisp,c_transfert)
         endif
         !passage au tronçon suivant
         iam = iav
      enddo
   enddo
   do kb = 1, modele%net%ibmax  ! boucle sur les biefs
      if (use_Splitting) then
         if (use_TVD) then
            call CL_bief_lw(c2, c1, modele, kb, npol, etat1%q, etat1%a, etat2%a, qm, t, dt)
         endif
      else
         call CL_bief_lw(c2, c1, modele, kb, npol, etat1%q, etat1%a, etat2%a, qm, t, dt)
      endif
   enddo
   do kb = 1, modele%net%ibmax  ! boucle sur les biefs
      call CL_bief(c2, modele, kb, npol, etat1%q, t)
   enddo
end subroutine run_Convection



subroutine run_Diffusion(c2, modele, t, dt, theta, npol, etat1, etat2, &
                         qm1, qm2, cd_q1a, cd_q3a, cd_q1b, cd_q3b)
!==============================================================================
!      Étape de diffusion sur un pas de temps pour un polluant donné
!
!  Entrée :
!     + c2 = concentration à la fin du pas de temps (à t) après l'étape de convection
!     + modele = jeu de données ; type dataset
!     + t   = temps en secondes ; réel
!     + dt  = pas de temps en secondes ; réel
!     + theta = paramètre du schéma implicite ; réel compris entre 0 et 1
!     + npol = numéro du polluant
!     + etat1 = état hydraulique au début du pas de temps (à t-dt)
!     + etat2 = état hydraulique à la fin du pas de temps (à t)
!     + qm1 = apports massiques latéraux au début du pas de temps (à t-dt)
!     + qm2 = apports massiques latéraux à la fin du pas de temps (à t)
!     + cdisp1 = coefficient de diffusion au début du pas de temps (à t-dt)
!     + cdisp2 = coefficient de diffusion à la fin du pas de temps (à t)
!  Sortie :
!     + c2 = concentration à la fin du pas de temps (à t)
!==============================================================================
   implicit none
   ! prototype
   real(kind=long), dimension (:), intent(inout) :: c2
   type (dataset), intent(in) :: modele
   real(kind=long), intent(in) :: t, dt, theta
   integer, intent(in) :: npol
   type (state), intent(in) :: etat1           ! etat1 pointe sur l'état à t-dt
   type (state), intent(in) :: etat2           ! etat2 pointe sur l'état à t
   real(kind=long), dimension (:), intent(in) :: qm1, qm2, cd_q1a, cd_q3a, cd_q1b, cd_q3b
   ! variables locales
   integer :: iam, iav, i, ib, kb
   real(kind=long), allocatable :: triD(:,:)
   real(kind=long), allocatable :: f(:)    !second membre
   integer :: i2big, nb2big, ib2big
   real(kind=long) :: pk2big
   character(len=15) :: hms

   if (.not.allocated(triD)) allocate(triD(modele%net%ismax,3))
   if (.not.allocated(f)) allocate(f(modele%net%ismax))
   nb2big = 0 ; i2big = 0 ; ib2big = 0
   ! DISPERSION ET TERMES SOURCES
   do kb = 1, modele%net%ibmax  ! boucle sur les biefs
      ib = modele%net%ibu(kb)
      iam = modele%biefs(ib)%is1 ; iav = modele%biefs(ib)%is2   !limites du bief courant
      ! Calcul des coefficients du système linéaire à résoudre ; le second membre est stocké dans le tableau F
      call diff_fc(triD,f,c2,iam,iav,dt,theta,etat1%x,etat1%a,etat2%a,etat2%q, &
                   qm1,qm2,modele%poll(npol)%cdc_riv,modele%im1,modele%ip1,    &
                   cd_q1a, cd_q3a, cd_q1b, cd_q3b,modele%net%ismax)
      ! Inversion du système par une méthode de Gauss adaptée pour une matrice tridiagonale
      call gauss4b(f,iam,iav,triD,modele%net%ismax,alarm_ON)
      !call gauss3(f,iam,iav,triD,modele%net%ismax)
      ! mise à jour
      c2(iam:iav) = f(iam:iav)
      ! traitement des concentrations négatives
      do i = iam, iav
         if (.not. (c2(i) > 1.0E-30_long)) c2(i) = 0._long
      enddo
      if (alarm_ON .and. abs(t-t_screen)<1._long) then
         do i = iam, iav
            if (c2(i) > too_big) then
               if (nb2big > 0) then
                  nb2big = nb2big + 1
               else
                  nb2big = 1
                  i2big  = i
                  ib2big = ib
                  pk2big = modele%sections(i)%pk
               endif
            endif
         enddo
      endif
   enddo
   if (nb2big > 0) then
      call heure(t,hms)
      write(output_unit,'(5a,i3,a,f10.3,a,1pe10.3)') '>>>> ATTENTION à ',hms,' : concentration pour ', &
                trim(modele%poll(npol)%name),' trop grande dans le bief ',ib2big,' au Pk ',pk2big,' : ',c2(i2big)
      if (nb2big > 1) write(output_unit,'(17x,i0.2,a)') nb2big,' autres occurrences...'
   endif
end subroutine run_Diffusion


! TODO: spécialiser calcul_Bilans() pour un polluant ce qui permettra de le faire entrer dans la zone parallèle
subroutine calcul_Bilans(modele, etat1, etat2, sol1, sol2, t, dt)
!==============================================================================
!         Calcul des bilans de masse globaux pour un polluant donné
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + etat1 = état hydraulique au début du pas de temps (à t-dt) ; type state
!     + etat2 = état hydraulique à la fin du pas de temps (à t) ; type state
!     + sol1 = solution au temps t-dt ; type solution
!     + sol2 = solution au temps t ; type solution
!     + t   = temps en secondes ; réel
!     + dt  = pas de temps en secondes ; réel
!  Sortie :
!     + sol2 = solution au temps t ; type solution
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(in) :: modele
   type (state), intent(in) :: etat1       ! etat1 pointe sur l'état à t-dt
   type (state), intent(in) :: etat2       ! etat2 pointe sur l'état à t
   type (solution), intent(in) :: sol1     ! solution à t-dt
   type (solution), intent(inout) :: sol2  ! solution à t
   real(kind=long), intent(in) :: t, dt
   ! variables locales
   integer :: np, iam, iav, i, ib, neu
#ifdef openmp
   integer :: rang
#endif /* openmp */
   real(kind=long) :: qm, dx, val_tmp

   !$omp parallel default(shared) private(np,rang,ib,i,val_tmp,qm,iam,neu,iav,dx) &
      !$omp firstprivate(nb_threads) num_threads(nb_threads)
   do np = 1, modele%nbpol
#ifdef openmp
      rang = omp_get_thread_num()
      if (mod(np,nb_threads) /= rang) cycle
#endif /* openmp */
      sol2%stock_h2o(np) = 0._long
      sol2%stock_fnd(np) = 0._long
      sol2%v_in(np) = sol1%v_in(np)
      sol2%v_out(np) = sol1%v_out(np)
      do ib = 1, modele%net%ibmax
         m_total_bief(ib,np) = 0._long
         neu = modele%biefs(ib)%nam
         if (modele%nodes(neu)%cl > 0) then  !apport en concentration (noeud amont)
            iam = modele%biefs(ib)%is1
            sol2%V_in(np) = sol2%V_in(np) + 0.5_long*dt*(sol1%c(iam,np)*etat1%q(iam)+sol2%c(iam,np)*etat2%q(iam))
         else if (modele%nodes(neu)%cl == 0) then !apport en débit massique (noeud interne)
            qm = interpole(modele%CL_pol(neu,np)%fdt,t) + interpole(modele%CL_pol(neu,np)%fdt,t-dt)
            sol2%V_in(np) = sol2%V_in(np) + 0.5_long*dt*qm
         endif
         if (modele%nodes(modele%biefs(ib)%nav)%cl < 0) then
            iav = modele%biefs(ib)%is2
            sol2%V_out(np) = sol2%V_out(np) + 0.5_long*dt*(sol1%c(iav,np)*etat1%q(iav)+sol2%c(iav,np)*etat2%q(iav))
         endif
         !calcul du stock dans l'eau et au fond
         ! demie maille amont uniquement pour l'eau, sol2%masse(i,:,:) = 0
         i = modele%biefs(ib)%is1
         dx = abs(etat2%x(modele%ip1(i))-etat2%x(i))
         sol2%stock_h2o(np) = sol2%stock_h2o(np) + 0.25_long * dx * (etat2%a(i) + etat2%a(modele%ip1(i))) * sol2%c(i,np)
         ! demie maille aval uniquement pour l'eau, sol2%masse(i,:,:) = 0
         i = modele%biefs(ib)%is2
         dx = abs(etat2%x(modele%im1(i))-etat2%x(i))
         sol2%stock_h2o(np) = sol2%stock_h2o(np) + 0.25_long * dx * (etat2%a(modele%im1(i)) + etat2%a(i)) * sol2%c(i,np)
         ! mailles internes
         do i = modele%biefs(ib)%is1+1, modele%biefs(ib)%is2-1
            ! sol2%masse(i,:,:) est la masse linéïque sur les demies mailles de part et d'autre de la section i
            !volume local = 1/2 maille amont + 1/2 maille aval
            dx = 0.5_long * (abs(etat2%x(modele%im1(i))-etat2%x(i)) + abs(etat2%x(modele%ip1(i))-etat2%x(i)))
            val_tmp = sol2%masse(i,1,np)+sol2%masse(i,2,np)+sol2%masse(i,3,np) !masse linéïque totale
            m_total_bief(ib,np) = m_total_bief(ib,np) + dx * val_tmp
            val_tmp = 0.25_long * abs(etat2%x(modele%im1(i))-etat2%x(i)) * (etat2%a(modele%im1(i)) + etat2%a(i)) ! demi volume de la maille amont
            val_tmp = val_tmp + 0.25_long * abs(etat2%x(modele%ip1(i))-etat2%x(i)) * (etat2%a(modele%ip1(i)) + etat2%a(i)) ! + demi volume de la maille aval
            sol2%stock_h2o(np) = sol2%stock_h2o(np) + val_tmp * sol2%c(i,np)
         enddo
         sol2%stock_fnd(np) = sol2%stock_fnd(np) + m_total_bief(ib,np)
      enddo
      if (modele%nbal(np) > 0) then
         do ib = 1, modele%nbal(np)
            qm = interpole(modele%AL_pol(ib,np)%qmt,t) + interpole(modele%AL_pol(ib,np)%qmt,t-dt)
            sol2%V_in(np) = sol2%V_in(np) + 0.5_long*dt*qm
         enddo
      endif
   enddo
   !$omp end parallel
end subroutine calcul_Bilans



subroutine couplage_granulo(global,modele,sol,t)
!==============================================================================
!                 mise à jour des données de couplage
!
! - diamètre du sédiment le plus grossier présent au fond
!   c'est le sédiment le plus grossier présent au fond qui donne le strickler de peau
! - concentration totale en MES
!==============================================================================
   !prototype
   type (coupling_data), intent(inout) :: global
   type (solution), intent(inout) :: sol
   type (dataset), intent(inout) :: modele
   real(kind=long), intent(in) :: t
   !variables locales
   integer :: is, npol

   if (modele%nbmes == 0) return
   global%time = t

!!$omp parallel default(private) shared(modele,global,sol,nb_threads) num_threads(nb_threads)
!!$omp do schedule(static)
   do is = 1, modele%net%ismax
      global%dsm(is) = -1._long  ;  global%dsg(is) = -1._long  ;  global%dsd(is) = -1._long
      global%c_total_MES(is) = 0._long
      do npol = 1, modele%nbpol
         if (modele%poll(npol)%particule_type == 0) cycle  ! pas MES -> on passe
         if (sol%Hg(is) > 0.3_long) then
! FIXME: rien ne garantit que modele%poll(npol) est présent dans la couche de MES déposée dans le lit majeur gauche
            global%dsg(is) = max(global%dsg(is),modele%poll(npol)%d)
         else
            global%dsg(is) = modele%sections(is)%d90
         endif
         if (sol%Hm(is) > 0.3_long) then
! FIXME: rien ne garantit que modele%poll(npol) est présent dans la couche de MES déposée dans le lit mineur
            global%dsm(is) = max(global%dsm(is),modele%poll(npol)%d)
         else
            global%dsm(is) = modele%sections(is)%d90
         endif
         if (sol%Hd(is) > 0.3_long) then
! FIXME: rien ne garantit que modele%poll(npol) est présent dans la couche de MES déposée dans le lit majeur droit
            global%dsd(is) = max(global%dsd(is),modele%poll(npol)%d)
         else
            global%dsd(is) = modele%sections(is)%d90
         endif
         global%c_total_MES(is) = global%c_total_MES(is) + sol%c(is,npol)
      enddo
      if (global%dsm(is) < zero) global%dsm(is) = 1.e+30
      if (global%dsg(is) < zero) global%dsg(is) = 1.e+30
      if (global%dsd(is) < zero) global%dsd(is) = 1.e+30
   enddo
!!$omp end do
!!$omp end parallel

end subroutine couplage_granulo



subroutine dump_contraintes_locales(modele, bin, global, date)
!==============================================================================
!              Calcul de la contrainte locale
!
! Fait à l'instant 'date' en chaque point de chaque profil
! Sortie sur le fichier filename (nommage automatique)
!==============================================================================
   implicit none
   ! -- Prototype --
   type (dataset), intent(inout), target :: modele
   type (cache), intent(inout) :: bin
   type (coupling_data), intent(in) :: global
   character (len=20), intent(in) :: date
   ! -- Variables locales --
   real(kind=long) :: t
   character(len=fnl) :: filename
   integer :: out, ib, is, k
   type(pointTS) :: pts
   type (state) :: etat
   real(kind=long) :: z, k_skin, v_loc
   type(data_hydrauliques), allocatable :: data_hyd(:)
   real(kind=long), parameter :: un6 = 1._long/6._long
   character(len=5) :: position
   real(kind=long) :: Rh_min, Rh_moy, P_correction, Rh_loc
   character(len=20) :: date_loc
   type (profil), pointer :: prof

   if (modele%nbmes == 0) then
      write(output_unit,*) '>>>> Calcul des constraintes locales impossible : pas de MES <<<<'
      return
   endif

   t = real(lire_Date(date),kind=long)
   if (t < t0) then
      write(output_unit,*) 'Contraintes locales : la date demandée est antérieure à l''état initial'
      stop 1016
   endif

   ! NOTE: MS-Windows n'aime pas les : dans les noms de fichier, on les remplace par des -
   date_loc = date
   call remplacer(date_loc,':','-')

   filename = trim(outdir)//'contraintes_locales_'//trim(adjustl(date_loc))//'.txt'
   open(newunit=out, file = trim(filename), form = 'formatted', status = 'unknown')
   write(output_unit,*) 'Nom du fichier des contraintes locales : ',trim(filename)
   ! récupération de l'état hydraulique à t
   call state_constructor(etat,modele%net%ismax)
   call getLigneDeauMAGE (t, modele, etat, bin)
   call debord_all(modele,etat,data_hyd,.true.)
   do ib = 1, modele%net%ibmax
      write(out,'(a,i3.0)') '### Bief ',ib
      do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
         prof => modele%sections(is)
         write(out,*) '* Profil ',prof%name,' au pk ',prof%pk
         z = etat%y(is) + prof%zf
         write(out,*) '* Ligne d''eau :  Q = ', etat%q(is), ' Z = ', z, ' S = ', etat%a(is), ' V = ',etat%u(is)
         write(out,'(2a)') ' *       X                          Y                  contrainte locale            profondeur', &
                           '              Rayon hydraulique         Vitesse locale           Strickler de peau'
         P_correction = 2._long*z-prof%xyz(prof%li(1))%z-prof%xyz(prof%li(2))%z
         Rh_min = data_hyd(is)%smin/(data_hyd(is)%pmin-P_correction)
         Rh_moy = data_hyd(is)%smoy/(data_hyd(is)%pmoy-P_correction)
         do k = 1, prof%np
            if (k < prof%li(1)) then      ! le point k est dans le lit majeur gauche
               position = 'maj_G'
               V_loc = data_hyd(is)%vmoy
               k_skin = strickler_peau(global%dsg(is))
               Rh_loc = Rh_moy
            else if (k < prof%li(2)) then ! le point k est dans le lit mineur
               position = 'min__'
               V_loc = data_hyd(is)%vmin
               k_skin = strickler_peau(global%dsm(is))
               Rh_loc = Rh_min
            else                               ! le point k est dans le lit majeur droit
               position = 'maj_D'
               V_loc = data_hyd(is)%vmoy
               k_skin = strickler_peau(global%dsd(is))
               Rh_loc = Rh_moy
            endif
            pts = prof%xyz(k)
            write(out,*) pts%x, pts%y, contrainte_locale(pts,z,v_loc,k_skin,Rh_loc), max(z-pts%z,zero), &
                         Rh_loc, v_loc, k_skin, position
         enddo
      enddo
   enddo
   close(out)
   deallocate(data_hyd)
end subroutine dump_contraintes_locales



logical function new_hydraulic_simulation_required(t,sol2)
!this function check if a new hydraulic simulation is needed
   ! -- prototype --
   real(kind=long) :: t
   type(solution), intent(inout) :: sol2
   ! -- variables locales --
   real(kind=long), save :: t_last
   logical, save :: first_call = .true.
   real(kind=long) :: dhg_moyen, dhm_moyen, dhd_moyen, dhg_max, dhm_max, dhd_max, dh_moyen, dh_max
   integer :: ismax

   if (first_call) then
      t_last = t0
      first_call = .false.
   endif
   ismax = modele%net%ismax

   dhg_moyen = sum(sol2%dhg(1:ismax))/real(ismax,kind=long)
   dhm_moyen = sum(sol2%dhm(1:ismax))/real(ismax,kind=long)
   dhd_moyen = sum(sol2%dhd(1:ismax))/real(ismax,kind=long)
   dh_moyen = max(abs(dhg_moyen),abs(dhm_moyen),abs(dhd_moyen))

   dhg_max = maxval(abs(sol2%dhg(1:ismax)))
   dhm_max = maxval(abs(sol2%dhm(1:ismax)))
   dhd_max = maxval(abs(sol2%dhd(1:ismax)))
   dh_max = max(dhg_max,dhm_max,dhd_max)

   if (t >= t_last + dtMage) then
      !si la durée dtMage depuis la dernière simulation est dépassée
      t_last = t
      new_hydraulic_simulation_required = .true.
   elseif (dh_moyen > 0.01_long .or. dh_max > 0.05_long) then
      !si la variation moyenne ou la variation locale dépassent certains seuils
      t_last = t
      new_hydraulic_simulation_required = .true.
   else
      new_hydraulic_simulation_required = .false.
   endif
end function new_hydraulic_simulation_required


subroutine backup_geometry(modele)
!backup des fichiers de géométrie si with_bed_change = .true.
   ! -- prototype --
   type(dataset), intent(in) :: modele
   ! -- variables locales --
   integer :: ib
   character(len=120) :: command

   !on ne peut pas faire ce travail sur le dossier contenant les fichiers de géométrie
   !car, même si en pratique tous les fichiers de géométrie sont dans un même sous-dossier
   !du dossier de travail de MAGE, ce n'est pas une obligation. À la limite les fichiers
   !de géométrie peuvent être éparpillés dans des dossiers différents.
   if (OS(1:5) == 'Linux') then
      do ib = 1, modele%net%ibmax
         write(command,'(a)') 'cp '//trim(modele%biefs(ib)%fichier_geo)//' '//trim(modele%biefs(ib)%fichier_geo)//'_BAK'
         call execute_command_line(command,wait=.true.)
      enddo
   else if (OS(1:3) == 'Win') then
      do ib = 1, modele%net%ibmax
         write(command,'(a)') 'copy '//trim(modele%biefs(ib)%fichier_geo)//' '//trim(modele%biefs(ib)%fichier_geo)//'_BAK'
         call execute_command_line(command,wait=.true.)
      enddo
   else
      write(output_unit,*) '>>>> Erreur : OS non supporté'
      stop 1017
   endif
end subroutine backup_geometry


subroutine restore_geometry(modele)
!restauration des fichiers de géométrie initiaux si with_bed_change = .true.
   ! -- prototype --
   type(dataset), intent(in) :: modele
   ! -- variables locales --
   integer :: ib
   character(len=120) :: command

   if (OS(1:5) == 'Linux') then
      do ib = 1, modele%net%ibmax
         ! NOTE: a priori mv -f est plus rapide que cp puisqu'on veut remplacer le fichier cible
         write(command,'(a)') 'mv -f '//trim(modele%biefs(ib)%fichier_geo)//'_BAK'//' '//trim(modele%biefs(ib)%fichier_geo)
         call execute_command_line(command,wait=.true.)
      enddo
   else if (OS(1:3) == 'Win') then
      do ib = 1, modele%net%ibmax
         write(command,'(a)') 'del '//trim(modele%biefs(ib)%fichier_geo)
         call execute_command_line(command,wait=.true.)
         write(command,'(a)') 'ren '//trim(modele%biefs(ib)%fichier_geo)//'_BAK'//' '//trim(modele%biefs(ib)%fichier_geo)
         call execute_command_line(command,wait=.true.)
      enddo
   else
      write(output_unit,*) '>>>> Erreur : OS non supporté'
      stop 1018
   endif
end subroutine restore_geometry


subroutine update_geometry(modele,sol1,sol2,geometry_hasBeen_modified)
!==============================================================================
!    mise à jour de la géométrie en utilisant les épaisseurs de sédiment :
!    on ajoute le cumul des dépôts et érosions depuis la dernière mise à jour
!==============================================================================
   ! -- prototype --
   type (dataset), intent(inout) :: modele
   type (solution), intent(inout) :: sol1,sol2
   logical, intent(inout) :: geometry_hasBeen_modified
   ! -- variables locales --
   integer :: is, nb_profils_changed
   logical :: is_changed

   nb_profils_changed = 0
   geometry_hasBeen_modified = .false.
   do is = 1, modele%net%ismax
      is_changed = .false.
      !lit moyen gauche
      if (abs(sol2%dhg(is)) > seuil_miseAjour_geometrie) then
         is_changed = .true.
         do i = modele%sections(is)%ibg, modele%sections(is)%irg
            modele%sections(is)%xyz(i)%z = modele%sections(is)%xyz(i)%z + sol2%dhg(is)
         enddo
         !correction sur les bords : on dépose aussi sur les bords s'ils se retrouvent plus bas
         do i = 1, modele%sections(is)%ibg-1
            modele%sections(is)%xyz(i)%z = max(modele%sections(is)%xyz(i)%z,modele%sections(is)%xyz(modele%sections(is)%ibg)%z)
         enddo
      endif
      !lit mineur
      if (abs(sol2%dhm(is)) > seuil_miseAjour_geometrie) then
         is_changed = .true.
         do i = modele%sections(is)%ifg, modele%sections(is)%ifd
            modele%sections(is)%xyz(i)%z = modele%sections(is)%xyz(i)%z + sol2%dhm(is)
         enddo
         !correction sur les bords : on dépose aussi sur les bords s'ils se retrouvent plus bas
         do i = modele%sections(is)%irg+1, modele%sections(is)%ifg-1
            modele%sections(is)%xyz(i)%z = max(modele%sections(is)%xyz(i)%z,modele%sections(is)%xyz(modele%sections(is)%ifg)%z)
         enddo
         do i = modele%sections(is)%ifd+1, modele%sections(is)%ird-1
            modele%sections(is)%xyz(i)%z = max(modele%sections(is)%xyz(i)%z,modele%sections(is)%xyz(modele%sections(is)%ifd)%z)
         enddo
      endif
      !lit moyen droit
      if (abs(sol2%dhd(is)) > seuil_miseAjour_geometrie) then
         is_changed = .true.
         do i = modele%sections(is)%ird, modele%sections(is)%ibd
            modele%sections(is)%xyz(i)%z = modele%sections(is)%xyz(i)%z + sol2%dhd(is)
         enddo
         !correction sur les bords : on dépose aussi sur les bords s'ils se retrouvent plus bas
         do i = modele%sections(is)%ibd+1, modele%sections(is)%np
            modele%sections(is)%xyz(i)%z = max(modele%sections(is)%xyz(i)%z,modele%sections(is)%xyz(modele%sections(is)%ibd)%z)
         enddo
      endif

      if (is_changed) then
         modele%sections(is)%wh_aJour = .false. !le profil a changé, il faut mettre à jour la discrétisation largeurs-cotes
         nb_profils_changed = nb_profils_changed +1
         call largeursCotes(modele%sections(is))
         modele%sections(is)%zf = minval(modele%sections(is)%xyz(1:modele%sections(is)%np)%z)
         !les cumuls ont été utilisés, on les remet à zéro.
         sol2%dhg(is) = zero  ;  sol1%dhg(is) = zero
         sol2%dhm(is) = zero  ;  sol1%dhm(is) = zero
         sol2%dhd(is) = zero  ;  sol1%dhd(is) = zero
         geometry_hasBeen_modified = .true.
      endif
   enddo
   if (nb_profils_changed > 0) write(output_unit,*) ' ----> La géométrie a été modifiée par le transport solide dans ',&
                                            nb_profils_changed,' profils !!!'
end subroutine update_geometry

subroutine ask_stop
!==============================================================================
!   SIGNAL handler
!==============================================================================
   use Parametres, only: long, OS
   use DataNum, only: tmax, t=>current_time
   use utilitaires, only: heure
   use iso_fortran_env
   ! -- Variables --
   character(len=1) :: cstop
   character(len=15) :: hms

   call heure(t,hms)
   write(output_unit,*) ' '
   write(output_unit,'(3a)') '---> Interception du signal SIGTSTP : voulez-vous interrompre la simulation à ',trim(hms),' ?'
   write(output_unit,*)      '     Non, je veux continuer     -> tapez c'
   write(output_unit,*)      '     Oui, tout de suite         -> tapez s[top]'
   write(output_unit,*)      '     Oui, à la prochaine minute -> tapez m[inute]'
   write(output_unit,*)      '     Oui, à la prochaine heure  -> tapez h[eure]'
   write(output_unit,*)      '     Oui, au prochain jour      -> tapez j[our]'
   read(input_unit,'(a)') cstop
   select case (cstop(1:1))
      case ('c','C') ; continue
      case ('s','S') ; tmax = real(floor(t,kind=INT64),kind=long)+1._long
      case ('m','M') ; tmax = (int(t/60._long)+1)*60._long
      case ('h','H') ; tmax = (int(t/3600._long)+1)*3600._long
      case ('j','J') ; tmax = (int(t/86400._long)+1)*86400._long
      case default ; tmax = t
   end select
   if (cstop /= 'c') then
      call heure(tmax,hms)
      write(output_unit,*) ' >>> interruption par l''utilisateur à ',trim(hms),' !!!'
   endif
end subroutine ask_stop


subroutine schema_Explicite(c1,c,q,a,a1,iam,iav,cam,cav,dt,modele,skcin,qm,cdisp,c_transfert)
!==============================================================================
!  Résolution complète par le schema d'Euler decentre amont (Upwind)
! Algorithme adapté à la forme conservative de l'équation d'advection-diffusion
!
! Très bonne conservation de la masse mais diffusion numérique trop grande
! À utiliser pour contrôler un schéma TVD
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(in), target :: modele
   real(kind=long), intent(in) :: a(:), a1(:), q(:)
   real(kind=long), intent(out) :: c1(:)
   real(kind=long), intent(in) :: dt, cam, cav, skcin
   real(kind=long), intent(in) :: c(:), qm(:), cdisp(:), c_transfert(:)
   integer, intent(in) :: iam, iav

   ! variables locales
   integer :: i, ip1, im1
   real(kind=long) :: flux1, flux2, grad1, grad2, skc, dtdx, qm_ouv
   integer :: ns

   !CL de type "C imposée" si flux entrant
   if(q(iav) < 0._long) then
      c1(iav) = cav
   endif
   if(q(iam) > 0._long) then
      c1(iam) = cam
   endif

   !boucle sur les sections de calcul pour le calcul des flux
   do i = iam+1, iav-1
      !sections avant et après en ignorant les dx nuls
      ip1=modele%ip1(i)
      im1=modele%im1(i)
      if (modele%sections(i)%iss /= 0 .and. .not.ignore_Ouvrages) then  !aval ouvrage
         ns = modele%sections(i)%iss
         c1(i) = c_transfert(ns) * c1(i-1)
!      elseif (modele%sections(i+1)%iss /= 0 .and. .not.ignore_Ouvrages) then  !amont ouvrage
!         ns = modele%sections(i+1)%iss
!         c1(i) = max(0._long,q(i-1)/q(i) - c_transfert(ns)) * c(i-1)
      else
         qm_ouv = 0._long
         if (i < iav-2) then
            if (modele%sections(i+3)%iss /= 0 .and. .not.ignore_Ouvrages) then
               qm_ouv = max(0._long , (q(i+3)*c(i+3) - q(i+2)*c(i+2))) / abs(modele%sections(i-1)%pk - modele%sections(i+2)%pk)
            endif
         elseif (i < iav-1) then
            if (modele%sections(i+2)%iss /= 0 .and. .not.ignore_Ouvrages) then
               qm_ouv = max(0._long , (q(i+2)*c(i+2) - q(i+1)*c(i+1))) / abs(modele%sections(i-2)%pk - modele%sections(i+1)%pk)
            endif
         elseif (modele%sections(i+1)%iss /= 0 .and. .not.ignore_Ouvrages) then
            qm_ouv = max(0._long , (q(i+1)*c(i+1) - q(i)*c(i))) / abs(modele%sections(i-3)%pk - modele%sections(i)%pk)
         else
            qm_ouv = 0._long
         endif
         skc = 0._long
         dtdx = 2._long * dt / (modele%dxp(i)+modele%dxm(i))
         ! advection
         if(q(i) < 0._long) then
            Flux1 = 0.5_long * (q(ip1)+q(i))*c(ip1)  ;  Flux2 = 0.5_long * (q(im1)+q(i))*c(i)
         else
            Flux1 = 0.5_long * (q(i)+q(ip1))*c(i)    ;  Flux2 = 0.5_long * (q(im1)+q(i))*c(im1)
         endif
         ! diffusion
         if (modele%sections(i+1)%iss /= 0 .and. ip1 == i+1) then !on ignore la diffusion si on entre dans une cellule avec ouvrage
            grad1 = 0._long
            grad2 = 0._long
         elseif (modele%sections(i)%iss /= 0 .and. im1 == i-1) then !on ignore la diffusion si on sort d'une cellule avec ouvrage
            grad1 = 0._long
            grad2 = 0._long
         else
             if (with_diffusion) then
               grad1 = 0.25_long * (cdisp(i)+cdisp(ip1)) * (a(i)+a(ip1)) * (c(ip1)-c(i)) / modele%dxp(i)
               grad2 = 0.25_long * (cdisp(im1)+cdisp(i)) * (a(im1)+a(i)) * (c(i)-c(im1)) / modele%dxm(i)
             else
               grad1 = 0._long
               grad2 = 0._long
            endif
         endif
         ! cinétique chimique
         if (skcin > 0._long) skc = a(i) * skcin * c(i)
         !
         c1(i) = ( a(i)*c(i) + dtdx*(Flux2-Flux1 + grad1-grad2) + dt*(qm(i)-skc - qm_ouv) ) / a1(i)
         if (.not.(c1(i) > 0._long)) c1(i) = 0._long
      endif
   enddo
end subroutine schema_Explicite


function vertical_model(V_mean,Rh,H_water,ws,d90,h)
! estimation de la répartition verticale de la concentration selon le modèle de Lucie Guertault (2015)
! renvoie le taux de correction de la concentration moyenne : C(z) = C_mean * vertical_model()
   implicit none
   ! -- Prototype --
   real(kind=long), intent(in) :: V_mean  !vitesse moyenne
   real(kind=long), intent(in) :: Rh      !rayon hydraulique
   real(kind=long), intent(in) :: H_water !hauteur de la surface libre (tirant d'eau)
   real(kind=long), intent(in) :: ws      !vitesse de chute pour le sédiment courant
   real(kind=long), intent(in) :: d90     !d90 de la couche de surface du sédiment déposé au fond
   real(kind=long), intent(in) :: h       !profondeur à laquelle on veut une estimation de la concentration locale
   real(kind=long) :: vertical_model
   ! -- Variables locales --
   real(kind=long) :: Rouse, u_star, C_ref
   real(kind=long), parameter :: karman = 0.4_long, dtier = 2._long/3._long

   u_star = sqrt(g*h) * V_mean / (strickler_peau(d90)*Rh**dtier)
   Rouse = ws / (karman*u_star)
   C_ref = Rouse / (1._long-exp(-Rouse))

   vertical_model =  C_ref * exp(-Rouse*h/H_water)

end function vertical_model


subroutine fonction_transfert_ouvrages(modele,etat,global,ws,c_transfert)
! calcul des débits corrigés par l'action de chaque ouvrage composite
! le débit massique à travers un ouvrage = c_transfert * q * concentration
! la correction est dûe au fait que la concentration dans la section d'écoulement d'un ouvrage
! n'est pas égale à la concentration moyenne dans le profil.
!
! fonction_transfert_ouvrages() utilise vertical_model() pour estimer cette concentration ou plutôt un facteur
! de correction de la concentration moyenne.
!
   implicit none
   ! -- prototype --
   type (dataset), intent(in), target :: modele
   type (state), intent(in), target :: etat
   type (coupling_data), intent(in) :: global
   real(kind=long), intent(in) :: ws  !vitesse de chute
   real(kind=long), intent(out) :: c_transfert(:)  !débit corrigé
   ! -- variables locales --
   type (profil), pointer :: prof
   integer :: ns, js, n, is, iuv, nk
   real(kind=long) :: d90, h, h_water, R_h, V_mean, z_water, z_aval

   if (modele%net%nsmax == 0) return

   do ns = 1, size(all_OuvCmp)
      !sections avant et après en ignorant les dx nuls
      js = all_OuvCmp(ns)%js
      is = js - 1
      prof => modele%sections(is)
      iuv = all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv
      if (ns /= modele%sections(js)%iss) then
         call do_crash('fonction_transfert_ouvrages()')
      elseif (ignore_Ouvrages) then
         c_transfert(ns) = 1._long !ouvrages ignorés <=> ouvrages transparents
      elseif (iuv == 1 .or. iuv == 3 .or. iuv == 5 .or. iuv == 7 .or. iuv == 9 .or. iuv > 90) then
         c_transfert(ns) = 1._long ! ni vanne ni orifice ni déversoir => ouvrage transparent
      else
         h_water = etat%y(is)
         z_water = prof%zf + h_water
         V_mean = etat%u(is)
         R_h = section(prof,Z_water) / perimetre(prof,Z_water)
         z_aval = etat%y(js) + modele%sections(js)%zf
         c_transfert(ns) = 0._long
         do n = 1, all_OuvCmp(ns)%ne
            nk = all_OuvCmp(ns)%OuEl(n,1)
            if (all_OuvCmp(ns)%OuEl(n,2) == 1) then
               d90 = global%dsg(is)
            elseif (all_OuvCmp(ns)%OuEl(n,2) == 3) then
               d90 = global%dsd(is)
            else
               d90 = global%dsm(is)
            endif
            select case (all_OuvEle(nk)%iuv)
               case (0, 10, 11)  ;  h = 0.5_long * (z_water + all_OuvEle(nk)%uv2) - prof%zf
               case (2, 4, 6)    ;  h = all_OuvEle(nk)%uv2 + 0.5_long * all_OuvEle(nk)%uv3 - prof%zf
               case (8)          ;  h = 0.5_long * (z_water + all_OuvEle(nk)%uv2 + all_OuvEle(nk)%uv3) - prof%zf
               case default      ;  h = h_water
            end select
            c_transfert(ns) = c_transfert(ns) + vertical_model(V_mean,R_h,H_water,ws,d90,h) * debit(nk,z_water,z_aval)
         enddo
         c_transfert(ns) = c_transfert(ns) / etat%q(js)
      endif
   enddo
end subroutine fonction_transfert_ouvrages


subroutine lire_SIN(modele,SIN_file,nbele)
!==============================================================================
!                          Lecture du fichier .SIN
!
! NOTE: dans cette version avec allocation dynamique nbele = size(all_OuvEle)
!==============================================================================
   use parametres, only: long, zero, un, nesup
   use iso_fortran_env, only: error_unit, output_unit
   use Adis_Types, only: dataset
   use Geometrie_Section, only: section_id_from_pk, numero_bief
   use utilitaires, only: next_int, next_real, next_string, erreur_lecture, err032
   use Ouvrages, only: all_OuvCmp, all_OuvEle, some_ouvrages_writable, there_is_pumps, w_inf
   implicit none
   ! -- prototype --
   type (dataset), intent(in), target :: modele
   character(len=*),intent(in) :: SIN_file
   integer,intent(out) :: nbele
   ! -- variables --
   logical :: bool9
   integer :: i, i11, ib, ibsa, indic, is, iuvn, js, jssa, k, l, ls, luu
   integer :: n, nbk, nk, nk1, nl, npk, ns, nsn, nss2, ns_orphan
   integer :: next_field, nf0, ios
   !integer :: nmax, riv
   real(kind=long) :: along, charg, coeff, cote, dbipom, dta, dtm, p1, p2, p3, p4, p5
   real(kind=long) :: xis, zal1, zal2, zal3, zf, zf0, xssa, pk
   !real(kind=long) :: dbi, qnom
   character :: car1=''
   character(len=10) :: un_nom=''
   character(len=132) :: ligne=''
   character(len=1), parameter :: separateur=''
   integer, pointer :: iss(:),ismax, nsmax, is1(:)
   real(kind=long), pointer :: zfd(:)
   integer :: ierr = huge(0)
   integer, allocatable :: jss(:)


   write(output_unit,*) 'Entrée dans lire_SIN()'
   write(output_unit,*) 'Fichier de définition des ouvrages : ',trim(SIN_file)

   iss(1:) => modele%sections(1:)%iss
   ismax => modele%net%ismax
   nsmax => modele%net%nsmax
   is1(1:) => modele%biefs(1:)%is1
   zfd(1:) => modele%sections(1:)%zf



   !---lecture du fichier .sin (ouvrages élémentaires)
   there_is_pumps = .false.     !there_is_pumps sera mis à .true. s'il y a des pompes
   bool9 = .false.
   nbele = 0       !nombre d'ouvrages élémentaires
   n = 0           !compteur d'ouvrages élémentaires
   ns = 0          !compteur des sections singulières
   l = 0

   if (SIN_file /= '') then
      open(newunit=luu,file=trim(SIN_file),status='old',form='formatted')
      nl = 0             !compteur des ouvrages élémentaires
      nk = nsmax         !compteur des ouvrages composites
      ns_orphan = nsmax  !compteur des sections singulières sans ouvrage
      allocate (jss(size(iss))) ; jss = iss
      do    ! 1ère passe pour compter les ouvrages élémentaires et faire les allocations de mémoire
         read(luu,'(a)',iostat=ios) ligne
         if (ios < 0) then
            exit                             !on est arrivé à la fin du fichier
         elseif (ligne(1:1) == '*') then      !ligne de commentaire : ne compte pas
            cycle
         else if (len_trim(ligne) < 1) then   !ligne vide : ne compte pas
            cycle
         else
            nl = nl + 1                      !nouvel ouvrage élémentaire (pas besoin de vérifier que le code existe bien)
            !on a déjà une estimation du nombre de sections singulières, on ajoute les ouvrages composites sans section singulière
            !c'est-à-dire entre 2 sections qui ne sont pas à la même abscisse
            next_field = 2 ; ierr = 1
            ib = next_int(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
            xis = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(SIN_file,ligne)
            is = section_id_from_pk(modele,ib,xis,0.01_long)
            if (iss(is+1) == 0) then
               nk = nk + 1  ;  iss(is+1) = - nk  !ouvrage sans section singulière
            else if (jss(is+1) /= 0) then
               ns_orphan = ns_orphan - 1         !cet ouvrage est dans une section singulière
               jss(is+1) = 0
            endif
         endif
      enddo
      nsmax = nk
      nl = nl + ns_orphan !il faut ajouter le nombre de sections singulières sans ouvrage au décompte des ouvrages élémentaires
                          !car on va créer un ouvrage élémentaire fictif pour chaque section singulière sans ouvrage
      deallocate (jss)
      !allocations des ouvrages élémentaires et composites
      print*,'lire_SIN - Allocations : ',nl,nsmax
      allocate (all_OuvEle(nl), all_OuvCmp(nsmax))
      !initialisation du nombre d'ouvrages élémentaires pour chaque ouvrage composite
      all_OuvCmp(1:nk)%ne = 0

      !retour au début du fichier pour la vraie lecture
      rewind (luu)
      do    !boucle de lecture des singularités sur le fichier [.sin]
         read(luu,'(a)',iostat=ios) ligne
         print*,'lire_SIN : ',trim(ligne)
         if (ios < 0) exit
         l = l+1
         car1 = ligne(1:1)
         if (car1 == '*') then           !ligne de commentaire : on saute
            cycle
         else if (len_trim(ligne) < 1) then   !ligne vide : on saute
            cycle
         else if (car1 == 'B' .or. car1 == 'C' .or. car1 == 'D'  &
             .or. car1 == 'I' .or. car1 == 'L' .or. car1 == 'O'  &
             .or. car1 == 'P' .or. car1 == 'A' .or. car1 == 'T'  &
             .or. car1 == 'V' .or. car1 == 'W' .or. car1 == 'X'  &
             .or. car1 == 'F') then
            next_field = 2 ; ierr = 1
            ib = next_int(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
            xis = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(SIN_file,ligne)
            is = section_id_from_pk(modele,ib,xis,0.01_long)
            if (is == 0) then
               write(error_unit,'(a,i3,a)') ' >>>> Fichier SIN : le bief ',ib,' est inconnu sur ce réseau <<<<'
               stop 192
            else if (is < 0) then
               write(error_unit,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN  : il n''y a pas de section à', &
                                           ' l''abscisse ',xis,' dans le bief ',ib,' <<<<'
               stop 193
            endif
            i11 = is-is1(ib)+1  !numéro local de la section
            if (is == modele%biefs(ib)%is2) then !l'ouvrage est dans la section aval du bief
               write(error_unit,'(1x,a,f9.2,a,i3,a)') 'L''ouvrage au Pk ',xis,' du bief ',ib,  &
                                          ' est dans la dernière section du bief.'
               write(error_unit,'(1x,a)') 'Cette section doit être dupliquée pour pouvoir continuer'
               stop 3
            elseif (iss(is+1) == 0) then   !détection des ouvrages sans section singulière (normalement c'est déjà fait)
               nsmax = nsmax+1 ; iss(is+1) = -nsmax
            endif
         endif

         !--lecture des paramètres d'ouvrages
         ierr = huge(0)
         !along = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
         p1 = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
         !cote = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
         p2 = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
         nf0 = next_field
         !charg = next_real(ligne,separateur,next_field)
         p3 = next_real(ligne,separateur,next_field)
         if (next_field == 0) charg = zero
         nf0 = next_field
         !coeff = next_real(ligne,separateur,next_field)
         p4 = next_real(ligne,separateur,next_field)
         if (next_field == 0) coeff = zero
         nf0 = next_field
         p5 = next_real(ligne,separateur,next_field)
         if (next_field == 0) then
            !pas de paramètre P5 mais peut-être un nom quand même
            p5 = zero
            next_field = nf0
         endif
         un_nom = next_string(ligne,separateur,next_field)
         if (ierr == 0) call erreur_lecture(SIN_file,trim(ligne))
         n = n+1   ! numéro d'ordre de l'ouvrage élémentaire
         if (n > size(all_OuvEle)) then
            write(error_unit,3000) size(all_OuvEle), n
            stop 137
         endif
         js = is+1 !numéro de section contenant l'ouvrage n
         nsn = iabs(iss(js))    !numéro de section singulière contenant l'ouv. n
         if (nsn == 0) call err032(xis,ib)
         if (all_OuvCmp(nsn)%ne == 0) then  !lecture du 1er ouvrage élémentaire de la section singulière nsn
            ns = ns+1
            iss(js) = -iss(js)  !marquage de la section singulière js qui contient un ouvrage
            all_OuvCmp(nsn)%js = js  ;  all_OuvCmp(nsn)%ne = 1  ;  all_OuvCmp(nsn)%OuEl(1,1) = n  ;  all_OuvCmp(nsn)%OuEl(1,2) = 2
         else                       !lecture des autres ouvrages élémentaires de la section singulière nsn
            all_OuvCmp(nsn)%ne = all_OuvCmp(nsn)%ne+1
            nss2 = all_OuvCmp(nsn)%ne
            if (nss2 > nesup) then
               write(error_unit,1000) xis, ib, nesup
               stop 138
            endif
            all_OuvCmp(nsn)%OuEl(nss2,1) = n ;  all_OuvCmp(nsn)%OuEl(nss2,2) = 2
         end if
         select case (car1)
            case ('X')               ! ouvrage défini par l'utilisateur
               all_OuvEle(n)%iuv = 9
               all_OuvEle(n)%uv1 = p1
               all_OuvEle(n)%uv2 = p2
               all_OuvEle(n)%uv3 = p3
               all_OuvEle(n)%uv4 = p4
               all_OuvEle(n)%uv5 = p5
            case ('F')                ! orifice-voute
               all_OuvEle(n)%iuv = 11
               all_OuvEle(n)%uv1 = p1              ! largeur
               all_OuvEle(n)%uv2 = p2              ! cote de déversement
               all_OuvEle(n)%uv3 = p3 - p2         ! hauteur de la partie rectangulaire
               all_OuvEle(n)%uv4 = p5 - p3         ! hauteur de la voute
               if (p4 == zero) p4 = 0.4_long
               all_OuvEle(n)%uv5 = p4              ! coeff de débit
               if (p1 < 2.*all_OuvEle(n)%uv4) then
                  write (error_unit,'(a,/,a,/,a)') ' >>>> erreur à la ligne ',ligne, &
                           ' la hauteur de la voute est plus grande que la demie largeur de l''ouvrage'
                  stop 139
               endif
            case ('T')               ! déversoir trapézoïdal
               all_OuvEle(n)%iuv = 10
               all_OuvEle(n)%uv1 = p1                !longueur déversante
               all_OuvEle(n)%uv2 = p2                !cote de déversement
               if (p3 < 0.001_long) p3 = 9999._long  !cote charge
               all_OuvEle(n)%uv3 = p3 - p2           !ouverture
               if (p4 == zero) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                !coeff débit
               all_OuvEle(n)%uv5 = p5                !fruit des joues du déversoir
            case ('B')               ! buse
               all_OuvEle(n)%iuv = 8
               charg = min(p3,p1-0.0001_long)        !hauteur d'envasement bornée par le diamètre
               cote = p2 + charg                     !cote du fond + hauteur d'envasement
               all_OuvEle(n)%uv1 = p1                !diamètre
               all_OuvEle(n)%uv2 = cote              !cote de déversement
               all_OuvEle(n)%uv3 = charg             !hauteur d'envasement
               if (p4 == zero) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                !coeff débit
               all_OuvEle(n)%uv5 = zero
            case ('A')               ! perte de charge à la borda
               all_OuvEle(n)%iuv = 91
               all_OuvEle(n)%uv1 = max(0.1_long,p1)             !pas d'espace
               if (p2 == zero .or. p2 >= 1._long) p2 = 0.15_long
               all_OuvEle(n)%uv2 = max(0.0001_long,p2)          !seuil de prise en compte de l'élargissement
               all_OuvEle(n)%uv3 = zero
               if (p4 == zero) p4 = 1._long
               all_OuvEle(n)%uv4 = p4
            case ('D')               !Déversoir-Orifice / seuil mobile
               all_OuvEle(n)%iuv = 0
               all_OuvEle(n)%uv1 = p1                 !longueur déversante
               all_OuvEle(n)%uv2 = p2                 !cote de déversement
               if (p3 == zero) p3 = 9999.999_long
               all_OuvEle(n)%uv3 = max(w_inf,p3 - p2) !ouverture de l'orifice
               if (p4 == zero) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                 !coeff de débit
               all_OuvEle(n)%uv5 = zfd(is)            !cote minimale du seuil mobile
            case ('O')               !Déversoir-Orifice / ouverture mobile
               all_OuvEle(n)%iuv = 6
               all_OuvEle(n)%uv1 = p1                 !longueur déversante
               all_OuvEle(n)%uv2 = p2                 !cote de déversement
               if (p3 == zero) p3 = 9999.999_long
               all_OuvEle(n)%uv3 = max(w_inf,p3 - p2) !ouverture de l'orifice
               if (p4 == zero) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                 !coeff de débit
               if (abs(p5) < 0.0001_long) p5 = p2 + 9999.999_long
               all_OuvEle(n)%uv5 = p5 - p2            !ouverture maximale
            case ('V')               !Vanne de fond (loi standard) / ouverture mobile
               all_OuvEle(n)%iuv = 2
               all_OuvEle(n)%uv1 = p1                 !longueur déversante
               all_OuvEle(n)%uv2 = p2                 !cote de déversement
               all_OuvEle(n)%uv3 = max(w_inf,p3 - p2) !ouverture de la vanne
               if (p4 == zero) p4 = 0.61_long
               all_OuvEle(n)%uv4 = p4                 !coeff de débit
               if (abs(p5) < 0.0001_long) p5 = p3
               all_OuvEle(n)%uv5 = p5 - p2            !ouverture maximale
            case ('W')               !Vanne de fond (loi simplifiée) / ouverture mobile
               all_OuvEle(n)%iuv = 4
               all_OuvEle(n)%uv1 = p1                 !longueur déversante
               all_OuvEle(n)%uv2 = p2                 !cote de déversement
               all_OuvEle(n)%uv3 = max(w_inf,p3 - p2) !ouverture de la vanne
               if (p4 == zero) p4 = 0.61_long
               all_OuvEle(n)%uv4 = p4                 !coeff de débit
               if (abs(p5) < 0.0001_long) p5 = p3
               all_OuvEle(n)%uv5 = p5 - p2            !ouverture maximale
            case ('C','I')           !clapets normaux et inversés : supprimés
               write(error_unit,*) '>>>> ERREUR : cette modélisation des clapets a été abandonnée'
               write(error_unit,*) '              utilisez plutôt un orifice avec une règle de régulation de type CLAPET'
               stop 150
            case ('P')               !Pompe
               there_is_pumps = .true.
               ierr = huge(0)
               next_field = 2   ! on relit la ligne en entier
               ib = next_int(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               xis = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               dbipom = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               dtm    = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               dta    = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               zal1   = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               zal2   = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               zal3   = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               ibsa   = next_int(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               if (ierr == 0) call erreur_lecture(SIN_file,trim(ligne))
               nf0    = next_field
               xssa   = next_real(ligne,separateur,next_field)
               if (next_field == 0) next_field = nf0
               jssa = section_id_from_pk(modele,ibsa,xssa,0.001_long)
               if (jssa == 0) then
                  write(error_unit,'(a,i3,a)') ' >>>> Fichier SIN (pompe) : le bief ',ibsa,' est inconnu sur ce réseau <<<<'
                  stop 192
               else if (jssa < 0) then
                  write(error_unit,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN (pompe) : il n''y a pas de section à',  &
                                              ' l''abscisse ',xssa,' dans le bief ',ibsa,' <<<<'
                  stop 193
               endif
               un_nom = next_string(ligne,separateur,next_field)
               all_OuvEle(n)%iuv = 3  !affectation du numéro de type d'ouvrage
               !affectation des paramètres de la pompe
               all_OuvEle(n)%uv1 = dbipom  ;  all_OuvEle(n)%uv2 = 1.E+30_long  ;  all_OuvEle(n)%uv3 = 1.E+31_long
               if (dtm /= zero) then
                  all_OuvEle(n)%uv4 = dtm
               else
                  all_OuvEle(n)%uv4 = 180._long
               end if
               if (dta /= zero) then
                  all_OuvEle(n)%uv5 = dta
               else
                  all_OuvEle(n)%uv5 = 180._long
               end if

               all_OuvEle(n)%za1 = zal1  !cote de démarrage
               all_OuvEle(n)%za2 = zal2  !cote d'arrêt
               if (all_OuvEle(n)%za1 == zero .and. all_OuvEle(n)%za2 == zero) then
                  all_OuvEle(n)%za1 = 9999._long
                  all_OuvEle(n)%za2 = 9998._long
               endif
               all_OuvEle(n)%za3 = zal3  !tirant d'eau de désamorçage
               if (all_OuvEle(n)%za3 == zero) then
                  all_OuvEle(n)%za3 = 0.1_long
               endif
               !section de référence
               if (ibsa == 0 .and. jssa == 0) then
                  all_OuvEle(n)%irf = js-1
                else
                  all_OuvEle(n)%irf = is1(ibsa)+jssa-1
               endif
            case ('L')               !déversoir latéral
               next_field = 2   ! on relit la ligne en entier
               ib = next_int(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               xis = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               along = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               cote  = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               charg = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               coeff = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               p5    = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               ibsa  = next_int(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               xssa  = next_real(ligne,separateur,next_field) ; ierr = min(ierr,next_field)
               un_nom  = next_string(ligne,separateur,next_field)
               if (ierr == 0) call erreur_lecture(SIN_file,trim(ligne))
               all_OuvEle(n)%iuv = 5  !affectation du numéro de type d'ouvrage
               !affectation des paramètre d'ouvrage
               all_OuvEle(n)%uv1 = along
               all_OuvEle(n)%uv2 = cote
               if (charg == zero) charg = cote + 9999._long  !valeur par défaut
               all_OuvEle(n)%uv3 = charg-cote
               if (coeff == zero) coeff = 0.4_long  !valeur par défaut
               all_OuvEle(n)%uv4 = coeff
               !définition de la section de rejet du déversoir latéral
               ! FIXME: à voir plus tard s'il y en a vraiment besoin dans Adis-TS
!               if (ibsa == 0) then
                  all_OuvEle(n)%irf = 0
!               else
!                  jssa = section_id_from_pk(modele,ibsa,xssa,0.001_long)  !calcul de jssa seulement si la section de rejet existe
!                  if (jssa == 0) then
!                     write(error_unit,'(a,i3,a)') ' >>>> Fichier SIN (déversoir latéral) : le bief ',ibsa,&
!                                         ' est inconnu sur ce réseau <<<<'
!                     stop 192
!                  else if (jssa < 0) then
!                     write(error_unit,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN (déversoir latéral) : ',  &
!                             'il n''y a pas de section à l''abscisse ',xssa,' dans le bief ',ibsa,' <<<<'
!                     stop 193
!                  endif
!                  all_OuvEle(n)%irf = is1(ibsa)+jssa-1
!                  if (nclat(rivg,all_OuvEle(n)%irf) == 0) then  !par défaut on fait le déversement depuis la rive gauche
!                     nclat(rivg,all_OuvEle(n)%irf) = -(js-1)  ;  riv = rivg
!                  else if (nclat(rivd,all_OuvEle(n)%irf) == 0) then
!                     nclat(rivd,all_OuvEle(n)%irf) = -(js-1)  ;  riv = rivd
!                  else
!                     write(error_unit,'(a,i3,a)') ' >>>> le déversoir latéral à la ligne ',l,  &
!                                          ' est concurrent avec un déversement latéral par les berges'
!                     stop 215
!                  endif
!                  if (rvclat(riv,all_OuvEle(n)%irf) == 0) then  !par défaut le déversement arrivera dans la rive droite
!                     rvclat(riv,all_OuvEle(n)%irf) = rivd
!                  else
!                     rvclat(riv,all_OuvEle(n)%irf) = rivg
!                  endif
!                  if (iss(all_OuvEle(n)%irf+1) /= 0) then
!                     write(error_unit,'(a,i3,a)') ' >>>> la section de rejet du déversoir latéral à la ligne ',l, &
!                                         ' est singulière <<<<'
!                     stop 216
!                  endif
!               endif
            case default
               write(error_unit,*) '>>>> ERREUR dans .sin : ouvrage inconnu à la ligne : '
               write(error_unit,*) trim(ligne)
               stop 142
         end select
         !fin de la lecture des paramètres d'ouvrage

         !Nommage des ouvrages
         if (un_nom == '    ') write(un_nom,'(a1,i3.3)')'#',l
         all_OuvEle(n)%is_writable = .true.
         all_OuvEle(n)%cvar = un_nom
         do i = 1, n-1
            if (all_OuvEle(i)%cvar == un_nom) then
               write(error_unit,*) ' >>>> Le nom d''ouvrage ',un_nom,' existe déjà'
               stop 143
            endif
         enddo

      enddo
      close(luu)
   else
      !pas de fichier SIN
      !si on arrive ici, les allocations n'ont pas encore été faites.
      !on va juste remplir chaque section singulière avec un ouvrage composite composé d'un seul ouvrage élémentaire fictif
      !d'où l'allocation à NSmax de all_OuvEle(:) et all_OuvCmp(:) s'ils ne sont pas déjà alloués
      if (.not.allocated(all_OuvEle)) allocate (all_OuvEle(nsmax))
      if (.not.allocated(all_OuvCmp)) allocate (all_OuvCmp(nsmax))
      ! NOTE: quand on arrive ici size(all_OuvCmp) = nsmax
      !initialisation du nombre d'ouvrages élémentaires pour chaque ouvrage composite
      all_OuvCmp(1:nsmax)%ne = 0
   endif

   if (ns < nsmax) then ! Il y des sections singulières sans ouvrages
      !Définition d'ouvrages fictifs avec perte de charge égale à la perte de
      !charge singulière éventuelle pour élargissement brusque
      do is = 1, ismax
         if (iss(is) < 0) then  !recherche des sections singulières sans ouvrage
            n = n+1
            if (n > size(all_OuvEle)) then
               write(error_unit,'(3(a,/),a,i3.3)') ' Erreur 144 : ',' la somme du nombre d''ouvrages élémentaires', &
                       ' et du nombre de sections singulières sans',' ouvrages dépasse ',size(all_OuvEle)
               stop 144
            endif
            all_OuvEle(n)%is_writable = .false.  !cet ouvrage fictif n'apparaitra pas dans .TRA
            all_OuvEle(n)%iuv = 99
            all_OuvEle(n)%uv1 = 0.01_long
            iss(is) = -iss(is)  !la section is est singulière et ne contient pas d'ouvrage
            ls = iss(is)  !numéro de la section singulière is dans nss
            all_OuvCmp(ls)%js = is  ;  all_OuvCmp(ls)%ne = 1  ;  all_OuvCmp(ls)%OuEl(1,1) = n
         endif
      enddo
   endif

   nbele = n  !nbele = Nombre total d'ouvrages élémentaires

   !---Vérifications--------------------------------------------------------------
   if (allocated(all_OuvEle) .and. nbele /= size(all_OuvEle)) then
      write(error_unit,3000) size(all_OuvEle), nbele
      stop 145
   endif
   if (allocated(all_OuvCmp) .and. nsmax /= size(all_OuvCmp)) then
      write(error_unit,3001) size(all_OuvCmp), nsmax
      stop 145
   endif


   do is = 1, ismax  !recherche des sections singulières sans ouvrage
      if (iss(is) < 0) then
         !bug dans la routine lire_sin() (ouvrages non-definis)
         write(error_unit,*) ' >>>> erreur : il reste des sections singulières sans ouvrage'
         write(error_unit,'(a)') ' Merci d''envoyer un rapport de bug'
         stop 228
      endif
   enddo

   do ns = 1, nsmax  !recherche des clapets mélés à d'autres ouvrages
      nbk = all_OuvCmp(ns)%ne  ;  nk1 = all_OuvCmp(ns)%OuEl(1,1)
      if (nbk > 1 .and. all_OuvEle(nk1)%iuv == 1) then
         is = all_OuvCmp(ns)%js-1  ;  ib = numero_bief(modele,is)  ;  pk = modele%sections(is)%pk
         write(error_unit,2000) pk,ib
         bool9 = .true.
      endif
      if (all_OuvEle(nk1)%iuv /= 1 .and. nbk > 1) then
         do n = 2, all_OuvCmp(ns)%ne
            nk = all_OuvCmp(ns)%OuEl(n,1)
            if (all_OuvEle(nk)%iuv == 1) then
               is = all_OuvCmp(ns)%js-1  ;  ib = numero_bief(modele,is)  ;  pk = modele%sections(is)%pk
               write(error_unit,2000) pk,ib
               bool9 = .true.
            endif
         enddo
      endif
   enddo

   do ns = 1, nsmax !recherche des pompes mélés à d'autres ouvrages
      nbk = all_OuvCmp(ns)%ne  ;  nk1 = all_OuvCmp(ns)%OuEl(1,1)
      npk = 0
      if (nbk > 1) then
         do k = 1, nbk
            nk = all_OuvCmp(ns)%OuEl(k,1)
            if (all_OuvEle(nk)%iuv == 3) npk = npk+1
         enddo
         if (npk > 0 .and. npk /= nbk) then
            is = all_OuvCmp(ns)%js-1  ;  ib = numero_bief(modele,is)  ;  pk = modele%sections(is)%pk
            write(error_unit,2001) pk,ib
            bool9 = .true.
         endif
      endif
   enddo

   if (bool9) then
      write(error_unit,'(a)') ' Erreurs dans la lecture des ouvrages'
      stop 146
   endif
   !---Fin des vérifications-------------------------------------------------------

   do ns = 1, nsmax  !cotes du fond des ouvrages
      is = all_OuvCmp(ns)%js
      all_OuvCmp(ns)%zfs = zfd(is)
   enddo

   !Vérification des hauteurs de pelle des ouvrages élémentaires déversants
   indic = 0
   do ns = 1, nsmax
      is = all_OuvCmp(ns)%js
      zf = all_OuvCmp(ns)%zfs
      zf0 = zfd(is-1)
      do k = 1, all_OuvCmp(ns)%ne
         n = all_OuvCmp(ns)%OuEl(k,1)  ;  iuvn = all_OuvEle(n)%iuv
         if (iuvn /= 3 .and. iuvn /= 9 .and. iuvn < 90) then
            if (all_OuvEle(n)%uv2 < max(zf,zf0)) then
               ib = numero_bief(modele,is-1)  ;  pk = modele%sections(is-1)%pk
               write(error_unit,2002) pk,ib,all_OuvEle(n)%cvar
               write(error_unit,2003) zf0,zf,all_OuvEle(n)%uv2
               indic = indic+1
            endif
            if (all_OuvEle(n)%uv2 - max(zf,zf0) < 0.0099_long) then
               ib = numero_bief(modele,is-1)  ;  pk = modele%sections(is-1)%pk
               write(error_unit,2004) pk,ib,all_OuvEle(n)%cvar
               write(error_unit,2003) zf0,zf,all_OuvEle(n)%uv2
               indic = indic+1
            endif
         end if
      enddo
   enddo

   if (indic > 0) then
      write(error_unit,'(a)') ' >>>> Erreurs sur les hauteurs déversantes des seuils'
      stop 147
   endif
   !Calcul de l'indicateur global d'écriture BOOLI : si tous les BOOLJ sont .F.
   !alors BOOLI est .F. et il n'y a pas d'écriture des résultats des ouvrages
   some_ouvrages_writable = .false.
   do n = 1, nbele
      if (all_OuvEle(n)%is_writable) then
         some_ouvrages_writable = .true.
         exit
      endif
   enddo
   continue


   1000 format(' >>>> la section au Pm ',f10.3,' du bief ',i3,' contient plus de ',i2.2,' ouvrages')
   2000 format(' Erreur :',/,' La section au Pm ',f10.3,' du bief ',i3,' contient un clapet et un ouvrage de type différent')
   2001 format(' Erreur :',/,' La section au Pm ',f10.3,' du bief ',i3,' contient une pompe et un ouvrage de type différent')
   2002 format(' >>>> Erreur ouvrage au Pm ',f10.3,' du bief ',i3,' : ',A/,' >>>> cote de déversement < cote du fond')
   2003 format(' cote fond amont : ',f8.3,' cote fond aval : ',f8.3,/,' cote déversement : ',f8.3)
   2004 format(' >>>> Erreur sur l''ouvrage situé au Pm ',f10.3,' du bief ',i3,' : ',A/,  &
               ' >>>> La pelle est inférieure à 1 cm : ',' Risque de problèmes de convergence <<<<')
   3000 format(' >>>> Erreur de comptage des ouvrages élémentaires : ',i0,' (1er) ',i0,' (2e)')
   3001 format(' >>>> Erreur de comptage des ouvrages composites : ',i0,' (1er) ',i0,' (2e)')
end subroutine Lire_SIN


subroutine lire_Positions_Ouvrages_Mobiles()
! lecture des fichiers <nom_ouvrage>.csv pour remplir le tableau recorded_positions(:,:)
   use iso_fortran_env, only: error_unit, output_unit
   use parametres, only: long
   use couplage_MAGE, only: mage_WorkingDir
   use Ouvrages, only: all_OuvCmp, all_OuvEle, recorded_positions, nb_mobiles
   implicit none
   ! -- variables locales --
   character(len=:), allocatable :: filename
   logical :: bExist
   integer :: ns, lu, last_record, fsize, total_size, ios, k, nk, nf
   real(kind=long) :: tt, wt
   character(len=30) :: ligne

   write(output_unit,*) '---> Lecture des positions des ouvrages mobiles'
   last_record = 0
   total_size = 0
   nb_mobiles = 0
   ! allocation du tableau recorded_positions(:,:)
   allocate(character(len=len_trim(mage_WorkingDir)+14) :: filename)
   do ns = 1, size(all_OuvCmp)
      do k = 1, all_OuvCmp(ns)%ne
         nk = all_OuvCmp(ns)%OuEl(k,1)  !numéro de l'ouvrage élémentaire
         filename = mage_WorkingDir//trim(adjustl(all_OuvEle(nk)%cvar))//'.csv'
         inquire(file=filename,exist=bExist)
         if (bExist) then
            inquire(file=filename,size=fsize)
            total_size = total_size + fsize
         endif
      enddo
   enddo
   allocate(recorded_positions(total_size/30+1,2)) !il y a au plus 30 caractères par ligne

   do ns = 1, size(all_OuvCmp)
      do k = 1, all_OuvCmp(ns)%ne
         nk = all_OuvCmp(ns)%OuEl(k,1)  !numéro de l'ouvrage élémentaire
         filename = mage_WorkingDir//trim(adjustl(all_OuvEle(nk)%cvar))//'.csv'
         inquire(file=filename,exist=bExist)
         if (bExist) then  !ouvrage mobile
            write(output_unit,'(5x,2a)') 'Lecture du fichier ',trim(filename)
            nb_mobiles = nb_mobiles + 1
            write(output_unit,'(5x,2a)') 'Lecture des positions de l''ouvrage ',trim(adjustl(all_OuvEle(nk)%cvar))
            open(newunit=lu,file=trim(filename),form='formatted',status='old')
            inquire(lu,size=fsize)
            read(lu,*)
            read(lu,*)
            all_OuvEle(nk)%itw = last_record + 1
            do
               read(lu,'(a)',iostat=ios) ligne
               if (ios < 0) then
                  all_OuvEle(nk)%jtw = last_record
                  write(output_unit,'(5x,2(i0,3x))') all_OuvEle(nk)%itw,all_OuvEle(nk)%jtw
                  close (lu)
                  exit
               elseif (ios > 0) then
                  write(error_unit,*) '>>>> ERREUR de lecture du fichier ',trim(filename)
                  stop 1
               else
                  nf = 1  ;  tt = next_real(ligne,';',nf)  ;  wt = next_real(ligne,';',nf)
                  last_record = last_record + 1
                  recorded_positions(last_record,1) = tt
                  recorded_positions(last_record,2) = wt
               endif
            enddo
         else  !ouvrage fixe
            all_OuvEle(nk)%itw = -1
            all_OuvEle(nk)%jtw = -1
         endif
      enddo
   enddo

end subroutine lire_Positions_Ouvrages_Mobiles


subroutine genissiat_extract(modele,etat,sol,global,t)
!==============================================================================
!            écritures sur fichiers CSV des concentrations locales dans
!                   les ouvrages de Génissiat
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + sol = solution au temps t ; type solution
!     + t   = temps en secondes ; réel
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(in), target :: modele
   type (state), intent(in)   :: etat
   type (solution),intent(in) :: sol
   type (coupling_data), intent(in) :: global
   real(kind=long),intent(in) :: t
   ! variables locales
   logical, save :: first_call = .true.
   integer :: np, nbp, i, k, nk, is
   integer, parameter :: ng = 8  !nb d'ouvrages élémentaires à Génissiat
   character(len=9), save :: liste_ouv(ng) = ['barrageam','vf       ','vdf      ','erd1     ','erd2     ',&
                                              'erd3     ','usine    ','bisus    ']
   integer, save :: lu(ng) !liste des unités logiques pour les fichiers CSV
   character :: stTemps*11, stDebit*9, stHauteur*9
   real(kind=long) :: Q_ouv, Zam, Zav, d90, h, h_water, R_h, V_mean
   integer, save :: i_ouv(ng)  ! index des ouvrages élémentaires de Génissiat
   integer, save :: ns_ouv(ng) ! index de l'ouvrage composé qui contient un ouvrage élémentaire de Génissiat
   type (profil), pointer :: prof
   real(kind=long) :: ws  !vitesse de chute
   real(kind=long), allocatable :: coeff(:)
   logical, save :: is_not_Genissiat


   nbp = modele%nbpol
   !fichier de sortie pour le contrôle des résultats ; longueur des champs = 15
   if (first_call) then
      is_not_Genissiat = .true.
      ! recherche des ouvrages élémentaires de Génissiat et identification des sections singulières associées
      i_ouv = 0
      if(allocated(all_OuvCmp))then
        do i = 1, size(all_OuvCmp)
          do k = 1, all_OuvCmp(i)%ne
              nk = all_OuvCmp(i)%OuEl(k,1)
              do np = 1, ng
                if (trim(all_OuvEle(nk)%cvar) == trim(liste_ouv(np)) .and. i_ouv(np) == 0) then
                    i_ouv(np) = nk
                    ns_ouv(np) = i
                    if (all_OuvEle(nk)%iuv /= 0 .and. all_OuvEle(nk)%iuv /= 2 .and. all_OuvEle(nk)%iuv /= 4 &
                                                                              .and. all_OuvEle(nk)%iuv /= 6) then
                      !on ignorera les ouvrages élémentaires qui ne sont ni des vannes ni des orifices ni des seuils
                      i_ouv(np) = -1
                    endif
                    is_not_Genissiat = .false.
                    exit
                endif
              enddo
          enddo
        enddo
      endif
      !vérification : tous les i_ouv(np) doivent être non nuls
      if (.not.is_not_Genissiat) then
         !si aucun des ouvrages n'est trouvé, ce n'est pas Génissiat, donc pas d'erreur
         i = 0
         do np = 1, ng
            if (i_ouv(np) == 0) then
               write(error_unit,*) '>>>> ERREUR dans extract_Genissiat() : ouvrage '//trim(liste_ouv(np))//' non trouvé !'
               i = i + 1
            endif
         enddo
         if (i > 0) stop 162100
         !ouverture des fichiers
         stTemps     = '"Temps (s)"'
         stDebit     = '"Q_vanne"'
         stHauteur   = '"Z_vanne"'
         do np = 1, ng
            !on ignore les ouvrages élémentaires qui ne sont ni des vannes ni des orifices
            if (i_ouv(np) < 0) cycle
            open(newunit=lu(np),file=trim(outdir)//'extract_'//trim(liste_ouv(np))//'.csv',form='formatted',status='unknown')
            write(lu(np),'(*(a))') stTemps,';',stDebit,';',stHauteur,(';"'//trim(modele%poll(i)%name)//'"',i=1,nbp)
         enddo
      endif
      first_call = .false.
   endif

   if (is_not_Genissiat) return

   allocate(coeff(nbp))

   do np = 1, ng
      if (i_ouv(np) < 0) cycle
      is = all_OuvCmp(ns_ouv(np))%js - 1
      prof => modele%sections(is)
      !cotes de l'eau amont et aval de l'ouvrage
      h_water = etat%y(is)
      V_mean = etat%u(is)
      Zam = h_water + prof%zf
      Zav = etat%y(is+1) + modele%sections(is+1)%zf
      Q_ouv = debit(i_ouv(np),zam,zav)
      !cote du centre de la section d'écoulement de l'ouvrage
      if (all_OuvEle(i_ouv(np))%iuv == 0) then
         h = 0.5_long * (zam + all_OuvEle(i_ouv(np))%uv2) - prof%zf
      else
         h = all_OuvEle(i_ouv(np))%uv2 + 0.5_long*all_OuvEle(i_ouv(np))%uv3 - prof%zf
      endif
      R_h = section(prof,Zam) / perimetre(prof,Zam)
      d90 = global%dsm(is)
      do i = 1, nbp
         ws = modele%poll(i)%ws
         coeff(i) = vertical_model(V_mean,R_h,H_water,ws,d90,h)
      enddo
      write(lu(np),'(i0,*(a1,f0.3))') int(t),';',Q_ouv,';',h+prof%zf,(';',coeff(i)*sol%c(is,i),i=1,nbp)
   enddo
   deallocate(coeff)
end subroutine genissiat_extract

end program Adis_TS
