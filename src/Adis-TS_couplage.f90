!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module couplage_MAGE
!===============================================================================
!  Gestion du couplage avec MAGE pour refaire les simulations hydrauliques
!  en tant que de besoin, par exemple si le charriage modifie la géométrie
!
! numérotation des STOP : 12nn
!===============================================================================
use iso_fortran_env, only: output_unit, error_unit
use parametres, only: fnl, long, OS, slash, zero
use utilitaires, only: date_et_heure, get_filename, heure, capitalize
use adis_types, only: dataset, cache, state, profil
use DataNum, only: dtMage, tmax
use resultats, only: outdir

implicit none

integer :: nbl !nombre de lignes du fichier REP modifié
character(len=7) :: basename = 'adis-ts'
character (len=:),allocatable :: mage_WorkingDir
character (len=fnl) :: repMage = ''

! cache pour conserver les différents stades de la géométrie
real(kind=long), allocatable :: z_bak(:,:)
integer :: nb_export
real(kind=long), allocatable :: export_time(:)


contains

subroutine new_hydraulic_simulation(modele,bin,etat,success)
!préparation et lancement d'une nouvelle simulation hydraulique pour une durée de dtMage
! NOTE: la conservation des données pour Mage initiales et intermédiaires est assurée grâce à l'option -z de Mage-8
   ! -- prototype --
   type(dataset), intent(in) :: modele
   type(cache), intent(in) :: bin
   type(state), intent(in) :: etat
   logical,intent(out) :: success
   ! -- variables locales --
   character (len=fnl) :: numFile
   integer :: lu
   real(kind=long) :: t_start, t_end

   t_start = etat%time
   t_end   = min(tmax,t_start + 1.1_long*dtMage)
   inquire(file=trim(bin%file),number=lu)
   close (lu)
   call export_geometrie(modele,t_start)
   call export_geometry_history(modele,t_start)
   call export_Mage_InitialState(modele,etat)
   numFile = get_filename(mage_WorkingDir//repMage,'NUM')
   call export_Mage_NUM(numFile,t_start,t_end,.true.)
   call modify_repMage('INI')
   call modify_repMage('NUM')
   call runMage(t_start,t_end,success)
   if (success) then
      write(output_unit,'(a)') ' <-- Fin de la simulation MAGE'
   else
      write(output_unit,'(a)') ' <-- Échec de la simulation MAGE : impossible de continuer :-('
   endif
   call restore_repMage

end subroutine new_hydraulic_simulation


subroutine runMage(t_start,t_end,success)
!lancement de l'exécution de MAGE de t_start à t_end
   implicit none
   ! -- prototype --
   real(kind=long),intent(in) :: t_start, t_end
   logical,intent(out) :: success
   ! -- variables locales --
   character(len=:),allocatable,save :: command
   character(len=80) :: exe_mage, options_mage
   logical :: bexist
   integer :: flag, lu
   integer, save :: nap = 0

   write(output_unit,'(4a)') ' --> Lancement de MAGE de ',date_et_heure(t_start),' à ',date_et_heure(t_end)

   if (nap == 0) then !1er lancement de Mage -> on fait les vérifications une seule fois
      call get_environment_variable('EXE_MAGE',exe_mage)
      call get_environment_variable('OPTIONS_MAGE',options_mage)
      if (len_trim(exe_mage) == 0) then
         write(error_unit,'(2a)') ' >>>> Erreur : La variable d''environnement EXE_MAGE donnant le chemin ',&
                         'de l''exécutable de MAGE n''est pas définie'
         stop 1201
      else
         write(output_unit,*) ' Chemin de l''exécutable de MAGE et options : ',trim(exe_mage)//' '//trim(options_mage)
         allocate(character(len=len_trim(exe_mage)+len_trim(mage_WorkingDir)+len_trim(repMage)) :: command)
         command = trim(exe_mage)//' -v > version_Mage'
         call execute_command_line(command,wait=.true.)
         open(newunit=lu,file='version_Mage', status='old',form='formatted')
         read(lu,'(5x,i1)') flag
         if (flag < 8) then
            write(error_unit,*) '>>>> Erreur : veuillez relancer une version de MAGE ≥ 8'
            stop 1202
         endif
         close(lu,status='delete')
         ! TODO: ajuster les options de Mage-8 selon qu'on utilise DEBORD ou ISM
         ! TODO: si DEBORD on peut vouloir -LC, si ISM ou mixte s'il vaut peut-être mieux éviter
         if (OS(1:5) == 'Linux') then
            command = 'cd '//trim(mage_WorkingDir)//' && '//trim(exe_mage)//' '//trim(options_mage)// &
                      ' -r -s -l FIN -z=0 '//trim(repMage)
         elseif (OS(1:3) == 'Win') then
            command = 'cd '//trim(mage_WorkingDir)//' && '//trim(exe_mage)//' '//trim(options_mage)// &
                      ' -r -s -l FIN -z=3 '//trim(repMage)
         else
            write(error_unit,*) '>>>> Erreur : OS non supporté'  ;  stop 1203
         endif
      endif
      nap = 1
   endif

   !lancement de MAGE
   call execute_command_line(command,wait=.true.)
   inquire(file=trim(mage_WorkingDir)//'Mage_Results',exist=bexist)
   if (bexist) then
      open(newunit=lu,file=trim(mage_WorkingDir)//'Mage_Results',form='unformatted',status='old')
      read (lu) flag
      success = (flag > 0)
      close(lu,status='delete')
   else
      success = .false.
   endif
end subroutine runMage


subroutine export_Mage_NUM(numFile,t_start,t_end,is_noPerm)
! écriture dans le fichier NUM de Mage des dates de début et de fin de la simulation hydraulique
   ! -- prototype --
   character(len=*),intent(in) :: numFile
   real(kind=long),intent(in) :: t_start,t_end
   logical,intent(in) :: is_noPerm
   ! -- variables --
   character(len=11),parameter :: NUM = 'adis-ts.NUM'
   character(len=80) :: ligne(19)
   integer :: i, lu, ios

   !lecture et copie du fichier NUM original
   open(newunit=lu,file=trim(mage_WorkingDir)//trim(numFile),status='old',form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' >>>> Erreur : ouverture du fichier ',trim(numFile), 'impossible'
      stop 1204
   endif
   do i = 1, 19
      read(lu,'(a)') ligne(i)
   enddo
   close (lu)

   !écriture du fichier modifié pour Adis-TS
   open(newunit=lu,file=trim(mage_WorkingDir)//NUM,status='unknown',form='formatted')
   if (is_noPerm) ligne(1) = ligne(1)(:51)
   call heure(t_start,ligne(2)(42:))
   i = scan(ligne(2),'j',back=.true.) ; ligne(2) = ligne(2)(1:i-1)//ligne(2)(i+1:)
   call heure(t_end,ligne(3)(42:))
   i = scan(ligne(3),'j',back=.true.) ; ligne(3) = ligne(3)(1:i-1)//ligne(3)(i+1:)
   do i = 1,19
      write(lu,'(a)') trim(ligne(i))
   enddo
   close(lu)
end subroutine export_Mage_NUM


subroutine export_Mage_InitialState(modele,etat)
!exporte l'état hydraulique courant comme état initial pour MAGE
   ! -- Prototype --
   type(dataset),intent(in) :: modele
   type(state),intent(in) :: etat
   ! -- variables locales --
   integer :: lu, ib, is
   character(len=11),parameter :: iniFile = 'adis-ts.INI'

   !écriture du nouveau fichier INI
   open(newunit=lu,file=trim(mage_WorkingDir)//iniFile,status='unknown',form='formatted')
   write(lu,'(2a)') '* Initial state generated by Adis-TS for time ',date_et_heure(etat%time)
   do ib = 1, modele%net%ibmax
      do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
         write(lu,'(1x,i3,i4,2x,f10.5,f11.6,f11.3)') ib, is, etat%q(is), modele%sections(is)%zf+etat%y(is),modele%sections(is)%pk
      enddo
   enddo
   close(lu)
end subroutine export_Mage_InitialState


subroutine modify_repMage(key)
!remplace dans repMage la ligne définie par key par la ligne key adis-ts.key
   ! -- Prototype --
   character(len=3),intent(in) :: key
   ! -- Variables locales --
   integer :: lu, k, ios, j
   character(len=80),allocatable :: lignes(:)
   character(len=80) :: ligne
   character(len=3) :: ext
   character(len=11) :: filename
   logical :: bool
   logical, save :: first_call = .true.

   call capitalize(key,ext)
   filename = trim(basename)//'.'//ext
   j = len_trim(filename)
   open(newunit=lu,file=trim(mage_WorkingDir)//trim(repMage),status='old',form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' Erreur : ouverture de ', trim(mage_WorkingDir)//trim(repMage),' (fichier REP de MAGE) impossible'
      stop 1205
   endif

   if (first_call) then
      !1ere lecture pour compter les lignes : on peut se le permettre parce que le fichier est court
      !nbl : variable globale du module
      nbl = 0
      do
         read(lu,'(a)',iostat=ios) ligne
         if (ios /= 0) then
            exit
         else
            nbl = nbl+1
         endif
      enddo
      first_call = .false.
      rewind (lu)
   endif
   ! NOTE: pour l'instant on n'ajoute pas plus de 2 lignes à repMage
   allocate (lignes(nbl))

   !2e lecture pour stocker les lignes du fichier et commenter la ligne KEY
   bool = .true.
   do k = 1, nbl
      read(lu,'(a)',iostat=ios) lignes(k)
      if (ios /= 0) then
         write(error_unit,'(2a)') ' >>>> Erreur dans la relecture du fichier REP de Mage : ',trim(repMage)
         stop 1206
      else if (lignes(k)(1:3) == ext .and. lignes(k)(5:j+4) /= filename) then
         ligne = lignes(k)
         lignes(k)(1:10) = '*!adis-ts*'
         lignes(k)(11:) = trim(ligne)
      else if (lignes(k)(1:3) == ext .and. lignes(k)(5:j+4) == filename) then
         bool = .false.
      endif
   enddo

   !réécriture du fichier
   rewind (lu)
   do k = 1, nbl
      write(lu,'(a)') trim(lignes(k))
   enddo
   !écriture de la nouvelle ligne KEY à la fin du fichier
   if (bool) then
      write(lu,'(a)') ext//' '//basename//'.'//ext
      nbl = nbl+1 !lors de la prochaine modification il y aura une ligne de plus
   endif

   !fermeture du fichier et nettoyage
   close (lu)
   deallocate (lignes)
end subroutine modify_repMage


subroutine restore_repMage()
!restaure le fichier repMage dans son état initial
!il s'agit d'éliminer les lignes adis-ts.ext et
!de décommenter les lignes *!adis-ts*
   ! -- Prototype --
   ! -- Variables locales --
   integer :: lu, k, ios, j, nbl_plus
   character(len=80),allocatable :: lignes(:)
   character(len=80) :: ligne

   open(newunit=lu,file=trim(mage_WorkingDir)//trim(repMage),status='old',form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' >>>> Erreur : ouverture de ', trim(mage_WorkingDir)//trim(repMage), &
                               ' (fichier REP de MAGE) impossible'
      stop 1207
   endif
   allocate (lignes(nbl))
   !lecture pour stocker les lignes du fichier et rechercher les lignes commentées par Adis-TS
   j = len_trim(basename)
   do k = 1, nbl
      read(lu,'(a)',iostat=ios) lignes(k)
      if (ios /= 0) then
         write(error_unit,'(2a)') ' >>>> Erreur dans la relecture du fichier REP de Mage : ',trim(repMage)
         stop 1208
      else if (lignes(k)(1:10) == '*!adis-ts*') then
         !on rétablit les lignes modifiées par Adis-TS
         ligne = lignes(k)
         lignes(k) = trim(ligne(11:))
      endif
   enddo
   !réécriture du fichier
   rewind (lu)
   nbl_plus = 0 !compteur des lignes ajoutées dans REP
   do k = 1, nbl
      if (lignes(k)(5:j+4) == basename) then
         !on saute les lignes ajoutées par Adis-TS
         nbl_plus = nbl_plus + 1
         cycle
      else
         write(lu,'(a)') trim(lignes(k))
      endif
   enddo
   nbl = nbl - nbl_plus !restauration de la valeur initiale de nbl

   !fermeture et nettoyage
   close (lu)
   deallocate (lignes)

end subroutine restore_repMage


subroutine export_geometrie(modele,t)
!export de la géométrie : un bief dans un fichier au format ST
!le nom du fichier ST est un attribut de l'objet bief
   ! -- prototype --
   type (dataset), intent(in), target :: modele
   real(kind=long), intent(in) :: t
   ! -- variables locales --
   integer :: lu, ib, is, n, inull=0, ierr
   real(kind=long) :: xy_end = 999.999_long
   type (profil), pointer :: prof
   ! -- variables pour gérer le cache de conservation des états de la géométrie
   logical, save :: first_call = .true.
   integer :: nb_pts, nb_col
   integer, parameter :: nb_col_incr = 100
   real(kind=long), allocatable :: z_bak_tmp(:,:), export_time_tmp(:)

   if (first_call) then
      !comptage du nombre de points XYZ total
      nb_pts = 0
      do ib = 1, modele%net%ibmax
         do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
            prof => modele%sections(is)
            do n = 1, prof%np
               nb_pts = nb_pts +1
            enddo
         enddo
      enddo
      nb_col = floor(tmax/dtmage) + 2
      allocate(z_bak(nb_pts,nb_col),export_time(nb_col))
      nb_export = 1
      first_call = .false.
   else
      nb_export = nb_export + 1
      nb_col = size(z_bak,dim=2)
      if (nb_export > nb_col) then
         nb_pts = size(z_bak,dim=1)
         allocate(z_bak_tmp(nb_pts,nb_col+nb_col_incr),export_time_tmp(nb_col+nb_col_incr))
         z_bak_tmp(1:nb_pts,1:nb_col) = z_bak(1:nb_pts,1:nb_col)
         export_time_tmp(1:nb_col) = export_time(1:nb_col)
         call move_alloc(z_bak_tmp,z_bak)
         call move_alloc(export_time_tmp,export_time)
         nb_col= nb_col+nb_col_incr
      endif
   endif
   export_time(nb_export) = t

   nb_pts = 0
   do ib = 1, modele%net%ibmax
      open(newunit=lu,file=trim(modele%biefs(ib)%fichier_geo),form='formatted',status='old',iostat=ierr)
      if (ierr /= 0) then
         write(error_unit,'(3a)') '>>>> Erreur : ouverture de ', trim(modele%biefs(ib)%fichier_geo),' impossible'
         write(error_unit,'(a)')  '              --> Export du fichier de géométrie impossible'
         stop 1209
      endif
      write(lu,'(4a)') '# geometry file generated by Adis-TS for bief ',trim(modele%biefs(ib)%name),' at t = ',date_et_heure(t)
      write(lu,'(a)') '#'
      do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
         prof => modele%sections(is)
         !ligne d'entête du profil
         write(lu,'(4i6,f13.4,1x,a20)') is-modele%biefs(ib)%is1+1, inull, inull, prof%np, prof%pk, prof%name
         !coordonnées x,y,z du profil
         do n = 1, prof%np
            nb_pts = nb_pts + 1
            z_bak(nb_pts,nb_export) = prof%xyz(n)%z
            write(lu,'(2(f12.4,1x),f12.6,1x,a3,1x,i2)') prof%xyz(n)%x, prof%xyz(n)%y, prof%xyz(n)%z, prof%xyz(n)%tag, prof%xyz(n)%nc
         enddo
         write(lu,'(2(f12.4,1x),f12.6)') xy_end, xy_end, zero
      enddo
      close (lu)
   enddo
end subroutine export_geometrie


subroutine export_geometry_history(modele,t)
!export de l'historique de la géométrie : un bief dans un fichier adapté du format ST en ajoutant des colonnes pour les Z
!le nom du fichier ST est un attribut de l'objet bief
   ! -- prototype --
   type (dataset), intent(in), target :: modele
   real(kind=long), intent(in) :: t
   ! -- variables locales --
   integer :: lu, ib, is, n, inull=0, ierr
   type (profil), pointer :: prof
   integer :: nb_pts, k
   character(len=8), parameter :: suffix = '_history'
   character(len=17) :: fmt
   character(len=fnl) :: filename

   write(error_unit,*) 'Export de l''historique de la géométrie'
   nb_pts = 0
   write(fmt,'(a,i0,a)') '(',nb_export+2,'f15.6,1x,a3)'
   do ib = 1, modele%net%ibmax
      k = scan(modele%biefs(ib)%fichier_geo,slash,.true.)
      filename = trim(outdir)//slash//trim(modele%biefs(ib)%fichier_geo(k+1:))//suffix
      open(newunit=lu,file=trim(filename),form='formatted',status='unknown',iostat=ierr)
      if (ierr /= 0) then
         write(error_unit,'(3a)') ' >>>> Erreur : ouverture de ', trim(filename),' impossible'
         write(error_unit,'(a)')  '               --> export du fichier d''historique de la géométrie impossible'
         stop 1210
      endif
      write(lu,'(4a)') '# History of geometry generated by Adis-TS for bief ',trim(modele%biefs(ib)%name), &
                       ' at t = ',date_et_heure(t)
      write(lu,'(a)') '#'
      write(lu,'(a,29x,9999a)') '#',(date_et_heure(export_time(n)),n=1,nb_export)
      do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
         prof => modele%sections(is)
         !ligne d'entête du profil
         write(lu,'(a1,4i6,f13.4,1x,a20)') '#',is-modele%biefs(ib)%is1+1, inull, inull, prof%np, prof%pk, prof%name
         !coordonnées x,y,z du profil
         do n = 1, prof%np
            nb_pts = nb_pts + 1
            write(lu,fmt) prof%xyz(n)%x, prof%xyz(n)%y, (z_bak(nb_pts,k),k=1,nb_export), prof%xyz(n)%tag
         enddo
      enddo
      close (lu)
   enddo
end subroutine export_geometry_history

end module couplage_MAGE
