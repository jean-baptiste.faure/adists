
import numpy as np
import sys
import matplotlib.animation as animation
import matplotlib.pyplot as plt

class real_result:
    def __init__(self, filename = ''):
        self.time = []
        self.values = []

    def append(self,t,v):
        self.time.append(t)
        self.values.append(v)


class data:
    def __init__(self, filename = ''):
        self.filename = filename
        if self.filename[-4:] == '.bin' or self.filename[-4:] == '.BIN':
            self.name = self.filename[:-4]
        else:
            self.name = self.filename
        self.values = {}
        self.values['C']=real_result()
        self.values['G']=real_result()
        self.values['M']=real_result()
        self.values['D']=real_result()
        self.values['L']=real_result()
        self.values['N']=real_result()
        self.values['R']=real_result()
        self.raw_data = []
        print("reading ",filename)
        with open(filename,'rb') as f:
            # header
            # first line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
            data = np.fromfile(f, dtype=np.int32, count=3)
            self.ibmax = data[0] # number of reaches
            self.ismax = data[1] # total number of cross sections
            self.kbl = data[2] * -1 # block size for .BIN header
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
            # second line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
            self.ibu = np.fromfile(f, dtype=np.int32, count=self.ibmax) # hydraulic rank of reaches (FORTRAN numbering)
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
            # third line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
            data = np.fromfile(f, dtype=np.int32, count=2*self.ibmax)
            self.is1 = np.zeros(self.ibmax,dtype=np.int32)
            self.is2 = np.zeros(self.ibmax,dtype=np.int32)
            for i in range(self.ibmax):
                self.is1[i] = data[2*i]  # first section of reach i (FORTRAN numbering)
                self.is2[i] = data[2*i+1] # last section of reach i (FORTRAN numbering)
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
            # fourth line
            self.pk = np.zeros(self.ismax,dtype=np.float32)
            for k in range(0, self.ismax, self.kbl):
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
                self.pk[k:min(k+self.kbl,self.ismax)] = np.fromfile(f, dtype=np.float32, count=min(k+self.kbl,self.ismax)-k)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
            # fifth line (useless)
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
            zmin_OLD = np.fromfile(f, dtype=np.float32, count=1)[0]
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
            # sixth line
            z = np.zeros(self.ismax*3,dtype=np.float32)
            for k in range(0, self.ismax, self.kbl):
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
                z[3*k:3*min(k+self.kbl,self.ismax)] = np.fromfile(f, dtype=np.float32, count=3*(min(k+self.kbl,self.ismax)-k))
                # z[i*3+1] and z[i*3+2] are useless
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
            self.zf = [z[i*3] for i in range(self.ismax)]
            # seventh line (useless)
            for k in range(0, self.ismax, self.kbl):
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
                zero = np.fromfile(f, dtype=np.int32, count=self.ismax)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
            # end header
            # data
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
            while data.size > 0:
                ismax = np.fromfile(f, dtype=np.int32, count=1)[0]
                t = np.fromfile(f, dtype=np.float64, count=1)[0]
                c = np.fromfile(f, dtype=np.byte, count=1)
                # possible values :
                # sediment : C, G, M, D, L, N, R
                # polutant : C
                print("t=", str(t), bytearray(c).decode())
                real_data = np.fromfile(f, dtype=np.float32, count=self.ismax)
                self.values[bytearray(c).decode()].append(t, real_data)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (end)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes) (start)
            # end data

def animate(i):
    l = []
    for j in range(len(lines)):
      x = res.pk[res.is1[j]-1:res.is2[j]]
      lines[j][0].set_data(x, values[i][res.is1[j]-1:res.is2[j]])
      l.append(lines[j][0])
    return l

if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = str(sys.argv[1])
    if len(sys.argv) > 2:
        val = str(sys.argv[2])
    else :
        print("ERROR: no input file name given")
        exit()

    res = data(filename)

    time = res.values[val].time
    values = res.values[val].values

    nt = len(time)

    fig = plt.figure()
    mm = 0.0
    jj = 0
    for j in range(nt):
        #m = max(max([r.values['Z'].values[j] for r in res]))
        m = max(values[j])
        if m > mm:
            jj = j
            mm = m

    lines = []
    line, = plt.plot(res.pk, values[jj])
    line2, = plt.plot(res.pk, [0]*len(res.pk))
    #line3, = plt.plot(res.pk, values[0]) ! condition initiale
    line.set_alpha(0.0)
    line2.set_alpha(0.0)
    for bief in range(res.ibmax):
        lines.append(plt.plot([], []))
    #if res.mage_version > 80:
        #line3 = plt.plot(res.pk, res.zfd)
    #else:
        #line3 = plt.plot(res.pk, res.zfd + res.zmin)
    fig.tight_layout()

    ani = animation.FuncAnimation(fig, animate, frames=nt,
                              interval=100, blit=True, repeat=True)
    plt.show()
