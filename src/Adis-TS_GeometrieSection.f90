!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module Geometrie_Section
!===============================================================================
!              Données géométriques des sections (profils en travers)
!
! numérotation des STOP : 15nn
!
!---> Sommaire
!--------> types dérivés
!  - pointAC : couple abscisse - cote ; utilisé pour profilAC
!  - pointLC : point de tabulation largeur - cote ; 4 valeurs :
!              - tirant d'eau
!              - largeur au miroir
!              - section mouillée
!              - périmètre mouillé
!  - pointTS : point nommé en coordonnées géographiques
!  - pointTS  : données géométriques brutes, y compris les couches sédimentaires
!  - profilAC : profil abscisses-cotes : liste de pointAC + Pk
!  - profil   : profil TS complet avec :
!               - nom, Pk, cote du fond et Strickler global
!               - données géométriques brutes :
!                         - tableau de pointTS (+ nb de points)
!                         - découpage en lits (+ nb de lits)
!               - données tabulées en largeurs-cotes pour la couche supérieure
!                         - tableau de pointLC (+ nb de points)
!                         - drapeau (booléen) pour savoir si la géométrie LC
!                           correspond à la géométrie XYZ (modification possible)
!--------> Variables
!  - ISmax : nb de sections de calcul
!  - jeuSection : tableau de profil ; collection de tous les profils
!
!--------> Fonctions
!  - distance2D : distance euclidienne entre 2 pointAC
!  - distance3D : distance euclidienne entre 2 pointTS
!  - extractXYZ : extraction d'un pointTS d'un pointTS
!  - largeur    : interpolation de la largeur au miroir dans un profil TS
!                 pour une cote donnée
!  - newProfil  : création d'un profil TS à partir d'un tableau de points TS
!                 et d'un pk
!  - perimetre  : interpolation du périmètre mouillé dans un profil TS
!                 pour une cote donnée
!  - projectionOrtho : projection orthogonale du point xyz sur la droite [xyz1,xyz2]
!  - projectionPlan  : conversion d'un profil XYZ en profil AC par projection
!                      sur le plan joignant les 2 points extrêmes
!  - removeDoublons  : suppression des doublons, c'est à dire les points
!                      identiques et successifs
!  - section         : interpolation de la section mouillée dans un profil TS
!                      pour une cote donnée
!--------> Subroutines
!  - addBief       : ajoute un bief à la collection jeuSection
!  - copyProfil    : copie un profil complet ; remplace l'affectation
!  - largeursCotes : tabulation d'un profil en travers à partir de ses
!                    données brutes
!  - lireSediment  : lecture d'un fichier de données sédimentaires
!  - newBief       : construction d'un jeu de profils TS par lecture d'un fichier
!                    de profils XYZ au format ST2 (couches sédimentaires
!                    comprises) et d'un fichier des classes granulométriques
!                    (CGfile lu par lireSediment)
!  - removeProfil  : désallocation d'un profil TS
!  - triZ          : classement d'un profil LC par Z croissants avec élimination
!                    des doublons
!
!
!--> historique des modifications
! 06/02/2007 : création à partir de Adis_sb_Common.for
! 27/02/2007 : complément de profil avec les indices donnant les berges rg et rd
!              ces points doivent être précisés avant projection rg <-> rd pour
!              créer les tableaux yg et yd
!              Création de la routine largeur pour interpoler les largeurs
!              à compléter par perimetre et section
! 20/04/2007 : données de géométrie brute avec liste des couches sédimentaires
!              remplacement des lits mineur/moyen/majeur par une liste de lits
!              création des types pointTS et pointLC
!              routine tabuler à implémenter
! ??/10/2007 : création des routines d'initialisation, projection et tabulation
!              création des routines d'interpolation
!              tests unitaires
! 07/11/2007 : routine lireSediment() et lecture des données sédimentaires
! NOTE: format de fichier ST étendu (géométrie)
! 25/01/2008 : format de fichier ST étendu : ~idem ST (Mocahy) plus des informations
!                                             sur les couches sédimentaires
!              Ligne d'entête du profil : (4i6,2f13.0,a20)
!                 n°_profil, null, null, nb_points, pk, Ks, nom_Profil
!                 pk est le PK du profil
!                 Ks est un strickler global pour le profil
!              Pour chaque point XYZ : (3f13.0,1x,a3,1x,i2,4x,20(f10.0,i10))
!                 X, Y, Z, tag, nb_couches, (zc(i), sediment_ID(i) , i=1,nb_couches)
!                 zc(i) est la cote plafond de la couche i
!                 sediment_ID(i) est l'étiquette du sédiment dans le fichier SED
! NOTE: format du fichier SED (sédiments)
! 18/05/2016 : format du fichier SED
!              le fichier SED est une liste de lignes de la forme suivante :
!              label, d50, sigma, tau
!              où label est l'étiquette du sédiment à utiliser dans ST2
!===============================================================================

use iso_fortran_env, only: output_unit, error_unit

use parametres, only: long, precision_alti, ncmax, slash, defaut
use Adis_Types, only: dataset, profil, profilAC, sediment, pointTS, pointAC, pointLC
use utilitaires, only: between, do_crash, next_int, next_real, next_string
implicit none


contains

subroutine addBief(profils, debut, fin, jeuSection, ismax)
!==============================================================================
!        ajoute un bief à la collection jeuSection et met à jour ISmax
!
!entrée : profils ; tableau de type profil : la liste des profils qui constituent
!         le nouveau bief
!         ISmax : la valeur courante
!         jeuSection : collection des profils
!sortie : debut, fin ; index de début et de fin de la sous-liste des
!         nouveaux profils dans jeuSection
!         jeuSection(debut) = profils(1) et jeuSection(fin) = profils(size(profils))
!==============================================================================
   ! prototype
   type (profil), intent(in), allocatable :: profils(:)
   type (profil), intent(inout) :: jeuSection(:)
   integer, intent(out) :: debut, fin
   integer, intent(inout) :: ismax
   ! variables locales
   integer :: nb_profils, is

   if (allocated(profils)) then
      nb_profils = size(profils)
      debut = ismax+1  ;  fin = ismax+nb_profils
   else
      write(output_unit,*) "addBief : profils() non alloué, appeler newBief en premier"
      stop 1501
   endif

   do is = 1, nb_profils
      jeuSection(ismax+is) = profils(is)
   enddo
   ismax = fin
end subroutine addBief



subroutine newBief(STfile, bief, les_Sediments)
!==============================================================================
!    construction d'un jeu de profils TS par lecture d'un fichier de
!    profils XYZ au format ST2 (couches sédimentaires comprises)
!
!    Les données sédimentaires sont dans le tableau les_Sediments()
!
!==============================================================================
   ! prototype
   character(len=*), intent(in) :: STfile
   type (profil), intent(out), allocatable, target :: bief(:)
   type (sediment), intent(in), optional :: les_Sediments(:)
   ! variables locales
   character(len=250) :: ligne
   character(len=3) :: tag
   character(len=10) :: chc(2*ncmax)
   character(len=20) :: nomProfil
   integer :: luu ! unité logique
   integer :: ierr, nligne, nprof, np, i, j, nc, inull, reversed, nf
   real(kind=long) :: x, y, z
   real(kind=long) :: pk
   real(kind=long) :: zc(ncmax)
   type (pointTS), allocatable :: xyz(:)
   logical :: bFail
   character(len=7) :: fmt
   logical :: charriage, rightToleft

   !on commence par tester la présence de les_Sediments pour savoir si on traite le charriage
   !si on ne s'occupe pas du charriage alors on ignore les couches sédimentaires
   charriage = present(les_Sediments)
   !lecture des données géométrique (STfile)
   open(newunit=luu,file=trim(STfile),status='old',form='formatted',iostat=ierr)
   if (ierr > 0) then
      write(output_unit,*) '>>>> Ouverture du fichier ST ',trim(STfile),' impossible'
      stop 1502
   else
      write(output_unit,*) 'Lecture du fichier de données géométriques : ',trim(STfile)
   endif
   nligne=0  !nbligne est le nombre de lignes du fichier de description des casiers

   if (allocated(bief)) deallocate(bief)

   !1ere lecture du fichier pour compter les profils et allouer la mémoire
   nprof = 0
   do !1ère lecture du fichier : comptage des profils
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
         write(error_unit,*) '>>>> Erreur 1 de lecture de ',STfile,' a la ligne ',nligne
         stop 1503
      elseif (ierr < 0) then
         exit
      else
         nligne = nligne+1
         if (ligne(1:1) /= '#' .and. ligne(1:1) /= '*') then
            !read(ligne,'(3(1x,f14.0),1x,a3)') x, y!, z, tag
            nf = 1  ;  x = next_real(ligne,'',nf)
                       y = next_real(ligne,'',nf)
            if (abs(x-999.999)+abs(y-999.999) < 0.001) nprof = nprof+1
         endif
      endif
   enddo

   !lecture des profils dans le fichier
   allocate (bief(nprof))
   rewind (luu)
   reversed = 0 !compteur de profils donnés de rive droite à rive gauche
   do !recherche de la ligne d'entête du 1er profil
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
         write(error_unit,*) '>>>> Erreur 2 de lecture de ',STfile,' a la ligne ',nligne
         stop 1504
      elseif (ierr < 0) then
         exit
      else
         if (ligne(1:1) == '#' .or. ligne(1:1) == '*') then
            cycle
         else
            exit
         endif
      endif
   enddo
   if (ierr < 0) then  !fin de fichier !!!
      write(error_unit,*) '>>>> Erreur 3 de lecture de ',STfile(1:len_trim(Stfile)),' : fin de fichier !!!'
      stop 1505
   endif
   ! ici on a la ligne d'entête du 1er profil
   nprof = 0
   do while (ierr == 0) !décodage du fichier
      !décodage de la ligne d'entête de section
      read(ligne,'(4i6,f13.0)') inull,inull,inull,np,pk
      x = inull  !ne sert à rien sauf à faire disparaître un avertissement sur inull (set but not used)
      nprof = nprof + 1  ! nouveau profil
      nomProfil = ligne(39:58)
      if (len_trim(nomProfil) == 0) then
         i = len_trim(STfile)
         j = scan(STfile,slash,back=.true.)+1
         write(nomProfil,'(2a,i6.6)') STfile(j:i),'&', int(pk)
      endif
      allocate (xyz(np))
      x = 0._long  ;  y = 0._long  ;  np = 0
      do   !lecture des coordonnées
         read(luu,'(a)') ligne
         if (ligne(1:1) /= '#' .and. ligne(1:1) /= '*') then
            chc = '' ; zc = 0._long
            !lecture de la ligne en 2 fois pour vérifier la valeur de nc
!            read(ligne,'(3(1x,f14.0),1x,a3,1x,i2)') x, y, z, tag, nc
            nf = 1  ;  x = next_real(ligne,'',nf)
                       y = next_real(ligne,'',nf)
                       z = next_real(ligne,'',nf)
                       tag = next_string(ligne,'',nf)
                       nc = next_int(ligne,'',nf)
            if (.not.charriage) nc = 0 !si pas de charriage on ignore les couches sédimentaires
            if (nc > ncmax) then
               write(output_unit,*) '>>>> Erreur : le nombre de couche sédimentaires est limité à ',ncmax
               write(output_unit,*) trim(ligne)
               stop 1506
            else if (nc > 0) then  ! rien à lire si nc = 0
               write(fmt,'(a1,i2.2,a4)') '(',2*nc,'a10)'
               read(ligne(51:),fmt) (chc(i) ,i=1,2*nc)
            endif
            if (nc == 0) then
               ! valeurs par défaut pour pouvoir utiliser les fichiers ST.
               nc = 1
               zc(1) = -9999._long
               chc(2) = defaut
            else
               do i = 1, nc
                  read(chc(2*i-1),'(f10.0)') zc(i)
               enddo
            endif
            if (abs(x-999.999)+abs(y-999.999) > 0.001) then
               np = np+1
               xyz(np)%x = x  ;  xyz(np)%y = y  ;  xyz(np)%z = z
               xyz(np)%tag = tag
               xyz(np)%nc = nc
               if (charriage) then  !si pas de charriage on ignore les couches sédimentaires
                  bFail = .false.
                  do i = 1, nc
                     xyz(np)%zc(i) = zc(i)
                     !vérification de l'empilement des couches sédimentaires
                     if (i == 1) then
                        if (zc(i) > xyz(np)%z) then
                           write(output_unit,*) '>>>> Erreur : le plancher de la 1ère couche est au-dessus de la cote du fond'
                           write(output_unit,*) trim(ligne)
                           bFail = .true.
                        endif
                     else
                        if (zc(i) > zc(i-1)) then
                           write(output_unit,*) '>>>> Erreur : le plancher de la couche ',i, &
                                                ' est au-dessus de celui de la couche ',i-1
                           write(output_unit,*) trim(ligne)
                           bFail = .true.
                        endif
                     endif
                     !Affectation des paramètres sédimentaires
                     do j = 1, size(les_Sediments)
                        if (trim(chc(2*i)) == trim(les_Sediments(j)%tag)) then
                           xyz(np)%d50(i) = les_Sediments(j)%d50
                           xyz(np)%sigma(i) = les_Sediments(j)%sigma
                           xyz(np)%tau(i) = les_Sediments(j)%tau
                        endif
                     enddo
                  enddo
                  if (bFail) then
                     write(output_unit,*) '>>>> Erreur dans la définition des couches sédimentaires'
                     stop 1507
                  endif
               endif
            else
               exit
            endif
         endif
      enddo
      bief(nprof) = newProfil(pk,removeDoublons(xyz),rightToleft)
      if (rightToleft) reversed = reversed+1
      deallocate (xyz)

      bief(nprof)%name = nomProfil
      read(luu,'(a)',iostat=ierr) ligne  !lecture de la ligne d'entête de section suivante
   enddo
   if (reversed > 0) then
      write(output_unit,'(a,i0,a)') ' >>>> ',reversed,' profil(s) ont été donnés de rive droite à rive gauche'
      write(output_unit,'(a)') '      Ces profils ont été retournés sans modifier le fichier de données'
   endif
   close (luu)
end subroutine newBief



function newProfil(pk,xyz,rightToleft)
!==============================================================================
!      création d'un profil TS à partir d'un tableau de points TS
!        dont on a déjà éliminé les doublons
!
!actions : copie des données brutes
!          affectation du pk
!          calcul et affectation de la cote du fond
!          tabulation largeurs-cotes
!          identification des lits
!
!entrée : pk, pk du profil
!         xyz, tableau de pointTS
!sortie : un profil
!
!==============================================================================
   !prototype
   type (pointTS), intent(in) :: xyz(:)
   real(kind=long), intent(in) :: pk
   logical, intent(out) :: rightToleft
   type (profil) :: newProfil
   !variables locales
   integer :: np, n, irg, ird, ibg, ibd, nl, ifg, ifd
   type (pointTS) :: p3d

   newProfil%pk = pk
   newProfil%zf = minval(xyz(:)%z)
   newProfil%iss = 0

   !allocation
   np = size(xyz)
   newProfil%np = np
   allocate(newProfil%xyz(np))
   newProfil%xyz(1:np) = xyz(1:np)

   !tabulation largeurs-cotes
   call largeursCotes(newProfil)

   !recherche des points RG et RD (par défaut on suppose le profil donné de RG vers RD)
   irg = -1  ;  ird = -np  ;  ifg = -1
   ibg = -1  ;  ibd = -np  ;  ifd = -1
   do n = 1, np
      ! NOTE: les points BG et BD servent à définir les largeurs utiles %l_g et %l_d sur lesquelles on va répartir les dépôts en lit moyen
      select case (trim(newProfil%xyz(n)%tag))
         case ('RG','rg','Rg','rG')  !Rive Gauche : débordement mineur - moyen gauche
            irg = n
         case ('RD','rd','Rd','rD')  !Rive Droite : débordement mineur - moyen droit
            ird = n
         case ('BG','bg','Bg','bG')  !Berge Gauche : 1er point ou point étiqueté BG
            ibg = n
         case ('BD','bd','Bd','bD')  !Berge Droite : dernier point ou point étiqueté BD
            ibd = n
         case ('FG','fg','Fg','fG')  !Fond Gauche : point étiqueté FG, pied de la berge gauche du mineur
            ifg = n
         case ('FD','fd','Fd','fD')  !Fond Droit : point étiqueté FD, pied de la berge droite du mineur
            ifd = n
         case default
      end select
   enddo

   !on ajoute les tags s'ils n'ont pas été définis
   if (irg < 0) then
      irg = abs(irg)
      newProfil%xyz(irg)%tag = 'RG'
   endif
   if (ird < 0) then
      ird = abs(ird)
      newProfil%xyz(ird)%tag = 'RD'
   endif
   if (ibg < 0) then
      ibg = abs(ibg)
      newProfil%xyz(ibg)%tag = 'BG'
   endif
   if (ibd < 0) then
      ibd = abs(ibd)
      newProfil%xyz(ibd)%tag = 'BD'
   endif

   !cas d'un profil donné de la rive droite à la rive gauche
   if (irg > ird) then
      rightToleft = .true.
      ! le profil est donné de RD vers RG, il faut le retourner
      !write(output_unit,*) '>>>> le profil du pk ', pk, ' est donné de RD vers RG, on le retourne'
      do n = 1, np/2
         p3d = newProfil%xyz(n)
         nl = np-n+1
         newProfil%xyz(n) = newProfil%xyz(nl)
         newProfil%xyz(nl) = p3d
      enddo
      irg = np-irg+1  ;  ird = np-ird+1
      if (ibg > ibd) then
         ibg = np-ibg+1  ;  ibd = np-ibd+1
      endif
      if (ifg > ifd) then
         ifg = np-ifg+1  ;  ifd = np-ifd+1
      endif
   else
      rightToleft = .false.
   endif

   ! NOTE: si FG et FD ne sont pas définis, on les place juste à coté de RG et RD dans le lit mineur
   ! si le profil était donné de droite à gauche il a déjà été retourné
   if (ifg < 0) then
      ifg = irg + 1
      newProfil%xyz(ifg)%tag = 'FG'
   endif
   if (ifd < 0) then
      ifd = ird - 1
      newProfil%xyz(ifd)%tag = 'FD'
   endif

   newProfil%ibg = ibg  ;  newProfil%ibd = ibd
   newProfil%irg = irg  ;  newProfil%ird = ird
   newProfil%ifg = ifg  ;  newProfil%ifd = ifd

   ! calcul des largeurs
   newProfil%l_g = distanceH(newProfil%xyz(ibg),newProfil%xyz(irg))
   newProfil%l_m = distanceH(newProfil%xyz(ifg),newProfil%xyz(ifd))
   newProfil%l_d = distanceH(newProfil%xyz(ird),newProfil%xyz(ibd))

end function newProfil



subroutine removeProfil(prf)
!==============================================================================
!     désalloue les tableaux avec allocation dynamique d'un profil
!
!entrée : prf, un profil (type profil)
!sortie : prf
!
!==============================================================================
   !prototype
   type (profil), intent(inout) :: prf

   deallocate(prf%xyz, prf%wh)
end subroutine removeProfil



subroutine largeursCotes(un_profil)
!==============================================================================
!        tabulation d'un profil en travers à partir de ses données brutes
!
!entrée : un_profil%xyz, les données brutes du profil (type profil)
!sortie : un_profil%wh, les données tabulées en largeurs-cotes du profil
!
!==============================================================================
   ! prototype
   type(profil),intent(inout) :: un_profil
   type (profilAC) :: pAC
   ! variables locales
   real(kind=long) :: z0, ll, pp, dy, dz, l1, l2, dh, hh
   integer :: i, j, np, point1
   type (pointAC) :: pc1, pc2 !limites gauche et droite du lit courant

   !initialisation du profil AC
   pAC = projectionPlan(un_profil)
   !arrondi à la précision precision_alti
   do i = 0, pAC%np+1
      pAC%yz(i)%z = anint(pAC%yz(i)%z / precision_alti , long) *  precision_alti
   enddo
   !initialisation du profil LC
   np = pAC%np
   un_profil%nh = np+1
   if(.not.allocated(un_profil%wh)) allocate(un_profil%wh(1:np+1))

   !décalage des cotes pour les zones plates
   !on suppose que l'utilisateur ne peut pas définir des cotes avec une précision
   !inférieure à precision_alti
   do i = 2, pAC%np-1
      if (abs(pAC%yz(i)%z - pAC%yz(i+1)%z) < precision_alti) then
         pAC%yz(i+1)%z = pAC%yz(i+1)%z - 2.0_long*precision_alti
         !il faut enlever 2*precision_alti et non ajouter sinon le calcul du
         !périmètre mouillé ne fonctionne pas !!! (JBF 2010-12-17)
      endif
   enddo

   !recherche des largeurs
   do i = 1, pAC%np  ! boucle sur toutes les altitudes du profil
      z0 = pAC%yz(i)%z
      ll = 0._long      !initialisation de la largeur au miroir cherchée
      pp = 0._long      !initialisation du périmètre mouillé
      point1 = -1
      do j = 1, pAC%np   !on cherche les points du profil qui encadrent cette altitude
         if (abs(z0-pAC%yz(j)%z) < precision_alti) then
            if (point1 < 0) then !nouveau lit
               if (pAC%yz(j+1)%z > z0) then
                  point1 = -1  ! branche montante : on est dans un minimum local du profil
               else
                  point1 = j  ;  pc1 = pAC%yz(j)
               endif
            else                 ! fin du lit en cours
               pc2 = pAC%yz(j)
               pp = pp + distance2D(pAC%yz(j-1),pc2)
               ll = ll + (pc2%y - pc1%y)
               if (z0 >= pAC%yz(j-1)%z .and. z0 > pAC%yz(j+1)%z) then
                  point1 = j  !nouveau lit : z0 est un sommet local
                  pc1 = pAC%yz(j)
               else
                  point1 = -1
               endif
            endif
         else if ( between(z0,pAC%yz(j-1)%z,pAC%yz(j)%z) ) then
            if (point1 < 0) then  !nouveau lit
               point1 = j-1
               dy = pAC%yz(j)%y - pAC%yz(j-1)%y
               dz = pAC%yz(j)%z - pAC%yz(j-1)%z
               pc1%y = pAC%yz(j-1)%y + (z0-pAC%yz(j-1)%z)*dy/dz
               pc1%z = z0
               pp = pp + distance2D(pc1,pAC%yz(j))
            else                  !fin du lit courant
               dy = pAC%yz(j)%y - pAC%yz(j-1)%y
               dz = pAC%yz(j)%z - pAC%yz(j-1)%z
               pc2%y = pAC%yz(j-1)%y + (z0-pAC%yz(j-1)%z)*dy/dz
               pc2%z = z0
               ll = ll + (pc2%y-pc1%y)
               pp = pp + distance2D(pc2,pAC%yz(j-1))
               point1 = -1
            endif
         else if (z0 >= pAC%yz(j-1)%z .and. z0 >= pAC%yz(j)%z) then
            if (point1 >= 0) pp = pp + distance2D(pAC%yz(j-1),pAC%yz(j))  !NB: point1 peut être nul
         else if (z0 <= pAC%yz(j-1)%z .and. z0 <= pAC%yz(j)%z) then
            continue
         else if (abs(z0-pAC%yz(j-1)%z) < precision_alti) then
            continue
         else
            write(output_unit,*) '>>>> Erreur : On ne devrait pas arriver ici !!!'
            write(output_unit,*) un_profil%pk, z0, pAC%yz(j-1)%z, pAC%yz(j)%z
            stop 1508
         endif
      enddo
      if (point1 > 0) then
         ll = ll + pAC%yz(np+1)%y - pc1%y
         pp = pp + z0 - pAC%yz(np)%z
      endif
      un_profil%wh(i)%h = z0 - un_profil%zf
      un_profil%wh(i)%l = ll
      un_profil%wh(i)%p = pp
   enddo
   un_profil%wh(np+1)%h = pAC%yz(0)%z-un_profil%zf
   un_profil%wh(np+1)%l = pAC%yz(np+1)%y
   !il faut classer le profil avant de mettre à jour le dernier périmètre mouillé
   call triZ(un_profil%wh,np)  !classement du profil LC par Z croissants
                               !np est modifié par triZ() !!!
   un_profil%wh(np)%p = un_profil%wh(np-1)%p +200._long
   un_profil%nh = np

   !élimination du premier point s'il est à la même cote que le 2ème (fond plat)
   !on suppose que l'utilisateur ne peut pas définir des cotes avec une précision
   !inférieure à precision_alti
   if (abs(un_profil%wh(1)%h-un_profil%wh(2)%h) < precision_alti) then
      hh = un_profil%wh(1)%h
      do i = 1, un_profil%nh-1
         un_profil%wh(i) = un_profil%wh(i+1)
      enddo
      un_profil%wh(1)%h = hh
      un_profil%nh = un_profil%nh - 1
   endif

   !calcul des sections mouillées par la méthode des trapèzes
   do i = 1, un_profil%nh
      un_profil%wh(i)%s = 0._long
   enddo
   do i = 2, un_profil%nh
      l1 = un_profil%wh(i-1)%l  ;  l2 = un_profil%wh(i)%l
      dh = un_profil%wh(i)%h - un_profil%wh(i-1)%h
      un_profil%wh(i)%s = un_profil%wh(i-1)%s + 0.5_long*(l1+l2)*dh
      !le périmètre mouillé ne peut pas être décroissant
      if (un_profil%wh(i)%p < un_profil%wh(i-1)%p) then
         write(output_unit,*) '>>>> Erreur dans la conversion en largeurs-cotes : périmètre mouillé décroissant'
         write(output_unit,*) ' Profil : ',un_profil%pk
         write(output_unit,*) un_profil%zf+un_profil%wh(i-1)%h, un_profil%wh(i-1)
         write(output_unit,*) un_profil%zf+un_profil%wh(i)%h, un_profil%wh(i)
         write(output_unit,*) ' >>>> profil largeurs-cotes complet :'
         do j = 1, un_profil%nh
            write(output_unit,*) un_profil%zf+un_profil%wh(j)%h, un_profil%wh(j)
         enddo
         write(output_unit,*) ' >>>> profil abscisses-cotes complet :'
         do j = 0, pAC%np+1
            write(output_unit,*)  pAC%yz(j), pAC%yz(j)%z-un_profil%zf
         enddo
         stop 1509
      endif
   enddo

   un_profil%wh_aJour = .true.
   un_profil%ip = 1
   deallocate(pAC%yz)
end subroutine largeursCotes



subroutine triZ(wh,nh)
!==============================================================================
!   classement d'un profil LC par Z croissants avec élimination des doublons
!   les doublons sont déportés au sommet de la liste et "oubliés" en réduisant nh

!entrée : wh, tableau de pointLC (largeurs-cotes)
!sortie : le tableau wh trié par cotes croissantes
!         nh, entier, la taille utile du tableau wh
!
!==============================================================================
   !prototype
   type (pointLC), intent(inout) :: wh(:)
   integer, intent(out) :: nh
   !variables locales
   integer :: i, nbperm, np
   real(kind=long) :: h0, l0, p0, s0

   !tri
   np = size(wh)  ;  nh = np
   nbperm = 1
   do while (nbperm > 0)
      nbperm = 0
      do i = 1, nh-1
         if (wh(i)%h > wh(i+1)%h) then  !permutation
            nbperm = nbperm + 1
            h0 = wh(i)%h ; l0 = wh(i)%l ; p0 = wh(i)%p ; s0 = wh(i)%s
            wh(i)%h = wh(i+1)%h  ;  wh(i+1)%h = h0
            wh(i)%l = wh(i+1)%l  ;  wh(i+1)%l = l0
            wh(i)%p = wh(i+1)%p  ;  wh(i+1)%p = p0
            wh(i)%s = wh(i+1)%s  ;  wh(i+1)%s = s0
         else if (abs(wh(i)%h - wh(i+1)%h) < precision_alti) then
            !élimination des doublons
            wh(i+1)%h = wh(i+1)%h + 9999._long
            nh = nh -1
         endif
      enddo
   enddo
end subroutine triZ



function distance3D(xyz1, xyz2)
!==============================================================================
!           distance euclidienne entre 2 pointTS

!entrée : xyz1 et xyz2, 2 pointTS
!sortie : la distance euclidienne entre ces deux points
!
!==============================================================================
   ! prototype
   type (pointTS), intent(in) :: xyz1, xyz2
   real(kind=long) :: distance3D

   distance3D = sqrt((xyz2%x - xyz1%x)**2 + (xyz2%y - xyz1%y)**2 + (xyz2%z - xyz1%z)**2)
end function distance3D



function distanceH(xyz1, xyz2)
!==============================================================================
!           distance euclidienne horizontale entre 2 pointTS

!entrée : xyz1 et xyz2, 2 pointTS
!sortie : la distance euclidienne entre ces deux points
!
!==============================================================================
   ! prototype
   type (pointTS), intent(in) :: xyz1, xyz2
   real(kind=long) :: distanceH

   distanceH = sqrt((xyz2%x - xyz1%x)**2 + (xyz2%y - xyz1%y)**2)
end function distanceH



function distance2D(yz1, yz2)
!==============================================================================
!                   distance euclidienne entre 2 pointAC

!entrée : yz1 et yz2, 2 pointAC
!sortie : la distance euclidienne entre ces deux points
!
!==============================================================================
   ! prototype
   type (pointAC), intent(in) :: yz1, yz2
   real(kind=long) :: distance2D

   distance2D = sqrt((yz2%y - yz1%y)**2 + (yz2%z - yz1%z)**2)
end function distance2D



function projectionOrtho(xyz1, xyz2, xyz) result(yz)
!==============================================================================
!     projection orthogonale du point xyz sur la droite [xyz1,xyz2]
!
! NB : dans cette version on ne tient pas compte de la pente du profil en long

!entrée : xyz1, xyz2 et xyz, 3 pointTS
!sortie : la projection orthogonale, de type pointAC
!
!==============================================================================
   ! prototype
   type (pointTS), intent(in) :: xyz1, xyz2, xyz
   type (pointAC) :: yz
   ! variables locales
   real(kind=long) :: x21, y21, a

   ! calcul
   x21 = xyz2%x - xyz1%x
   y21 = xyz2%y - xyz1%y
   a   = x21*x21 + y21*y21
   if (a > 0.000001_long) then
      yz%y = ((xyz%x - xyz1%x)*x21 + (xyz%y - xyz1%y)*y21) / sqrt(a)
   else
      yz%y = 0._long
   endif
   yz%z = xyz%z
end function projectionOrtho



function projectionPlan(pXYZ) result(pAC)
!==============================================================================
!        conversion d'un profil XYZ en profil AC par projection
!              sur le plan joignant les 2 points extrêmes
!
! NB : dans cette version on ne tient pas compte de la pente du profil en long

!entrée : pXYZ, un profil
!sortie : le même profil converti en abscisses-cotes, de type profilAC
!
!==============================================================================
   !prototype
   type (profil), intent(in), target :: pXYZ
   type (profilAC) :: pAC
   !variables locales
   type (pointTS), pointer :: xyz1, xyz2, xyz
   integer :: ip, np

   !les points extrêmes du profil
   np = pXYZ%np
   xyz1 => pXYZ%xyz(1)
   xyz2 => pXYZ%xyz(np)

   !initialisation du profil AC
   pAC%np = np
   pAC%pk = pXYZ%pk
   allocate(pAC%yz(0:np+1))  !on ajoute 2 points à 100 m au dessus des extrémités
   pAC%yz(0)%y = 0._long  ;  pAC%yz(0)%z = max(xyz1%z+100._long , xyz2%z+100._long)
   pAC%yz(1)%y = 0._long  ;  pAC%yz(1)%z = xyz1%z
   do ip = 2, np
      xyz => pXYZ%xyz(ip)
      pAC%yz(ip) = projectionOrtho(xyz1,xyz2,xyz)
   enddo
   pAC%yz(np+1)%y = pAC%yz(np)%y  ;  pAC%yz(np+1)%z = pAC%yz(0)%z
end function projectionPlan



function largeur(un_profil,z)
!==============================================================================
! interpolation de la largeur au miroir dans un profil pour une cote donnée
!        travaille sur la représentation largeurs-cotes du profil
!
! NB : cette fonction modifie le profil en fixant son attribut ip

!entrée : un_profil, de type profil
!         z, la cote à laquelle il faut calculer la largeur
!sortie : la valeur de la largeur à la cote z
!
!==============================================================================
   ! prototype
   type(profil),intent(inout), target :: un_profil  !cette fonction modifie le profil
                                                    !en fixant son attribut ip
   real(kind=long),intent(in) :: z
   real(kind=long) :: largeur
   ! variables locales
   integer, pointer :: ip0
   integer :: i
   real(kind=long), pointer :: lg1, lg2
   real(kind=long) :: hh

   !calcul
   hh = z-un_profil%zf

   if (.NOT.un_profil%wh_aJour) stop 1510
   ip0 => un_profil%ip
   if (hh >= un_profil%wh(ip0)%h .AND. hh < un_profil%wh(ip0+1)%h) then  !niveau courant
      lg1 => un_profil%wh(ip0)%l ; lg2 => un_profil%wh(ip0+1)%l
      largeur = lg1 + (hh-un_profil%wh(ip0)%h)*(lg2-lg1)/(un_profil%wh(ip0+1)%h - un_profil%wh(ip0)%h)  ! interpolation linéaire
      return
   elseif (hh < un_profil%wh(ip0)%h) then                            ! recherche vers le bas
      do i = ip0, 1, -1
         if (hh >= un_profil%wh(i)%h) then
            lg1 => un_profil%wh(i)%l ; lg2 => un_profil%wh(i+1)%l
            largeur = lg1 + (hh-un_profil%wh(i)%h)*(lg2-lg1)/(un_profil%wh(i+1)%h - un_profil%wh(i)%h)  ! interpolation linéaire
            un_profil%ip = i
            return
         endif
      enddo
      write(output_unit,*) 'PB au profil : ',un_profil%name, un_profil%pk, un_profil%zf, hh
      stop 1511 ! on arrive au fond du profil
   elseif (hh >= un_profil%wh(ip0+1)%h) then                         !recherche vers le haut
      do i = ip0, un_profil%nh-1
         if (hh < un_profil%wh(i+1)%h) then
            lg1 => un_profil%wh(i)%l ; lg2 => un_profil%wh(i+1)%l
            largeur = lg1 + (hh-un_profil%wh(i)%h)*(lg2-lg1)/(un_profil%wh(i+1)%h - un_profil%wh(i)%h)  ! interpolation linéaire
            un_profil%ip = i
            return
         endif
      enddo
      write(output_unit,*) '>>>> Erreur au profil ',un_profil%name, hh, un_profil%nh, un_profil%np
      do i = 1, un_profil%nh
         write(output_unit,*) un_profil%wh(i)%h, un_profil%wh(i)%l
      enddo
      stop 1512 ! on arrive au sommet du profil
   else
      largeur = un_profil%wh(ip0)%l
   endif
   largeur = -1.
   write(output_unit,*) '>>>> Erreur pas normale dans GeometrieSection_largeur()'
   stop 1513
end function largeur



function perimetre(un_profil,z)
!==============================================================================
!    interpolation du périmètre mouillé dans un profil pour une cote donnée
!        travaille sur la représentation largeurs-cotes du profil
!
! NB : cette fonction modifie le profil en fixant son attribut ip

!entrée : un_profil, de type profil
!         z, la cote à laquelle il faut calculer le périmètre
!sortie : la valeur du périmètre à la cote z
!
!==============================================================================
   ! prototype
   type(profil),intent(inout), target :: un_profil
   real(kind=long),intent(in) :: z
   real(kind=long) :: perimetre
   ! variables locales
   integer :: i, ip0
   real(kind=long), pointer :: pr1, pr2
   real(kind=long) :: hh

   if (.NOT.un_profil%wh_aJour) stop 1514

   hh = z-un_profil%zf
   ip0 = un_profil%ip
   if (hh >= un_profil%wh(ip0)%h .AND. hh < un_profil%wh(ip0+1)%h) then  !niveau courant
      pr1 => un_profil%wh(ip0)%p ; pr2 => un_profil%wh(ip0+1)%p
      perimetre = pr1 + (hh-un_profil%wh(ip0)%h)*(pr2-pr1)/(un_profil%wh(ip0+1)%h - un_profil%wh(ip0)%h)  ! interpolation linéaire
      return
   elseif (hh < un_profil%wh(ip0)%h) then                            ! recherche vers le bas
      do i = ip0, 1, -1
         if (hh >= un_profil%wh(i)%h) then
            pr1 => un_profil%wh(i)%p ; pr2 => un_profil%wh(i+1)%p
            perimetre = pr1 + (hh-un_profil%wh(i)%h)*(pr2-pr1)/(un_profil%wh(i+1)%h - un_profil%wh(i)%h)  ! interpolation linéaire
            un_profil%ip = i
            return
         endif
      enddo
      stop 1515 ! on arrive au fond du profil
   elseif (hh >= un_profil%wh(ip0+1)%h) then                         !recherche vers le haut
      do i = ip0, un_profil%nh-1
         if (hh < un_profil%wh(i+1)%h) then
            pr1 => un_profil%wh(i)%p ; pr2 => un_profil%wh(i+1)%p
            perimetre = pr1 + (hh-un_profil%wh(i)%h)*(pr2-pr1)/(un_profil%wh(i+1)%h - un_profil%wh(i)%h)  ! interpolation linéaire
            un_profil%ip = i
            return
         endif
      enddo
      stop 1516 ! on arrive au sommet du profil
   else
      perimetre = un_profil%wh(ip0)%p
   endif
   perimetre = -1.
   write(output_unit,*) '>>>> Erreur pas normale dans GeometrieSection_perimetre()'
   stop 1517
end function perimetre



function section(un_profil,z)
!==============================================================================
! interpolation de la section mouillée dans un profil pour une cote donnée
!        travaille sur la représentation largeurs-cotes du profil
!
! NB : cette fonction modifie le profil en fixant son attribut ip

!entrée : un_profil, de type profil
!         z, la cote à laquelle il faut calculer la section
!sortie : la valeur de la section à la cote z
!
!==============================================================================
   ! prototype
   type(profil),intent(inout), target :: un_profil
   real(kind=long),intent(in) :: z
   real(kind=long) :: section
   ! variables locales
   integer, pointer :: ip0
   real(kind=long), pointer :: lg1
   real(kind=long) :: lg2

   !calcul
   if (.NOT.un_profil%wh_aJour) then
      write(output_unit,*) '>>>> Erreur dans Section : ', un_profil%name, un_profil%pk
      !erreur volontaire pour provoquer un backtrace
      section = -1._long  !on définit section pour faire plaisir au compilateur
      call do_crash('section()')
   else
      lg2 = largeur(un_profil,z)
      ip0 => un_profil%ip
      lg1 => un_profil%wh(ip0)%l
      section = un_profil%wh(ip0)%s + 0.5_long*(lg1+lg2)*(z-un_profil%zf-un_profil%wh(ip0)%h)
   endif
end function section



subroutine limite_Eau(un_profil, z, irg, ird)
!==============================================================================
!   détermination des points limites RG et RD pour un niveau d'eau donné
!
!   irg et ird sont les premiers indices en partant des rives gauche et
!   droite qui correspondent à des points sous la surface de l'eau
!==============================================================================
   ! prototype
   type(profil),intent(in) :: un_profil
   real(kind=long),intent(in) :: z
   integer, intent(out) :: irg, ird
   ! variables locales
   integer :: i, np, j

   !calcul
   irg = -1  ;  ird = -1
   if (z < un_profil%zf) then
      write(output_unit,*) '>>>> limite_Eau() impossible : niveau < cote du fond'
      return
   endif
   np = un_profil%np
   do i = 1, un_profil%np
      if (irg > 0 .and. ird > 0) then
         exit
      else
         if (irg < 0) then
            if (un_profil%xyz(i)%z < z) irg = i
         endif
         if (ird < 0) then
            j = np-i+1
            if (un_profil%xyz(j)%z < z) ird = j
         endif
      endif
   enddo
   if (irg < 0 .or. ird < 0) then
      write(output_unit,*) '>>>> Erreur dans limite_Eau() : profil sec'
      stop 1518
   endif
end subroutine limite_Eau



function removeDoublons (pXYZ) result(qXYZ)
!==============================================================================
!       suppression des doublons, ie les points identiques et successifs
!         si l'un des deux points est nommé c'est celui-ci qu'on garde
!
! NB: le tableau résultat est alloué par la fonction
!
!entrée : un tableau de pointTS
!sortie : copie du tableau donné en entrée, expurgé des points en double
!         de type pointTS.
!
!==============================================================================
   ! prototype
   type (pointTS), intent(in) :: pXYZ(:)
   type (pointTS), allocatable :: qXYZ(:)
   ! variables locales
   integer :: np, n, i
   integer, allocatable :: liste(:)

   ! calcul
   np = size(pXYZ)
   allocate(liste(np))

   i = 1  ;  liste(i) = 1  ;  n = 2  !on garde le premier point
   do while (n < np)
      if (distance3D(pXYZ(n),pXYZ(n-1)) < 0.001_long) then
         if (pXYZ(n)%tag == '' .OR. pXYZ(n)%tag == pXYZ(n-1)%tag) then
            continue  !on oublie ce point
         else
            i = i+1  ;  liste(i) = n  !on garde ce point
         endif
      else
         i = i+1  ;  liste(i) = n  ! on garde ce point
      endif
      n = n+1
   enddo
   i = i+1 ; liste(i) = np  !on garde le dernier point

   !création du profil nettoyé
   allocate(qXYZ(i))
   do n = 1, i
      qXYZ(n) = pXYZ(liste(n))
   enddo
   deallocate(liste)
end function removeDoublons



function section_id_from_pk (modele, bief_id, pk, seuil)
!==============================================================================
! retrouve le numéro de la section se trouvant à l'abscisse pk du bief bief_id
!==============================================================================
   ! prototype
   type (dataset), intent(in) :: modele
   integer, intent(in) :: bief_id
   real(kind=long), intent(in) :: pk, seuil
   integer :: section_id_from_pk
   ! variables locales
   integer :: ib, kb, is01, is02

   ib = bief_id
   is01 = modele%biefs(ib)%is1  ;  is02 = modele%biefs(ib)%is2
   if (pk < min(modele%sections(is01)%pk,modele%sections(is02)%pk)) then
      section_id_from_pk = modele%biefs(ib)%is1
      return
   elseif (pk > max(modele%sections(is01)%pk,modele%sections(is02)%pk)) then
      section_id_from_pk = modele%biefs(ib)%is2
      return
   else
      do kb = is01, is02
         if (abs(modele%sections(kb)%pk - pk) < seuil) then
            section_id_from_pk = kb
            return
         endif
      enddo
      write(output_unit,'(a,f12.3,2a)') ' >>>> Erreur : aucune section ne correspond au pk ',pk,&
                             ' du bief ',modele%biefs(ib)%name
      write(output_unit,'(3a,f8.4)') 'Liste des Pk du bief ',modele%biefs(ib)%name,' seuil = ',seuil
      do kb = is01, is02
         write(output_unit,*) kb, modele%sections(kb)%pk,abs(modele%sections(kb)%pk - pk)
      enddo
      stop 1519
   endif
end function section_id_from_pk


subroutine get_data_geom(un_profil,z,lg,pr,sf)
!==============================================================================
! interpolation de la largeur au miroir, du périmètre et de la surface
! mouillés dans un profil pour une cote donnée à partir de la représentation en
! largeurs-cotes
!
! NB : cette fonction modifie le profil en fixant son attribut ip
!
! entrée : un_profil, de type profil
!          z, la cote à laquelle il faut calculer la largeur
! sortie : la valeur de la largeur, du périmètre et de la surface
!          à la cote z
!
!==============================================================================
   ! prototype
   type(profil),intent(inout), target :: un_profil  !cette fonction modifie le profil
                                                    !en fixant son attribut ip
   real(kind=long),intent(in) :: z
   real(kind=long),intent(out) :: lg, pr, sf
   ! variables locales
   integer :: i, ip0
   real(kind=long), pointer :: lg1, lg2, pr1, pr2
   real(kind=long) :: dh, hh, dz

   !calcul
   hh = z-un_profil%zf

   if (.NOT.un_profil%wh_aJour) stop 1520
   ip0 = un_profil%ip
   if (hh >= un_profil%wh(ip0)%h .AND. hh < un_profil%wh(ip0+1)%h) then  !niveau courant
      lg1 => un_profil%wh(ip0)%l ; lg2 => un_profil%wh(ip0+1)%l
      pr1 => un_profil%wh(ip0)%p ; pr2 => un_profil%wh(ip0+1)%p
      dh = un_profil%wh(ip0+1)%h - un_profil%wh(ip0)%h
      dz = hh - un_profil%wh(ip0)%h
      lg = lg1 + dz*(lg2-lg1)/dh  ! interpolation linéaire
      pr = pr1 + dz*(pr2-pr1)/dh  ! interpolation linéaire
      sf = un_profil%wh(ip0)%s + dz*0.5_long*(lg1+lg) !méthode des trapèzes
      return
   elseif (hh < un_profil%wh(ip0)%h) then                            ! recherche vers le bas
      do i = ip0, 1, -1
         if (hh >= un_profil%wh(i)%h) then
            lg1 => un_profil%wh(i)%l ; lg2 => un_profil%wh(i+1)%l
            pr1 => un_profil%wh(i)%p ; pr2 => un_profil%wh(i+1)%p
            dh = un_profil%wh(i+1)%h - un_profil%wh(i)%h
            dz = hh - un_profil%wh(i)%h
            lg = lg1 + dz*(lg2-lg1)/dh  ! interpolation linéaire
            pr = pr1 + dz*(pr2-pr1)/dh  ! interpolation linéaire
            sf = un_profil%wh(i)%s + dz*0.5_long*(lg1+lg) !méthode des trapèzes
            un_profil%ip = i
            return
         endif
      enddo
      write(output_unit,*) 'PB au profil : ',un_profil%name, un_profil%pk, un_profil%zf, hh
      stop 1521 ! on arrive au fond du profil
   elseif (hh >= un_profil%wh(ip0+1)%h) then                         !recherche vers le haut
      do i = ip0, un_profil%nh-1
         if (hh < un_profil%wh(i+1)%h) then
            lg1 => un_profil%wh(i)%l ; lg2 => un_profil%wh(i+1)%l
            pr1 => un_profil%wh(i)%p ; pr2 => un_profil%wh(i+1)%p
            dh = un_profil%wh(i+1)%h - un_profil%wh(i)%h
            dz = hh - un_profil%wh(i)%h
            lg = lg1 + dz*(lg2-lg1)/dh  ! interpolation linéaire
            pr = pr1 + dz*(pr2-pr1)/dh  ! interpolation linéaire
            sf = un_profil%wh(i)%s + dz*0.5_long*(lg1+lg) !méthode des trapèzes
            un_profil%ip = i
            return
         endif
      enddo
      write(output_unit,*) '>>>> Erreur au profil ',un_profil%name,z, un_profil%nh, un_profil%np
      do i = 1, un_profil%nh
         write(output_unit,*) i,un_profil%wh(i)%h,un_profil%wh(i)%l,&
                      un_profil%wh(i)%p,un_profil%wh(i)%s
      enddo
      stop 1522 ! on arrive au sommet du profil
   else
      lg = -1._long  ;  pr = -1._long  ;  sf = -1._long
      write(output_unit,*) '>>>> Erreur pas normale dans get_data_geom()'
      stop 1523
   endif
end subroutine get_data_geom


function numero_bief(modele,is)
!==============================================================================
!  Renvoie le numéro du bief (ordre des données de .NET) de la section
!  de numéro IS dans la collection modele%sections(:)
!  Renvoie -1 si le numéro de bief n'est pas trouvé
!==============================================================================
   implicit none
   ! -- le prototype --
   type (dataset), intent(in) :: modele
   integer,intent(in) :: is
   integer :: numero_bief
   ! -- les variables --
   integer :: ib

   do ib = 1, modele%net%ibmax
      if (is >= modele%biefs(ib)%is1 .and. is <= modele%biefs(ib)%is2) then
         numero_bief = ib
         return
      endif
   enddo
   numero_bief = -1
end function numero_bief

end module Geometrie_Section
