!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module Geochimie
!===============================================================================
!                      Variables propre à la geochimie
!       Définir ici les variables propres au comportement géochimique
!       des polluants : coeff de disparition cinétique
!
! numérotation des STOP : 14nn
!===============================================================================
use iso_fortran_env, only: output_unit, error_unit
use parametres, only: long, defaut, g, rho, gnu2, nu, un
use Adis_Types, only: dataset, solution, polluant, pointTS, sediment
use DataNum, only: c_initiale
use Utilitaires, only: next_string, next_real, next_int
use Geometrie_Section, only: section_id_from_pk
use ConditionsLimites, only: lireCL, lireAL
implicit none

contains

subroutine lirePolluant(filename, npol, modele)
!===============================================================================
!          Lecture d'un fichier de description d'un polluant
!
! entrées :
!          filename : nom du fichier POL
!          npol : numéro du polluant
!          modele  : dataset
!===============================================================================
   implicit none
   ! prototype
   character(len=*), intent(in) :: filename  !nom du fichier de données
   integer, intent(in) :: npol               !numéro du polluant (identifiant), sans objet si cl hydro
   type (dataset), intent(inout) :: modele      !jeu de données complet (dataset)
   ! variables locales
   integer :: luu ! unité logique
   integer :: ierr, nf
   character :: ligne*80, key*30
   logical :: theta_defined = .false.

   open(newunit=luu, file=trim(filename), status='old', form='formatted',iostat=ierr)
   if (ierr > 0) then
      write(output_unit,'(3a)') ' Ouverture de polluant ', trim(filename),' impossible'
      stop 1401
   else
      write(output_unit,'(2a)') ' Lecture du fichier de polluant : ',trim(filename)
   endif

   !initialisation
   modele%poll(npol)%name = ''
   modele%poll(npol)%particule_type = 0
   modele%poll(npol)%d = -1._long
   modele%poll(npol)%rho = 2650._long
   modele%poll(npol)%p = 0.4_long
   modele%poll(npol)%cdc_riv = 0._long
   modele%poll(npol)%cdc_cas = 0._long
   modele%poll(npol)%apd_ero = 0.1_long
   modele%poll(npol)%apd_dep = 0.1_long
   modele%poll(npol)%ac = 0.001_long
   modele%poll(npol)%bc = 1._long
   modele%poll(npol)%file_CL = ''
   modele%poll(npol)%file_ALD = ''
   modele%poll(npol)%file_INI = ''

   do
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr < 0) exit !fin de fichier
      if (ligne(1:1) == '*' .or. trim(ligne)=='') cycle
      nf = 1  ;  key = next_string(ligne,'=',nf)
      if (trim(key) == 'name') then
         modele%poll(npol)%name = trim(next_string(ligne,'=',nf))
      else if (trim(key) == 'type') then
         modele%poll(npol)%particule_type = next_int(ligne,'=',nf)
      else if (trim(key) == 'd50' .or. trim(key) == 'diametre' .or. trim(key) == 'diamètre') then
         modele%poll(npol)%d = next_real(ligne,'=',nf)
         write(output_unit,*) ' diamètre pour ',trim(modele%poll(npol)%name),modele%poll(npol)%d
      else if (trim(key) == 'rho') then
         modele%poll(npol)%rho = next_real(ligne,'=',nf)
         write(output_unit,*) ' rho pour ',trim(modele%poll(npol)%name),modele%poll(npol)%rho
      else if (trim(key) == 'porosity') then
         modele%poll(npol)%p = next_real(ligne,'=',nf)
      else if (trim(key) == 'cdc_riv') then
         modele%poll(npol)%cdc_riv = next_real(ligne,'=',nf)
      else if (trim(key) == 'cdc_cas') then
         modele%poll(npol)%cdc_cas = next_real(ligne,'=',nf)
      else if (trim(key) == 'apd') then
         modele%poll(npol)%apd_ero = next_real(ligne,'=',nf)
         modele%poll(npol)%apd_dep = modele%poll(npol)%apd_ero
      else if (trim(key) == 'apd_dep') then
         modele%poll(npol)%apd_dep = next_real(ligne,'=',nf)
      else if (trim(key) == 'apd_ero') then
         modele%poll(npol)%apd_ero = next_real(ligne,'=',nf)
      else if (trim(key) == 'ac') then
         modele%poll(npol)%ac = next_real(ligne,'=',nf)
      else if (trim(key) == 'bc') then
         modele%poll(npol)%bc = next_real(ligne,'=',nf)
      else if (trim(key) == 'file_cl' .or. trim(key) == 'file_CL') then
         modele%poll(npol)%file_CL = trim(next_string(ligne,'=',nf))
      else if (trim(key) == 'file_ald' .or. trim(key) == 'file_ALD') then
         modele%poll(npol)%file_ALD = trim(next_string(ligne,'=',nf))
      else if (trim(key) == 'file_ini' .or. trim(key) == 'file_INI') then
         modele%poll(npol)%file_INI = trim(next_string(ligne,'=',nf))
      else if (trim(key) == 'theta') then
         modele%poll(npol)%theta_dep = next_real(ligne,'=',nf)
         modele%poll(npol)%theta_ero = modele%poll(npol)%theta_dep
         theta_defined = .true.
      else if (trim(key) == 'theta_ero') then
         modele%poll(npol)%theta_ero = next_real(ligne,'=',nf)
         theta_defined = .true.
      else if (trim(key) == 'theta_dep') then
         modele%poll(npol)%theta_dep = next_real(ligne,'=',nf)
         theta_defined = .true.
      endif
   enddo
   !vérification et initialisations par défaut
   if (modele%poll(npol)%name == '') then
      write(output_unit,*) '>>>> Erreur : le fichier ',trim(filename),'ne définit pas le nom du polluant'
      stop 1402
   endif
   ! NOTE: tous les apports ponctuel de ce polluant ont déjà été fixés à 0 par initAllCLzero() -> rien à faire si le fichier n'existe pas
   if (modele%poll(npol)%file_CL /= '') then
      call lireCL(modele%poll(npol)%file_CL, modele, npol)
   endif
   ! NOTE: tous les apports latéraux (diffus) de ce polluant ont déjà été fixés à 0 par initAllCLzero() -> rien à faire si le fichier n'existe pas
   if (modele%poll(npol)%file_ALD /= '') then
      call lireAL(modele%poll(npol)%file_ALD, modele, npol)
   endif
   if (.not. theta_defined) then
      modele%poll(npol)%theta_dep = theta_critique(modele%poll(npol))
        modele%poll(npol)%theta_ero = modele%poll(npol)%theta_dep
   endif
   if (modele%poll(npol)%theta_dep > modele%poll(npol)%theta_ero) then
      modele%poll(npol)%theta_ero = modele%poll(npol)%theta_dep
      write(output_unit,*) '>>>> Attention : le fichier ',trim(filename),&
                               ' donne theta_dep > theta_ero. On garde theta_ero = theta_dep.'
   endif
   if (modele%poll(npol)%particule_type > 0) then
      modele%nbmes = modele%nbmes + 1
      modele%poll(npol)%tcr_dep = tau_critique_ero(modele%poll(npol))
      modele%poll(npol)%tcr_ero = tau_critique_dep(modele%poll(npol))
      modele%poll(npol)%ws = settling_velocity(modele%poll(npol))
      write(output_unit,'(3a,2(e12.6,a))') ' ==> Polluant ',trim(modele%poll(npol)%name),' : ', modele%poll(npol)%tcr_ero, &
                               ' (tau critique erosion) , ', modele%poll(npol)%tcr_dep, &
                               ' (tau critique depot) , ',modele%poll(npol)%ws,' (vitesse de chute)'
   endif
   ! NOTE: lecture de file_INI doit être faite après celle de BIN (état hydraulique)
   !       car c'est là qu'on repère les lits (mineur, moyen-gauche et moyen-droit avec MAGE)
   !       utiliser init_Solution() pour cela.
   close (luu)
   write(output_unit,'(3a)') ' Lecture du fichier de polluant : ',trim(filename),' : OK !'
end subroutine lirePolluant



subroutine init_Solution (modele, sol1, sol2)
!===============================================================================
!               initialisation de la solution
!
!- valeur par défaut partout
!- écrasement de la valeur par défaut par celles des fichiers file_INI(npol)
!
! Entrées / sorties :
!                    modele : dataset
!                    sol1 : solution à l'instant t
!                    sol2 : solution à l'instant t+dt
! sol1 contient la solution à l'instant initial
! sol2 est initialisé identique à sol1
!===============================================================================
   implicit none
   !prototype
   type (dataset), intent(inout) :: modele
   type (solution), intent(inout) :: sol1, sol2
   !variables locales
   integer :: npol

   write(output_unit,*) '==> Initialisation de la solution'
   !allocations
   allocate (sol1%c(modele%net%ismax,modele%nbpol), sol2%c(modele%net%ismax,modele%nbpol))
   allocate (sol1%qm(modele%net%ismax,0:3,modele%nbpol), sol2%qm(modele%net%ismax,0:3,modele%nbpol))
   allocate (sol1%masse(modele%net%ismax,3,modele%nbpol),sol2%masse(modele%net%ismax,3,modele%nbpol))
   allocate (sol1%v_in(modele%nbpol), sol1%v_out(modele%nbpol))
   allocate (sol1%stock_h2o(modele%nbpol), sol1%stock_fnd(modele%nbpol))
   allocate (sol1%st0_h2o(modele%nbpol), sol1%st0_fnd(modele%nbpol))
   allocate (sol2%v_in(modele%nbpol), sol2%v_out(modele%nbpol))
   allocate (sol2%stock_h2o(modele%nbpol), sol2%stock_fnd(modele%nbpol))
   allocate (sol2%st0_h2o(modele%nbpol), sol2%st0_fnd(modele%nbpol))
   allocate (sol1%c_eq_ero(modele%net%ismax,3,modele%nbpol),sol2%c_eq_ero(modele%net%ismax,3,modele%nbpol))
   allocate (sol1%c_eq_dep(modele%net%ismax,3,modele%nbpol),sol2%c_eq_dep(modele%net%ismax,3,modele%nbpol))
   allocate (sol1%C_total(modele%net%ismax),sol2%C_total(modele%net%ismax))
   allocate (sol1%Hg(modele%net%ismax),sol2%Hg(modele%net%ismax))
   allocate (sol1%Hm(modele%net%ismax),sol2%Hm(modele%net%ismax))
   allocate (sol1%Hd(modele%net%ismax),sol2%Hd(modele%net%ismax))
   allocate (sol1%dhg(modele%net%ismax),sol2%dhg(modele%net%ismax))
   allocate (sol1%dhm(modele%net%ismax),sol2%dhm(modele%net%ismax))
   allocate (sol1%dhd(modele%net%ismax),sol2%dhd(modele%net%ismax))

   !initialisation à 0
   sol1%c = c_initiale
   sol1%qm = 0._long
   sol1%masse = 0._long
   sol1%C_eq_ero = 0._long
   sol1%C_eq_dep = 0._long
   sol1%v_in = 0._long       ;  sol1%v_out = 0._long
   sol1%stock_h2o = 0._long  ;  sol1%st0_h2o = 0._long
   sol1%stock_fnd = 0._long  ;  sol1%st0_fnd = 0._long
   sol1%C_total = 0._long
   sol1%Hg = 0._long  ;  sol1%Hm = 0._long  ;  sol1%Hd = 0._long
   sol1%dhg = 0._long  ;  sol1%dhm = 0._long  ;  sol1%dhd = 0._long
   write(output_unit,*) '   --> initialisation générale à 0 : OK !'

   !lecture des fichiers file_INI
   do npol = 1, modele%nbpol
      if (trim(modele%poll(npol)%file_INI) /= '') then
         call lire_INI(modele, sol1, npol, modele%poll(npol)%file_INI)
      else
         !il faut allouer modele%corr_tau_consolide si on ne passe pas dans lire_INI()
         if (.not.allocated(modele%corr_tau_consolide)) then
            allocate(modele%corr_tau_consolide(modele%net%ismax,modele%nbpol))
            modele%corr_tau_consolide = 1
         endif
         cycle
      endif
   enddo

   sol2 = sol1
   write(output_unit,*) '==> Initialisation de la solution : OK !'
end subroutine init_Solution



subroutine lire_INI(modele, sol, npol, iniFile)
!===============================================================================
!          lecture du fichier d'état initial pour le polluant npol
!
! Entrées / Sorties
!     modele : dataset
!     iniFile : fichier INI provenant de MAGE
!     sol : solution à l'instant initial
!     npol : numéro du polluant
!===============================================================================
   implicit none
   !prototype
   type (dataset), intent(inout) :: modele
   type (solution), intent(inout) :: sol
   integer, intent(in) :: npol
   character (len=*), intent(in) :: iniFile
   ! variables locales
   character (len=80) :: ligne
   character (len=30) :: tmp, secteur
   integer :: lu, ios, nligne, nf, is, ich, i, bief_id, is1, is2, ib
   real(kind=long) :: pkmin, pkmax, C_ini, M_rg, M_rd, M_min, anp, tau_consol

   !initialisation : on suppose qu'on a bien initialisé la solution avec init_Solution()

   if (.not.allocated(modele%corr_tau_consolide)) then
      allocate(modele%corr_tau_consolide(modele%net%ismax,modele%nbpol))
      modele%corr_tau_consolide = 1
   endif

   if (iniFile == '') return
   write(output_unit,'(2a)') '    --> Lecture du fichier d''état initial : ',trim(iniFile)
   open(newunit=lu,file=trim(iniFile), status='old', form='formatted',iostat=ios)
   if (ios > 0) then
      write(output_unit,'(3a)') ' Ouverture du fichier d''état initial ', trim(iniFile),' impossible'
      stop 1403
   endif

   nligne = 0
   anp = modele%poll(npol)%rho * (1._long - modele%poll(npol)%p) !pour conversion épaisseur -> masse
   M_rg = 0._long ; M_min = 0._long ; M_rd = 0._long

   do
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      nligne = nligne+1
      !conversion en majuscules
      do i = 1, 80
         ich = ichar(ligne(i:i))
         if (ich > 96 .and. ich < 123) ligne(i:i) = char(ich-32)
      enddo
      !décodage des lignes
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then  !commentaire ou ligne vide
         cycle
      else if (ligne(1:6) == 'DEFAUT' .or. ligne(1:7) == 'DEFAULT') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         C_ini = next_real(ligne,'',nf)
         if (modele%poll(npol)%particule_type > 0) then
            M_rg = next_real(ligne,'',nf)
            M_min = next_real(ligne,'',nf)
            M_rd = next_real(ligne,'',nf)
            tau_consol = next_real(ligne,'',nf)
            if(nf == -1 .or. abs(tau_consol) < 0.001_long) tau_consol = 1._long
         endif

         write(output_unit,'(a,4f8.4)') '    --> Concentration et épaisseurs par défaut = ',C_ini, M_rg, M_min, M_rd
         !conversion épaisseur --> masse linéïque = masse dans un volume largeur * 1m
         do ib = 1, modele%net%ibmax
            sol%c(modele%biefs(ib)%is1:modele%biefs(ib)%is2,npol) = c_ini
            if (modele%poll(npol)%particule_type > 0) then
               modele%corr_tau_consolide(modele%biefs(ib)%is1:modele%biefs(ib)%is2,npol) = tau_consol
               do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
                  sol%masse(is,1,npol) = M_rg  * modele%sections(is)%l_g * anp
                  sol%masse(is,2,npol) = M_min * modele%sections(is)%l_m * anp
                  sol%masse(is,3,npol) = M_rd  * modele%sections(is)%l_d * anp
               enddo
            endif
         enddo
      else
         nf = 1  ;  secteur = next_string(ligne,'=',nf)
         bief_id = next_int(ligne,'',nf)
         if (bief_id == 0) cycle
         pkmin = next_real(ligne,'',nf)  ;  pkmax = next_real(ligne,'',nf)
         C_ini = next_real(ligne,'',nf)
         if (modele%poll(npol)%particule_type > 0) then
            M_rg = next_real(ligne,'',nf)
            M_min = next_real(ligne,'',nf)
            M_rd = next_real(ligne,'',nf)
            tau_consol = next_real(ligne,'',nf)
            if(nf == -1 .or. abs(tau_consol) < 0.001_long) tau_consol = 1._long
         endif
         is1 = max(modele%biefs(bief_id)%is1,section_id_from_pk (modele, bief_id, pkmin, 10.1_long))
         is2 = min(modele%biefs(bief_id)%is2-1,section_id_from_pk (modele, bief_id, pkmax, 10.1_long))
         if (is1 > is2) stop 1404
         sol%c(is1:is2,npol) = c_ini
         !conversion épaisseur --> masse linéïque = masse dans un volume largeur * 1m
         if (modele%poll(npol)%particule_type > 0) then
            modele%corr_tau_consolide(is1:is2,npol) = tau_consol
            do is = is1, is2
               sol%masse(is,1,npol) = M_rg  * modele%sections(is)%l_g * anp
               sol%masse(is,2,npol) = M_min * modele%sections(is)%l_m * anp
               sol%masse(is,3,npol) = M_rd  * modele%sections(is)%l_d * anp
            enddo
         endif
         write(output_unit,'(a,i3,a,f10.2,a,f10.2,a,4f8.4,f6.2)') '    --> Concentration et épaisseurs initiales au bief ',&
                    bief_id,' de ',modele%sections(is1)%pk,' a ',modele%sections(is2)%pk,' : ',&
                    C_ini, M_rg, M_min, M_rd, tau_consol
      endif
   enddo
   write(output_unit,'(5a)') '    --> Lecture état initial ',trim(iniFile),' pour ',&
                   trim(modele%poll(npol)%name),' : OK'
   close (lu)
end subroutine lire_INI



subroutine lire_D90 (modele, d90File)
!==============================================================================
!       lecture du fichier des diamètres caractéristiques d90 du fond stable
!
!           initialisation du tableau modele%section(:)%d90
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + d90File = nom du fichier D90
!==============================================================================
   !prototype
   type (dataset), intent(inout) :: modele
   character (len=*), intent(in) :: d90File
   ! variables locales
   character (len=80) :: ligne
   character (len=30) :: tmp, secteur
   integer :: lu, ios, nligne, nf, is, ich, i, bief_id, is1, is2
   real(kind=long) :: valeur, pkmin, pkmax

   !initialisation avec la valeur 0.01 m
   do is = 1, modele%net%ismax
      modele%sections(is)%d90 = 0.01_long
   enddo

   if (trim(d90File) == '') then
      write(output_unit,'(a,f8.4)') ' Pas de fichier D90 --> Valeur par défaut pour le d90 du fond stable : ', &
                                    modele%sections(1)%d90
      return
   endif
   write(output_unit,*)
   write(output_unit,'(3a)') ' Lecture du fichier des diamètres caractéristiques d90 du fond stable : ',trim(d90File)
   open(newunit=lu,file=trim(d90File), status='old', form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier D90 ',trim(d90File),' impossible'
      stop 1405
   endif
   nligne = 0

   do
      read(lu,'(a)', iostat=ios) ligne
      if (ios /= 0) exit
      nligne = nligne+1
      !conversion en majuscules
      do i = 1, 80
         ich = ichar(ligne(i:i))
         if (ich > 96 .and. ich < 123) ligne(i:i) = char(ich-32)
      enddo
      !décodage des lignes
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then  !commentaire ou ligne vide
         cycle
      else if (ligne(1:6) == 'DEFAUT' .or. ligne(1:7) == 'DEFAULT') then
         nf = 1  ;  tmp = next_string(ligne,'=',nf)
         valeur = next_real(ligne,'',nf)
         write(output_unit,'(a,f8.4)') ' d90 par défaut = ',valeur
         do is = 1, modele%net%ismax
            modele%sections(is)%d90 = valeur
         enddo
      else
         nf = 1  ;  secteur = next_string(ligne,'=',nf)
         bief_id = next_int(ligne,'',nf)
         pkmin = next_real(ligne,'',nf)  ;  pkmax = next_real(ligne,'',nf)
         valeur = next_real(ligne,'',nf)
         is1 = section_id_from_pk (modele, bief_id, pkmin, 10.1_long)
         is2 = section_id_from_pk (modele, bief_id, pkmax, 10.1_long)
         if (is1 > is2 .or. bief_id == 0) then
            write(output_unit,'(3a)') ' >>>> Erreur dans le lecture du fichier D90 (',trim(d90File),') à la ligne :'
            write(output_unit,'(a)') trim(ligne)
            stop 1406
         endif
         do is = is1, is2
            modele%sections(is)%d90 = valeur
         enddo
         write(output_unit,'(a,i3,a,f10.2,a,f10.2,a,f8.4,3a)') ' d90 au bief ',&
                    bief_id,' de ',modele%sections(is1)%pk,' a ',modele%sections(is2)%pk,' : ',&
                    valeur,'  (',trim(secteur),')'
      endif
   enddo
   close (lu)
end subroutine lire_D90

function theta_critique(poll)
!==============================================================================
!  estimation du parametre de Shield
!==============================================================================
   implicit none
   ! prototype
   type (polluant), intent(in) :: poll
   real(kind=long) :: theta_critique, d_star
   real(kind=long), parameter :: a = 1._long/3._long
   ! calcul

   d_star = poll%d*((poll%rho - rho)*gnu2)**a
   write(output_unit,*)  '---> d_star = ',d_star

   !paramètre de Shields -- expression de Soulsby & Whitehouse 1997
   theta_critique = 0.24_long/d_star + 0.055_long * (un-exp(-d_star*0.02_long))
end function theta_critique

function tau_critique_dep(poll)
!==============================================================================
!  estimation de la contrainte critique
!==============================================================================
   implicit none
   ! prototype
   type (polluant), intent(in) :: poll
   real(kind=long) :: tau_critique_dep
   ! calcul

   write(output_unit,*)  '---> theta_critique depot = ',poll%theta_dep

   tau_critique_dep = poll%theta_dep*g*poll%d*(poll%rho - rho)
end function tau_critique_dep

function tau_critique_ero(poll)
!==============================================================================
!  estimation de la contrainte critique
!==============================================================================
   implicit none
   ! prototype
   type (polluant), intent(in) :: poll
   real(kind=long) :: tau_critique_ero
   ! calcul

   write(output_unit,*)  '---> theta_critique erosion = ',poll%theta_ero

   tau_critique_ero = poll%theta_ero*g*poll%d*(poll%rho - rho)
end function tau_critique_ero



function settling_velocity(poll)
!==============================================================================
!   estimation de la vitesse de chute des particules en suspension
!==============================================================================
   implicit none
   ! prototype
   real(kind=long) :: settling_velocity
   type (polluant), intent(in) :: poll
   !variables locales
   real(kind=long) :: A, B, m
   real(kind=long), parameter :: c1=0.25_long, c2=4._long/3._long, c3=0.5_long
   real(kind=long) :: dfs3, ab, tmp, m1

   select case (poll%particule_type)
      case (0) !soluté
         write(output_unit,*) '>>>> Erreur de type dans settling_velocity() : soluté'
         stop 1407
      case (1) !particules sphériques
         A = 24.0  ;  B = 0.39  ;  m = 1.92
      case (2) !galets lisses
         A = 24.5  ;  B = 0.62  ;  m = 1.71
      case (3) !sable naturel
         A = 24.6  ;  B = 0.96  ;  m = 1.53
      case (4) !sable concassé
         A = 24.7  ;  B = 1.36  ;  m = 1.36
      case (5) !cylindres longs
         A = 36.0  ;  B = 1.51  ;  m = 1.40
      case (6) !limon, particules cohésives
         A = 38.0  ;  B = 3.55  ;  m = 1.12
      case (7) !flocs
         A = 26.8  ;  B = 2.11  ;  m = 1.19
      case default
         write(output_unit,*) '>>>> Erreur dans settling_velocity() : type de particule inconnu'
         stop 1408
   end select
   m1 = un / m  ;  ab = (A/B)**m1
   dfs3 = (poll%rho-rho)*gnu2*poll%d**3

   tmp = (sqrt(c1*ab*ab + (c2*dfs3/B)**m1) - c3*ab)**m

   settling_velocity = nu * tmp / poll%d
   !write(output_unit,*) 'settling_velocity : ',a,b,m,m1,dfs3,tmp,nu,poll%d,settling_velocity

end function settling_velocity



subroutine MES_correlation(modele)
!==============================================================================
!  matrice de couplage des sédiments
!  on repère les sédiments identiques
!==============================================================================
   implicit none
   !prototype
   type (dataset), intent(inout) :: modele
   !variables locales
   integer :: np, k

   allocate (modele%MES_corr(modele%nbpol,modele%nbpol))
   modele%MES_corr = 0
   do np = 1, modele%nbpol
      modele%MES_corr(np,np) = 1
      if (modele%poll(np)%particule_type == 0) cycle
      do k = 1, modele%nbpol
         if (modele%poll(k)%particule_type > 0) then
            modele%MES_corr(np,k) = 1
         else
            modele%MES_corr(np,k) = 0
         endif
      enddo
   enddo

end subroutine MES_correlation



function contrainte_locale(pts,Z,V_loc,K_skin,Rh_loc)
   implicit none
   ! -- Prototype --
   real(kind=long) :: contrainte_locale
   type(pointTS), intent(in) :: pts
   real(kind=long), intent(in) :: Z, V_loc, K_skin, Rh_loc
   ! -- Variables locales --
   real(kind=long), parameter :: dtier = 2._long/3._long
   !real(kind=long), parameter :: qtier = 4._long/3._long

   if (Z - pts%z > 0._long .and. Rh_loc > 0._long) then
      !contrainte_locale = rho * g * (Z - pts%z) * V_loc * V_loc / (K_skin * K_skin * Rh_loc**qtier)
      contrainte_locale = rho * g * (Z - pts%z) * (V_loc / (K_skin * Rh_loc**dtier))**2
   else
      contrainte_locale = 0._long
   endif

end function contrainte_locale


subroutine lire_Sediments(filename,les_Sediments)
!==============================================================================
!          Lecture d'un fichier de description des sédiments
!             des couches sédimentaires pour le charriage
!
!   le fichier SED est une liste de lignes de la forme suivante :
!   label  d50  sigma  tau
!   où :
!     label est l'étiquette du sédiment à utiliser dans ST2
!     d50 est le diamètre médian
!     sigma est l'étendue granulométrique
!     tau est la contrainte de mise en mouvement
!
!   les lignes commençant par * ou vides sont ignorées
!
! NOTE: on a choisi le d50 plutôt que le d90 parce que le d50 est utilisé par PamHyr-RubarBE
!
!==============================================================================
   ! -- prototype --
   character(len=*), intent(in) :: filename  !nom du fichier de données
   type(sediment), intent(inout), allocatable :: les_Sediments(:)
   ! -- variables --
   integer :: luu, ierr, nligne, n, nf
   character(len=80) :: ligne

   if (filename == '') then
      ! cas où aucun fichier SED n'est fourni -> une seule couche sédimentaire définie par défaut
      write(output_unit,*) '==> Aucun fichier SED => définition par défaut : d50 = 0.0001, sigma = 3., tau = -1.'
      allocate(les_sediments(1))
      les_sediments(1)%tag = defaut
      les_sediments(1)%d50 = 0.0001_long
      les_sediments(1)%sigma = 3._long
      les_sediments(1)%tau = -1._long
      return
   endif

   open(newunit=luu, file=filename, status='old', form='formatted',iostat=ierr)
   if (ierr > 0) then
      write(output_unit,'(3a)') ' Ouverture du fichier de sédiments ', filename,' impossible'
      stop 1409
   else
      write(output_unit,'(2a)') ' Lecture du fichier de sédiments : ',trim(filename)
   endif

   nligne = 0
   do
      !1er parcours du fichier pour compter les lignes utiles
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr < 0) exit !fin de fichier
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then
         cycle
      else
         nligne = nligne+1
      endif
   enddo
   !write(output_unit,*) 'Nb de lignes dans le fichier ',trim(filename),' : ',nligne
   allocate(les_sediments(nligne+1)) !on prévoit la place pour le sédiment par défaut s'il n'est pas fourni
   rewind(luu)
   n = 0
   do
      !2e parcours du fichier pour lire les données
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr < 0) exit !fin de fichier
      if (ligne(1:1) == '*' .or. trim(ligne)=='') then
         cycle
      else
         n = n+1  ;  nf = 1
         les_sediments(n)%tag = next_string(ligne,' ',nf)
         les_sediments(n)%d50 = next_real(ligne,' ',nf)
         les_sediments(n)%sigma = next_real(ligne,' ',nf)
         les_sediments(n)%tau = next_real(ligne,' ',nf)
      endif
   enddo
   !On vérifie qu'une valeur par défaut a bien été définie, sinon on la définit
   do nf = 1, n
      if (trim(les_sediments(nf)%tag) == defaut) return
   enddo
   ! si on arrive ici c'est qu'il n'y a pas de valeur par défaut
   write(output_unit,*) '>>>> ATTENTION : il manque la définition de la valeur par défaut dans le fichier SED'
   write(output_unit,*) '                 Adis-TS a défini la même valeur par défaut que dans le cas où le ' // &
                        'fichier SED n''existe pas'
   n = n+1
   les_sediments(n)%tag = defaut
   les_sediments(n)%d50 = 0.0001_long
   les_sediments(n)%sigma = 3._long
   les_sediments(n)%tau = -1._long
end subroutine lire_Sediments


end module Geochimie
