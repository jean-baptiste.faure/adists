!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module resultats
!===============================================================================
! Routines d'affichage, d'écriture et d'extraction de résultats
!
! numérotation des STOP : 18nn
!===============================================================================
use iso_fortran_env, only: output_unit, error_unit
use parametres, only: sp, long, fnl
use utilitaires, only: lire_Date, next_int, capitalize, heure, bubble_sort, remplacer
use adis_types, only: dataset, solution, point_net
#ifdef openmp
use omp_lib
#endif /* openmp */

implicit none
character(len=fnl) :: outdir !dossier de résultats
integer :: nb_out !nb de profils de sortie de résultats
character (len=15), allocatable :: entete_csv(:)
character (len=3), allocatable :: numID(:)
integer, allocatable :: liste(:)

contains

subroutine write_screen(modele, sol, t, dt_moyen, dt_inf, dt_sup, peclet, peclet_min, peclet_max, cdisp, simul_lenght)
!==============================================================================
!                  écritures à l'écran
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + sol = solution au temps t ; type solution
!     + t   = temps en secondes ; réel
!     + dt_moyen  = pas de temps moyen en secondes ; réel
!     + dt_inf  = pas de temps min en secondes ; réel
!     + dt_sup  = pas de temps max en secondes ; réel
!     + peclet = nombre de peclet global (minimum) ; réel
!     + simul_lenght = durée de la simulation = t_end - t_start ; réel
!                      permet de choisir l'unité d'affichage du temps
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(in)  :: modele
   type (solution), intent(in) :: sol
   real(kind=long), intent(in) :: t, dt_moyen, dt_inf, dt_sup, peclet, peclet_min, peclet_max, simul_lenght
   real(kind=long), intent(in) :: cdisp(:)
   ! variables locales
   real(kind=long) :: tj, c2max, c2min, pcent, delta_stock_h2o, delta_stock_fnd, balance, cdisp_moy, cdisp_min, cdisp_max
   integer :: np, ltt
   integer, save :: lt
   character(len=30) :: space='          '
   character(len=6), save :: unite
   logical, save :: first_call = .true.
   character(len=8), save :: ref


   if (first_call) then
      if (simul_lenght > 864000.) then
         tj = t / 86400.  ;  unite='jours '
      else
         tj = t / 3600.   ;  unite='heures'
      endif
      first_call = .false.
      write(output_unit,'(a,f0.3,1x,a,3(a,f0.3),3(a,g0.5),a)') ' t = ',tj,unite,' dt moyen = ',dt_moyen, &
                             ' dt inf = ',dt_inf,' dt sup = ',dt_sup, ' Nb de Peclet = ',peclet, &
                             ' (min = ',peclet_min,' max = ',peclet_max,')'
      lt = 0
      do np = 1, modele%nbpol
         lt = max(lt,len_trim(modele%poll(np)%name))
      enddo
      do np = 1, modele%nbpol
         ltt = len_trim(modele%poll(np)%name)
         c2max = maxval(sol%c(1:modele%net%ismax,np))  ;  c2min = minval(sol%c(1:modele%net%ismax,np))
         write(output_unit,'(3x,a,1p,3(a,g0.3))') trim(modele%poll(np)%name)//space(ltt+1:lt),' Cmax = ',c2max,&
                 ' Cmin = ',c2min,' Caval = ',sol%c(modele%biefs(abs(modele%net%iub(modele%net%ibmax)))%is2,np)
         write(output_unit,'(3x,a,4(a,1p,e10.3,a))') space(1:lt),' Masses : in = ',sol%V_in(np),' kg', &
           ' Out = ',sol%v_out(np),' kg',' Eau = ',sol%stock_h2o(np),' kg', &
           ' Fond = ',sol%stock_fnd(np),' kg'
      enddo
   else
      if (unite == 'heures') then
         tj = t / 3600.
      else
         tj = t / 86400.
      endif
      write(output_unit,'(//,a,f0.3,1x,a,3(a,f0.3),3(a,g0.5),a)') ' t = ',tj,unite,' dt moyen = ',dt_moyen, &
                             ' dt inf = ',dt_inf,' dt sup = ',dt_sup
      write(output_unit,'(10x,3(a,g0.5),a)') ' Nombre de Peclet = ',peclet,' (min = ',peclet_min,' max = ',peclet_max,')'
      cdisp_moy = sum(cdisp(1:modele%net%ismax)) / real(modele%net%ismax,kind=long)
      cdisp_min = minval(cdisp(1:modele%net%ismax))
      cdisp_max = maxval(cdisp(1:modele%net%ismax))
      write(output_unit,'(10x,3(a,g0.5),a,/)') ' Coefficient de diffusion = ',cdisp_moy,' (min = ',cdisp_min,' max = ',cdisp_max,')'
!      write(output_unit,'(/,a,f0.3,1x,a,3(a,f0.3),(a,g0.5))') ' t = ',tj,unite,' dt moyen = ',dt_moyen, &
!                             ' dt inf = ',dt_inf,' dt sup = ',dt_sup, ' Nb de Peclet = ',peclet
      do np = 1, modele%nbpol
         ltt = len_trim(modele%poll(np)%name)
         c2max = maxval(sol%c(1:modele%net%ismax,np))  ;  c2min = minval(sol%c(1:modele%net%ismax,np))
!         pcent = abs(sol%V_in(np) * 0.01_long)
         write(output_unit,'(3x,a,1p,3(a,e10.3))') trim(modele%poll(np)%name)//space(ltt+1:lt),' Cmax = ',c2max,&
                 ' Cmin = ',c2min,' Caval = ',sol%c(modele%biefs(abs(modele%net%iub(modele%net%ibmax)))%is2,np)
         delta_stock_h2o = sol%stock_h2o(np) - sol%st0_h2o(np)
         delta_stock_fnd = sol%stock_fnd(np) - sol%st0_fnd(np)
         balance = sol%V_out(np) - (sol%V_in(np) - delta_stock_h2o - delta_stock_fnd)
         if (modele%poll(np)%particule_type > 0) then
            !pcent = abs(delta_stock_fnd * 0.01_long)
            !ref = ' % /D_fd'
            pcent = 0.01_long * (abs(sol%V_in(np)) - min(delta_stock_fnd,0._long))
            ref = ' % /in) '
         else
            pcent = abs(sol%V_in(np) * 0.01_long)
            ref = ' % /in) '
         endif
         if (pcent > 0._long) then
            write(output_unit,'(3x,2a,1p,e10.3,a,1p,3(a,e10.3,a),a,2(SPg0.3,a))') space(1:lt),' Masses : in = ',sol%V_in(np),' kg',&
              ' Out = ',sol%v_out(np),' kg',' Delta_Eau = ',delta_stock_h2o,' kg', ' Delta_Fond = ',delta_stock_fnd,' kg', &
              ' Bilan = ',balance,' kg (',balance/pcent,ref
         elseif (abs(balance) > 0._long) then
            write(output_unit,'(3x,2a,1p,e10.3,a,1p,3(a,e10.3,a),(a,SPg0.3,a))') space(1:lt),' Masses : in = ',sol%V_in(np),' kg', &
              ' Out = ',sol%v_out(np),' kg',' Delta_Eau = ',delta_stock_h2o,' kg', ' Delta_Fond = ',delta_stock_fnd,' kg', &
              ' Bilan = ',balance,' kg'
         else
            write(output_unit,'(3x,2a,1p,e10.3,a,1p,3(a,e10.3,a),a,2(g0.3,a))') space(1:lt),' Masses : in = ',sol%V_in(np),' kg', &
              ' Out = ',sol%v_out(np),' kg',' Delta_Eau = ',delta_stock_h2o,' kg', ' Delta_Fond = ',delta_stock_fnd,' kg', &
              ' Bilan = ',balance,' kg (',balance,ref
         endif

      enddo
   endif
end subroutine write_screen



subroutine write_csv(modele,sol,m_total_bief,t,nb_threads)
!==============================================================================
!            écritures sur fichier texte en format CSV
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + sol = solution au temps t ; type solution
!     + t   = temps en secondes ; réel
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(in) :: modele
   type (solution),intent(in) :: sol
   real(kind=long),intent(in) :: m_total_bief(:,:)
   real(kind=long),intent(in) :: t
   integer,intent(in)         :: nb_threads
   ! variables locales
   logical, save :: first_call = .true.
   integer :: rang, np, i, lw
   integer, allocatable, save :: lu(:), lv(:) !liste des unités logiques pour les fichiers CSV
   integer, save :: n1, n2
   character(len=15) :: stTemps
   character(len=11) :: stBief = '" MES Bief_'
   character(len=15) :: stMentrant, stMsortant, stStockH2O, stStockFond, stBilan
   character(len=40), save :: fmt1, fmt2 !format d'écriture des lignes du csv


   !fichier de sortie pour le contrôle des résultats ; longueur des champs = 15
   if (first_call) then
      stMentrant  = '"masse entrant"'
      stMsortant  = '"masse sortant"'
      stStockH2O  = '"  Stock H2O  "'
      stStockFond = '"  Stock Fond "'
      stBilan     = '"    Bilan    "'
      stTemps     = '"    Temps    "'
      n1 = 16 * (1+nb_out+5)-1  ;  n2 = 16 * (1+modele%net%ibmax+2)-1 !longueur des lignes
      write(fmt1,'(a1,i3.3,a)') '(',nb_out,'(a,'';''),a)'
      write(fmt2,'(a1,i3.3,a)') '(',modele%net%ibmax+5,'(a,'';''),a)'
      allocate (lu(modele%nbpol),lv(modele%nbpol))
      do np = 1, modele%nbpol
         open(newunit=lw,file=trim(outdir)//trim(modele%poll(np)%name)//'.mdata',form='formatted',status='unknown')
         write(lw,'(a)') '* ne pas changer l''ordre des lignes commençant par * '
         write(lw,'(a)') '* Titre : *Concentrations pour *'//trim(modele%poll(np)%name)
         write(lw,'(a)') '* Courbes'
         write(lw,'(a)') '* X : *Temps *Secondes'
         write(lw,'(a)') '* Y : *Concentration  *g/L'
         write(lw,'(a)') '* 1ere colonne : *1'
         write(lw,'(a)') '* Abscisse : *1'
         close(lw)
         open(newunit=lu(np),file=trim(outdir)//trim(modele%poll(np)%name)//'.csv',form='formatted',status='unknown')
         write(lu(np),fmt1) stTemps,(entete_csv(i),i=1,nb_out)

         open(newunit=lw,file=trim(outdir)//trim(modele%poll(np)%name)//'_masse.mdata',form='formatted',status='unknown')
         write(lw,'(a)') '* ne pas changer l''ordre des lignes commençant par * '
         write(lw,'(a)') '* Titre : *Masses pour *'//trim(modele%poll(np)%name)
         write(lw,'(a)') '* Courbes'
         write(lw,'(a)') '* X : *Temps *Secondes'
         write(lw,'(a)') '* Y : *Masse *kg'
         write(lw,'(a)') '* 1ere colonne : *1'
         write(lw,'(a)') '* Abscisse : *1'
         close(lw)
         open(newunit=lv(np),file=trim(outdir)//trim(modele%poll(np)%name)//'_masse.csv',form='formatted',status='unknown')
         write(lv(np),fmt2) stTemps,(stBief//numID(i)//'"',i=1,modele%net%ibmax),stMentrant,stMsortant,stStockH2O,&
                                                                              stStockFond, stBilan
      enddo
      write(fmt1,'(a,i3.3,a)') '(4x,i11,'';''',nb_out-1,'(e15.8,'';''),e15.8)'
      write(fmt2,'(a,i3.3,a)') '(4x,i11,'';''',modele%net%ibmax+4,'(e15.8,'';''),e15.8)'
      first_call = .false.
   endif
   !$omp parallel default(shared) private(np,rang,i) num_threads(nb_threads)
   do np = 1, modele%nbpol
#ifdef openmp
      rang = omp_get_thread_num()
      if (mod(np,nb_threads) /= rang) cycle
#endif /* openmp */
      write(lu(np),fmt1) int(t),(sol%c(liste(i),np),i=1,nb_out)
      !bilan = volume sorti + variation stock dans l'eau + variation stock au fond - volume entré
      write(lv(np),fmt2) int(t),(m_total_bief(i,np),i=1,modele%net%ibmax),sol%v_in(np),sol%v_out(np), &
                           sol%stock_h2o(np),sol%stock_fnd(np),sol%V_out(np)+sol%stock_h2o(np) &
                          -sol%st0_h2o(np)+sol%stock_fnd(np)-sol%st0_fnd(np) -sol%V_in(np)
   enddo
   !$omp end parallel
end subroutine write_csv



subroutine write_bin(modele,sol,t)
!==============================================================================
!                  écritures sur fichier binaire
!
!  Entrée :
!     + modele = jeu de données ; type dataset
!     + sol = solution au temps t ; type solution
!     + t   = temps en secondes ; réel
!==============================================================================
   implicit none
   ! prototype
   type (dataset), intent(in)  :: modele
   type (solution), intent(inout) :: sol
   real(kind=long), intent(in) :: t
   ! variables locales
   logical, save :: first_call = .true.
   integer :: np, i, ib, ks, is
   integer, allocatable, save :: lu(:)  !unités logiques des fichiers BIN
   integer :: ismax, ibmax, kbl = 1000
   real(kind=sp) :: zmin_OLD = 0. !on a supprimé la normalisation des cotes par Zmin dans MAGE
   real(kind=long), save, allocatable :: al_g(:,:), al_m(:,:), al_d(:,:)


   ismax = modele%net%ismax  ;  ibmax = modele%net%ibmax
   !fichier de sortie pour le contrôle des résultats
   if (first_call) then
      allocate (al_g(modele%net%ismax,modele%nbpol),al_m(modele%net%ismax,modele%nbpol),al_d(modele%net%ismax,modele%nbpol))
      allocate (lu(0:modele%nbpol))
      open(newunit=lu(0),file=trim(outdir)//'total_sediment'//'.bin',form='unformatted',status='unknown')
      do np = 1, modele%nbpol
         open(newunit=lu(np),file=trim(outdir)//trim(modele%poll(np)%name)//'.bin',form='unformatted',status='unknown')
      enddo
      do np = 0, modele%nbpol
         ! écriture de l'entête, identique à celle de BIN
         write(lu(np)) ibmax, ismax, -kbl !version pour MAGE modifie
         write(lu(np)) (modele%net%ibu(ib),ib=1,ibmax)
         write(lu(np)) (modele%biefs(ib)%is1,modele%biefs(ib)%is2,ib=1,ibmax)
         do ks = 1, ismax, kbl
            write(lu(np)) (real(modele%sections(is)%pk,kind=sp), is=ks,min(ks+kbl-1,ismax))
         enddo
         write(lu(np)) zmin_OLD
         do ks = 1, ismax, kbl
            write(lu(np)) (real(modele%sections(is)%zf,kind=sp),zmin_OLD,zmin_OLD,is=ks,min(ks+kbl-1,ismax))
         enddo
         do ks = 1, ismax, kbl
            write(lu(np)) (0, is=ks,min(ks+kbl-1,ismax))
         enddo

      enddo
      first_call = .false.
   endif
   do np = 1, modele%nbpol
      write(lu(np)) ismax,t,'C',(real(sol%c(i,np),kind=sp),i=1,ismax)

      if (modele%poll(np)%particule_type > 0) then
         write(lu(np)) ismax,t,'G',(real(sol%masse(i,1,np),kind=sp),i=1,ismax)
         write(lu(np)) ismax,t,'M',(real(sol%masse(i,2,np),kind=sp),i=1,ismax)
         write(lu(np)) ismax,t,'D',(real(sol%masse(i,3,np),kind=sp),i=1,ismax)
         write(lu(np)) ismax,t,'L',(real(sol%c_eq_ero(i,1,np),kind=sp),i=1,ismax)
         write(lu(np)) ismax,t,'N',(real(sol%c_eq_ero(i,2,np),kind=sp),i=1,ismax)
         write(lu(np)) ismax,t,'R',(real(sol%c_eq_ero(i,3,np),kind=sp),i=1,ismax)
      endif
   enddo
   write(lu(0)) ismax,t,'C',(real(sol%C_total(i),kind=sp),i=1,ismax)
   write(lu(0)) ismax,t,'G',(real(sol%Hg(i),kind=sp),i=1,ismax)
   write(lu(0)) ismax,t,'M',(real(sol%Hm(i),kind=sp),i=1,ismax)
   write(lu(0)) ismax,t,'D',(real(sol%Hd(i),kind=sp),i=1,ismax)
end subroutine write_bin



subroutine extraire(binFile, type2courbe, p1, p2, tmpFile)
!==============================================================================
!            Extraction de données d'un fichier de résultats binaire
!
!  On peut extraire
!     + soit une F(t) à x fixé
!     + soit une F(x) à t fixé
!
!  Entrée :
!     + binFile = nom du fichier de résultat
!     + type2courbe = type de la courbe à extraire ; chaine de 3 caractères
!     + p1 = paramètre ; chaine de caractères
!     + p2 = paramètre ; chaine de caractères
!     + tmpFile = nom du fichier de sortie qui contient les données extraites
!                 si tmpFile est la chaine vide les données extraites sont
!                 affichées à l'écran
!==============================================================================
   implicit none
   ! prototype
   character(len=fnl), intent(in) :: binFile
   character(len=3), intent(in) :: type2courbe
   character(len=*), intent(in) :: p1, p2
   character(len=*), intent(in) :: tmpFile
   ! variables locales
   integer :: ibmax, ismax, kbl
   integer, allocatable :: ibu(:), is1(:), is2(:), ibs(:)
   real(kind=sp), allocatable :: y(:), xl(:), zfd(:), ygeo(:), ybas(:)
   real(kind=sp) :: zmin_OLD
   character(len=3) :: type_courbe

   integer :: lu, i, ns, is, ib, isa, isb, npts, ks, ltmp, ios
   integer :: jjj, hh, mm, ss, nf, ierr
   character(len=1) :: a
   real(kind=long) :: t, pk, dxmin, tr

   ! fichier de sortie
   if (tmpFile /= '') then
      open(newunit=ltmp, file=trim(tmpFile), form='formatted', status='unknown')
   else
      ltmp = output_unit
   endif

   call capitalize(type2courbe,type_courbe)

   ! ouverture du fichier et lecture de l'entête
   open(newunit=lu,file=trim(binFile),form='unformatted',status='old',iostat=ios)
   if (ios /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier BIN ',trim(binFile),' impossible'
      stop 1801
   endif
   read(lu) ibmax, ismax, kbl !version pour MAGE modifie
   if (kbl > 0) then
      write(output_unit,*) '>>>> Erreur : Veuillez relancer une version de Adis-TS > 0.82'  !temps stockés en 64 bits
      stop 1802
   else
      kbl = -kbl
   endif
   !allocations
   allocate(ibu(ibmax), is1(ibmax), is2(ibmax))
   allocate(ibs(ismax), xl(ismax), zfd(ismax), ygeo(ismax), ybas(ismax), y(ismax))

   read(lu) (ibu(ib),ib=1,ibmax)
   read(lu) (is1(ib),is2(ib),ib=1,ibmax)
   do ks = 1, ismax, kbl
      read(lu) (xl(is), is=ks,min(ks+kbl-1,ismax))
   enddo
   read(lu) zmin_OLD
   do ks = 1, ismax, kbl
      read(lu) (zfd(is),ygeo(is),ybas(is),is=ks,min(ks+kbl-1,ismax))
   enddo
   do ks = 1, ismax, kbl
      read(lu) (ibs(is), is=ks,min(ks+kbl-1,ismax))
   enddo

   t = -huge(0._long)  ;  isa = -1  ;  isb = -1  ;  is = 0  !pour faire plaisir au compilateur
   if (type_courbe(3:3) == 't' .OR. type_courbe(3:3) == 'T') then
      is = 0  ;  dxmin = 1.e+30
      read(p1,*) ib  ;  read(p2,*) pk
      do ns = is1(ib), is2(ib)
         if (abs(xl(ns)-Pk) < dxmin) then
            dxmin = abs(xl(ns)-Pk)  ;  is = ns
         endif
      enddo
      if (is == 0) then
         write(output_unit,*) '>>>> Erreur : Pk non trouvé !!!'
         stop 1803
      endif
      write(ltmp,*) '# Bief ',ib,' Pk = ',xl(is)
   elseif (type_courbe(3:3) == 'x' .OR. type_courbe(3:3) == 'X') then
      read(p1,*) ib  !;  read(p2,*) t
      nf = 1
!       JJJ = next_int(p2,':',nf)  ;  HH = next_int(p2,':',nf)
!       MM = next_int(p2,':',nf)   ;  SS = next_int(p2,':',nf)
!       t = real(jjj,long)*86400._long + hh*3600._long + mm*60 + ss
      t = real(lire_Date(p2),kind=long)
      write(ltmp,'(a,i3.3,3a,i0,a)') '# Bief ',ib,' au temps ',trim(p2),' (',int(t),' secondes)'
      isa = is1(ib)  ;  isb = is2(ib)
   else
      write(output_unit,*) '>>>> Erreur : type de courbe à extraire invalide'
      stop 1804
   endif

   i = ichar(type_courbe(1:1))
   if (i >= ichar('a') .AND. i <= ichar('z')) type_courbe(1:1) = char(i+ichar('A')-ichar('a'))
   select case (type_courbe(3:3))
      case ('x', 'X')
         do
            read (lu, iostat=ierr) npts,tr,a,(y(i),i=1,npts)
            if (ierr < 0) then !fin du fichier
               write(output_unit,*) '>>>> Erreur : Résultats non trouvés'
               stop 1805
            else if (ierr > 0) then  !erreur
               stop 1806
            else if (a == type_courbe(1:1) .and. tr >= t) then
               write(ltmp,'(2g14.6)') (xl(i), y(i), i=isa,isb)
               exit
            endif
         enddo
      case ('t', 'T')
         do
            read (lu, iostat=ierr) npts,tr,a,(y(i),i=1,npts)
            if (ierr < 0) then  !fin du fichier
               exit
            else if (ierr > 0) then !erreur
               stop 1807
            else if (a == type_courbe(1:1)) then
               write(ltmp,*) tr/3600., y(is)
            endif
         enddo
      case default
         write(output_unit,*) '>>>> Erreur : type de courbe à extraire invalide'
         stop 1808
   end select
   close (lu)
   if (ltmp /= output_unit) close (ltmp)
   deallocate(ibu, is1, is2, ibs, xl, zfd, ygeo, ybas, y)
end subroutine extraire



subroutine extraire_FDT(binFile, var, pts, nbc, outName)
!==============================================================================
!      Extraction de plusieurs courbes F(t) d'un fichier de résultats binaire
!
!  Entrée :
!     + binFile = nom du fichier de résultat
!     + var = variable à extraire ; 1 caractère
!     + pts = liste de points ; type point_net
!     + nbc = nombre de courbes à extraire
!  Sortie :
!     + 2 fichiers texte :
!     + outFile : fichier csv
!     + metaFile : fichier de métadonnées
!==============================================================================
   implicit none
   ! prototype
   character(len=fnl), intent(in) :: binFile
   character(len=1), intent(in)  :: var
   type(point_net), intent(in)   :: pts(*)
   integer, intent(in)           :: nbc
   character(len=*), optional    :: outName
   ! variables locales
   integer, allocatable :: js(:)
   character(len=30), allocatable :: entete(:)
   integer :: ibmax, ismax, kbl
   integer, allocatable :: ibu(:), is1(:), is2(:), ibs(:)
   real(kind=sp), allocatable :: y(:), xl(:), zfd(:), ygeo(:), ybas(:)
   real(kind=sp) :: zmin_OLD

   integer :: lu, i, ns, is, ib, npts, ks, out, n, meta
   integer :: ierr
   character(len=1) :: a, fdt
   character(len=60) :: outFile, metaFile
   real(kind=long) :: pk, dxmin, tr
   character :: ctmp*60, fmt1*40
   character(len=30) :: varName, titre, unite, stTemps = '"    Temps    "'
   character(len=120) :: cmdline
   character :: current_date*8, current_time*10

   allocate (js(nbc),entete(nbc))

   call capitalize(var,fdt)

   !fichier de sortie
   if (present(outName)) then
      outFile = trim(outName)//'.csv'
      metaFile = trim(outName)//'.mdata'
   else
      call date_and_time(date=current_date,time=current_time)  ;  current_time(7:9) = current_time(8:10)
      n = len_trim(binFile)
      write(ctmp,'(*(a))') binFile(1:n-4),'_b_',current_date,'_',current_time(1:9)
      outFile = trim(ctmp)//'.csv'
      metaFile = trim(ctmp)//'.mdata'
   endif
   ! NOTE: MS-Windows n'aime pas les : dans les noms de fichier
   call remplacer(outFile,':','-')
   call remplacer(metaFile,':','-')
   open(newunit=out, file=trim(outFile), form='formatted', status='unknown')
   open(newunit=meta, file=trim(metaFile), form='formatted', status='unknown')

   ! ouverture du fichier et lecture de l'entête
   open(newunit=lu,file=trim(binFile),form='unformatted',status='old',iostat=ierr)
   if (ierr /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier BIN ',trim(binFile),' impossible'
      stop 1809
   endif
   read(lu) ibmax, ismax, kbl !version pour MAGE modifie
   if (kbl > 0) then
      write(output_unit,*) '>>>> Erreur : Veuillez relancer une version de Adis-TS > 0.82'  !temps stockés en 64 bits
      stop 1810
   else
      kbl = -kbl
   endif
   !allocations
   allocate(ibu(ibmax), is1(ibmax), is2(ibmax))
   allocate(ibs(ismax), xl(ismax), zfd(ismax), ygeo(ismax), ybas(ismax), y(ismax))

   read(lu) (ibu(ib),ib=1,ibmax)
   read(lu) (is1(ib),is2(ib),ib=1,ibmax)
   do ks = 1, ismax, kbl
      read(lu) (xl(is), is=ks,min(ks+kbl-1,ismax))
   enddo
   read(lu) zmin_OLD
   do ks = 1, ismax, kbl
      read(lu) (zfd(is),ygeo(is),ybas(is),is=ks,min(ks+kbl-1,ismax))
   enddo
   do ks = 1, ismax, kbl
      read(lu) (ibs(is), is=ks,min(ks+kbl-1,ismax))
   enddo
   ! fin de la lecture de l'entête de binFile

   ! écriture du fichier de métadonnées
   select case (var)
      case ('c','C')
         varName = 'Concentration'
         titre = 'Concentrations'
         unite = 'g/L'
      case ('m', 'M', 'g', 'G', 'd', 'D')
         if (trim(binFile) == 'total_sediment.bin') then
            varName = 'Épaisseur'
            titre = 'Épaisseurs'
            unite = 'm'
         else
            varName = 'Masse'
            titre = 'Masses'
            unite = 'kg'
         endif
      case ('l', 'L', 'n', 'N', 'r', 'R')
         varName = 'C_équilibre'
         titre = 'Concentrations d''équilibre'
         unite = 'g/L'
   end select
   ctmp = binFile(1:len_trim(binFile)-4)  ! nom du polluant
   write(meta,'(a)') '* ne pas changer l''ordre des lignes commençant par * '
   write(meta,'(a)') '* Titre : *'//trim(titre)//' pour *'//trim(ctmp)
   write(meta,'(a)') '* Courbes'
   write(meta,'(a)') '* X : *Temps *Secondes'
   write(meta,'(a)') '* Y : *'//trim(varName)//'  *'//trim(unite)
   write(meta,'(a)') '* 1ere colonne : *1'
   write(meta,'(a)') '* Abscisse : *1'
   call get_command(cmdline)
   write(meta,'(a)') '* '//trim(cmdline)
   close (meta)

   ! repérage des colonnes à extraire
   do n = 1, nbc
      js(n) = 0  ;  dxmin = 1.e+30
      ib = pts(n)%ib  ;  pk = pts(n)%pk
      do ns = is1(ib), is2(ib)
         if (abs(xl(ns)-Pk) < dxmin) then
            dxmin = abs(xl(ns)-Pk)  ;  js(n) = ns
         endif
      enddo
      if (js(n) == 0) then
         write(output_unit,*) '>>>> Erreur : Pk non trouvé !!!'
         stop 1811
      endif
      write(ctmp,'(f0.3)') pk
      write(entete(n),'(a,i3.3,3a)') '"Point_',ib,'_',trim(ctmp),'"'
   enddo
   ! écriture de la ligne d'entête du fichier csv
   write(fmt1,'(a1,i3.3,a)') '(',nbc,'(a,'';''),a)'
   write(out,fmt1) trim(stTemps),(trim(entete(n)),n=1,nbc)

   ! lecture de binFile et extraction des colonnes voulues
   if (nbc > 1) then
      write(fmt1,'(a,i3.3,a)') '(4x,i11,'';''',nbc-1,'(e15.8,'';''),e15.8)'
   else
      write(fmt1,'(a)') '(4x,i11,'';'',e15.8)'
   endif
   do
! NOTE: utiliser kbl comme borne pour y(:) est une très mauvaise idée, car kbl est indéterminé lors de la dernière tentative de
! NOTE: lecture du fichier. En mode debug le dépassement de tableau fait planter le code avant la détection de la fin du fichier
      read (lu, iostat=ierr) npts,tr,a,(y(i),i=1,ismax)
      if (ierr < 0) then  !fin du fichier
         exit
      else if (ierr > 0) then !erreur
         stop 1812
      else if (a == fdt) then
         write(out,fmt1) int(tr), (y(js(n)),n=1,nbc)
      else
         cycle
      endif
   enddo
   close (lu)
   deallocate(ibu, is1, is2, ibs, xl, zfd, ygeo, ybas, y)
   deallocate (js,entete)

end subroutine extraire_FDT



subroutine extraire_FDX(binFile, var, kb, dates, nbc, outName)
!==============================================================================
!      Extraction de plusieurs courbes F(x) d'un fichier de résultats binaire
!
!  Entrée :
!     + binFile = nom du fichier de résultat
!     + var = variable à extraire ; 1 caractère
!     + kb = numéro du bief
!     + dates = liste de dates en jjjjjj[:hh:mm:ss]
!     + nbc = nombre de courbes à extraire
!  Sortie :
!     + 2 fichiers texte :
!     + outFile : fichier csv
!     + metaFile : fichier de métadonnées
!==============================================================================
   implicit none
   ! prototype
   character(len=fnl), intent(in) :: binFile
   character(len=1), intent(in)  :: var
   character(len=15), intent(in) :: dates(*)
   integer, intent(in)           :: kb, nbc
   character(len=*), optional    :: outName
   ! variables locales
   integer :: ibmax, ismax, kbl
   integer, allocatable :: ibu(:), is1(:), is2(:), ibs(:)
   real(kind=sp), allocatable :: xl(:), zfd(:), ygeo(:), ybas(:)
   real(kind=sp) :: zmin_OLD

   integer :: lu, i, is, ib, isa, isb, npts, ks, out, n, meta
   integer :: ierr
   character(len=1) :: fdx, a
   character(len=60) :: outFile, metaFile
   real(kind=long) :: tr
   real(kind=sp), allocatable :: yl(:,:)
   real(kind=long), allocatable :: ts(:)
   integer, allocatable :: js(:)
   character(len=17), allocatable :: entete(:)
   character :: ctmp*300, fmt1*40
   character(len=15) :: stPk = '"      Pk     "'
   character(len=30) :: varName, titre, unite
   character(len=120) :: cmdline
   character :: current_date*8, current_time*10

   call capitalize(var,fdx)
   allocate (js(nbc),entete(nbc),ts(nbc))

   ! conversion des dates en réels (secondes)
   do n = 1, nbc
      ts(n) = real(lire_Date(dates(n)),kind=long)
   enddo
   ! il faut que la liste des dates soit triée en ordre croissant
   call bubble_sort(ts)

   !fichier de sortie
   if (present(outName)) then
      outFile = trim(outName)//'.csv'
      metaFile = trim(outName)//'.mdata'
   else
      call date_and_time(date=current_date,time=current_time)  ;  current_time(7:9) = current_time(8:10)
      n = len_trim(binFile)
      write(ctmp,'(*(a))') binFile(1:n-4),'_b_',current_date,'_',current_time(1:9)
      outFile = trim(ctmp)//'.csv'
      metaFile = trim(ctmp)//'.mdata'
   endif
   ! NOTE: MS-Windows n'aime pas les : dans les noms de fichier
   call remplacer(outFile,':','-')
   call remplacer(metaFile,':','-')
   open(newunit=out, file=trim(outFile), form='formatted', status='unknown')
   open(newunit=meta, file=trim(metaFile), form='formatted', status='unknown')


   ! ouverture du fichier et lecture de l'entête
   open(newunit=lu,file=trim(binFile),form='unformatted',status='old',iostat=ierr)
   if (ierr /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier BIN ',trim(binFile),' impossible'
      stop 1813
   endif
   read(lu) ibmax, ismax, kbl !version pour MAGE modifie
   if (kbl > 0) then
      write(output_unit,*) '>>>> Erreur : Veuillez relancer une version de Adis-TS > 0.82'  !temps stockés en 64 bits
      stop 1814
   else
      kbl = -kbl
   endif
   !allocations
   allocate(ibu(ibmax), is1(ibmax), is2(ibmax))
   allocate(ibs(ismax), xl(ismax), zfd(ismax), ygeo(ismax), ybas(ismax))
   allocate(yl(ismax,nbc))

   read(lu) (ibu(ib),ib=1,ibmax)
   read(lu) (is1(ib),is2(ib),ib=1,ibmax)
   do ks = 1, ismax, kbl
      read(lu) (xl(is), is=ks,min(ks+kbl-1,ismax))
   enddo
   read(lu) zmin_OLD
   do ks = 1, ismax, kbl
      read(lu) (zfd(is),ygeo(is),ybas(is),is=ks,min(ks+kbl-1,ismax))
   enddo
   do ks = 1, ismax, kbl
      read(lu) (ibs(is), is=ks,min(ks+kbl-1,ismax))
   enddo
   ! fin de la lecture de l'entête de binFile

   ! lecture de binFile et extraction des lignes voulues
   if (kb < 1) then
      write(output_unit,*) '>>>> Erreur : le numéro de bief doit être positif'
      stop 1815
   elseif (kb > ibmax) then
      write(output_unit,'(a,i3.3)') ' >>>> Erreur : le numéro de bief doit être inférieur ou égal à ',ibmax
      stop 1816
   else
      isa = is1(kb)  ;  isb = is2(kb)
   endif
   do n = 1, nbc
      do
! NOTE: utiliser kbl comme borne pour yl(:) est une très mauvaise idée, car kbl est indéterminé lors de la dernière tentative de
! NOTE: lecture du fichier. En mode debug le dépassement de tableau fait planter le code avant la détection de la fin du fichier
         read (lu, iostat=ierr) npts,tr,a,(yl(i,n),i=1,ismax)
         if (ierr < 0) then  !fin du fichier
            exit
         else if (ierr > 0) then !erreur
            write(output_unit,*) '>>>> Erreur : ',ierr, ' --> ',n, a, ts(n), tr
            stop 1817
         else if (a == fdx .and. tr >= ts(n)) then
            ! on garde les yl lus
            exit
         else
            cycle
         endif
      enddo
   enddo
   close (lu)

   ! création des entêtes et écriture de la ligne d'entête du fichier csv
   do n = 1, nbc
      call heure(ts(n),ctmp(1:15))
      write(entete(n),'(3a)') '"',ctmp(1:15),'"'
   enddo
   write(fmt1,'(a1,i3.3,a)') '(',nbc,'(a,'';''),a)'
   write(out,fmt1) stPk,(trim(entete(n)),n=1,nbc)
   ! écriture du fichier csv
   if (nbc > 1) then
      write(fmt1,'(a,i3.3,a)') '(4x,f11.3,'';''',nbc-1,'(e17.8,'';''),e17.8)'
   else
      write(fmt1,'(a)') '(4x,f11.3,'';'',e17.8)'
   endif
   do i = isa, isb
      write(out,fmt1) xl(i),(yl(i,n),n=1,nbc)
   enddo
   close (out)

   ! écriture du fichier de métadonnées
   select case (var)
      case ('c','C')
         varName = 'Concentration'
         titre = 'Concentrations'
         unite = 'g/L'
      case ('m', 'M', 'g', 'G', 'd', 'D')
         if (trim(binFile) == 'total_sediment.bin') then
            varName = 'Épaisseur'
            titre = 'Épaisseurs'
            unite = 'm'
         else
            varName = 'Masse'
            titre = 'Masses'
            unite = 'kg'
         endif
      case ('l', 'L', 'n', 'N', 'r', 'R')
         varName = 'C_équilibre'
         titre = 'Concentrations d''équilibre'
         unite = 'g/L'
   end select
   ctmp = binFile(1:len_trim(binFile)-4)  ! nom du polluant
   write(meta,'(a)') '* ne pas changer l''ordre des lignes commençant par * '
   write(meta,'(a,i3.3)') '* Titre : *'//trim(titre)//' pour *'//trim(ctmp)//' * bief * ',kb
   write(meta,'(a)') '* Courbes'
   write(meta,'(a)') '* X : *Pk *mètres'
   write(meta,'(a)') '* Y : *'//trim(varName)//'  *'//trim(unite)
   write(meta,'(a)') '* 1ere colonne : *1'
   write(meta,'(a)') '* Abscisse : *1'
   call get_command(cmdline)
   write(meta,'(a)') '* '//trim(cmdline)
   close (meta)

   deallocate(ibu, is1, is2, ibs, xl, zfd, ygeo, ybas, yl)
   deallocate (js,entete,ts)

end subroutine extraire_FDX



subroutine extraire_BarChart(csvFile, dates, nbc, outName)
!==============================================================================
!      Extraction de plusieurs lignes d'un fichier de résultats csv
!   et transposition de ces lignes en vue de les représenter en bar-chart
!
!  Entrée :
!     + csvFile = nom du fichier de résultat
!     + dates = liste de dates en jjjjjj[:hh:mm:ss]
!     + nbc = nombre de lignes à extraire
!  Sortie :
!     + 2 fichiers texte :
!     + outFile : fichier csv
!     + metaFile : fichier de métadonnées
!==============================================================================
   implicit none
   ! prototype
   character(len=fnl), intent(in) :: csvFile
   character(len=15), intent(in) :: dates(*)
   integer, intent(in)           :: nbc
   character(len=*), optional    :: outName
   ! variables locales
   character(len=60) :: outFile, metaFile
   character(len=15), allocatable :: entete(:)

   integer :: in, out, meta, ierr, nf, n, nct, k, j
   real(kind=long), allocatable :: ts(:)
   character :: ctmp*300
   character(len=1800) :: ligne
   character(len=15), allocatable :: strings(:,:)
   character(len=120) :: cmdline
   character :: current_date*8, current_time*10

   allocate (entete(nbc),ts(nbc))

   ! conversion des dates en réels (secondes)
   do n = 1, nbc
      ts(n) = real(lire_Date(dates(n)),kind=long)
   enddo
   ! il faut que la liste des dates soit triée en ordre croissant
   call bubble_sort(ts)

   ! fichiers de sortie
   if (present(outName)) then
      outFile = trim(outName)//'.csv'
      metaFile = trim(outName)//'.mdata'
   else
      call date_and_time(date=current_date,time=current_time)  ;  current_time(7:9) = current_time(8:10)
      n = len_trim(csvFile)
      write(ctmp,'(*(a))') csvFile(1:n-4),'_c_',current_date,'_',current_time(1:9)
      outFile = trim(ctmp)//'.csv'
      metaFile = trim(ctmp)//'.mdata'
   endif
   ! NOTE: MS-Windows n'aime pas les : dans les noms de fichier
   call remplacer(outFile,':','-')
   call remplacer(metaFile,':','-')
   open(newunit=out, file=trim(outFile), form='formatted', status='unknown')
   open(newunit=meta, file=trim(metaFile), form='formatted', status='unknown')
   open(newunit=in, file=trim(csvFile), form='formatted', status='old',iostat=ierr)
   if (ierr /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier CSV ',trim(csvFile),' impossible'
      stop 1818
   endif

   !extraction des catégories
   ligne(1:1) = '#'
   do while (ligne(1:1) == '#' .or. ligne(1:1) == '*')
      read(in,'(a)') ligne
   enddo
   nf = 0 ; nct = 0 ; j = 0
   do
      nf = scan(ligne(j+1:),';')
      if (nf > 0) then
         j = j + nf
         nct = nct+1 !nombre de catégories c'est-à-dire de colonnes dans le csv
      elseif (nf == 0) then
         nct = nct+1
         exit
      else
         write(output_unit,*) '>>>> Erreur : Problème dans le comptage des catégories'
         stop 1819
      endif
   enddo
   allocate(strings(0:nbc,nct))
   nf = 0 ; j = 0
   do k = 1, nct-1
      nf = scan(ligne(j+1:),';')
      strings(0,k) = ligne(j+2:j+nf-2)
      j = j+nf
   enddo
   strings(0,nct) = ligne(j+2:len_trim(ligne)-1)
   do n = 1, nbc
      do
         read(in,'(a)') ligne
         nf = 1
         if (next_int(ligne,';',nf) == int(ts(n))) then
            nf = 0 ; j = 0
            do k = 1, nct-1
               nf = scan(ligne(j+1:),';')
               strings(n,k) = ligne(j+1:j+nf-1)
               j = j+nf
            enddo
            strings(n,nct) = trim(ligne(j+1:))
            exit
         endif
      enddo
   enddo
   close (in)
   ! écriture de outFile
   do k = 1, nct
      write(out,'(*(a))') strings(0,k)(:13),';',(trim(strings(n,k)),';',n=1,nbc-1),trim(strings(nbc,k))
   enddo
   close (out)
   ! écriture du fichier de métadonnées par recopie et modification du fichier de métadonnées de csvFile
   n = scan(csvFile,'.',back=.true.)
   open(newunit=in,file=csvFile(1:n)//'mdata',form='formatted',status='old',iostat=ierr)
   if (ierr /= 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier mdata ',csvFile(1:n)//'mdata',' impossible'
      stop 1820
   endif
   do
      read(in,'(a)',iostat=ierr) ligne
      if (ierr < 0) then
         exit
      elseif (ierr > 0) then
         write(output_unit,*) '>>>> Erreur de relecture du fichier de métadonnées'
         stop 1821
      else
         if (ligne(1:9) == '# Courbes') then
            write(meta,'(a)') '# Histogramme'
         else
            write(meta,'(a)') trim(ligne)
         endif
      endif
   enddo
   call get_command(cmdline)
   write(meta,'(a)') '# '//trim(cmdline)
   close (meta)
end subroutine extraire_BarChart


end module resultats
