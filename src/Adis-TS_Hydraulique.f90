!##############################################################################
!#                                                                            #
!#                           PROGRAM Adis-TS                                  #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program is free software; you can redistribute it and/or              #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program is distributed in the hope that it will be useful, but        #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program. If not, see <http://www.gnu.org/licenses/>.                  #
!##############################################################################
!
module Hydraulique
!===============================================================================
!                      Routines hydrauliques
!
! numérotation des STOP : 16nn
!===============================================================================
use iso_fortran_env, only: output_unit, error_unit
use parametres, only: sp, long, fnl, g, rho, zero, un, pi, deuxtiers
use utilitaires, only: date_et_heure
use Geometrie_Section, only: perimetre, section, get_data_geom, limite_Eau
use adis_types, only: state, cache, dataset, profil, data_hydrauliques, init_datahyd, state_constructor
use fatal_errors, only: fatal_error
use datanum, only: nb_threads

#ifdef openmp
use omp_lib
#endif /* openmp */

implicit none


contains

subroutine copy_etat (e1, e2, ismax)
!==============================================================================
!  copie globalement l'état e1 sur l'état e2
!==============================================================================
   !prototype
   type (state), intent(inout) :: e1
   type (state), intent(inout) :: e2
   integer, intent(in) :: ismax

   !variables locale
   integer :: is

   ! normalement ça ne doit pas arriver, mais on vérifie quand même
   if (.not.allocated(e1%x) .or. .not.allocated(e2%x)) then
      write(output_unit,*) 'etats non alloués dans copy_etat()'
      stop 1601
   endif
   e2%time = e1%time
!$omp parallel default(shared) private(is) num_threads(nb_threads)
!$omp do schedule(static)
   do is = 1, ismax
      e2%x(is) = e1%x(is)
      e2%y(is) = e1%y(is)
      e2%a(is) = e1%a(is)
      e2%q(is) = e1%q(is)
      e2%u(is) = e1%u(is)
   enddo
!$omp end do
!$omp end parallel
end subroutine copy_etat



subroutine copy_etat_elt(e1, e2, is)
!==============================================================================
!  copie l'état e1 sur l'état e2 pour la section is
!==============================================================================
   !prototype
   type (state), intent(in) :: e1
   type (state), intent(inout) :: e2
   integer, intent(in) :: is

   e2%x(is) = e1%x(is)
   e2%y(is) = e1%y(is)
   e2%a(is) = e1%a(is)
   e2%q(is) = e1%q(is)
   e2%u(is) = e1%u(is)
end subroutine copy_etat_elt



function is_short_BIN(bin)
!==============================================================================
!     détecte si le fichier BIN fourni est un fichier court ou long
!     fichier court : seulement Q et Z à chaque pas de temps
!     fichier long  : Q, Z + 4 autres champs
!==============================================================================
   !prototype
   type (cache), intent(inout) :: bin  !cache pour les lignes d'eau (LE)
   logical :: is_short_BIN

   !variables locales
   integer :: ibmax, ismax, kbl
   integer, allocatable :: ibu(:), is1(:), is2(:), ibs(:)
   real(kind=sp) :: zmin_OLD !on a supprimé la normalisation des cotes par Zmin dans MAGE
   real(kind=sp), allocatable :: xl(:), zfd(:), ygeo(:), ybas(:)
   real(kind=long) :: tr1, tr2, tr3
   character (len=1) :: a
   integer :: lu, ib, ks, is, size_ecr, mage_version
   logical :: is_Mage8

   !ouverture du fichier BIN
   open(newunit=lu,file=trim(bin%file),status='old',form='unformatted',err=994)

   ! lecture de l'entête
   read(lu) ibmax, ismax, mage_version !version pour MAGE modifie
   !vérifications et allocations
   if (mage_version >= 80) then
      !nouvel entête de BIN pour Mage-8
      is_Mage8 = .true.
   else
      !ancien entête de BIN pour Mage-7
      is_Mage8 = .false.
      kbl = abs(mage_version)
   endif
   ! allocations
   allocate(ibu(ibmax), is1(ibmax), is2(ibmax))
   allocate(ibs(ismax), xl(ismax), zfd(ismax), ygeo(ismax), ybas(ismax))
   ! suite lecture
   if (is_Mage8) then
      read(lu) (is1(ib),is2(ib),ib=1,ibmax)
      read(lu) (xl(is), is=1,ismax)
      read(lu) (zfd(is),ygeo(is),ybas(is),is=1,ismax)
      size_ecr = sp*(3+2*ibmax+4*ismax)
   else
      read(lu) (ibu(ib),ib=1,ibmax)
      read(lu) (is1(ib),is2(ib),ib=1,ibmax)
      do ks = 1, ismax, kbl
         read(lu) (xl(is), is=ks,min(ks+kbl-1,ismax))
      enddo
      read(lu) zmin_OLD
      do ks = 1, ismax, kbl
         read(lu) (zfd(is),ygeo(is),ybas(is),is=ks,min(ks+kbl-1,ismax))
      enddo
      do ks = 1, ismax, kbl
         read(lu) (ibs(is), is=ks,min(ks+kbl-1,ismax))
      enddo
      size_ecr = sp*(3+3*ibmax+ismax+1+3*ismax+ismax+14)
   endif

   !lecture des deux premiers enregistrements : on doit trouver Q et Z
   read(lu,end=995) kbl, tr1, a, (xl(is),is=1,ismax)
   if (a .ne. 'Q') then
      write(output_unit,*) '>>>> Erreur dans is_short_BIN : ',a, ' au lieu de Q'
      stop 1603
   endif
   read(lu,end=995) kbl, tr2, a, (xl(is),is=1,ismax)
   if (a .ne. 'Z') then
      write(output_unit,*) '>>>> Erreur dans is_short_BIN : ',a, ' au lieu de Z'
      stop 1604
   endif
   !lecture du 3e enregistrement : si Q alors c'est un fichier court
   read(lu,end=995) kbl, tr3, a, (xl(is),is=1,ismax)
   if (a .eq. 'Q' .and. tr3 .gt. tr1) then
      is_short_BIN = .true.
   else
      is_short_BIN = .false.
   endif
   close(lu)
   deallocate (ibu, is1, is2, ibs, xl, zfd, ygeo, ybas)
   return

   994 write(error_unit,'(2a)') ' >>>> Erreur à l''ouverture du fichier BIN ',trim(bin%file)
   stop 1605

   995 write(error_unit,'(3a)') '>>>> Apparement le fichier BIN ',trim(bin%file),' est incomplet'
   write(output_unit,'(a)') '>>>> veuillez vérifier votre simulation MAGE'
   stop 1606
end function is_short_BIN



subroutine initLigneDeauMAGE (t, modele, etat1, etat2, bin, finFile)
!==============================================================================
!  initialise les états etat1 et etat2 à partir d'un fichier BIN de MAGE
!
!==============================================================================
   !prototype
   real(kind=long), intent(in) :: t
   type (dataset), intent(inout), target :: modele
   type (state), target, intent(inout)  :: etat1, etat2  !2 états hydrauliques successifs t et t+dt
   type (cache), intent(inout) :: bin                  !cache pour les lignes d'eau (LE), contient le nom du fichier BIN
   character (len=fnl), intent(in) :: finFile           !fichier état final fourni par MAGE (Mage_fin.ini)

   !variables locales
   integer :: ibmax, ismax, kbl, io_status, nb_lignes, nb_tables, mage_version
   integer, allocatable :: ibu(:), is1(:), is2(:), ibs(:)
   real(kind=sp) :: zmin_OLD
   real(kind=sp), allocatable :: xl(:), zfd(:), ygeo(:), ybas(:), q(:), z(:)
   real (kind=long) :: tr, kmin, kmaj
   character (len=1) :: a
   character (len=240) :: ligne

   integer :: lu, ib, ks, is, size_ecr, lsize, ks0, kb, js, nval(13), err
   integer :: nblemax, nble !nb de lignes d'eau max stockées dans le cache
   integer(kind=8) :: fsize

   logical :: err_track, is_Mage8

   real(kind=long) :: st, qt, zt
   type (profil), pointer :: pfl


   !ouverture et lecture du fichier finFile
   open(newunit=lu,file=trim(finFile),status='old',form='formatted',iostat=io_status)
   if (io_status /= 0) then
      write(error_unit,'(3a)') ' Ouverture de ',trim(finFile),' impossible'
      stop 1607
   endif
   do
      read(lu,'(a)', iostat=io_status) ligne
      if (io_status /= 0) then
         exit
      else if (ligne(1:1) == '*' .or. ligne(1:1)=='$' .or. trim(ligne)=='') then
         cycle
      else
         read(ligne(2:4),'(i3)') kb  !N° du bief
         read(ligne(5:8),'(i4)') js  !N° de profil
         read(ligne(124:132),'(f9.0)') kmin
         read(ligne(133:141),'(f9.0)') kmaj
         is = modele%biefs(kb)%is1+js-1
         modele%sections(is)%nzone = 3
         modele%sections(is)%k(1) = kmaj  ;  modele%sections(is)%li(1) = 1
         modele%sections(is)%k(2) = kmin  ;  modele%sections(is)%li(2) = modele%sections(is)%np
         modele%sections(is)%k(3) = kmaj
!         do ks = 1, modele%sections(is)%np
!            if (modele%sections(is)%xyz(ks)%tag == 'RG' .or. modele%sections(is)%xyz(ks)%tag == 'rg') then
!               modele%sections(is)%li(1) = ks
!            else if (modele%sections(is)%xyz(ks)%tag == 'Rg' .or. modele%sections(is)%xyz(ks)%tag == 'rG') then
!               modele%sections(is)%li(1) = ks
!            else if (modele%sections(is)%xyz(ks)%tag == 'RD' .or. modele%sections(is)%xyz(ks)%tag == 'rd') then
!               modele%sections(is)%li(2) = ks
!            else if (modele%sections(is)%xyz(ks)%tag == 'Rd' .or. modele%sections(is)%xyz(ks)%tag == 'rD') then
!               modele%sections(is)%li(2) = ks
!            endif
!         enddo
      endif
   enddo
   close (lu)

   !ouverture du fichier BIN
   write(output_unit,*)
   write(output_unit,'(2a)') ' ==> Lecture du fichier des lignes d''eau : ',trim(bin%file)
   bin%is_long = .not.is_short_BIN(bin)
   if (bin%is_long) then
      write(output_unit,'(1x,3a)') trim(bin%file),' est un fichier BIN de MAGE complet'
   else
      write(output_unit,'(1x,3a)') trim(bin%file),' est un fichier BIN de MAGE court (Q et Z seuls)'
   endif

   open(newunit=lu,file=trim(bin%file),status='old',form='unformatted',iostat=io_status)
   if (io_status > 0) then
      write(output_unit,*) '>>>> Erreur d''ouverture du fichier ', bin%file
      stop 1619
   endif

   ! lecture de l'entête
   read(lu) ibmax, ismax, mage_version !version pour MAGE modifie
   !vérifications et allocations
   if (mage_version >= 80) then
      !nouvel entête de BIN pour Mage-8
      is_Mage8 = .true.
   else
      !ancien entête de BIN pour Mage-7
      is_Mage8 = .false.
      kbl = mage_version
      if (kbl > 0) then
         write(output_unit,*) ' kbl =  ',kbl
         write(output_unit,*)  'Veuillez relancer une version de MAGE ≥ 7.2.012'  !temps stockés en 32 bits
         stop 1608
      else
         kbl = -kbl
      endif
   endif
   if (ibmax /= modele%net%ibmax) then
      write(output_unit,*) '>>>> Erreur : Topologies MAGE et Adis-TS différentes (nombres de biefs différents)'
      stop 1609
   else
      allocate(ibu(ibmax), is1(ibmax), is2(ibmax))
   endif
   if (ismax /= modele%net%ismax) then
      write(output_unit,*) '>>>> Erreur : Topologies MAGE et Adis-TS différentes (nombres de points de calcul différents)'
      stop 1610
   else
      allocate(ibs(ismax), xl(ismax), zfd(ismax), ygeo(ismax), ybas(ismax), q(ismax), z(ismax))
   endif
   ! suite lecture
   if (is_Mage8) then
      !si BIN a été produit par Mage-8 alors les lignes d'eau sont stockées dans l'ordre des données
      !et non selon le rang de calcul de calcul.
      do ib = 1, ibmax
         ibu(ib) = ib
      enddo
      read(lu) (is1(ib),is2(ib),ib=1,ibmax)
      read(lu) (xl(is), is=1,ismax)
      read(lu) (zfd(is),ygeo(is),ybas(is),is=1,ismax)
      size_ecr = sp*(3+2*ibmax+4*ismax)
   else
      read(lu) (ibu(ib),ib=1,ibmax)
      read(lu) (is1(ib),is2(ib),ib=1,ibmax)
      do ks = 1, ismax, kbl
         read(lu) (xl(is), is=ks,min(ks+kbl-1,ismax))
      enddo
      read(lu) zmin_OLD
      do ks = 1, ismax, kbl
         read(lu) (zfd(is),ygeo(is),ybas(is),is=ks,min(ks+kbl-1,ismax))
      enddo
      do ks = 1, ismax, kbl
         read(lu) (ibs(is), is=ks,min(ks+kbl-1,ismax))
      enddo
      size_ecr = sp*(3+3*ibmax+ismax+1+3*ismax+ismax+14)
   endif

   !!vérification de l'ordre de calcul ; c'est juste informatif car ça ne pose pas de problème
   !!il n'y a pas d'ordre de calcul ("hydraulique") unique.
   !!ce qui est important c'est que le repérage des sections dans BIN soit correct
   do ib = 1, ibmax
      if (ibu(ib) /= modele%net%ibu(ib)) then
         write(output_unit,'(a,i3,a,2i3)') ' >>>> INFO : Rangs de calcul différents dans Mage et Adis-TS pour le bief ',&
                                 ib,' :: ',ibu(ib),modele%net%ibu(ib)
      endif
   enddo

   write(output_unit,'(3a)') '    --> Lecture de l''entête de ',trim(bin%file),' : OK'

   !calcul du nombre de lignes d'eau disponibles
   call stat(bin%file,nval,io_status)
   !lsize = sp*(2+ismax+2)+1
   lsize = sp + long + 1 + ismax*sp  !taille d'un enregistrement de BIN en octets
   if (nval(8) < 0) then
      inquire (file=bin%file,size=fsize)
      write(output_unit,'(3a,i11,a)') '    --> Fichier ',trim(bin%file),' très gros !!!  ',fsize,' octets'
   else
      fsize = nval(8)
   endif
   if (bin%is_long) then
      ! FIXME: avec Mage-7 il y a 6 enregistrements par ligne d'eau et il y en a 11 avec Mage-8
      nb_tables = 11
   else
      nb_tables = 2
   endif
   nblemax = int(real(fsize-size_ecr,long)/real(nb_tables*lsize,long))
   write(output_unit,'(3a,i8)') '    --> Nombre de ligne d''eau enregistrées dans ',trim(bin%file),' = ',nblemax
   nble = min(nblemax,bin%size / (nb_tables*lsize))  !il y a nb_tables tableaux par ligne d'eau stockée
   bin%nble = nble

   if (.not. allocated(bin%LE)) then
      do
         allocate (bin%LE(nble), stat=err)
         do ib = 1, nble
            call state_constructor(bin%LE(ib),ismax)
         enddo
         if (err /= 0 .and. bin%size > 1000000) then
            write(output_unit,*) 'Allocation du cache de lignes d''eau impossible, nouvelle essai avec un cache 2 fois plus petit'
            bin%size = bin%size / 2
            nble = min(nblemax,bin%size / (nb_tables*lsize))
            bin%nble = nble
         else if (err /= 0) then
            write(output_unit,'(a)') 'Mémoire insuffisante pour allouer le cache des lignes d''eau'
            stop 1611
         else
            write(output_unit,'(a,i8)') '    --> Nombre de lignes d''eau stockables en cache : ',nble
            if (nble < 2) then
               write(output_unit,'(a)') 'Mémoire insuffisante pour stocker plusieurs lignes d''eau dans le cache'
               stop 1612
            endif
            exit
         endif
      enddo
   endif
   ks0 = 1

   !initialisation du tableau bin%irg
   err_track = .false.
   do ib = 1, modele%net%ibmax  !boucle dans l'ordre du fichier NET
      !!vérification que les biefs ont le même nombre de sections dans Adis-TS et dans BIN
      if (modele%biefs(ib)%is2-modele%biefs(ib)%is1 /= is2(ibu(ib))-is1(ibu(ib)))  then
      !if (modele%biefs(ib)%is2-modele%biefs(ib)%is1 /= is2(ib)-is1(ib))  then
         write(output_unit,*) 'Pb nb section dans bief ',ib,modele%net%ibu(ib),ibu(ib),modele%biefs(ib)%is2,&
                                           modele%biefs(ib)%is1,is2(ibu(ib)),is1(ibu(ib))
         stop 1613
      endif
      !!vérification que les pk sont les mêmes ici et dans BIN
      !!NB : on a fait un arrondi sur les pk dans initTopoGeometrie() car les pk sont arrondis
      !!     par PamHyr pour le format TAL utilisé pour la géométrie par MAGE
      do is = modele%biefs(ib)%is1, modele%biefs(ib)%is2
         bin%irg(is) = is1(ibu(ib)) + is - modele%biefs(ib)%is1
         if (abs(modele%sections(is)%pk-xl(bin%irg(is))) > 1.11_long) then
            err_track = .true.
            write(output_unit,'(a,a,i3,a,i4,a,f10.2,a,f10.2)') &
                    ' >>>> Erreur dans BIN, les pk ne correspondent pas : ',&
                    'bief ',ib,' - section ',is-modele%biefs(ib)%is1+1,' - pk Adis-TS ',&
                    modele%sections(is)%pk,' - pk BIN ',xl(bin%irg(is))
         endif
      enddo
   enddo
   if (err_track) stop 1614

   nb_lignes = 0
   lecture_fichier: do
      !lecture des premières lignes d'eau
      !NB : on ne lit pas la vitesse qui est recalculée à partir de q et z
      ib = ks0-1
      a = ' '
      do ks = ks0, bin%nble
         do while (a /= 'Q')
! NOTE: utiliser kbl comme borne pour q(:) est une très mauvaise idée, car kbl est indéterminé lors de la dernière tentative de
! NOTE: lecture du fichier. En mode debug le dépassement de tableau fait planter le code avant la détection de la fin du fichier
            read(lu,iostat=io_status) kbl, tr, a, (q(is),is=1,ismax)
            if (io_status < 0) then !fin du fichier
               write(output_unit,*) 'Fermeture du fin BIN - fichier BIN en entier dans le cache'
               close (lu)
               exit lecture_fichier
            elseif (io_status > 0) then
               write(error_unit,*) '>>>> Erreur de lecture du fichier BIN'
               stop 1624
            endif
         enddo
         nb_lignes = nb_lignes + 1
         read(lu,iostat=io_status) kbl, tr, a, (z(is),is=1,ismax)
         if (io_status /= 0) then !ici un fin de fichier est aussi une erreur
            write(error_unit,*) '>>>> Erreur de lecture du fichier BIN'
            stop 1625
         elseif (a .ne. 'Z') then
            write(output_unit,*) '>>>> Erreur dans initLigneDeauMAGE : ',a, ' au lieu de Z'
            stop 1616
         endif
         if (kbl /= ismax) stop 1617
         ib = ib+1
!$omp parallel default(shared) private(is,js,pfl,st,qt,zt) num_threads(nb_threads)
!$omp do schedule(static)
         do is = 1, ismax
            js = bin%irg(is)  ;  pfl => modele%sections(is)
                                      bin%LE(ks)%x(is) = pfl%pk
            zt = real(z(js),long)  ;  bin%LE(ks)%y(is) = zt - pfl%zf
            st = section(pfl,zt)   ;  bin%LE(ks)%a(is) = st
            qt = real(q(js),long)  ;  bin%LE(ks)%q(is) = qt
                                      bin%LE(ks)%u(is) = qt / st
         enddo
!$omp end do
!$omp end parallel
         bin%LE(ks)%time = tr
      enddo
      if (bin%LE(nble)%time < t) then
         !il faut continuer la lecture de BIN mais on garde la dernière ligne d'eau du cache
         !comme 1ère ligne d'eau du cache suivant
         write(output_unit,*) 'Cache des lignes d''eau : dernière ligne d''eau trouvée = ', &
                               bin%LE(nble)%time,' il faut lire la suite'
         ks0 = 2
         call copy_etat(bin%LE(nble), bin%LE(1), ismax)
      else
         exit lecture_fichier
      endif
   enddo lecture_fichier
   bin%nble = ib
   write(output_unit,'(3a,i8)') '    --> Nombre de lignes d''eau lues dans ',trim(bin%file),' : ',nb_lignes
   write(output_unit,'(a,i8)')  '    --> Nombre de lignes d''eau dans le cache : ',bin%nble

   !initialisation : recherche dans le cache de la date t
   if (bin%LE(1)%time > t .or. bin%LE(bin%nble)%time < t) then
      write(output_unit,'(3a)') 'La ligne d''eau pour l''instant initial ',date_et_heure(t), &
                                ' n''est pas dans la période simulée !!!'
      write(output_unit,'(4a)') 'Période simulée : ',date_et_heure(bin%LE(1)%time),' à ', date_et_heure(bin%LE(bin%nble)%time)
      write(output_unit,'(a)') 'Si cela correspond à ce que vous vouliez, vous pouvez relancer Adis-TS avec l''option -m'
      write(output_unit,'(a)') 'pour forcer le lancement préalable de Mage'
      stop 1618
   endif
   ib = 1
   do ks = 1, bin%nble
      if (abs(bin%LE(ks)%time -t) < 1.0) then
         ib = ks  ;  exit
      endif
   enddo
   call copy_etat(bin%LE(ib), etat1, ismax)  ;  call copy_etat(etat1, etat2, ismax)

   deallocate (ibu, is1, is2, ibs, xl, zfd, ygeo, ybas, q, z)

end subroutine initLigneDeauMAGE



subroutine getLigneDeauMAGE (t, modele, etat2, bin)
!==============================================================================
!     Récupère la ligne d'eau de l'instant t et met à jour l'état etat2
!
!   NB : etat2 pointe toujours sur l'état à t+dt
!        la permutation des états doit être gérée par la boucle
!        sur le temps
!
!==============================================================================
   !prototype
   real(kind=long), intent(in) :: t
   type (dataset), intent(inout), target :: modele
   type (state), intent(inout) :: etat2
   type (cache), intent(inout) :: bin

   !variables locales
   integer :: lu, is, nble, nb, nb1, kbl, ks, ios, js, ismax
   real(kind=long) :: st, qt, zt, tr
   real(kind=sp), allocatable :: q(:), z(:), xl(:)
   character (len=1) :: aa
   integer, save :: nb_actu = 1
   logical, save :: perm = .false.
   type (profil), pointer :: pfl
   logical :: bOpen

   nble = bin%nble
   ismax = modele%net%ismax

   if (perm ) then
      etat2%time = t  ;  return
   endif
   if (nb_actu > nble) nb_actu = 1  !on a certainement réinitialisé le cache à la suite d'une nouvelle simulation hydraulique

   !état à t
   if (bin%LE(nble)%time < t+0.1_long) then    !il faut lire la suite de BIN
      nb_actu = 1
      inquire(file=trim(bin%file),opened=bOpen,number=lu)
      if (.not.bOpen) then  !BIN a déjà été lu en entier => on continue avec la dernière ligne d'eau
         call copy_etat(bin%LE(nble), etat2, modele%net%ismax)  ;  etat2%time = t
         return
      else
         !write(output_unit,*) 'Lecture des lignes d''eau suivantes ',bin%LE(nble)%time/3600. , t/3600., nble
         write(output_unit,*) 'Mise à jour du cache des lignes d''eau à t = ',date_et_heure(t)
         call copy_etat(bin%LE(nble-1), bin%LE(1), modele%net%ismax)
         call copy_etat(bin%LE(nble), bin%LE(2), modele%net%ismax)
         !lecture des lignes d'eau suivantes
         !NB : on ne lit pas la vitesse qui est recalculée à partir de q et z
         nb = 2  ;  nb1 = nb+1  !nb est le compteur des lignes d'eau lues
                                !il est initialisé à 2 car on garde les 2 dernières du stock précédent
         if (.not.allocated(q)) allocate(xl(modele%net%ismax),q(modele%net%ismax),z(modele%net%ismax))
         aa = ' '
         suite_lecture_BIN: do ks = nb1, nble
            do while (aa /= 'Q')
               read(lu,iostat=ios) kbl, tr, aa, (q(is),is=1,ismax)
               if (ios < 0) then !fin du fichier
                  bin%nble = nb
                  close (lu)
                  write(output_unit,*) 'Fermeture du fichier ',trim(bin%file),' : lu en entier'
                  write(output_unit,*) 'Dernière ligne d''eau fournie par BIN à t = ',date_et_heure(bin%LE(ks-1)%time)
                  exit suite_lecture_BIN
               elseif (ios > 0) then !erreur de lecture
                  write(error_unit,*) '>>>> Erreur de lecture du fichier BIN'
                  stop 1626
               endif
            enddo
            read(lu,iostat=ios) kbl, tr, aa, (z(is),is=1,ismax)
            if (ios /= 0) then
               write(output_unit,*) 'fichier BIN incomplet'
               stop 1625
            elseif (aa .ne. 'Z') then
               write(output_unit,*) '>>>> Erreur dans getLigneDeauMAGE : ',aa, ' au lieu de Z'
               stop 1621
            else
               nb = nb+1
            endif
            if (kbl /= modele%net%ismax) stop 1622
!$omp parallel default(shared) private(is,js,pfl,st,qt,zt) num_threads(nb_threads)
!$omp do schedule(static)
            do is = 1, modele%net%ismax
               js = bin%irg(is)  ;  pfl => modele%sections(is)
                                         bin%LE(ks)%x(is) = pfl%pk
               zt = real(z(js),long)  ;  bin%LE(ks)%y(is) = zt - pfl%zf
               st = section(pfl,zt)   ;  bin%LE(ks)%a(is) = st
               qt = real(q(js),long)  ;  bin%LE(ks)%q(is) = qt
                                         bin%LE(ks)%u(is) = qt / st
            enddo
!$omp end do
!$omp end parallel
            bin%LE(ks)%time = tr
         enddo suite_lecture_BIN
         bin%nble = nb
         write(output_unit,*) 'Nombre de lignes d''eau lues dans ',trim(bin%file),' : ',bin%nble-2 !, nb_actu
         deallocate(xl,q,z)
      endif
   endif

   if (bin%nble > 1) then
      do while (bin%LE(nb_actu)%time > t)
         nb_actu = nb_actu - 1
         if (nb_actu == 0) then
            write(output_unit,*) '>>>> Erreur : problème dans la gestion du pas de temps à ',t
            write(output_unit,*) '              première ligne d''eau à ',bin%LE(1)%time
            stop 1623
         endif
      enddo
      do nb = nb_actu, bin%nble-1
         if (abs(bin%LE(nb)%time - t) < 1._long) then
            call copy_etat(bin%LE(nb), etat2, modele%net%ismax)  ;  etat2%time = t
            nb_actu = nb  ;  return
         else if (abs(bin%LE(nb+1)%time - t) < 1._long) then
            call copy_etat(bin%LE(nb+1), etat2, modele%net%ismax)  ;  etat2%time = t
            nb_actu = nb+1  ;  return
         else if (bin%LE(nb)%time < t .AND. bin%LE(nb+1)%time > t) then
            call etat_from_BIN(etat2, bin, t, nb, modele%net%ismax, modele)
            nb_actu = nb  ;  return
         else
            cycle
         endif
      enddo
      write(error_unit,'(a,i0,a,i0,2f12.3)') ' >>>> Erreur dans getLigneDeauMAGE : nb_actu = ',nb_actu, ' bin%nble = ', &
                                    bin%nble, bin%LE(bin%nble)%time, t
      call fatal_error(modele,code=1624) !normalement on ne doit pas sortir par là !
   else
      call copy_etat(bin%LE(1), etat2, modele%net%ismax)  ;  etat2%time = t
   endif
end subroutine getLigneDeauMAGE



subroutine etat_from_BIN (etat, bin, t, nb, ismax, modele)
!==============================================================================
!     Interpole la ligne d'eau de l'instant t à partir du cache "bin"
!     met à jour l'état hydraulique "etat"
!
!     la ligne d'eau cherchée correspond à la date t et se trouve entre les
!     lignes d'eau indexées nb et nb+1 dans le cache
!
!     Seuls le débit et le tirant d'eau sont interpolés, ensuite la section
!     mouillée est calculée à partir de la cote puis la vitesse moyenne
!==============================================================================
   type (state), intent(inout) :: etat
   type (cache), intent(in) :: bin
   real(kind=long), intent(in) :: t
   integer, intent(in) :: nb, ismax
   type (dataset), intent(in), target :: modele

   integer :: is, nb1
   real(kind=long) :: a, b, st, qt
   type (profil), pointer :: pfl

   a = (bin%LE(nb+1)%time - t) / (bin%LE(nb+1)%time - bin%LE(nb)%time)
   b = (t - bin%LE(nb)%time) / (bin%LE(nb+1)%time - bin%LE(nb)%time)
   nb1 = nb+1
!$omp parallel default(shared) private(is,st,qt,pfl) firstprivate(a,b,nb,nb1) num_threads(nb_threads)
!$omp do schedule(static)
   do is = 1, ismax
      qt = a*bin%LE(nb)%q(is) + b*bin%LE(nb1)%q(is)
      etat%x(is) = bin%LE(nb)%x(is)
      etat%y(is) = a*bin%LE(nb)%y(is) + b*bin%LE(nb1)%y(is)
      pfl => modele%sections(is)
      st = section(pfl,etat%y(is)+pfl%zf)
      etat%a(is) = st
      etat%q(is) = qt
      etat%u(is) = qt/st
   enddo
!$omp end do
!$omp end parallel
   etat%time = t
end subroutine etat_from_BIN



subroutine debord_all(modele,etat,data_hyd,geometry_changed)
!==============================================================================
!
!                   Application de la formule Debord
!
! Variables d'entrée :
!     - modele -> profils et Stricklers (résistance à l'écoulement)
!     - etat : cote de l'eau (altitude) et débit
!     - data_hyd() : variables hydrauliques issues de l'appel précédent ou d'une
!                    modification extérieure à debord_all()
! Variables de sortie :
!     - data_hyd() : variables hydrauliques après modification éventuelle en
!                    de l'état hydraulique passé en argument. Pour les profils
!                    où le niveau change très peu (seuil Z_seuil) on conserve
!                    les valeurs de data_hyd()
!
! Si data_hyd() n'est pas alloué, il faut réinitialiser le cache ZT et refaire
! tous les calculs
!
!==============================================================================
   implicit none
   ! prototype
   type(dataset),intent(in),target :: modele
   type(state), intent(in) :: etat
   type(data_hydrauliques), allocatable, intent(inout) :: data_hyd(:)
   logical, intent(in) :: geometry_changed
   ! variables locales
   real(kind=long) :: Z, Q
   real(kind=long), pointer :: Kmin, Kmoy
   type(profil),pointer :: prof
   real(kind=long) :: ldyn, pdyn, sdyn
   real(kind=long) :: r, a, a0, f, r23, h, dz
   logical :: debord_ok
   real(kind=long), parameter :: untier = 1._long/3._long
   integer :: irg, ird, i, is
   !cache : on conserve (attribut SAVE) les variables suivantes d'une exécution de la routine à la suivante
   real(kind=long),save, allocatable :: z_mineur(:), lg_min(:), pr_min(:), st_min(:), zt(:)
   logical, save :: mineur_connu = .false.

! TODO: faire des essais réels pour choisir une valeur optimale de Z_seuil ci-dessous
   real(kind=long), parameter :: z_seuil = 0.001_long  !paramètre pour décider s'il faut mettre à jour data_hyd()
   integer :: ismax

   ! calcul
   if (modele%nbmes == 0) return  !rien à faire si pas de MES.

   if (geometry_changed) then
      mineur_connu = .false.
      if(allocated(z_mineur)) deallocate (z_mineur, lg_min, pr_min, st_min, zt)
   endif

   if (.not.mineur_connu) then
      ! DONE: ajouter un critère pour forcer la mise à jour de ce cache en cas de modification de la géométrie.
      ismax = modele%net%ismax
      allocate(z_mineur(ismax), lg_min(ismax), pr_min(ismax), st_min(ismax), zt(ismax))
      zt = -1000._long
!$omp parallel default(shared) private(is,prof) num_threads(nb_threads)
!$omp do schedule(static)
      do is = 1, modele%net%ismax
         prof => modele%sections(is)
         z_mineur(is) = min(prof%xyz(modele%sections(is)%li(1))%z,prof%xyz(modele%sections(is)%li(2))%z)
         call get_data_geom(prof,z_mineur(is),lg_min(is),pr_min(is),st_min(is))
      enddo
!$omp end do
!$omp end parallel
      mineur_connu = .true.
   endif

   if (.not.allocated(data_hyd)) then
      !on réinitialise le cache de Z car il n'y a aucune donnée dans data_hyd
      !cela se produit au 1er pas de temps et lors de l'appel par dump_contraintes_locales()
      allocate(data_hyd(modele%net%ismax))
      zt = -1000._long
   endif
!!!-> avec le cache ZT aucun intérêt de paralléliser, les calculs sont faits sur seulement une partie des sections
   do is = 1, modele%net%ismax
      !variable d'entrée
      prof => modele%sections(is)
      Z = etat%y(is) + prof%zf
      ! On ne refait les calculs que si la cote Z a changé suffisamment par rapport à la valeur stockée Zt(is)
      ! ce cache permet de ne faire les calculs que pour les sections où la cote de l'eau change significativement
      if (abs(z-zt(is)) < z_seuil) then
         cycle      ! on passe
      else
         zt(is) = Z ! mise à jour du cache
         call init_datahyd(data_hyd(is)) ! réinitialisation des données hydrauliques pour ce profil
      endif
      Q = etat%q(is)
      Kmin => prof%k(2)
      Kmoy => prof%k(1)

      call get_data_geom(prof,z,ldyn,pdyn,sdyn)
      if (Z < z_mineur(is)) then
         debord_ok = .false.
         data_hyd(is)%lmin = ldyn  ;  data_hyd(is)%smin = sdyn  ;  data_hyd(is)%pmin = pdyn
         data_hyd(is)%lmoy = zero  ;  data_hyd(is)%smoy = zero  ;  data_hyd(is)%pmoy = zero
      else                                       ! débordement en lit moyen et/ou majeur
         debord_ok = .true.
         ! lit mineur
         data_hyd(is)%lmin = lg_min(is)
         Dz = z-z_mineur(is)
         data_hyd(is)%pmin = pr_min(is) + 2._long*Dz   ! on tient compte de l'interface mineur / moyen
         data_hyd(is)%smin = st_min(is) + data_hyd(is)%lmin*Dz
         ! lit moyen
         data_hyd(is)%lmoy = ldyn - data_hyd(is)%lmin
         data_hyd(is)%pmoy = pdyn - data_hyd(is)%pmin + 4._long*Dz  ! on tient compte de l'interface mineur / moyen
         data_hyd(is)%smoy = sdyn - data_hyd(is)%smin
         ! vérifications smoy > 0 et pmoy > 0
         if (data_hyd(is)%smoy < zero) then  ! verification sur la section mouillee
            if (abs(data_hyd(is)%smoy) < 0.0001*data_hyd(is)%smin) then
               ldyn = data_hyd(is)%lmin  ;  sdyn = data_hyd(is)%smin  ;  pdyn = data_hyd(is)%pmin
               data_hyd(is)%lmoy = zero  ;  data_hyd(is)%smoy = zero  ;  data_hyd(is)%pmoy = zero
               debord_ok = .false.
            else
               write(output_unit,*) '>>>> Erreur dans Debord() : section lit moyen négative !!!',' Pk = ',prof%pk
               write(output_unit,*) z, sdyn, data_hyd(is)%smin, data_hyd(is)%smoy, lg_min(is), st_min(is), z_mineur(is)
               do i = 1, prof%nh
                  write(output_unit,*) i,prof%zf+prof%wh(i)%h, prof%wh(i)
               enddo
               call fatal_error(modele,code=1626)
            endif
         endif
         if (data_hyd(is)%pmoy < zero) then  ! verification sur le perimetre mouille
            call limite_Eau(prof,z,irg,ird)
            pr_min(is) = perimetre(prof,z_mineur(is))
            pdyn = perimetre(prof,z)
            write(output_unit,*) '>>>> Erreur dans Debord() : périmètre lit moyen négatif !!!',&
                       pdyn, data_hyd(is)%pmin, data_hyd(is)%pmoy,is,prof%zf,prof%name, &
                       z, z_mineur(is), prof%xyz(prof%li(1))%tag, &
                       prof%xyz(prof%li(2))%tag,irg,prof%xyz(irg)%z,ird,prof%xyz(ird)%z,&
                       pr_min(is),dz,prof%ip
            do i = 1, prof%nh
               write(output_unit,*) i,prof%wh(i)
            enddo
            call fatal_error(modele,code=1627)
         endif
         ! cas d'un tout petit lit moyen : section dynamique = lit mineur
         if ((data_hyd(is)%smoy >= zero) .and. (data_hyd(is)%smoy < 0.001_long*sdyn)) then ! le lit moyen est traite comme un lit majeur (stockage seul)
            ldyn = data_hyd(is)%lmin  ;  sdyn = data_hyd(is)%smin  ;  pdyn = data_hyd(is)%pmin
            data_hyd(is)%lmoy = zero  ;  data_hyd(is)%smoy = zero  ;  data_hyd(is)%pmoy = zero
            debord_ok = .false.
         end if
      endif

      if (debord_ok) then
         data_hyd(is)%rmoy = data_hyd(is)%smoy/data_hyd(is)%pmoy  ! rayons hydrauliques
         data_hyd(is)%rmin = data_hyd(is)%smin/data_hyd(is)%pmin
         r = data_hyd(is)%rmoy/data_hyd(is)%rmin
         !---calcul de a
         a0 = 0.9_long*(Kmoy/Kmin)**deuxtiers
         if (r > 0.3_long) then
            a = a0
          else
            a = (un+a0-(a0-un)*cos(pi*r*3.333333_long))*0.5_long
         end if
         !---calcul de h=qmoy/qmin
         r23 = r**deuxtiers
         ! NOTE: la variable f ci-dessous vient d'essais faits sur MAGE ; voir la routine correspondante dans MAGE
         f = sqrt(max(1.e-4_long,data_hyd(is)%smoy*(data_hyd(is)%smoy+data_hyd(is)%smin*(un-a*a))))
         h = Kmoy * f * r23/(Kmin*a*data_hyd(is)%smin)
         data_hyd(is)%Qmin = Q / (un+h)  ;  data_hyd(is)%Vmin = data_hyd(is)%Qmin / data_hyd(is)%smin
         data_hyd(is)%Qmoy = Q - data_hyd(is)%Qmin    ;  data_hyd(is)%Vmoy = data_hyd(is)%Qmoy / data_hyd(is)%smoy
         data_hyd(is)%Beta = sdyn * (h*h/data_hyd(is)%smoy+un/data_hyd(is)%smin)/(un+h)/(un+h)
         data_hyd(is)%Deb = Kmin*data_hyd(is)%smin*(un+h)*data_hyd(is)%rmin**deuxtiers
      else
         data_hyd(is)%Qmin = Q     ;  data_hyd(is)%Vmin = data_hyd(is)%Qmin / sdyn
         data_hyd(is)%Qmoy = zero  ;  data_hyd(is)%Vmoy = zero
         data_hyd(is)%Rmin = data_hyd(is)%smin / data_hyd(is)%pmin ; data_hyd(is)%Rmoy = zero
         data_hyd(is)%Deb = Kmin*sdyn*(sdyn/pdyn)**deuxtiers
         data_hyd(is)%Beta = un
      endif
      ! partie hydraulique de la contrainte au fond
      ! la formule n'est pas tout-à-fait la même que pour la contrainte locale mais c'est normal car il s'agit ici
      ! de la contrainte globale au fond et elle est bien nulle dans le lit moyen quand il n'y a pas de débordement
      ! tout comme elle tend vers zéro dans le mineur si le tirant d'eau tend vers zéro.
      data_hyd(is)%tau_min = rho * g * data_hyd(is)%Vmin*data_hyd(is)%Vmin / (data_hyd(is)%Rmin**untier)
      if (debord_ok) then
         data_hyd(is)%tau_moy = rho * g * data_hyd(is)%Vmoy*data_hyd(is)%Vmoy / (data_hyd(is)%Rmoy**untier)
      else
         data_hyd(is)%tau_moy = zero
      endif

   enddo
end subroutine debord_all





end module Hydraulique
